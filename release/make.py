import os
import sys

# XXX do something like
# git ls-tree --full-tree --full-name --name-only -r HEAD
# in the future (so we don't get files not in the repository)

DIRECTORIES = [
    'data',
    'server'
]

FILES = [
    'README.txt',
    'alsoft.conf'
]

if sys.platform == 'win32':
    FILES += [
        'install.bat'
    ]
else:
    FILES += [
        'run_client.sh',
        'run_server.sh'
    ]

TOOLS = [
    'slabvox.py',
    'bytes.py'
]

BINARIES = [
    'client',
    'server'
]


if sys.platform == 'win32':
    DLLS = [
        'OpenAL32.dll',
        'python27.dll'
    ]
    RUNTIMES = ['Microsoft.VC90.CRT']
else:
    RUNTIMES = []
    DLLS = [
        'libenet.so.2',
        'libopenal.so.1',
        'libvorbisfile.so.3',
        'libvorbis.so.0',
        'libogg.so.0',
        'libfreetype.so.6'
    ]

import platform

is_32bit = platform.architecture()[0] == '32bit'

if sys.platform == 'win32':
    import zipfile
    archive = 'st-windows.zip'
    fp = zipfile.ZipFile(archive, 'w')
elif sys.platform == 'linux2':
    import tarfile
    if is_32bit:
        archive = 'st-linux-i686.tar.gz'
    else:
        archive = 'st-linux-x86_64.tar.gz'
    fp = tarfile.open(archive, 'w:gz', dereference = True)

def write_file(filename, arc_path = None, executable = False):
    if arc_path is None:
        arc_path = [os.path.basename(filename)]
    new_path = '/'.join(['Subversive Takedown'] + arc_path)
    if sys.platform == 'win32':
        fp.write(filename, new_path)
    elif sys.platform == 'linux2':
        tarinfo = fp.gettarinfo(filename, new_path)
        if executable:
            tarinfo.mode = 0100755
        f = open(filename, 'rb')
        fp.addfile(tarinfo, f)
        f.close()

IGNORE_EXTENSIONS = [
    '.pyc',
    '.default'
]

IGNORE_FILES = [
    '../server/config.py'
]

def add_dir(src_dir, arc_path):
    save_cwd = os.getcwd()
    src_dir = os.path.abspath(src_dir)
    os.chdir(src_dir)

    for dirpath, dirnames, filenames in os.walk('.'):
        for name in filenames:
            ext = os.path.splitext(name)[-1]
            if ext in IGNORE_EXTENSIONS:
                continue
            src_path = os.path.normpath(os.path.join(dirpath, name))
            ignore = False
            for ignore_file in IGNORE_FILES:
                if os.path.samefile(ignore_file, src_path):
                    ignore = True
                    break
            if ignore:
                continue
            dst_path = arc_path + src_path.split(os.sep)
            if not os.path.isfile(src_path):
                continue
            write_file(src_path, dst_path)

    os.chdir(save_cwd)

for directory in DIRECTORIES:
    add_dir('../%s' % directory, directory.split('/'))

for f in FILES:
    write_file('../%s' % f)

write_file('../config.txt.default', ['config.txt'])
write_file('../server/config.py.default', ['server', 'config.py'])

for f in TOOLS:
    write_file('../tools/%s' % f, ['tools', f])

for dll in DLLS:
    if sys.platform == 'win32':
        write_file('../DLLs/%s' % dll)
    else:
        PATHS = [
            '/usr/local/lib'
        ]
        if is_32bit:
            PATHS += ['/usr/lib/i386-linux-gnu']
        else:
            PATHS += ['/usr/lib/x86_64-linux-gnu']
        for path in PATHS:
            new_path = os.path.join(path, dll)
            if os.path.exists(new_path):
                write_file(new_path, ['lib', dll], executable = True)
                break
        else:
            raise IOError('Shared library not found: %r' % dll)

for runtime in RUNTIMES:
    add_dir('../DLLs/%s' % runtime, [runtime])

for bin in BINARIES:
    if sys.platform == 'win32':
        write_file('../build/Release/%s.exe' % bin)
    else:
        write_file('../build/%s' % bin, ['bin', bin], executable = True)

fp.close()

# upload to server if on my machine
import shutil

if platform.node() == 'matpow2':
    release_path = 'W:\\s\\mp2.dk\\st\\builds'
    print 'Uploading...'
    dist_path = os.path.join(release_path, archive)
    shutil.copy(archive, dist_path)
    print 'Done!'