from stlib import server
import sys

class Entity(server.Entity):
    pass

class CharacterScript(object):
    def __init__(self, parent, character):
        self.parent = parent
        self.server = parent.server
        self.character = character
        character.scripts.append(self)
        parent.scripts.append(self)
        self.on_start()

    def on_destroy(self):
        self.unload()

    def on_command(self, name, args):
        if self.parent.commands is None:
            return
        f = self.parent.commands.get(name, None)
        if f is None:
            return
        ret = f(self, *args)
        if ret is not None:
            self.character.send_chat(ret)
        return False

    def unload(self):
        if self.parent is None:
            return
        self.character.scripts.remove(self)
        self.parent.scripts.remove(self)
        self.on_stop()
        self.character = self.parent = self.server = None

    def on_start(self):
        pass

    def on_stop(self):
        pass

    def on_intel_capture(self):
        pass

    def on_intel_pickup(self):
        pass

    def on_intel_lose(self):
        pass

class ServerScript(object):
    character_script = CharacterScript
    commands = None

    def __init__(self, server):
        self.server = server
        server.scripts.append(self)
        self.scripts = []
        self.on_start()
        for character in server.characters:
            self.on_character_exists(character)

    def on_character_exists(self, character):
        self.character_script(self, character)

    def on_character_created(self, character):
        self.character_script(self, character)

    def unload(self):
        if self.server is None:
            return
        self.on_stop()
        self.server.scripts.remove(self)
        for script in self.scripts[:]:
            script.unload()
        self.scripts = None
        self.server = None

    def get_entity(self, name):
        data = self.server.get_entity(name)
        if data is None:
            return None
        return Entity(data)

    def update(self, dt):
        pass

    def on_start(self):
        pass

    def on_stop(self):
        pass

    def on_map_start(self):
        pass

    def on_time_out(self):
        pass

    def on_map_end(self):
        pass

# decorators for commands
def command(func, klass = None, level = None):
    if klass is None:
        klass = sys.modules[func.__module__].get_class()
    if klass.commands is None:
        klass.commands = {}
    func.level = level
    klass.commands[func.func_name] = func
    return func