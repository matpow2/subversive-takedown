"""
Base server
"""

from stlib import server
from stlib import constants
import config
import imp
import shlex
import itertools

BaseCharacter = server.Character
BaseServer = server.Server

def call_handler(script, name, *arg, **kw):
    f = getattr(script, name, None)
    if f is None:
        return
    ret = f(*arg, **kw)
    if ret is False:
        return False

class Team(object):
    score = 0
    def __init__(self, server, name, identifier):
        self.name = name
        self.identifier = identifier
        self.server = server

    def add_score(self, value):
        self.score += value
        if self.score >= config.cap_limit:
            self.win()
            return True
        return False

    def get_objective(self):
        return self.server.get_objective(self.identifier)

    def win(self):
        self.server.end_round(self)

    def reset(self):
        self.score = 0

    def get_characters(self):
        for character in self.server.characters:
            if character.team is self:
                yield character

class Character(BaseCharacter):
    name = property(BaseCharacter.get_name)
    score = property(BaseCharacter.get_score, BaseCharacter.set_score)
    quickchat_time = 0.0

    def __init__(self, server, data):
        super(Character, self).__init__(data)
        self.scripts = []
        self.server = server

    def on_death(self, by = None):
        if by is None:
            return
        if by.team is self.team:
            by.score -= 1
        else:
            by.score += 1

    def on_hit(self, by = None):
        if by is None:
            return
        if self.team is by.team and not config.friendly_fire:
            return False

    def on_destroy(self):
        self.server.characters.remove(self)
        self.call_scripts('on_destroy')

    def reset(self):
        self.score = 0

    def on_command(self, name, args):
        self.call_scripts('on_command', name, args)

    def on_team_chat(self, message, sound):
        return

    def on_chat(self, message, sound):
        if message.startswith('/'):
            try:
                splitted = shlex.split(message[1:])
            except ValueError:
                # shlex failed. let's just split per space
                splitted = value.split(' ')
            if splitted:
                command = splitted.pop(0)
            else:
                command = ''
            self.on_command(command, splitted)
            return False
        if sound:
            if self.quickchat_time > self.server.global_time:
                return False
            self.quickchat_time = self.server.global_time + 4.0


    def on_team(self, new_team):
        if new_team == constants.TEAM_SPEC:
            return
        obj = self.server.teams[new_team].get_objective()
        if not obj:
            return
        self.send_chat(obj)

    def set_team(self, obj):
        BaseCharacter.set_team(self, obj.identifier)

    def get_team(self):
        return self.server.teams[BaseCharacter.get_team(self)]

    team = property(get_team, set_team)

    def on_intel_capture(self):
        if self.server.next_map_timer.is_running():
            return False
        if not self.call_scripts('on_intel_capture'):
            return False
        self.score += 10
        if self.team.add_score(1):
            return
        self.server.send_chat('%s captured the intel!' % self.name)
        self.server.play_sound('score', True)

    def on_intel_lose(self):
        if self.server.next_map_timer.is_running():
            return False
        if not self.call_scripts('on_intel_lose'):
            return False
        self.server.send_chat('%s lost the intel!' % self.name)
        self.server.play_sound("intellost", True)

    def on_intel_pickup(self):
        if self.server.next_map_timer.is_running():
            return False
        if not self.call_scripts('on_intel_pickup'):
            return False
        self.server.send_chat('%s picked up the intel!' % self.name)
        self.server.play_sound("intelpickup", True)

    def call_scripts(self, name, *arg, **kw):
        for script in self.scripts:
            if call_handler(script, name, *arg, **kw) is False:
                return False
        return True

class Timer(object):
    time = None

    def __init__(self, f, *arg, **kw):
        self.f = f
        self.arg = arg
        self.kw = kw

    def is_running(self):
        return self.time is not None

    def start(self, time):
        self.time = time

    def update(self, dt):
        if self.time is None:
            return
        self.time -= dt
        if self.time > 0:
            return
        self.time = None
        self.f(*self.arg, **self.kw)

def script(f):
    def wrapper(self, *arg, **kw):
        ret = f(self, *arg, **kw)
        self.call_scripts(f.func_name, *arg, **kw)
        return ret
    return wrapper

def random_choice_cycle(choices):
    while 1:
        yield random.choice(choices)

class Server(BaseServer):
    character_class = Character
    name = config.name
    max_crates = config.max_crates
    crate_spawn_time = config.crate_spawn_time
    map_script = None
    map_time = property(BaseServer.get_map_time, BaseServer.set_map_time)
    random_rotation = config.random_rotation
    master = config.master
    global_time = 0.0

    def __init__(self, data):
        super(Server, self).__init__(data)
        self.next_map_timer = Timer(self.on_map_end)
        self.characters = []
        self.team_spec = Team(self, 'Spectator', constants.TEAM_SPEC)
        self.team_1 = Team(self, 'Greenite', constants.TEAM_1)
        self.team_2 = Team(self, 'Bluekin', constants.TEAM_2)
        self.team_single = Team(self, 'Singleplayer', constants.TEAM_SINGLE)
        self.teams = {
            self.team_spec.identifier : self.team_spec,
            self.team_1.identifier : self.team_1,
            self.team_2.identifier : self.team_2,
            self.team_single.identifier : self.team_single
        }
        self.scripts = []

        for script in config.scripts:
            self.load_script(script)

        self.set_map_rotation(config.maps)

    def load_script(self, name, is_map = False):
        if is_map:
            path = './data/maps/%s.py' % name
        else:
            path = './server/scripts/%s.py' % name
        try:
            mod = imp.load_source(name, path)
        except IOError:
            return None
        script = mod.get_class()(self)
        return script

    def call_scripts(self, name, *arg, **kw):
        for script in self.scripts:
            if call_handler(script, name, *arg, **kw) is False:
                return False
        return True

    def format(self, line):
        values = {
            'map' : self.get_map_name()
        }
        return line % values

    def set_map(self, name):
        if self.map_script is not None:
            self.map_script.unload()
        super(Server, self).set_map(name)
        for team in self.teams.values():
            team.reset()
        for character in self.characters:
            character.reset()
        self.map_script = self.load_script(name, True)

    def on_start(self):
        self.advance_map()

    def set_map_rotation(self, maps):
        if self.random_rotation:
            cycle_type = random_choice_cycle
        else:
            cycle_type = itertools.cycle
        self.map_cycle = cycle_type(maps)

    def advance_map(self):
        self.set_map(self.map_cycle.next())
        self.call_scripts('on_map_start')

    def create_character(self, data):
        character = self.character_class(self, data)
        if config.announce:
            for line in config.welcome:
                character.send_chat(self.format(line))
            self.send_chat('%s joined the game!' % character.name)
        self.characters.append(character)
        self.call_scripts('on_character_created', character)
        return character

    def update(self, dt):
        self.global_time += dt
        if not self.next_map_timer.is_running() and self.map_time < 0:
            self.call_scripts('on_time_out')
            self.end_round()
        self.next_map_timer.update(dt)
        self.call_scripts('update', dt)

    def on_map_end(self):
        self.call_scripts('on_map_end')
        self.advance_map()

    def end_round(self, team = None):
        if self.next_map_timer.is_running():
            return
        if team is None:
            self.send_chat('Round ended in a tie')
            self.play_sound('tie', True)
        else:
            self.send_chat('%ss won the round!' % team.name)
            self.play_sound('win', True)
        self.next_map_timer.start(20)

    def add_time(self, time):
        self.map_time += time
        self.send_chat('Time extended by %s minutes' % int(time / 60.0))

    # handlers

    def on_intel_return(self):
        self.send_chat('Intel returned.')

def create_server(data):
    return Server(data)