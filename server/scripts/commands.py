from script import ServerScript, CharacterScript, command
from stlib import constants

class CommandServer(ServerScript):
    def on_start(self):
        pass

    def on_stop(self):
        pass

def get_class():
    return CommandServer

@command
def kill(script):
    script.character.kill()

@command
def score(script):
    server = script.character.server
    team1 = server.team_1
    team2 = server.team_2
    return 'Score is %s to %ss, %s to %ss' % (
        team1.score, team1.name,
        team2.score, team2.name)