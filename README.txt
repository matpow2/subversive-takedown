===================
SUBVERSIVE TAKEDOWN
===================

This is Subversive Takedown. ST is a third-person, tactical voxel shooter,
inspired by Voxatron and Wolfenstein: Enemy Territory. The game is still very
much in development, but feel free to send me feedback on IRC or at 
matpow2@gmail.com.

-----
Setup
-----

* Extract the files somewhere (e.g. C:\Subversive Takedown)
* Edit config.txt to change your name
* Run install.bat once (not necessary for every update, it registers the 
                        st:// protocol)
* Head over to http://mp2.dk/st/servers and pick a server

There will probably be an installer in the future, but for now, this portable
approach should work.

---------------
Troubleshooting
---------------

Are you getting a low framerate or other graphics problems? Try setting
"graphics = 0" and/or "samples = 0" in the config.txt file. This disables
some of the graphical features in case your GPU doesn't support them, or
if it cannot run the game at a playable framerate.

--------
Controls
--------

W - Move forward
S - Move backward
A - Strafe left
S - Strafe right
Space - Jump
LMB - Shoot
RMB - Orient/rotate
Mouse wheel - Zoom in/out
M - Show minimap
C - Pick color
V - Quick chat
Comma - Change team
Period - Selfkill
E - Switch between locked and overview camera

1 - Select pickaxe tool
2 - Select block tool
    Up/down/left/right - Navigate palette
3 - Select handgun tool
4 - Select lazer tool
5 - Select RPG tool

------
Server
------

There is now a dedicated game server available for Subversive Takedown. It uses
Python for scripts, even though the functionality is minimal at the moment, but
there are plans to extend this in the future.

UDP port 7777 needs to be open on the server host. The master server won't yet
check if this is the case, but it will annoy users if they can't connect.

In terms of modding, the idea is to not change the base server at all, but to
extend the server through scripts. Scripts should be self-contained "dropins"
that define the game mode or adds/changes features. At the moment, this has not
been implemented yet, but it's in the works.

The game/master server compiles on Linux, OS X and FreeBSD, but I've yet to find
a buildbot for those platforms. Once ST is in a more presentable state, I will
look into binaries for those platforms.

------------
Model editor
------------

Tools
    Block tool
        LMB - Place block
        RMB - Remove block

    Pencil tool
        LMB - Paint block
        RMB - Pick color

ALT + LMB - Rotate camera
ALT + RMB - Move camera
Mouse scroll - Zoom camera
C - Pick color
Insert - Rapidly use current tool
Del - Rapidly use current tool alternative
Shift + Size buttons - Resize in other direction
Right arrow - Move to next animation frame
Left arrow - Move to previous animation frame
Ctrl + S - Save current file

----------
Map editor
----------

W/S/A/D - Move cursor/camera
Space - Elevate cursor/camera
C - Lower cursor/camera
ALT + LMB - Move cursor/camera
LMB - Select model
RMB - Rotate camera
Ctrl+C - Copy currently selected model
Ctrl+V - Paste current copied model at cursor
Mouse scroll - Zoom camera

-----------
Misc. tools
-----------

slabvox.py
    Converts between SLAB6 and ST vox files. There is a slight color degration,
    but it shouldn't be too noticable. Also, the model offset is lost in
    conversion, but that's probably not a big dealbreaker either.

---------
Changelog
---------

Build 41
* New hitscan weapon, the lazer gun!
* Quick chat (still need to find good voices)
* Fix intel bug
* Fix "falling spectator" bug
* Change Metal Bridge to have more uniform voxel models
* Change Republic of Bataac to have the intel spawn behind Bluekins
* New command: /score
* New tie music
* Change overview camera to have locked z rotation
* Fix backwards kilfeed

Build 40
* New file input dialog
* Add PhysFS for file IO
* Pickaxe
* New locked third-person camera
* Copyright notice in README.txt
* Frustum culling
* Republic of Bataac now has a more uniform world structure
* Kill feed
* Time extensions
* Map rotations
* Master on/off
* Intel is returned after 60 seconds
* Map icons that show important places
* Characters now hold the weapon they're using
* Models now have reference points
* Map objects are now restricted to power-of-two sizes and positions.
  This allows for more uniform voxel worlds.
* New palette
* Fix crashing bugs
* Version check
* Hit indicator (a small sound plays when you hit someone)

Build 39
* Change crate voxel model
* Add commands.py script (currently only has /kill)
* Add command keybindings (period for selfkill only per default)
* Added teamchat
* Added quit dialog
* Add spawn delay between selecting teams
* Fix crash when holding intel on map restart
* Fix intel lost/capture bug
* Fix HUD text wrapping around camera

Build 38 (bugfix release)
* Fix network movement interpolation
* Fix rockets not appearing on other clients
* Fix exception in character scripts

Build 37
* Added a lot of new scripting features (see republic.py). This includes the
  ability to manipulate entities and map scripting.
* Added rotation around the X axis
* Added zoom
* Added transparency when e.g. a building is blocking the view between the
  character and camera eye
* Added first "real" map, Republic of Bataac. Still WIP.
* Added train entity
* Added minimal spectator mode
* Added ability to change teams
* Fixed problems with npot textures on integrated cards
* Added new config options (music and vsync)
* Added map duration
* Added crates
* Added RPG
* Other changes I don't remember

Build 36
* Updated title screen
* Added minimap
* Changed button colors and added rounded rectangles
* Added rubberbanding so when the extrapolation messes up, it doesn't mess
  up too badly
* Added tool display
* Added block damage
* Added max block/handgun ammunition
* Added healing for spawnpoints
* Changed crosshair a bit

Build 35
* Changed the physics system to use Bullet Physics, should fix problems with
  collisions and similar issues
* Rigid body physics! Blocks have proper collision response now
* New logo!
* Master/official server domain changed to mp2.dk
* Added new tools to convert to/from SLAB6 .vox files. 
* Local character uses raytracted position to calculate the look angle now
* Some drawing optimizations (uses vertex arrays for particles now)
* Added rotation to the model editor

Build 34
* Added new bullets
* Shake effect when falling too fast
* ESC to exit chat
* Made shadow get smaller instead of bigger when you jump
* Fix base cube colors
* Fixed map editor team values to be locked to -1, 1 and 2
* Changed palette for player models and added distinctive team patterns
* Added grid option to map editor
* New editor tools and commands
* SSAO effect
* Building

-------
License
-------

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE

Copyright (c) 2013 Mathias Kaerlev