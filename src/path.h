#ifndef ST_PATH_H
#define ST_PATH_H

#include <sys/stat.h>
#include <fstream>
#include <iostream>

#ifdef _WIN32
#define PATH_SEP "\\/"
#else
#define PATH_SEP "/"
#endif

inline bool file_exists(const std::string & filename)
{
    struct stat buf;
    if (stat(filename.c_str(), &buf) != -1)
        return true;
    return false;
}

inline void read_file(const char * filename, char ** data, size_t * ret_size,
                      bool binary = true)
{
    std::ifstream fp;
    fp.open(filename, std::ios::in | std::ios::binary | std::ios::ate);
    if (!fp) {
        std::cout << "Could not load file " << filename << std::endl;
        return;
    }

    size_t size = fp.tellg();
    fp.seekg(0, std::ios::beg);
    if (binary)
        *data = new char[size];
    else
        *data = new char[size + 1];
    fp.read(*data, size);
    fp.close();
    if (!binary)
        (*data)[size] = 0;
    *ret_size = size;
}

inline size_t get_file_size(const char * filename)
{
    struct stat st;
    stat(filename, &st);
    return st.st_size;
}

inline std::string get_path_filename(const std::string & path)
{
    size_t pos = path.find_last_of(PATH_SEP);
    if (pos == std::string::npos)
        return path;
    return path.substr(pos + 1);
}

inline std::string get_path_dirname(const std::string & path)
{
    size_t pos = path.find_last_of(PATH_SEP);
    if (pos == std::string::npos)
        return "";
    return path.substr(0, pos + 1);
}

inline std::string get_path_basename(const std::string & path)
{
    std::string path2 = get_path_filename(path);
    return path2.substr(0, path2.find_last_of("."));
}

#endif // ST_PATH_H
