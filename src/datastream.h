#ifndef ST_DATASTREAM_H
#define ST_DATASTREAM_H

#include <iostream>
#include <fstream>

#include "filesystem.h"
#include "glm.h"

class BaseStream
{
public:
    bool read(std::string & str, size_t len);
    bool read(std::string & str);
    char read_int8();
    unsigned char read_uint8();
    short read_int16();
    unsigned short read_uint16();
    int read_int32();
    unsigned int read_uint32();
    void read_string(std::string & str);
    float read_float();
    void read_vec3(vec3 & v);
    void read_quat(quat & q);

    void write(const std::string & str);
    void write_int8(char v);
    void write_uint8(unsigned char v);
    void write_int16(short v);
    void write_uint16(unsigned short v);
    void write_int32(int v);
    void write_uint32(unsigned int v);
    void write_string(const std::string & str);
    void write_float(float v);
    void write_vec3(const vec3 & v);
    void write_quat(const quat & q);

    bool at_end();
    unsigned char peek(size_t peekpos);

    // implementation-specific
    virtual bool read(char * data, size_t len);
    virtual void write(char * data, size_t len);
    virtual size_t get_size();
    virtual size_t tell();
    virtual void seek(size_t pos);
};

class DataStream : public BaseStream
{
public:
    char * data;
    size_t size;
    size_t pos;
    bool is_writer;

    DataStream(char * data, size_t size);
    DataStream();
    ~DataStream();

    using BaseStream::read;
    using BaseStream::write;
    bool read(char * data, size_t len);
    void write(char * data, size_t len);
    bool ensure_size(size_t size);
    size_t get_size();
    size_t tell();
    void seek(size_t pos);
};

class FileStream : public BaseStream
{
public:
    FSFile * fp;

    FileStream(FSFile * fp);

    using BaseStream::read;
    using BaseStream::write;
    bool read(char * data, size_t len);
    void write(char * data, size_t len);
    size_t get_size();
    size_t tell();
    void seek(size_t pos);
};

#endif // ST_DATASTREAM_H
