#ifndef ST_MASTER_H
#define ST_MASTER_H

#include <string>

#include "network.h"

class MasterHost : public NetworkHost
{
public:
    bool closed;

    MasterHost(int port);
    NetworkPeer * create_peer(ENetPeer * peer, bool is_client);
    void update_servers(float dt);
    void run();
    void close();
};

class MasterPeer : public NetworkPeer
{
public:
    unsigned char id;
    unsigned short port;
    unsigned int count, max_players;
    bool verified;
    std::string host_ip;
    std::string identifier;
    std::string name, escaped_name;

    MasterPeer(NetworkHost * host, ENetPeer * peer);
    bool on_connect();
    void on_disconnect(unsigned int reason);
    void on_packet(Packet & packet);
    void update(float dt);
};

#endif // ST_MASTER_H
