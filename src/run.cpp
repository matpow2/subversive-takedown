#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <physfs.h>

#include "assets.h"
#include "audiomanager.h"
#include "constants.h"
#include "fonts.h"
#include "include_gl.h"
#include "manager.h"
#include "scene.h"
#include "scenes.h"
#include "shader.h"
#include "timer.h"
#include "utf8.h"
#include "draw.h"

#ifdef _WIN32
#include <direct.h>
#define getcwd _getcwd
#define chdir _chdir
#else
#include <unistd.h>
#endif

inline bool check_opengl_extension(const char * name)
{
    if (glewGetExtension(name) == GL_TRUE)
        return true;
    std::cout << "OpenGL extension '" << name << "' not supported." << std::endl;
    return false;
}

inline bool check_opengl_extensions()
{
    const char * extensions[] = {
       // "GL_EXT_framebuffer_object",
       "GL_ARB_vertex_shader",
       "GL_ARB_fragment_shader",
        NULL
    };
    for (int i = 0; extensions[i] != NULL; i++)
        if (!check_opengl_extension(extensions[i]))
            return false;
    return true;
}

GameManager * global_manager = NULL;

void _mouse_callback(GLFWwindow * window, int button, int action)
{
    global_manager->on_mouse_key(button, action == GLFW_PRESS);
}

void _mouse_move_callback(GLFWwindow * window, double x, double y)
{
    global_manager->on_mouse_move(int(x), int(y));
}

void _button_callback(GLFWwindow * window, int button, int action)
{
    if (action == GLFW_REPEAT && button != GLFW_KEY_BACKSPACE)
        return;
    global_manager->on_key(button, action != GLFW_RELEASE);
}

void _mouse_scroll_callback(GLFWwindow * window, double dx, double dy)
{
    global_manager->on_mouse_scroll(dx, dy);
}

void _resize_callback(GLFWwindow * window, int w, int h)
{
    global_manager->on_resize(w, h);
}

void _char_callback(GLFWwindow * window, unsigned int ch)
{
    if (ch > 127)
        // font doesn't support these characters
        return;
    unsigned char u[4];
    unsigned char * end = utf8::append(ch, u);
    std::string text((const char*)u, size_t(end - u));
    global_manager->on_text(text);
}

void _error_callback(int error, const char * msg)
{
    std::cout << "Window error (" << error << "): " << msg << std::endl;
}

GameManager::GameManager()
: scene(NULL), next_scene(NULL), has_quit(false), try_quit(false)
{
    file_mount("data", "data", 1);
    file_set_write_dir(".");

    config = new ConfigFile("config.txt");

    glfwSetErrorCallback(_error_callback);

    if(!glfwInit()) {
        exit(EXIT_FAILURE);
        return;
    }

    st_init_time();

    if (config->samples != 0)
        glfwWindowHint(GLFW_SAMPLES, config->samples);
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
    window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT,
        WINDOW_NAME, NULL, NULL);
    glfwMakeContextCurrent(window);
    if (config->vsync)
        glfwSwapInterval(1);
    else
        glfwSwapInterval(0);

    setup_opengl();

    // initialize OpenGL extensions
    glewInit();

    // check extensions
    if (!check_opengl_extensions()) {
        std::cout << "Not all OpenGL extensions supported. Setting graphics"
            " level to minimum" << std::endl;
        config->graphics = LOW_GFX;
    }

    glfwSetMouseButtonCallback(window, _mouse_callback);
    glfwSetCursorPosCallback(window, _mouse_move_callback);
    glfwSetKeyCallback(window, _button_callback);
    glfwSetCharCallback(window, _char_callback);
    glfwSetScrollCallback(window, _mouse_scroll_callback);
    glfwSetWindowSizeCallback(window, _resize_callback);

    if (config->has_high_graphics()) {
        load_shaders();

        // setup shaders
        ssao_shader->bind();
        ssao_shader->uniform("color_texture", 0);
        ssao_shader->uniform("depth_texture", 1);
        Shader::unbind();

        glGenTextures(1, &color_texture);
        glBindTexture(GL_TEXTURE_2D, color_texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glGenTextures(1, &depth_texture);
        glBindTexture(GL_TEXTURE_2D, depth_texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);
/*        // generate color buffer
        glGenTextures(1, &color_texture);
        GLenum color_target = GLEW_EXT_framebuffer_multisample ?
            GL_TEXTURE_2D_MULTISAMPLE : GL_TEXTURE_2D;
        glBindTexture(color_target, color_texture);
        if (GLEW_EXT_framebuffer_multisample)
            glTexImage2DMultisample(color_target, 4, GL_RGBA8,
                WINDOW_WIDTH, WINDOW_HEIGHT, NULL);
        else
            glTexImage2D(color_target, 0, GL_RGBA8, WINDOW_WIDTH, WINDOW_HEIGHT,
                0, GL_BGRA, GL_UNSIGNED_BYTE, NULL);
        glTexParameteri(color_target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(color_target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(color_target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(color_target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        // generate depth buffer
        glGenTextures(1, &depth_texture);
        glBindTexture(GL_TEXTURE_2D, depth_texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);
        if (GLEW_EXT_packed_depth_stencil)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8,
                WINDOW_WIDTH, WINDOW_HEIGHT, 0, GL_DEPTH_STENCIL,
                GL_UNSIGNED_INT_24_8, 0);
        else
            glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
                WINDOW_WIDTH, WINDOW_HEIGHT, 0, GL_DEPTH_COMPONENT,
                GL_FLOAT, 0);

        glGenFramebuffersEXT(1, &screen_fbo);
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, screen_fbo);
        if (GLEW_EXT_packed_depth_stencil)
            glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,
                GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, depth_texture,
                0);
        else
            glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,
                GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, depth_texture, 0);
        glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,
            GL_COLOR_ATTACHMENT0_EXT, color_target, color_texture, 0);
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);*/
    }

    set_display_size(WINDOW_WIDTH, WINDOW_HEIGHT);
    on_resize(WINDOW_WIDTH, WINDOW_HEIGHT);

    audio = new AudioManager(config);
    initialize_assets();
    initialize_fonts();
    initialize_scenes(this);

    this->time = st_get_time();

    srand((unsigned int)(::time(NULL)));
}

void GameManager::update()
{
    if (next_scene != NULL) {
        if (scene != NULL)
            scene->on_end();
        scene = next_scene;
        next_scene = NULL;
        scene->time = 0.0;
        scene->on_start();
    }

    glfwPollEvents();

    if (try_quit || glfwWindowShouldClose(window))
        quit();

    if (has_quit)
        return;

    // update mouse position
    double x, y;
    glfwGetCursorPos(window, &x, &y);
    mouse_x = int((x - off_x) * (float(WINDOW_WIDTH) / x_size));
    mouse_y = int((y - off_y) * (float(WINDOW_HEIGHT) / y_size));

    double current_time = st_get_time();
    // limit min dt value to 20 FPS
    double dt = current_time - time;
    float dt_f = float(dt);
    time = current_time;

    Scene * old_scene = scene;

    scene->time += dt;
    audio->update(dt_f);
    scene->update(dt_f);

#ifdef ST_PROFILE
    std::cout << "Scene update took " << (st_get_time() - current_time)
        << std::endl;
    current_time = st_get_time();
#endif

    if (scene != old_scene)
        return;

    draw();
    glfwSwapBuffers(window);

#ifdef ST_PROFILE
    std::cout << "Draw took " << (float(glfwGetTime()) - current_time)
        << std::endl;
#endif
}

void GameManager::quit()
{
    try_quit = false;
    if (!scene->on_quit())
        return;
    has_quit = true;
}

void GameManager::on_resize(int w, int h)
{
    if (scene != NULL)
        scene->on_resize(w, h);
    window_width = w;
    window_height = h;

    if (config->has_high_graphics()) {
        glBindTexture(GL_TEXTURE_2D, color_texture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_BGRA,
            GL_UNSIGNED_BYTE, NULL);
        glBindTexture(GL_TEXTURE_2D, depth_texture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
            w, h, 0, GL_DEPTH_COMPONENT,  GL_FLOAT, NULL);

        ssao_shader->bind();
        ssao_shader->uniform("texture_size", float(w), float(h));
        Shader::unbind();
    }
}

void GameManager::transform_mouse_coordinates(int & x, int & y)
{
    y = window_height - y;
    if (display_width == -1 && display_height == -1)
        return;
    if (display_width == window_width && display_height == window_height)
        return;
    x = int((float(display_width) / float(window_width)) * x);
    y = int((float(display_height) / float(window_height)) * y);
}

void GameManager::on_mouse_key(int button, bool value)
{
    scene->on_mouse_key(button, value);
}

void GameManager::on_mouse_move(int x, int y)
{
    transform_mouse_coordinates(x, y);
    scene->on_mouse_move(x, y);
}

void GameManager::on_mouse_scroll(double dx, double dy)
{
    scene->on_mouse_scroll(dx, dy);
}

void GameManager::on_key(int button, bool value)
{
    try_quit = button == GLFW_KEY_ESCAPE && value;
    scene->on_key(button, value);
}

void GameManager::on_text(const std::string & text)
{
    scene->on_text(text);
}

void GameManager::run()
{
    while (true) {
        update();
        if (has_quit)
            break;
    }

    glfwTerminate();
    file_deinit();
}

void GameManager::set_normal_mouse()
{
    glfwSetInputMode(window, GLFW_CURSOR_MODE, GLFW_CURSOR_NORMAL);
}

void GameManager::set_hidden_mouse()
{
    glfwSetInputMode(window, GLFW_CURSOR_MODE, GLFW_CURSOR_HIDDEN);
}

void GameManager::set_captured_mouse()
{
    glfwSetInputMode(window, GLFW_CURSOR_MODE, GLFW_CURSOR_CAPTURED);
}

void GameManager::set_display_size(int w, int h)
{
    display_width = w;
    display_height = h;
}

void GameManager::resize(int w, int h)
{
    glfwSetWindowSize(window, w, h);
}

void GameManager::get_mouse_pos(int & x, int & y)
{
    double x2, y2;
    glfwGetCursorPos(window, &x2, &y2);
    x = int(x2);
    y = int(y2);
    transform_mouse_coordinates(x, y);
}

void GameManager::set_scene(Scene * new_scene)
{
    next_scene = new_scene;
}

void GameManager::post_process(float znear, float zfar)
{
    if (!config->has_high_graphics())
        return;

    glPushAttrib(GL_ENABLE_BIT);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0, window_width, 0, window_height);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glDisable(GL_DEPTH_TEST);
    glLoadIdentity();

    // texture 0, color
    glActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, color_texture);
    glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 0, 0,
        window_width, window_height, 0);

    // texture 1, depth
    glActiveTexture(GL_TEXTURE1);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, depth_texture);
    glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, 0, 0,
        window_width, window_height, 0);

    int off_x, off_y;
    off_x = off_y = 0;
    x_size = window_width;
    y_size = window_height;
    int x2 = off_x + x_size;
    int y2 = off_y + y_size;

    ssao_shader->bind();
    ssao_shader->uniform("znear", znear * 0.02f);
    ssao_shader->uniform("zfar", zfar * 0.02f);

    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    glDisable(GL_BLEND);
    glBegin(GL_QUADS);
    glTexCoord2f(0.0f, 0.0f);
    glVertex2i(off_x, off_y);
    glTexCoord2f(1.0f, 0.0f);
    glVertex2i(x2, off_y);
    glTexCoord2f(1.0f, 1.0f);
    glVertex2i(x2, y2);
    glTexCoord2f(0.0f, 1.0f);
    glVertex2i(off_x, y2);
    glEnd();
    glEnable(GL_BLEND);

    glBindTexture(GL_TEXTURE_2D, 0);
    glActiveTexture(GL_TEXTURE0);
    glDisable(GL_TEXTURE_2D);
    Shader::unbind();

    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopAttrib();
}

void GameManager::draw()
{
    if (window_width <= 0 || window_height <= 0)
        // for some reason, GLFW sets these properties to 0 when minimized.
        return;

    glViewport(0, 0, window_width, window_height);

    bool high_graphics = config->has_high_graphics();

/*    if (high_graphics)
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, screen_fbo);*/

    glEnable(GL_MULTISAMPLE);
    scene->draw();
    glDisable(GL_MULTISAMPLE);

#if 0
    if (high_graphics) {
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

        // resize the window contents if necessary (fullscreen mode)
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        glLoadIdentity();

/*        bool resize = window_width != WINDOW_WIDTH || window_height != WINDOW_HEIGHT;*/

/*        if (resize) {
            // aspect-aware resize
            float aspect_width = window_width / float(WINDOW_WIDTH);
            float aspect_height = window_height / float(WINDOW_HEIGHT);

            if (aspect_width > aspect_height) {
                x_size = int(aspect_height * float(WINDOW_WIDTH));
                y_size = int(aspect_height * float(WINDOW_HEIGHT));
            } else {
                x_size = int(aspect_width * float(WINDOW_WIDTH));
                y_size = int(aspect_width * float(WINDOW_HEIGHT));
            }

            off_x = (window_width - x_size) / 2;
            off_y = (window_height - y_size) / 2;
        } else {
            off_x = off_y = 0;
            x_size = WINDOW_WIDTH;
            y_size = WINDOW_HEIGHT;
        }*/

        off_x = off_y = 0;
        x_size = WINDOW_WIDTH;
        y_size = WINDOW_HEIGHT;
        int x2 = off_x + x_size;
        int y2 = off_y + y_size;

        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, color_texture);
        glDisable(GL_BLEND);
        glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 0.0f);
        glVertex2i(off_x, off_y);
        glTexCoord2f(1.0f, 0.0f);
        glVertex2i(x2, off_y);
        glTexCoord2f(1.0f, 1.0f);
        glVertex2i(x2, y2);
        glTexCoord2f(0.0f, 1.0f);
        glVertex2i(off_x, y2);
        glEnd();
        glEnable(GL_BLEND);
        glDisable(GL_TEXTURE_2D);
    }
#endif
}

int main(int argc, char *argv[])
{
    file_init(argv[0]);

#if !defined(ST_DEBUG) && defined(WIN32)
    // change working directory because we were opened from the browser
    if (argc == 2) {
        char exe_path[MAX_PATH];
        strcpy(exe_path, argv[0]);
        char * last_slash = strrchr(exe_path, '\\');
        if (last_slash != NULL) {
            *strrchr(exe_path, '\\') = '\0';
            chdir(exe_path);
        }
    }
#endif

    GameManager manager = GameManager();
    global_manager = &manager;
    if (argc == 2) {
        std::string arg = argv[1];
        arg = arg.substr(5); // "st://"
        size_t i = arg.find(':');
        if (i == std::string::npos) {
            manager.host_ip = arg.substr(0, i);
            manager.host_port = NETWORK_PORT;
        } else {
            manager.host_ip = arg.substr(0, i);
            manager.host_port = string_to_int(arg.substr(i+1));
        }
        manager.set_scene(game_scene);
    } else {
        manager.host_ip = NETWORK_IP;
        manager.host_port = NETWORK_PORT;
        manager.set_scene(main_menu_scene);
    }

    manager.run();
    return EXIT_SUCCESS;
}
