#include <algorithm>
#include <iostream>
#include <stdlib.h>

#include "constants.h"
#include "datastream.h"
#include "timer.h"
#include "network.h"
#include "stringcommon.h"

// NetworkPeer

NetworkPeer::NetworkPeer(NetworkHost * host, ENetPeer * peer)
: host(host), peer(peer)
{

}

void NetworkPeer::send_packet(Packet & packet)
{
    DataStream stream;
    write_packet(packet, stream);
    ENetPacket * new_packet = enet_packet_create(stream.data, stream.size,
        packet.flags);
    enet_peer_send(peer, packet.channel, new_packet);
}

void NetworkPeer::disconnect(bool now)
{
    if (now)
        enet_peer_disconnect_now(peer, 0);
    else
        enet_peer_disconnect(peer, 0);
}

NetworkPeer::~NetworkPeer()
{
    peer->data = NULL;
}

// NetworkHost

NetworkHost::NetworkHost(int port)
{
    static bool initialized = false;
    if (!initialized) {
        if (enet_initialize() != 0) {
            std::cerr << "Could not initialize network" << std::endl;
            return;
        }
        atexit(enet_deinitialize);
        initialized = true;
    }

    ENetAddress * address = NULL;
    int connection_count = 1;

    if (port != -1) {
        address = new ENetAddress;
        address->host = ENET_HOST_ANY;
        address->port = port;
        connection_count = MAX_CONNECTIONS;
    }

    host = enet_host_create(address, connection_count, CHANNEL_COUNT, 0, 0);

    if (host == NULL) {
        std::cerr << "Could not initialize host" << std::endl;
    }

    enet_host_compress_with_range_coder(host);
}

void NetworkHost::connect(const std::string & ip, int port)
{
    ENetAddress address;
    enet_address_set_host(&address, ip.c_str());
    address.port = port;

    ENetPeer * peer = enet_host_connect(host, &address, CHANNEL_COUNT,
        ST_VERSION);
    NetworkPeer * network_peer = create_peer(peer, true);
    peer->data = network_peer;

    if (peer == NULL) {
        std::cerr << "No available peers for initiating an ENet connection."
            << std::endl;
    }
}

void NetworkHost::update(unsigned int timeout)
{
    ENetEvent event;
    NetworkPeer * peer;
    Packet * packet;

    while (true) {
        if (enet_host_service(host, &event, timeout) <= 0)
            break;

        switch (event.type) {
            case ENET_EVENT_TYPE_CONNECT:
                if (event.peer->data == NULL) {
                    peer = create_peer(event.peer, false);
                    event.peer->data = peer;
                    if (peer == NULL)
                        break;
                    peers.push_back(peer);
                }
                peer = (NetworkPeer*)event.peer->data;
                peer->on_connect();
                break;
            case ENET_EVENT_TYPE_RECEIVE:
                // event.channelID
                peer = (NetworkPeer*)event.peer->data;
                if (peer == NULL) {
                    enet_packet_destroy(event.packet);
                    break;
                }
                packet = read_packet((char*)event.packet->data,
                    event.packet->dataLength);
                if (packet == NULL)
                    break;
                enet_packet_destroy(event.packet);
#ifdef SIMULATE_LATENCY
                stored_packets.push_back(StoredPacket(peer, packet,
                    st_get_time()));
#else
                peer->on_packet(*packet);
                delete packet;
#endif
                break;
            case ENET_EVENT_TYPE_DISCONNECT:
                peer = (NetworkPeer*)event.peer->data;
                if (peer == NULL)
                    break;
                peer->on_disconnect(event.data);
                peers.erase(std::remove(peers.begin(), peers.end(), peer),
                    peers.end());
                delete peer;
                break;
        }
    }

#ifdef SIMULATE_LATENCY
    double current_time = st_get_time();
    StoredPackets::iterator it = stored_packets.begin();
    while (it != stored_packets.end() && it->end_time <= current_time) {
        it->peer->on_packet(*it->packet);
        delete it->packet;
        it = stored_packets.erase(it);
    }
#endif
}

void NetworkHost::flush()
{
    enet_host_flush(host);
}

NetworkHost::~NetworkHost()
{
    if (host == NULL)
        return;
    PeerList::iterator it;
    for (it = peers.begin(); it != peers.end(); it++)
        delete *it;
    enet_host_destroy(host);
}
