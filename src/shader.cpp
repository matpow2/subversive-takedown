#include "shader.h"

#include "path.h"

Shader::Shader(const std::string & vert, const std::string & frag)
{
    program = glCreateProgram();
    GLuint vert_shader = attach_source(vert, GL_VERTEX_SHADER);
    GLuint frag_shader = attach_source(frag, GL_FRAGMENT_SHADER);
    glLinkProgram(program);

    GLint status;
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if (status == GL_FALSE) {
        GLint info_len;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &info_len);
        GLchar * info_log = new GLchar[info_len + 1];
        glGetProgramInfoLog(program, info_len, NULL, info_log);
        std::cout << "Linker failure: " << info_log << std::endl;
        delete[] info_log;
    }

    glDetachShader(program, vert_shader);
    glDetachShader(program, frag_shader);
}

GLuint Shader::attach_source(const std::string & path, GLenum type)
{
    GLuint shader = glCreateShader(type);
    GLchar * data;
    size_t length;
    read_file(path.c_str(), &data, &length);
    GLint len = length;
    glShaderSource(shader, 1, (const GLchar**)&data, &len);
    delete[] data;
    glCompileShader(shader);

    GLint status;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    if (status == GL_FALSE) {
        GLint info_len;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_len);
        GLchar * info_log = new GLchar[info_len + 1];
        glGetShaderInfoLog(shader, info_len, NULL, info_log);
        std::cout << "Compile error in " << type << ":" << std::endl <<
            info_log << std::endl;
        delete[] info_log;
    } else {
        glAttachShader(program, shader);
    }
    return shader;
}

void Shader::bind()
{
    glUseProgram(program);
}

void Shader::unbind()
{
    glUseProgram(0);
}

void Shader::uniform(const char * value, GLfloat f)
{
    glUniform1f(get_uniform(value), f);
}

void Shader::uniform(const char * value, GLfloat f1, GLfloat f2)
{
    glUniform2f(get_uniform(value), f1, f2);
}

void Shader::uniform(const char * value, GLfloat f1, GLfloat f2, GLfloat f3)
{
    glUniform2f(get_uniform(value), f1, f2);
}

void Shader::uniform(const char * value, GLint i)
{
    glUniform1i(get_uniform(value), i);
}

GLint Shader::get_uniform(const char * value)
{
    return glGetUniformLocation(program, value);
}

Shader * ssao_shader = NULL;

Shader * load_shader(const std::string & name)
{
    std::string base = "data/shaders/" + name;
    return new Shader(base + ".vert", base + ".frag");
}

void load_shaders()
{
    ssao_shader = load_shader("ssao");
}
