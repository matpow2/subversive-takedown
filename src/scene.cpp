#include "scene.h"

#include "include_gl.h"

Scene::Scene(GameManager * manager)
{
    this->manager = manager;
    audio = manager->audio;
}

void Scene::set_scene(Scene * new_scene)
{
    manager->set_scene(new_scene);
}

void Scene::play_select()
{
    audio->play_sample("select", 0.5f);
}

float Scene::get_time()
{
    return float(time);
}
