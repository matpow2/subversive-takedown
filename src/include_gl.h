#ifndef ST_INCLUDE_GL_H
#define ST_INCLUDE_GL_H

#ifdef _WIN32
#define _WINSOCKAPI_
#define NOMINMAX
#include <windows.h>
#endif

#include <GL/glew.h>
#include <GL/glfw3.h>

#endif // ST_INCLUDE_GL_H
