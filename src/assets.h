#ifdef ST_LOAD_ASSETS
#define LOAD_IMAGE(name) name##_image = load_image(#name)
#define LOAD_IMAGE_RETRO(name) name##_image = load_image(#name, 0, 0, false)
#define LOAD_IMAGE_CENTER(name) name##_image = load_image_center(#name)
#define LOAD_IMAGE_CENTER_RETRO(name) name##_image = load_image_center(\
    #name, false)
#define LOAD_ANIMATION_RETRO(name, d) name##_animation = load_image_animation( \
    #name, d, false, false)
#define LOAD_ANIMATION_CENTER_RETRO(name, d) name##_animation = \
    load_image_animation(#name, d, true, false)
#define LOAD_MODEL_ANIMATION(name, d) name##_animation = load_voxel_animation( \
    #name, d)
#define LOAD_MODEL(name) name##_model = load_model(#name)
#define LOAD_VOXEL(name) name##_voxel = load_voxel(#name)

#elif defined(ST_DECLARE_ASSETS)

#define LOAD_IMAGE(name) Image * name##_image = NULL
#define LOAD_IMAGE_RETRO(name) LOAD_IMAGE(name)
#define LOAD_IMAGE_CENTER(name) LOAD_IMAGE(name)
#define LOAD_IMAGE_CENTER_RETRO(name) LOAD_IMAGE(name)
#define LOAD_ANIMATION(name, d) ImageAnimationData * name##_animation = NULL
#define LOAD_ANIMATION_RETRO(name, d) LOAD_ANIMATION(name, d)
#define LOAD_ANIMATION_CENTER_RETRO(name, d) LOAD_ANIMATION(name, d)
#define LOAD_MODEL_ANIMATION(name, d) VoxelAnimationData * name##_animation = NULL
#define LOAD_MODEL(name) VoxelModel * name##_model = NULL
#define LOAD_VOXEL(name) VoxelFile * name##_voxel = NULL

#else

#include <string>
#include "animation.h"
#include "image.h"
#include "voxel.h"
void initialize_assets();

#define LOAD_IMAGE(name) extern Image * name##_image
#define LOAD_IMAGE_RETRO(name) LOAD_IMAGE(name)
#define LOAD_IMAGE_CENTER(name) LOAD_IMAGE(name)
#define LOAD_IMAGE_CENTER_RETRO(name) LOAD_IMAGE(name)
#define LOAD_ANIMATION(name, d) extern ImageAnimationData * name##_animation
#define LOAD_ANIMATION_RETRO(name, d) LOAD_ANIMATION(name, d)
#define LOAD_ANIMATION_CENTER_RETRO(name, d) LOAD_ANIMATION(name, d)
#define LOAD_VOXEL(name) extern VoxelFile * name##_voxel
#define LOAD_MODEL_ANIMATION(name, d) extern VoxelAnimationData * name##_animation
#define LOAD_MODEL(name) extern VoxelModel * name##_model
#endif // ST_LOAD_ASSETS

LOAD_IMAGE_RETRO(tile);
LOAD_IMAGE_CENTER_RETRO(iconbox);
LOAD_IMAGE_CENTER_RETRO(pencil_tool);
LOAD_IMAGE_CENTER_RETRO(block_tool);
LOAD_IMAGE_CENTER_RETRO(shadow);
LOAD_IMAGE_CENTER_RETRO(crosshair);
LOAD_IMAGE_CENTER_RETRO(build_crosshair);
LOAD_IMAGE_CENTER_RETRO(intel);
LOAD_IMAGE_CENTER_RETRO(transmitter);
LOAD_IMAGE_RETRO(cursor);
LOAD_IMAGE_CENTER(splash);
LOAD_MODEL_ANIMATION(characterwalk, 0.15f);
LOAD_MODEL_ANIMATION(character2walk, 0.15f);
LOAD_MODEL(table);
LOAD_IMAGE_CENTER_RETRO(grass);
LOAD_VOXEL(palette);
LOAD_MODEL(bullet);
LOAD_MODEL(intel);
LOAD_MODEL(crate);
LOAD_MODEL(intel1);
LOAD_MODEL(intel2);
LOAD_MODEL(base1);
LOAD_MODEL(base2);
LOAD_MODEL(base3);
LOAD_MODEL(gun);
LOAD_MODEL(lazergun);
LOAD_MODEL(lazertracer);
LOAD_MODEL(block);
LOAD_MODEL(rpg);
LOAD_MODEL(pickaxe);
LOAD_MODEL(rocket);
LOAD_MODEL(selfkill);

#undef LOAD_IMAGE
#undef LOAD_IMAGE_RETRO
#undef LOAD_IMAGE_CENTER_RETRO
#undef LOAD_IMAGE_CENTER
#undef LOAD_ANIMATION
#undef LOAD_ANIMATION_RETRO
#undef LOAD_ANIMATION_CENTER_RETRO
#undef LOAD_MODEL_ANIMATION
#undef LOAD_MODEL
#undef LOAD_VOXEL
