#ifndef ST_AUDIOMANAGER_H
#define ST_AUDIOMANAGER_H

#include <string>
#include <vector>

#include "audio.h"
#include "glm.h"
#include "mathcommon.h"
#include "types.h"

typedef fast_map<std::string, Sample*> SampleMap;
typedef std::vector<SoundBase*> SoundVector;

class ConfigFile;

class AudioManager
{
public:
    SoundStream * music;
    SampleMap cached_samples;
    SoundVector sounds;
    SoundBase * event_sample;
    float music_volume;
    ConfigFile * config;
    std::string music_name;

    AudioManager(ConfigFile * config);
    ~AudioManager();
    void play_music(const std::string & name, float volume = 1.0f);
    void stop_music();
    SoundBase * get_sample(const std::string & name);
    SoundBase * play_sample(const std::string & name, float volume = 1.0f);
    SoundBase * play_event_sample(const std::string & name);
    SoundBase * play_sample(const std::string & name, const vec3 & pos,
                            float volume = 1.0f);
    void set_listener(const vec3 & pos, float rotation);
    void update(float dt);
    void stop_samples();
};

#endif // ST_AUDIOMANAGER_H
