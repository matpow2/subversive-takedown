#include "propeditor.h"
#include "mainwindow.h"
#include "editorcommon.h"
#include "map.h"
#include "mapeditor.h"
#include "constants.h"

#include <QSpinBox>
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QComboBox>

int to_pot(int n)
{
    if (n <= 0)
        return 0;
    return int(pow(2.0, double(n+1)));
}

int from_pot(int n)
{
    if (n <= 0)
        return 0;
    return int(log(double(n)) / log(2.0)) - 1;
}

QSpinBox * create_pos_spinbox(QWidget * parent)
{
    QSpinBox * box = new QSpinBox(parent);
    box->setMinimum(-300000);
    box->setMaximum(300000);
    return box;
}

PropEditor::PropEditor(MainWindow * parent)
: QWidget(parent), window(parent), ignore_changes(true), map_obj(NULL)
{
    QVBoxLayout * layout = new QVBoxLayout(this);

    QHBoxLayout * pos_layout = new QHBoxLayout;
    pos_layout->addWidget(create_label("Pos"));
    pos_x = create_pos_spinbox(this);
    pos_y = create_pos_spinbox(this);
    pos_z = create_pos_spinbox(this);
    connect(pos_x, SIGNAL(valueChanged(int)), SLOT(on_change()));
    connect(pos_y, SIGNAL(valueChanged(int)), SLOT(on_change()));
    connect(pos_z, SIGNAL(valueChanged(int)), SLOT(on_change()));
    pos_layout->addWidget(pos_x);
    pos_layout->addWidget(pos_y);
    pos_layout->addWidget(pos_z);
    layout->addLayout(pos_layout);

    QHBoxLayout * scale_layout = new QHBoxLayout;
    scale_layout->addWidget(create_label("Scale"));
    scale = new QSpinBox(this);
    scale->setMinimum(1);
    connect(scale, SIGNAL(valueChanged(int)), SLOT(on_change()));
    scale_layout->addWidget(scale);
    layout->addLayout(scale_layout);

    QHBoxLayout * id_layout = new QHBoxLayout;
    id_layout->addWidget(create_label("ID"));
    id = new QLineEdit(this);
    connect(id, SIGNAL(textChanged(QString)), SLOT(on_change()));
    id_layout->addWidget(id);
    layout->addLayout(id_layout);

    QHBoxLayout * icon_layout = new QHBoxLayout;
    icon_layout->addWidget(create_label("Icon"));
    icon = new QLineEdit(this);
    connect(icon, SIGNAL(textChanged(QString)), SLOT(on_change()));
    icon_layout->addWidget(icon);
    layout->addLayout(icon_layout);

    QHBoxLayout * team_layout = new QHBoxLayout;
    team_layout->addWidget(create_label("Team"));
    team = new QComboBox(this);
    team->addItem("None", QVariant(TEAM_NONE));
    team->addItem("Greenites", QVariant(TEAM_1));
    team->addItem("Bluekins", QVariant(TEAM_2));
    connect(team, SIGNAL(currentIndexChanged(int)), SLOT(on_change()));
    team_layout->addWidget(team);
    layout->addLayout(team_layout);

    QHBoxLayout * health_layout = new QHBoxLayout;
    health_layout->addWidget(create_label("Health"));
    health = new QSpinBox(this);
    health->setMaximum(9000);
    connect(health, SIGNAL(valueChanged(int)), SLOT(on_change()));
    health_layout->addWidget(health);
    layout->addLayout(health_layout);

    QHBoxLayout * grid_layout = new QHBoxLayout;
    grid_layout->addWidget(create_label("Grid"));
    grid = new QSpinBox(this);
    grid->setValue(4);
    grid_layout->addWidget(grid);
    layout->addLayout(grid_layout);

    setLayout(layout);

    ignore_changes = false;

    set_object(NULL);
}

void PropEditor::set_object(MapObject * m)
{
    ignore_changes = true;
    map_obj = m;
    bool disabled = m == NULL;
    setDisabled(disabled);
    if (disabled)
        return;
    if (map_obj->voxel == NULL)
        scale->setDisabled(true);
    else {
        scale->setDisabled(false);
        scale->setValue(from_pot(map_obj->scale));
    }
    pos_x->setValue(map_obj->pos.x);
    pos_y->setValue(map_obj->pos.y);
    pos_z->setValue(map_obj->pos.z);
    id->setText(map_obj->id.c_str());
    icon->setText(map_obj->icon.c_str());
    team->setCurrentIndex(team->findData(map_obj->team));
    health->setValue(map_obj->health);
    ignore_changes = false;
}

int PropEditor::get_grid_value()
{
    return to_pot(grid->value());
}

void PropEditor::on_change()
{
    if (map_obj == NULL || ignore_changes)
        return;
    map_obj->scale = to_pot(scale->value());
    map_obj->pos.x = pos_x->value();
    map_obj->pos.y = pos_y->value();
    map_obj->pos.z = pos_z->value();
    map_obj->id = convert_str(id->text());
    map_obj->icon = convert_str(icon->text());
    map_obj->team = team->itemData(team->currentIndex()).toInt();
    map_obj->health = health->value();

    const std::string & type = map_obj->type;
    if (type == "" || type == "model") {
        if (map_obj->health == 0) {
            map_obj->set_list(&map_obj->map.props);
            map_obj->type = "";
        } else {
            map_obj->set_list(&map_obj->map.objects);
            map_obj->type = "model";
        }
    }

    window->get_map_editor()->update_selected();
}