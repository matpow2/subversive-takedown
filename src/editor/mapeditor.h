#include "glm.h"
#include "world.h"
#include "editorcommon.h"

#include <QGLWidget>

class VoxelFile;
class VoxelModel;
class MainWindow;
class btCollisionObject;
class Map;
class MapObject;

class MapEditor : public QGLWidget
{
    Q_OBJECT

public:
    World::ObjectList selected_list;
    World::ObjectList copied_list;

    std::string map_name;
    World::World * world;
    Map * map;
    MainWindow * window;

    mat4 projection_matrix, view_matrix, mvp, inverse_mvp;
    vec4 viewport;

    vec3 pos;
    float scale;
    float rotate_x, rotate_z;

    bool forward, backward, strafe_left, strafe_right, up, down;

    PositionArrows pos_arrows;

    QPoint last_pos;

    MapEditor(MainWindow * parent);
    void load(const std::string & name);
    void reset();
    void on_changed();
    ~MapEditor();
    void move_camera(const vec3 & move);
    void add_selected(World::WorldObject * obj);
    World::WorldObject * get_selected();
    bool has_selected();
    vec3 get_selected_center();
    void update_selected();
    void update_object(World::WorldObject * obj);
    World::WorldObject * add_object(MapObject * obj);
    vec3 get_grid_pos(const vec3 & p);
    void copy_selected();
    void paste_objects();
    void delete_selected();
    void new_object();
    void voxel_cache_changed(const std::string & name);

protected:
    void initializeGL();
    void paintGL();
    void update_view_matrix();
    void resizeGL(int w, int h);
    void keyPressEvent(QKeyEvent *e);
    void keyReleaseEvent(QKeyEvent *e);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *e);
    void wheelEvent(QWheelEvent * e);
    void closeEvent(QCloseEvent *event);

public slots:
    void update_camera();
    void save();
    void save_as();
};