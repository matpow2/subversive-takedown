#include "mainwindow.h"
#include "voxeditor.h"
#include "mapeditor.h"
#include "palette.h"
#include "modelproperties.h"
#include "mapproperties.h"
#include "propeditor.h"
#include "voxel.h"
#include "editorcommon.h"

#include <QToolBar>
#include <QMenuBar>
#include <QApplication>
#include <QDockWidget>
#include <QMdiArea>
#include <QTabBar>
#include <QMdiSubWindow>
#include <QClipboard>
#include <QMimeData>
#include <QStatusBar>
#include <QCloseEvent>
#include <QGLWidget>

QAction * create_tool_icon(const QString & name, const QString & v,
                           QActionGroup * group, int id)
{
    QAction * action = new QAction(name, group);
    QPixmap img(v);
    img = img.scaled(img.size() * 2);
    action->setIcon(QIcon(img));
    action->setCheckable(true);
    if (id == BLOCK_EDIT_TOOL)
        action->setChecked(true);
    action->setData(id);
    return action;
}

MainWindow::MainWindow(QWidget * parent)
: QMainWindow(parent)
{
    gl_format = QGLFormat::defaultFormat();
    gl_format.setSampleBuffers(true);
    gl_format.setSamples(4);

    shared_gl = new QGLWidget(gl_format, this);

    mdi = new QMdiArea(this);
    mdi->setViewMode(QMdiArea::TabbedView);
    mdi->setTabsClosable(true);
    mdi->setTabsMovable(true);
    setCentralWidget(mdi);
    connect(mdi, SIGNAL(subWindowActivated(QMdiSubWindow*)), 
        SLOT(on_window_change(QMdiSubWindow*)));

    create_actions();
    create_menus();

    QToolBar * tool = new QToolBar("Tools", this);
    tool_group = new QActionGroup(this);
    tool->addAction(create_tool_icon("Pointer", "editor/pointer_tool.png",
        tool_group, POINTER_EDIT_TOOL));
    tool->addAction(create_tool_icon("Block", "editor/block_tool.png",
        tool_group, BLOCK_EDIT_TOOL));
    tool->addAction(create_tool_icon("Pencil", "editor/pencil_tool.png",
        tool_group, PENCIL_EDIT_TOOL));
    addToolBar(Qt::LeftToolBarArea, tool);

    model_dock = new QDockWidget("Model");
    model_properties = new ModelProperties(this);
    model_dock->setWidget(model_properties);
    addDockWidget(Qt::RightDockWidgetArea, model_dock);

    palette_dock = new QDockWidget("Palette");
    palette_editor = new PaletteEditor(this);
    palette_dock->setWidget(palette_editor);
    addDockWidget(Qt::RightDockWidgetArea, palette_dock);

    map_dock = new QDockWidget("Map");
    map_properties = new MapProperties(this);
    map_dock->setWidget(map_properties);
    addDockWidget(Qt::RightDockWidgetArea, map_dock);

    prop_dock = new QDockWidget("Prop");
    prop_editor = new PropEditor(this);
    prop_dock->setWidget(prop_editor);
    addDockWidget(Qt::RightDockWidgetArea, prop_dock);

    on_window_change(0);

    set_status("Ready...");
}

void MainWindow::create_menus()
{
    file_menu = menuBar()->addMenu(tr("&File"));
    file_menu->addAction(new_model_action);
    file_menu->addAction(open_model_action);
    file_menu->addSeparator();
    file_menu->addAction(new_map_action);
    file_menu->addAction(open_map_action);
    file_menu->addSeparator();
    file_menu->addAction(save_action);
    file_menu->addAction(save_as_action);
    file_menu->addSeparator();
    file_menu->addAction(exit_action);

    model_menu = menuBar()->addMenu(tr("&Model"));
    model_menu->addAction(double_size_action);
    model_menu->addAction(half_size_action);
    model_menu->addAction(optimize_action);
    model_menu->addAction(rotate_action);

    palette_menu = menuBar()->addMenu(tr("&Palette"));
    palette_menu->addAction(copy_palette_action);
    palette_menu->addAction(paste_palette_action);
    palette_menu->addAction(reset_palette_action);

    map_menu = menuBar()->addMenu(tr("&Map"));
    map_menu->addAction(new_object_action);
}

bool MainWindow::test_current_window(QWidget * other)
{
    return get_current_window() == other;
}

QWidget * MainWindow::get_current_window()
{
    QMdiSubWindow * w = mdi->currentSubWindow();
    if (!w)
        return NULL;
    return w->widget();
}

VoxelEditor * MainWindow::get_voxel_editor()
{
    return qobject_cast<VoxelEditor*>(get_current_window());
}

MapEditor * MainWindow::get_map_editor()
{
    return qobject_cast<MapEditor*>(get_current_window());
}

VoxelFile * MainWindow::get_voxel()
{
    VoxelEditor * ed = get_voxel_editor();
    if (ed)
        return ed->voxel;
    return NULL;
}

Map * MainWindow::get_map()
{
    MapEditor * ed = get_map_editor();
    if (ed == NULL)
        return NULL;
    return ed->map;
}

int MainWindow::get_palette_index()
{
    return palette_editor->grid->palette_index;
}

void MainWindow::set_palette_index(int i)
{
    palette_editor->grid->palette_index = i;
    palette_editor->set_current();
}

int MainWindow::get_tool()
{
    return tool_group->checkedAction()->data().toInt();
}

void MainWindow::model_changed()
{
    get_voxel_editor()->on_changed();
}

void MainWindow::voxel_cache_changed(const std::string & name)
{
    QList<QMdiSubWindow *> list = mdi->subWindowList();
    QList<QMdiSubWindow *>::iterator it;
    for (it = list.begin(); it != list.end(); it++) {
        MapEditor * ed = qobject_cast<MapEditor*>((*it)->widget());
        if (ed == NULL)
            continue;
        ed->voxel_cache_changed(name);
    }
}

void MainWindow::create_actions()
{
    // file menu

    new_model_action = new QAction(tr("New voxel model"), this);
    connect(new_model_action, SIGNAL(triggered()), this, SLOT(new_model()));

    new_map_action = new QAction(tr("New map"), this);
    connect(new_map_action, SIGNAL(triggered()), this, SLOT(new_map()));
 
    open_model_action = new QAction(tr("Open voxel model"), this);
    connect(open_model_action, SIGNAL(triggered()), this, SLOT(open_model()));

    open_map_action = new QAction(tr("Open map"), this);
    connect(open_map_action, SIGNAL(triggered()), this, SLOT(open_map()));
 
    save_action = new QAction(tr("&Save"), this);
    save_action->setShortcuts(QKeySequence::Save);
    connect(save_action, SIGNAL(triggered()), this, SLOT(save()));
 
    save_as_action = new QAction(tr("Save &As..."), this);
    save_as_action->setShortcuts(QKeySequence::SaveAs);
    connect(save_as_action, SIGNAL(triggered()), this, SLOT(save_as()));
 
    exit_action = new QAction(tr("E&xit"), this);
    exit_action->setShortcuts(QKeySequence::Quit);
    exit_action->setStatusTip(tr("Exit the application"));
    connect(exit_action, SIGNAL(triggered()), qApp, SLOT(closeAllWindows()));
 
/*        cut_action = new QAction(tr("Cu&t"), this);
    cut_action->setShortcuts(QKeySequence::Cut);
    cut_action->setStatusTip(tr("Cut the current selection's contents to the "
                            "clipboard"));
    connect(cut_action, SIGNAL(triggered()), this, SLOT(cut()));
 
    copy_action = new QAction(tr("&Copy"), this);
    copy_action->setShortcuts(QKeySequence::Copy);
    copy_action->setStatusTip(tr("Copy the current selection's contents to the "
                             "clipboard"));
    connect(copy_action, SIGNAL(triggered()), this, SLOT(copy()));
 
    paste_action = new QAction(tr("&Paste"), this);
    paste_action->setShortcuts(QKeySequence::Paste);
    paste_action->setStatusTip(tr("Paste the clipboard's contents into the current "
                              "selection"));
    connect(paste_action, SIGNAL(triggered()), this, SLOT(paste()));*/

    // palette menu

    copy_palette_action = new QAction(tr("Copy palette"), this);
    // new_action->setShortcuts(QKeySequence::New);
    connect(copy_palette_action, SIGNAL(triggered()), this, 
        SLOT(copy_palette()));
 
    paste_palette_action = new QAction(tr("Paste palette"), this);
    // open_action->setShortcuts(QKeySequence::Open);
    connect(paste_palette_action, SIGNAL(triggered()), this, 
        SLOT(paste_palette()));
 
    reset_palette_action = new QAction(tr("Reset palette"), this);
    // save_action->setShortcuts(QKeySequence::Save);
    connect(reset_palette_action, SIGNAL(triggered()), this,
        SLOT(reset_palette()));

    // model menu

    double_size_action = new QAction(tr("Double size"), this);
    // new_action->setShortcuts(QKeySequence::New);
    connect(double_size_action, SIGNAL(triggered()), this, 
        SLOT(double_size()));
 
    half_size_action = new QAction(tr("Half size"), this);
    // open_action->setShortcuts(QKeySequence::Open);
    connect(half_size_action, SIGNAL(triggered()), this, 
        SLOT(half_size()));
 
    optimize_action = new QAction(tr("Optimize dimensions"), this);
    // save_action->setShortcuts(QKeySequence::Save);
    connect(optimize_action, SIGNAL(triggered()), this,
        SLOT(optimize()));

    rotate_action = new QAction(tr("Rotate 90 degrees"), this);
    // save_action->setShortcuts(QKeySequence::Save);
    connect(rotate_action, SIGNAL(triggered()), this,
        SLOT(rotate()));

    // map menu

    new_object_action = new QAction(tr("New object"), this);
    new_object_action->setShortcut(QKeySequence(Qt::Key_N));
    connect(new_object_action, SIGNAL(triggered()), this, 
        SLOT(new_object()));

}

void MainWindow::closeEvent(QCloseEvent * event)
{

    while (true) {
        QMdiSubWindow * w = mdi->activeSubWindow();
        if (w == NULL)
            break;
        if (w->close())
            continue;
        event->ignore();
        return;
    }
    event->accept();
}

MainWindow::~MainWindow()
{

}

void MainWindow::set_status(const std::string & text)
{
    statusBar()->showMessage(text.c_str());
}

void MainWindow::on_window_change(QMdiSubWindow * w)
{
    VoxelEditor * v = get_voxel_editor();
    MapEditor * m = get_map_editor();
    bool model_visible = v != 0;
    bool map_visible = m != 0;

    model_dock->setVisible(model_visible);
    palette_dock->setVisible(model_visible);
    palette_menu->setEnabled(model_visible);
    model_menu->setEnabled(model_visible);

    prop_dock->setVisible(map_visible);
    map_menu->setEnabled(map_visible);
    map_dock->setVisible(map_visible);

    if (model_visible) {
        model_properties->update_controls();
        palette_editor->set_current();
    }

    if (map_visible) {
        map_properties->update_controls();
    }
}

void MainWindow::new_map()
{
    MapEditor * ed = new MapEditor(this);
    QMdiSubWindow * w = mdi->addSubWindow(ed);
    ed->reset();
    w->showMaximized();
}

void MainWindow::open_map()
{
    std::string name = get_map_name(this);
    if (name.empty())
        return;
    MapEditor * ed = new MapEditor(this);
    QMdiSubWindow * w = mdi->addSubWindow(ed);
    ed->load(name);
    w->showMaximized();
}

void MainWindow::new_model()
{
    VoxelEditor * ed = new VoxelEditor(this);
    QMdiSubWindow * w = mdi->addSubWindow(ed);
    ed->reset();
    w->showMaximized();
}

void MainWindow::open_model()
{
    std::string name = get_model_name(this);
    if (name.empty())
        return;
    VoxelEditor * ed = new VoxelEditor(this);
    QMdiSubWindow * w = mdi->addSubWindow(ed);
    ed->load(name);
    w->showMaximized();
}

void MainWindow::save()
{
    QWidget * w = get_current_window();
    if (!w)
        return;
    QMetaObject::invokeMethod(w, "save");
}

void MainWindow::save_as()
{
    QWidget * w = get_current_window();
    if (!w)
        return;
    QMetaObject::invokeMethod(w, "save_as");
}

void MainWindow::copy_palette()
{
    VoxelFile * voxel = get_voxel();
    QImage img((unsigned char*)voxel->palette, 16, 16, QImage::Format_RGB888);
    QApplication::clipboard()->setImage(img.copy());
}

void MainWindow::paste_palette()
{
    const QClipboard * clipboard = QApplication::clipboard();
    const QMimeData * data = clipboard->mimeData();
    if (!data->hasImage())
        return;
    QImage img = qvariant_cast<QImage>(data->imageData());
    if (img.width() != 16 || img.height() != 16)
        return;
    VoxelFile * voxel = get_voxel();
    for (int y = 0; y < 16; y++)
    for (int x = 0; x < 16; x++) {
        const QRgb c = img.pixel(x, y);
        voxel->palette[x + y * 16] = RGBColor(qRed(c), qGreen(c), qBlue(c));
    }
    palette_editor->update();
    model_changed();
}

void MainWindow::reset_palette()
{
    palette_editor->reset_palette();
    model_changed();
}

void MainWindow::double_size()
{
    VoxelFile * voxel = get_voxel();
    voxel->scale(2.0f, 2.0f, 2.0f);
    model_properties->update_controls();
    model_changed();
}

void MainWindow::half_size()
{
    VoxelFile * voxel = get_voxel();
    voxel->scale(0.5f, 0.5f, 0.5f);
    model_properties->update_controls();
    model_changed();
}

void MainWindow::optimize()
{
    VoxelFile * voxel = get_voxel();
    voxel->optimize();
    model_properties->update_controls();
    model_changed();
}

void MainWindow::rotate()
{
    VoxelFile * voxel = get_voxel();
    voxel->rotate();
    model_properties->update_controls();
    model_changed();
}

void MainWindow::new_object()
{
    MapEditor * ed = get_map_editor();
    if (ed == NULL)
        return;
    ed->new_object();
}