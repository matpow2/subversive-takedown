#include "glm.h"
#include "editorcommon.h"

#include <QGLWidget>

class VoxelFile;
class VoxelModel;
class MainWindow;
class btCollisionObject;
class QPaintEvent;
class QRubberBand;

class SelectedVoxel
{
public:
    int x, y, z;
    unsigned char v;

    SelectedVoxel(int x, int y, int z, unsigned char v)
    : x(x), y(y), z(z), v(v)
    {
    }
};

typedef std::vector<SelectedVoxel> SelectedVoxels;

class VoxelEditor : public QGLWidget
{
    Q_OBJECT

public:
    std::string model_name;

    MainWindow * window;

    VoxelFile * voxel;
    VoxelModel * model;

    mat4 projection_matrix, view_matrix, mvp, inverse_mvp;
    vec4 viewport;

    std::string current_model;

    vec3 pos;
    float scale;
    float rotate_x, rotate_z;

    bool has_hit, hit_floor;
    ivec3 hit_next, hit_block;

    QRubberBand * rubberband;
    QPoint start_drag;
    SelectedVoxels selected_list;
    SelectedVoxels copied_list;
    PositionArrows pos_arrows;

    QPoint last_pos;

    VoxelEditor(MainWindow * parent);
    void load(const std::string & name);
    void reset();
    void on_changed();
    ~VoxelEditor();

protected:
    void initializeGL();
    void paintGL();
    void paintEvent(QPaintEvent * e);
    void resizeGL(int w, int h);
    void keyPressEvent(QKeyEvent *e);
    btCollisionObject * get_collision_object();
    ivec3 get_pos_vec(const vec3 & v);
    void update_hit();
    void update_drag();
    void offset_selected(int dx, int dy, int dz);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void pick_color();
    void use_tool_primary(bool click);
    void use_tool_secondary(bool click);
    void wheelEvent(QWheelEvent * e);
    void closeEvent(QCloseEvent *event);
    void deselect();
    void copy_selected();
    void delete_selected();
    void paste();

public slots:
    void save();
    void save_as();
};