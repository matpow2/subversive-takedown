#ifndef ST_SCENES_COMMONEDITOR_H
#define ST_SCENES_COMMONEDITOR_H

#include "glm.h"
#include "scene.h"

#define EDITOR_WIDTH 1024
#define EDITOR_HEIGHT 768

#define CAMERA_ROTATION_SPEED 0.25f
#define CAMERA_MOVE_SPEED 1.0f

#define CONTROL_PAD 10
#define NAVIGATION_PAD CONTROL_PAD

#define NAVIGATION_WIDTH 80
#define NAVIGATION_HEIGHT 20
#define NAVIGATION_Y1 (EDITOR_HEIGHT - NAVIGATION_HEIGHT - NAVIGATION_PAD)

#define BACK_X1 NAVIGATION_PAD
#define BACK_Y1 NAVIGATION_Y1
#define BACK_WIDTH NAVIGATION_WIDTH
#define BACK_HEIGHT NAVIGATION_HEIGHT

#define MAP_VOXEL_X1 (BACK_X1 + BACK_WIDTH + NAVIGATION_PAD)
#define MAP_VOXEL_Y1 NAVIGATION_Y1
#define MAP_VOXEL_WIDTH NAVIGATION_WIDTH
#define MAP_VOXEL_HEIGHT NAVIGATION_HEIGHT

#define RIGHT_PANEL_WIDTH 170
#define RIGHT_PANEL_HEIGHT (EDITOR_HEIGHT - CONTROL_PAD * 2)
#define RIGHT_PANEL_X1 (EDITOR_WIDTH - RIGHT_PANEL_WIDTH - CONTROL_PAD)
#define RIGHT_PANEL_Y1 (EDITOR_HEIGHT - RIGHT_PANEL_HEIGHT - CONTROL_PAD)
#define RIGHT_PANEL_X2 (RIGHT_PANEL_X1 + RIGHT_PANEL_WIDTH)
#define RIGHT_PANEL_Y2 (RIGHT_PANEL_Y1 + RIGHT_PANEL_HEIGHT)
#define RIGHT_PANEL_MARGIN 5
#define RIGHT_PANEL_CONTROL_X1 (RIGHT_PANEL_X1 + RIGHT_PANEL_MARGIN)
#define RIGHT_PANEL_CONTROL_WIDTH (RIGHT_PANEL_WIDTH - RIGHT_PANEL_MARGIN * 2)
#define OUTLINE_SIZE 1

#define GENERIC_CONTROL_HEIGHT 20

#define GREY_R 75
#define GREY_G 75
#define GREY_B 75

#define RED_R 248
#define RED_G 0
#define RED_B 53

#define BLUE_R 30
#define BLUE_G 117
#define BLUE_B 255

#define GREEN_R 154
#define GREEN_G 200
#define GREEN_B 2

#define STATUS_X 20
#define STATUS_Y 20

class CommonEditor : public Scene
{
public:
    mat4 projection_matrix, view_matrix, mvp, inverse_mvp;
    vec4 viewport;
    int old_x, old_y;
    float rotate_x, rotate_z;
    vec3 pos;
    float scale;
    bool orient;
    bool left_mouse, right_mouse;
    std::string status;

    CommonEditor(GameManager * manager);
    void on_end();
    void on_start();
    void update(float dt);
    void on_mouse_move(int x, int y);
    void on_key(int button, bool value);
    void on_mouse_key(int button, bool value);
    void on_mouse_scroll(double dx, double dy);
};

#endif // ST_SCENES_COMMONEDITOR_H
