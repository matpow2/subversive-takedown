#include "commoneditor.h"

#include <iostream>

#include "constants.h"

CommonEditor::CommonEditor(GameManager * manager)
: Scene(manager), rotate_x(0), rotate_z(0), orient(false),
  left_mouse(false), scale(0), right_mouse(false), pos(0.0f)
{
}

void CommonEditor::on_start()
{
    manager->set_normal_mouse();
    manager->get_mouse_pos(old_x, old_y);
    manager->set_display_size(-1, -1);
    manager->resize(EDITOR_WIDTH, EDITOR_HEIGHT);
    status = "Ready.";
}

void CommonEditor::on_end()
{
    manager->set_display_size(WINDOW_WIDTH, WINDOW_HEIGHT);
}

void CommonEditor::on_key(int key, bool value)
{
    if (key == GLFW_KEY_LEFT_ALT || key == GLFW_KEY_RIGHT_ALT)
        orient = value;
}

void CommonEditor::on_mouse_key(int key, bool value)
{
    if (key == GLFW_MOUSE_BUTTON_LEFT)
        left_mouse = value;
    else if (key == GLFW_MOUSE_BUTTON_RIGHT)
        right_mouse = value;
}

void CommonEditor::on_mouse_move(int x, int y)
{
    float dx, dy;
    dx = float(x - old_x);
    dy = float(y - old_y);
    old_x = x;
    old_y = y;
    if (orient && left_mouse) {
        dx *= CAMERA_ROTATION_SPEED;
        dy *= CAMERA_ROTATION_SPEED;
        rotate_x = std::min<float>(0.0f, std::max<float>(-180.0f,
            rotate_x - float(dy)));
        rotate_z += float(dx);
    } else if (orient && right_mouse) {
        dx *= CAMERA_MOVE_SPEED;
        dy *= CAMERA_MOVE_SPEED;
        pos.x += float(dx);
        pos.y += float(dy);
    }
}

void CommonEditor::on_mouse_scroll(double dx, double dy)
{
    scale = std::max<float>(0.1f, scale + float(dy) * 0.25f);
}

void CommonEditor::update(float dt)
{

}
