#include "glm.h"
#include "filesystem.h"
#include "mainwindow.h"

#include <QApplication>
#include <QMainWindow>
#include <QMdiArea>
#include <QMdiSubWindow>
#include <QActionGroup>

int main(int argc, char *argv[])
{
    file_init(argv[0]);
    file_mount("data", "data", 1);
    file_set_write_dir(".");

    QCoreApplication::setApplicationName("stedit");
    QGuiApplication::setApplicationDisplayName("stedit");
    QApplication::setStyle("fusion");
    QApplication app(argc, argv);
    MainWindow window;
    window.show();
    return app.exec();
}