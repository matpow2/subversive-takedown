#include "mapproperties.h"
#include "mapeditor.h"
#include "mainwindow.h"
#include "editorcommon.h"
#include "map.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QLineEdit>
#include <QPushButton>
#include <QCheckBox>

// MapProperties

MapProperties::MapProperties(MainWindow * parent)
: QWidget(parent), window(parent), ignore_changes(true)
{
    QVBoxLayout * layout = new QVBoxLayout(this);

    QHBoxLayout * name_layout = new QHBoxLayout;
    name_layout->addWidget(create_label("Name"));
    name = new QLineEdit(this);
    connect(name, SIGNAL(textChanged(QString)), SLOT(on_change()));
    name_layout->addWidget(name);
    layout->addLayout(name_layout);

    QHBoxLayout * music_layout = new QHBoxLayout;
    music_layout->addWidget(create_label("Music"));
    music = new QLineEdit(this);
    connect(music, SIGNAL(textChanged(QString)), SLOT(on_change()));
    music_layout->addWidget(music);
    layout->addLayout(music_layout);

    QHBoxLayout * skybox_layout = new QHBoxLayout;
    skybox_layout->addWidget(create_label("Skybox"));
    skybox = new QLineEdit(this);
    connect(skybox, SIGNAL(textChanged(QString)), SLOT(on_change()));
    skybox_layout->addWidget(skybox);
    layout->addLayout(skybox_layout);

    QHBoxLayout * objective1_layout = new QHBoxLayout;
    objective1_layout->addWidget(create_label("Objective 1"));
    objective1 = new QLineEdit(this);
    connect(objective1, SIGNAL(textChanged(QString)), SLOT(on_change()));
    objective1_layout->addWidget(objective1);
    layout->addLayout(objective1_layout);

    QHBoxLayout * objective2_layout = new QHBoxLayout;
    objective2_layout->addWidget(create_label("Objective 2"));
    objective2 = new QLineEdit(this);
    connect(objective2, SIGNAL(textChanged(QString)), SLOT(on_change()));
    objective2_layout->addWidget(objective2);
    layout->addLayout(objective2_layout);

    QHBoxLayout * timer_layout = new QHBoxLayout;
    timer_layout->addWidget(create_label("Time"));
    timer = new QSpinBox(this);
    connect(timer, SIGNAL(valueChanged(int)), SLOT(on_change()));
    timer_layout->addWidget(timer);
    layout->addLayout(timer_layout);

    QHBoxLayout * singleplayer_layout = new QHBoxLayout;
    singleplayer_layout->addWidget(create_label("Singlepl."));
    singleplayer = new QCheckBox(this);
    connect(singleplayer, SIGNAL(stateChanged(int)), SLOT(on_change()));
    singleplayer_layout->addWidget(singleplayer);
    layout->addLayout(singleplayer_layout);

    setLayout(layout);
    ignore_changes = false;
}

void MapProperties::update_controls()
{
    Map * map = window->get_map();
    if (map == NULL)
        return;

    ignore_changes = true;

    name->setText(map->name.c_str());
    music->setText(map->music.c_str());
    skybox->setText(map->skybox.c_str());
    objective1->setText(map->objective1.c_str());
    objective2->setText(map->objective2.c_str());
    timer->setValue(int(map->timer / 60.0f));
    singleplayer->setCheckState(map->singleplayer ? Qt::Checked : Qt::Unchecked);
    ignore_changes = false;
}

void MapProperties::on_change()
{
    if (ignore_changes)
        return;
    Map * map = window->get_map();
    if (map == NULL)
        return;

    map->name = convert_str(name->text());
    map->music = convert_str(music->text());
    map->skybox = convert_str(skybox->text());
    map->objective1 = convert_str(objective1->text());
    map->objective2 = convert_str(objective2->text());
    map->timer = float(timer->value()) * 60.0f;
    map->singleplayer = singleplayer->checkState() == Qt::Checked;
    window->get_map_editor()->on_changed();
}