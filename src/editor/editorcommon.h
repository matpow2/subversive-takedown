#ifndef ST_EDITORCOMMON_H
#define ST_EDITORCOMMON_H

#include <QString>
#include <string>
#include "glm.h"

class QLabel;
class QWidget;
class btCompoundShape;

std::string convert_str(const QString & str);
QLabel * create_label(const QString & text);
std::string get_model_name(QWidget * parent);
std::string get_map_name(QWidget * parent);
void set_window_file_path(QWidget * w, const QString & name);

#define NONE_CONE -1
#define X_CONE 0
#define Y_CONE 1
#define Z_CONE 2

class PositionArrows
{
public:
    float scale;

    int pan;
    vec3 normal, pos, last, add;

    btCompoundShape * shape;

    PositionArrows(float scale);
    ~PositionArrows();
    void set_scale(float scale);
    void set_pos(const vec3 & p);
    void on_mouse_press(const vec3 & ray_pos, const vec3 & ray_dir);
    void on_mouse_move(const vec3 & ray_pos, const vec3 & ray_dir);
    void on_mouse_release();
    void update(const vec3 & ray_pos, const vec3 & ray_dir);
    vec3 get(float grid);
    int ray_test(const vec3 & pos, const vec3 & dir);
    void draw();
};

#define RED_R 248
#define RED_G 0
#define RED_B 53

#define BLUE_R 30
#define BLUE_G 117
#define BLUE_B 255

#define GREEN_R 154
#define GREEN_G 200
#define GREEN_B 2

#endif // ST_EDITORCOMMON_H