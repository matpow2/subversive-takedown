#include "mapeditor.h"
#include "mainwindow.h"
#include "draw.h"
#include "world.h"
#include "propeditor.h"
#include <btBulletDynamicsCommon.h>

#include <QKeyEvent>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QDockWidget>
#include <QComboBox>
#include <QInputDialog>
#include <QMessageBox>
#include <QTimer>

#define CAMERA_ROTATION_SPEED 0.25f
#define CAMERA_MOVE_SPEED 1.0f

#define ZNEAR 5.0f
#define ZFAR 8000.0f

#define COORD_LINE_SIZE 200.0f
#define OBJECT_SIZE 32

using World::WorldObject;
using World::ObjectList;

class ModelObject : public World::ModelObject
{
public:
    MapObject * map_obj;

    ModelObject(MapEditor * ed, MapObject * obj)
    : World::ModelObject(ed->world, obj->get_voxel(), obj->pos, obj->scale), 
      map_obj(obj)
    {

    }
};

class AbstractObject : public WorldObject
{
public:
    MapObject * map_obj;
    bool drawn;

    AbstractObject(MapEditor * ed, MapObject * obj)
    : WorldObject(ed->world, World::ABSTRACT), map_obj(obj)
    {
        const static float half = OBJECT_SIZE / 2.0f;
        static World::BoxShape shape(
            vec3(-half, -half, 0), vec3(half, half, OBJECT_SIZE));
        initialize(&shape, obj->pos, 0.0f);
    }

    void draw()
    {
        vec3 min, max;
        get_aabb(min, max);
        draw_cube(min.x, min.y, min.z, max.x, max.y, max.z,
                  20, 20, 147, 255);
        drawn = true;
    }
};

vec3 get_list_center(const ObjectList & list)
{
    ObjectList::const_iterator it;
    vec3 min, max;
    bool set = false;
    for (it = list.begin(); it != list.end(); it++) {
        vec3 a, b;
        (*it)->get_aabb(a, b);
        if (!set) {
            set = true;
            min = a;
            max = b;
        } else {
            min = glm::min(a, min);
            max = glm::max(b, max);
        }
    }
    return (min + max) * 0.5f;
}

MapObject * get_map_obj(WorldObject * obj)
{
    if (obj == NULL)
        return NULL;
    MapObject * map_obj;
    if (obj->type == World::ABSTRACT)
        map_obj = ((AbstractObject*)obj)->map_obj;
    else
        map_obj = ((ModelObject*)obj)->map_obj;
    return map_obj;
}

MapEditor::MapEditor(MainWindow * parent)
: QGLWidget(parent->gl_format, parent, parent->shared_gl), scale(0.7f), 
  rotate_x(-52.0f), rotate_z(-150.0f), window(parent), up(false), down(false), 
  forward(false), backward(false), strafe_left(false), strafe_right(false),
  pos_arrows(1.0f)
{
    setFocusPolicy(Qt::StrongFocus);
    setMouseTracking(true);
    world = new World::World;
    map = &world->map;
    QTimer * timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), SLOT(update_camera()));
    timer->start(5);
}

WorldObject * MapEditor::add_object(MapObject * obj)
{
    setWindowModified(true);
    if (obj->get_voxel()) {
        return new ModelObject(this, obj);
    } else
        return new AbstractObject(this, obj);
}

void MapEditor::new_object()
{
    bool ok;
    QStringList object_types;
    object_types.append("prop");
    object_types.append("spawn");
    object_types.append("intel");
    object_types.append("train");
    object_types.append("node");
    object_types.append("crate");
    QString t = QInputDialog::getItem(this, 
        QObject::tr("New object dialog"), QObject::tr("Object type:"), 
        object_types, 0, false, &ok);
    if (!ok)
        return;
    std::string type = convert_str(t);
    MapObject * obj;
    if (type == "prop") {
        std::string name = get_model_name(this);
        if (name.empty())
            return;
        obj = map->add_prop(name, -pos, 1.0f);
    } else
        obj = map->add_object(type, -pos, 1.0f);
    add_object(obj);

}

void MapEditor::load(const std::string & name)
{
    map_name = name;
    window->set_status("Loaded map " + name);
    set_window_file_path(this, name.c_str());
    world->set_map(name, false);

    // add nodes and other entities, etc.
    MapObjects::const_iterator it;
    for (it = map->objects.begin(); it != map->objects.end(); it++)
        add_object(*it);
    for (it = map->props.begin(); it != map->props.end(); it++)
        add_object(*it);
}

void MapEditor::reset()
{
    map->reset();
    setWindowModified(true);
}

void MapEditor::on_changed()
{
    update();
    setWindowModified(true);
}

MapEditor::~MapEditor()
{
    delete world;
}

void MapEditor::move_camera(const vec3 & move)
{
    pos += glm::rotateZ(move, -rotate_z) * (1.0f / scale);
}

void MapEditor::update_camera()
{
    vec3 move(0.0f);

    if (forward)
        move.y += 1.0f;
    if (backward)
        move.y -= 1.0f;
    if (strafe_left)
        move.x -= 1.0f;
    if (strafe_right)
        move.x += 1.0f;
    if (up)
        move.z -= 1.0f;
    if (down)
        move.z += 1.0f;

    move *= (0.005f) * 200.0f;
    move_camera(move);

    if (glm::length(move) > 0.0f)
        update();
}

void MapEditor::initializeGL()
{
    setup_opengl();

    glEnable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);
    glEnable(GL_CULL_FACE);
    glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

    pos = vec3(0.0f, 0.0f, 0.0f);

}

void MapEditor::paintGL()
{
    glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glEnable(GL_DEPTH_TEST);

    vec3 rotation(0.0f, 0.0f, 1.0f);
    rotation = glm::rotateX(rotation, rotate_x);
    rotation = glm::rotateZ(rotation, -rotate_z);
    vec3 center = -pos;
    vec3 eye = rotation * (500.0f / scale) + center;
    view_matrix = glm::lookAt(eye, center, vec3(0.0f, 0.0f, 1.0f));
    mvp = projection_matrix * view_matrix;
    inverse_mvp = glm::inverse(mvp);

    multiply_matrix(view_matrix);

    glEnable(GL_LIGHTING);

    setup_lighting();

    World::ObjectList::const_iterator it;
    for (it = world->objects.begin(); it != world->objects.end(); it++) {
        WorldObject * obj = *it;
        if (obj->type == World::ABSTRACT)
            ((AbstractObject*)obj)->drawn = false;
    }

    world->draw(mvp);

    float size = 5.0f;
    draw_cube(-pos.x, -pos.y, -pos.z, size, 200, 20, 20, 255);

    glDisable(GL_LIGHTING);

    if (has_selected()) {
        ObjectList::const_iterator it;
        for (it = selected_list.begin(); it != selected_list.end(); it++) {
            vec3 min, max;
            (*it)->get_aabb(min, max);
            draw_wireframe_cube(min.x, min.y, min.z, max.x, max.y, max.z,
                255, 255, 255, 255);
        }

        glClear(GL_DEPTH_BUFFER_BIT);
        pos_arrows.draw();
    }

    glDisable(GL_DEPTH_TEST);

    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

    for (it = world->objects.begin(); it != world->objects.end(); it++) {
        WorldObject * obj = *it;
        if (obj->type != World::ABSTRACT)
            continue;
        AbstractObject * obj2 = (AbstractObject*)obj;
        if (!obj2->drawn)
            continue;
        MapObject * map_obj = obj2->map_obj;
        vec3 min, max;
        obj2->get_aabb(min, max);
        vec3 mid = (min + max) * 0.5f;
        vec2 proj;
        project(mid, mvp, viewport, proj);
        proj.x -= map_obj->type.size() * 0.5f * 5.0f;
        renderText(proj.x, height() - proj.y, map_obj->type.c_str());
    }
}

void MapEditor::resizeGL(int w, int h)
{
    viewport = vec4(0, 0, w, h);
    projection_matrix = glm::perspective(PERSPECTIVE_FOV, float(w) / float(h),
        ZNEAR, ZFAR);
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    load_matrix(projection_matrix);
    glMatrixMode(GL_MODELVIEW);
}

void MapEditor::keyPressEvent(QKeyEvent *e)
{
    int key = e->key();
    switch (key) {
        case Qt::Key_W:
            forward = true;
            break;
        case Qt::Key_S:
            backward = true;
            break;
        case Qt::Key_A:
            strafe_left = true;
            break;
        case Qt::Key_D:
            strafe_right = true;
            break;
        case Qt::Key_Space:
            up = true;
            break;
        case Qt::Key_C:
            if (e->modifiers() & Qt::ControlModifier)
                copy_selected();
            else
                down = true;
            break;
        case Qt::Key_V:
            if (e->modifiers() & Qt::ControlModifier)
                paste_objects();
            break;
        case Qt::Key_Delete:
            delete_selected();
            break;
    }
}

void MapEditor::copy_selected()
{
    copied_list = selected_list;
    window->set_status("Copied objects");
}

void MapEditor::paste_objects()
{
    ObjectList::const_iterator it;
    selected_list.clear();
    for (it = copied_list.begin(); it != copied_list.end(); it++)
        selected_list.push_back(add_object(get_map_obj(*it)->clone()));
    window->set_status("Pasted objects");
    update();
}

void MapEditor::delete_selected()
{
    ObjectList::const_iterator it;
    for (it = selected_list.begin(); it != selected_list.end(); it++) {
        delete get_map_obj(*it);
        delete *it;
        copied_list.erase(std::remove(copied_list.begin(), copied_list.end(), 
            *it), copied_list.end());
    }
    selected_list.clear();
    window->set_status("Deleted objects");
    update();
}

void MapEditor::keyReleaseEvent(QKeyEvent *e)
{
    int key = e->key();
    switch (key) {
        case Qt::Key_W:
            forward = false;
            break;
        case Qt::Key_S:
            backward = false;
            break;
        case Qt::Key_A:
            strafe_left = false;
            break;
        case Qt::Key_D:
            strafe_right = false;
            break;
        case Qt::Key_Space:
            up = false;
            break;
        case Qt::Key_C:
            down = false;
            break;
    }
}

void MapEditor::add_selected(WorldObject * obj)
{
    if (obj == NULL)
        selected_list.clear();
    else {
        ObjectList::iterator end = std::remove(
            selected_list.begin(), selected_list.end(), obj);
        if (end != selected_list.end())
            selected_list.erase(end, selected_list.end());
        else
            selected_list.push_back(obj);
        pos_arrows.set_pos(get_selected_center());
    }
    window->prop_editor->set_object(get_map_obj(get_selected()));
    update();
}

WorldObject * MapEditor::get_selected()
{
    if (selected_list.size() != 1)
        return NULL;
    return selected_list[0];
}

bool MapEditor::has_selected()
{
    return selected_list.size() > 0;
}

vec3 MapEditor::get_selected_center()
{
    return get_list_center(selected_list);
}

void MapEditor::mouseReleaseEvent(QMouseEvent * event)
{
    if (event->button() != Qt::LeftButton)
        return;
    pos_arrows.on_mouse_release();
    update();
}

vec3 MapEditor::get_grid_pos(const vec3 & pos)
{
    int grid = window->prop_editor->get_grid_value();
    if (grid == 0)
        return pos;
    float gridf = float(grid);
    return glm::round(pos / gridf) * gridf;
}

void MapEditor::mousePressEvent(QMouseEvent *event)
{
    last_pos = event->pos();
    bool shift = event->modifiers() & Qt::ShiftModifier;
    bool alt = event->modifiers() & Qt::AltModifier;
    if (alt || event->button() != Qt::LeftButton)
        return;
    vec2 mouse(last_pos.x(), height() - last_pos.y());
    vec3 pos, dir;
    get_window_ray(mouse, inverse_mvp, viewport, pos, dir);

    if (has_selected() && !shift) {
        pos_arrows.on_mouse_press(pos, dir);
        update();
        if (pos_arrows.pan != NONE_CONE)
            return;
    }

    WorldObject * obj = world->test_ray_all(pos, pos + dir * 5000.0f);

    if (has_selected() && !shift)
        add_selected(NULL);

    add_selected(obj);
}

void MapEditor::update_object(WorldObject * obj)
{
    MapObject * map_obj = get_map_obj(obj);
    world->physics->removeRigidBody(obj->body);
    if (obj->type != World::ABSTRACT) {
        ModelObject * obj2 = (ModelObject*)obj;
        bool changed = false;
        changed = obj2->scale != map_obj->scale || 
                  obj2->model->file->shapes.size() == 0; 
        obj2->scale = map_obj->scale;
        if (changed) {
            delete obj2->body;
            obj2->initialize(map_obj->get_voxel()->get_shape(map_obj->scale), 
                obj2->get_position(), 0.0f);
        }
    }
    map_obj->pos = get_grid_pos(map_obj->pos);
    obj->set_position(map_obj->pos, false);
    world->physics->addRigidBody(obj->body);
    setWindowModified(true);
}

void MapEditor::update_selected()
{
    ObjectList::const_iterator it;
    for (it = selected_list.begin(); it != selected_list.end(); it++)
        update_object(*it);
    window->prop_editor->set_object(get_map_obj(get_selected()));
    update();
}

void MapEditor::voxel_cache_changed(const std::string & name)
{
    ObjectList::const_iterator it;
    for (it = world->objects.begin(); it != world->objects.end(); it++) {
        MapObject * obj = get_map_obj(*it);
        if (obj->model != name)
            continue;
        update_object(*it);
    }
}

void MapEditor::mouseMoveEvent(QMouseEvent *e)
{
    QPoint dpos = e->pos() - last_pos;
    last_pos = e->pos();

    float dx = dpos.x();
    float dy = dpos.y();

    bool left = e->buttons() & Qt::LeftButton;
    bool right = e->buttons() & Qt::RightButton;
    bool alt = e->modifiers() & Qt::AltModifier;

    if (pos_arrows.pan != NONE_CONE) {
        vec2 mouse(last_pos.x(), height() - last_pos.y());
        vec3 pos, dir;
        get_window_ray(mouse, inverse_mvp, viewport, pos, dir);
        pos_arrows.on_mouse_move(pos, dir);
        vec3 add = pos_arrows.get(window->prop_editor->get_grid_value());
        if (add != vec3(0.0f)) {
            ObjectList::const_iterator it;
            for (it = selected_list.begin(); it != selected_list.end(); it++)
                get_map_obj((*it))->pos += add;
            update_selected();
        }
    } else if (right) {
        dx *= CAMERA_ROTATION_SPEED;
        dy *= CAMERA_ROTATION_SPEED;
        rotate_x = std::min<float>(-10.0f, std::max<float>(-90.0f,
            rotate_x + dy));
        rotate_z += dx;
    } else if (alt && left) {
        dx *= CAMERA_MOVE_SPEED;
        dy *= CAMERA_MOVE_SPEED;
        move_camera(vec3(-dx, dy, 0.0f));
    }
    update();
}

void MapEditor::wheelEvent(QWheelEvent * e)
{
    QPoint pixels = e->pixelDelta();
    QPoint degrees = e->angleDelta();
    float scroll = 0.0f;

    if (!pixels.isNull()) {
        scroll = pixels.y();
    } else if (!degrees.isNull()) {
        QPoint steps = degrees / 8 / 15;
        scroll = steps.y();
    }

    scale = std::max<float>(0.1f, scale + float(scroll) * 0.25f);
    e->accept();
    update();
}

void MapEditor::closeEvent(QCloseEvent *event)
{
    if (!isWindowModified()) {
        event->accept();
        return;
    }
    QMessageBox * msg = new QMessageBox(this);
    msg->setText("Do you want to save your changes?");
    msg->setStandardButtons(
        QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    msg->setDefaultButton(QMessageBox::Save);
    int ret = msg->exec();
    if (ret == QMessageBox::Save) {
        save();
        event->accept();
    } else if (ret == QMessageBox::Discard) {
        event->accept();
    } else
        event->ignore();
}

void MapEditor::save()
{
    if (map_name.empty()) {
        save_as();
        return;
    }
    map->save(get_map_path(map_name));
    setWindowModified(false);
}

void MapEditor::save_as()
{
    map_name = get_map_name(this);
    set_window_file_path(this, map_name.c_str());
    if (map_name.empty())
        // ok, user really doesn't want to save the map
        return;
    save();
}