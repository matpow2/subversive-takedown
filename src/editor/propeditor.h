#include <QWidget>

class MainWindow;
class QSpinBox;
class MapObject;
class QLineEdit;
class QComboBox;

class PropEditor : public QWidget
{
    Q_OBJECT

public:
    bool ignore_changes;
    MainWindow * window;
    MapObject * map_obj;

    QSpinBox * pos_x;
    QSpinBox * pos_y;
    QSpinBox * pos_z;
    QSpinBox * scale;
    QSpinBox * grid;
    QSpinBox * health;
    QLineEdit * id;
    QLineEdit * icon;
    QComboBox * team;

    PropEditor(MainWindow * parent);
    void set_object(MapObject * m);
    int get_grid_value();

public slots:
    void on_change();
};