#include "glm.h"

#include <QMainWindow>
#include <QAction>
#include <QGLFormat>

class VoxelFile;
class VoxelEditor;
class Map;
class MapEditor;
class PaletteEditor;
class QActionGroup;
class ModelProperties;
class QMdiSubWindow;
class QMdiArea;
class PropEditor;
class QDockWidget;
class MapProperties;
class QCloseEvent;
class QGLWidget;

#define POINTER_EDIT_TOOL 0
#define BLOCK_EDIT_TOOL 1
#define PENCIL_EDIT_TOOL 2

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    PaletteEditor * palette_editor;
    ModelProperties * model_properties;
    MapProperties * map_properties;
    PropEditor * prop_editor;
    QActionGroup * tool_group;
    QGLFormat gl_format;
    QGLWidget * shared_gl;

    QMdiArea * mdi;
    QMenu * file_menu;
    QMenu * palette_menu;
    QMenu * model_menu;
    QMenu * map_menu;

    QAction * new_model_action;
    QAction * new_map_action;
    QAction * open_model_action;
    QAction * open_map_action;
    QAction * save_action;
    QAction * save_as_action;
    QAction * exit_action;

    QAction * copy_palette_action;
    QAction * paste_palette_action;
    QAction * reset_palette_action;

    QAction * double_size_action;
    QAction * half_size_action;
    QAction * optimize_action;
    QAction * rotate_action;

    QAction * new_object_action;

    QDockWidget * model_dock;
    QDockWidget * palette_dock;
    QDockWidget * prop_dock;
    QDockWidget * map_dock;

    MainWindow(QWidget * parent = 0);
    void closeEvent(QCloseEvent * event);
    void create_menus();
    void create_actions();
    VoxelFile * get_voxel();
    Map * get_map();
    int get_palette_index();
    void set_palette_index(int i);
    int get_tool();
    QWidget * get_current_window();
    VoxelEditor * get_voxel_editor();
    MapEditor * get_map_editor();
    bool test_current_window(QWidget * other);
    ~MainWindow();

    void model_changed();
    void voxel_cache_changed(const std::string & name);
    void set_status(const std::string & text);

private slots:
    void on_window_change(QMdiSubWindow * w);

    void new_model();
    void new_map();
    void open_model();
    void open_map();
    void save();
    void save_as();

    void copy_palette();
    void paste_palette();
    void reset_palette();

    void double_size();
    void half_size();
    void optimize();
    void rotate();

    void new_object();
};