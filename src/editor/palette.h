#include "color.h"

#include <QWidget>

class MainWindow;
class PaletteGrid;
class QSpinBox;

class ColorSpace : public QWidget
{
    Q_OBJECT

public:
    QPixmap * pix;
    float hue, sat, val;

    ColorSpace(QWidget * parent = 0);
    void set_hue(float h);
    void set_hsv(float h, float s, float v);
    void set_mouse_pos(const QPoint & p);
    void mousePressEvent(QMouseEvent * event);
    void mouseMoveEvent(QMouseEvent* event);
    void paintEvent(QPaintEvent * event);
};

class ColorSlider : public QWidget
{
    Q_OBJECT

public:
    QPixmap * pix;
    float value;

    ColorSlider(QWidget * parent = 0);
    void set(float v);
    void set_mouse_pos(const QPoint & p);
    void mousePressEvent(QMouseEvent * event);
    void mouseMoveEvent(QMouseEvent* event);
    void paintEvent(QPaintEvent * event);
};

class PaletteGrid : public QWidget
{
    Q_OBJECT

public:
    MainWindow * window;
    int palette_index;
    QPoint drag_start;

    PaletteGrid(MainWindow * parent);
    int get_index(const QPoint & p);
    void paintEvent(QPaintEvent * event);
    void mousePressEvent(QMouseEvent * event);
    void mouseMoveEvent(QMouseEvent * event);
    void dragEnterEvent(QDragEnterEvent * event);
    void dropEvent(QDropEvent * event);
};

class PaletteEditor : public QWidget
{
    Q_OBJECT

public:
    MainWindow * window;
    PaletteGrid * grid;
    ColorSpace * color_space;
    ColorSlider * color_slider;
    QSpinBox * r_edit;
    QSpinBox * g_edit;
    QSpinBox * b_edit;
    bool ignore_rgb;

    PaletteEditor(MainWindow * parent);
    int get_palette_index();
    void set_current();
    void set_palette();
    RGBColor & get_palette_color();
    QSpinBox * create_color_spinbox();
    void reset_palette();

public slots:
    void rgb_changed();
};