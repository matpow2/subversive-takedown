#include <QWidget>

class MainWindow;
class QSpinBox;
class QComboBox;
class QPushButton;
class QLineEdit;
class QCheckBox;

class MapProperties : public QWidget
{
    Q_OBJECT

public:
    bool ignore_changes;

    MainWindow * window;

    QLineEdit * name;
    QLineEdit * music;
    QLineEdit * skybox;
    QLineEdit * objective1;
    QLineEdit * objective2;
    QSpinBox * timer;
    QCheckBox * singleplayer;

    MapProperties(MainWindow * parent);
    void update_controls();

public slots:
    void on_change();
};