#ifndef ST_VOXEL_H
#define ST_VOXEL_H

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "color.h"
#include "filesystem.h"
#include "glm.h"
#include "types.h"

#define NORMAL_SIZE 3
#define VOXEL_AIR 255

#ifdef ST_IS_CLIENT

#include "include_gl.h"

class VoxelFile;
class ReferencePoint;

class VoxelModel
{
public:
    VoxelFile * file;
    GLuint value;
    bool changed;

    VoxelModel(VoxelFile * file);
    ~VoxelModel();
    void draw();
    void draw_immediate(float alpha = 1.0f, bool offset = true);
    void update(bool force = true);
    ReferencePoint * get_point(const std::string & name);
};

#endif // ST_IS_CLIENT

typedef std::vector<vec3> Vectors;

namespace World {
class ModelShape;
}

typedef fast_map<float, World::ModelShape*> CachedShapes;

class ReferencePoint
{
public:
    std::string name;
    int x, y, z;

    ReferencePoint(const std::string & name, int x, int y, int z);
    void translate();
};

typedef std::vector<ReferencePoint> ReferencePoints;

class VoxelFile
{
public:
    unsigned char * data;
    RGBColor palette[256];
    int x_size, y_size, z_size;
    int x_offset, y_offset, z_offset;
    vec3 min, max;
    std::string name;
    ReferencePoints points;
    CachedShapes shapes;

    VoxelFile();
    VoxelFile(const std::string & filename);
    VoxelFile(FSFile * fp);
    VoxelFile(int x_size, int y_size, int z_size);

    void load_fp(FSFile * fp);
    bool load(const std::string & filename);
    void save(const std::string & filename);
    void save_fp(FSFile * fp);
    void reset(int x, int y, int z);
    void add_point(const std::string & name, int x, int y, int z);
    void remove_point(size_t i);
    ReferencePoint * get_point(const std::string & name);
    ReferencePoint * get_point(int i);
    void resize(int x1, int y1, int z1, int x_size, int y_size, int z_size);
    void scale(float sx, float sy, float sz);
    void set_offset(int x, int y, int z);
    void update_box();
    void optimize();
    void rotate();
    World::ModelShape * get_shape(float scale);
    void reset_shapes();

    inline unsigned char & get(int x, int y, int z)
    {
        return data[z + y * z_size + x * z_size * y_size];
    }

    inline const RGBColor & get_color(int x, int y, int z)
    {
        return palette[get(x, y, z)];
    }

    inline bool is_solid(int x, int y, int z)
    {
        if (x < 0 || y < 0 || z < 0 ||
            x >= x_size || y >= y_size || z >= z_size)
            return false;
        return get(x, y, z) != VOXEL_AIR;
    }

    inline bool is_solid_fast(int x, int y, int z)
    {
        return get(x, y, z) != VOXEL_AIR;
    }

    inline bool is_surface(int x, int y, int z)
    {
        return !(is_solid(x - 1, y, z) && is_solid(x + 1, y, z) &&
                 is_solid(x, y - 1, z) && is_solid(x, y + 1, z) &&
                 is_solid(x, y, z - 1) && is_solid(x, y, z + 1));
    }

    void set(int x, int y, int z, unsigned char i)
    {
        if (x < 0 || y < 0 || z < 0 ||
            x >= x_size || y >= y_size || z >= z_size)
            return;
        get(x, y, z) = i;
#ifdef ST_IS_CLIENT
        update_model();
#endif
    }

#ifdef ST_IS_CLIENT
    VoxelModel * model;
    VoxelModel * get_model();
    void update_model();
#endif
};

VoxelFile * load_voxel(const std::string & name);
void reload_voxels();
void reload_voxel(const std::string & name);

#ifdef ST_IS_CLIENT

VoxelModel * load_model(const std::string & name);

#endif // ST_IS_CLIENT

#endif // ST_VOXEL_H
