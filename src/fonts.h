#ifndef ST_FONTS_H
#define ST_FONTS_H

#include <string>

#include "font.h"

extern FTTextureFont * fixedsys_font;

void initialize_fonts();

#endif // ST_FONTS_H
