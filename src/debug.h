#ifndef ST_DEBUG_H
#define ST_DEBUG_H

#include <string>

#include "color.h"
#include "glm.h"

class Debugger
{
public:
    Debugger();

    float get_float(const char * name);
    RGBColor get_color(const char * name);
    vec3 get_vec3(const char * name);
};

extern Debugger debug;

#endif // ST_DEBUG_H
