#include "server.h"

#include <enet/enet.h>
#include <iostream>

#include "constants.h"
#include "datastream.h"
#include "packet.h"
#include "random.h"
#include "signalhandler.h"
#include "stringcommon.h"
#include "timer.h"

void set_entity_packet(ChangeEntity & packet, Entity * entity)
{
    packet.id = entity->id;
    Character * character = (Character*)entity->character;
    if (character == NULL)
        packet.unset();
    else
        packet.set(character->peer->id);
    vec3 pos = entity->get_position();
    packet.pos = pos;
    packet.team = entity->team;
    packet.health = entity->get_health();
    if (entity->entity_type == World::MODEL) {
        World::ModelEntity * model_entity = (World::ModelEntity*)entity;
        packet.model = model_entity->voxel->name;
        packet.scale = model_entity->scale;
    } else
        packet.model = "";
}

// ServerWorld

ServerWorld::ServerWorld(ServerHost * host)
: World(), host(host)
{

}

void ServerWorld::on_entity_create(Entity * entity)
{
    entity->id = host->entity_ids.take();
    CreateEntity packet;
    packet.id = entity->id;
    packet.entity_type = entity->entity_type;
    host->broadcast_packet(packet);
    on_entity_change(entity);
}

void ServerWorld::on_entity_destroy(Entity * entity)
{
    host->entity_ids.put_back(entity->id);
    DestroyEntity packet;
    packet.id = entity->id;
    host->broadcast_packet(packet);
    NamedObjects::const_iterator it;
    for (it = host->named_objects.begin(); it != host->named_objects.end();
         it++) {
        if (it->second != entity)
            continue;
        host->named_objects.erase(it);
        break;
    }
}

void ServerWorld::on_entity_change(Entity * entity)
{
    ChangeEntity packet;
    set_entity_packet(packet, entity);
    host->broadcast_packet(packet);
}

// SpawnPoint

SpawnPoint::SpawnPoint(ServerHost * host, const vec3 & pos, char team)
: World::SpawnPoint(host->world, pos, team), host(host)
{
    enable_collision_callback();
}

void SpawnPoint::on_collision(World::WorldObject * obj, btManifoldPoint & pt,
                              bool is_a)
{
    Character * character = (Character*)obj;
    if (character->team != team)
        return;
    #define HEAL_DELAY 10.0f
    if (character->heal_delay <= 0.0f)
        character->heal();
    if (character->hold_entity == NULL ||
        character->hold_entity->entity_type != World::INTEL)
        return;
    if (!character->script->call_handler("on_intel_capture"))
        return;
    ((Intel*)character->hold_entity)->reset();
}

// Intel

Intel::Intel(ServerHost * host, const vec3 & pos, char team)
: World::Intel(host->world, pos, team), start_pos(pos), host(host),
  has_reset(false), reset_time(0.0f)
{
    enable_collision_callback();
}

void Intel::update(float dt)
{
    if (character != NULL)
        return;

    if (reset_time <= 0.0f)
        return;

    reset_time -= dt;
    if (reset_time > 0.0f)
        return;

    set_position(start_pos, false);
    update_state();
    host->script->call_void("on_intel_return");
}

void Intel::reset()
{
    has_reset = true;
    set_character(NULL);
}

void Intel::on_collision(World::WorldObject * obj, btManifoldPoint & pt,
                         bool is_a)
{
    if (this->character != NULL)
        return;
    Character * character = (Character*)obj;
    if (character->dead)
        return;
    if (character->team == team)
        return;
    if (!character->script->call_handler("on_intel_pickup"))
        return;
    set_character(character);
}

void Intel::on_set(World::Character * new_char)
{
    if (new_char == NULL) {
        Character * character = (Character*)this->character;
        if (has_reset) {
            has_reset = false;
            set_position(start_pos, false);
        } else {
            if (character->old_velocity.z <= MAX_VELOCITY) {
                set_position(start_pos, false);
            } else {
                reset_time = 60.0f;
                set_position(character->get_position(), false);
            }
            character->script->call_void("on_intel_lose");
        }
    }
}

// Crate

Crate::Crate(ServerHost * host, const vec3 & pos, CrateData * data)
: World::Crate(host->world, pos), data(data), host(host)
{
    enable_collision_callback();
    host->crate_count++;
    data->spawned = true;
}

void Crate::on_collision(World::WorldObject * obj, btManifoldPoint & pt,
                         bool is_a)
{
    Character * character = (Character*)obj;
    int type = 1; // randint(2);
    if (type == 0)
        character->set_hp(100, false);
    else
        character->add_ammo(RPG_TOOL, 4);
    destroy();
}

void Crate::on_destroy()
{
    data->spawned = false;
    host->crate_count--;
    World::Crate::on_destroy();
}

// BlockObject

BlockObject::BlockObject(ServerHost * host, const vec3 & pos,
                         const RGBColor & color)
: World::BlockObject(host->world, pos, color)
{

}

void BlockObject::apply_damage(float value, World::WorldObject * src)
{
    World::BlockObject::apply_damage(value, src);
    if (!is_destroyed())
        return;
    RemoveBlocks packet;
    packet.add(get_index());
    ((ServerWorld*)world)->host->broadcast_packet(packet);
    destroy();
}

// Character

Character::Character(World::World * world, ServerPeer * peer,
                     const std::string & name, const vec3 & pos, char team)
: World::Character(world, name, pos, team), peer(peer), has_jumped(false),
  heal_delay(0.0f)
{
    script = new ScriptCharacter(this);
}

void Character::set_hp(float value, bool show_hit, Character * by)
{
    value = std::max<float>(0.0f, std::min<float>(255.0f, value));
    if (by != NULL && by != this) {
        HitPacket hit_packet;
        hit_packet.id = peer->id;
        by->peer->send_packet(hit_packet);
    }
    hp = value;
    if (value <= 0.0f) {
        kill(by);
    } else {
        HPPacket new_packet;
        new_packet.show_hit = show_hit;
        new_packet.value = int(value);
        peer->send_packet(new_packet);
    }
}

void Character::set_score(int value)
{
    score = value;
    ScorePacket new_packet;
    new_packet.id = peer->id;
    new_packet.value = value;
    ((ServerHost*)peer->host)->broadcast_packet(new_packet);
}

void Character::update(float dt)
{
    heal_delay -= dt;

    World::Character::update(dt);
    if (dead)
        return;

    old_velocity = get_velocity();

    if (old_velocity.z <= MAX_VELOCITY)
        // we're falling too fast!
        kill();
}

void Character::reset()
{
    World::Character::reset();
    heal_delay = 0.0f;
}

void Character::kill()
{
    kill(NULL);
}

void Character::kill(Character * by)
{
    if (dead)
        return;
    World::Character::kill();
    remove_entity();
    KillPacket new_packet;
    new_packet.id = peer->id;
    if (by == NULL)
        new_packet.by = NONE_CHARACTER;
    else
        new_packet.by = by->peer->id;
    ((ServerHost*)peer->host)->broadcast_packet(new_packet);
    peer->respawn(RESPAWN_TIME);

    if (by == NULL)
        script->call_void("on_death");
    else
        script->call_void("on_death", by->script->object);
}

void Character::play_sound(const std::string & name, bool music)
{
    PlaySample packet;
    packet.name = name;
    packet.music = music;
    peer->send_packet(packet);
}

void Character::update_ammo(Tool * tool)
{
    if (tool->max_ammo == -1)
        return;
    SetAmmo new_packet;
    new_packet.tool = tool->id;
    new_packet.ammo = tool->ammo;
    peer->send_packet(new_packet);
}

void Character::add_ammo(ToolType id, int value)
{
    Tool * tool = get_tool(id);
    tool->set(tool->ammo + value);
    update_ammo(tool);
}

bool Character::use_tool()
{
    if (!World::Character::use_tool())
        return false;
    update_ammo(tool);
    return true;
}

void Character::set_tool(ToolType id)
{
    World::Character::set_tool(id);
    ServerSetTool packet;
    packet.id = peer->id;
    packet.tool = id;
    peer->broadcast = false;
    ((ServerHost*)peer->host)->broadcast_packet(packet);
    peer->broadcast = true;
}

void Character::heal()
{
    heal_delay = HEAL_DELAY;
    for (int i = 0; i < TOOL_MAX; i++) {
        Tool * tool = get_tool(ToolType(i));
        tool->reset();
        update_ammo(tool);
    }
    set_hp(100, false);
    play_sound("heal", false);
}

void Character::set_team(char team)
{
    this->team = team;
    if (!dead)
        kill();
    ServerSetTeam packet;
    packet.id = peer->id;
    packet.team = team;
    ((ServerHost*)peer->host)->broadcast_packet(packet);
}

void Character::apply_damage(float value, World::WorldObject * obj)
{
    if (dead)
        return;
    PyObject * other = NULL;
    World::Character * character = NULL;
    switch (obj->type) {
        case World::CHARACTER:
            character = (World::Character*)obj;
            break;
        case World::BULLET:
            character = ((World::Bullet*)obj)->character;
            break;
        case World::ROCKET:
            character = ((World::Rocket*)obj)->character;
            break;
    }
    if (character != NULL)
        other = ((Character*)character)->script->object;

    if (!script->call_handler("on_hit", other))
        return;

    float damage = value * HIT_DAMAGE;
    set_hp(hp - int(damage), true, (Character*)character);
}

// Bullet

Bullet::Bullet(World::World * world, const vec3 & pos, const vec3 & dir,
               Character * character)
: World::Bullet(world, pos, dir, character)
{
}

void Bullet::on_hit(World::WorldObject * object)
{
    object->apply_damage(1, this);
}

// Rocket

Rocket::Rocket(World::World * world, const vec3 & pos, const vec3 & dir,
               Character * character)
: World::Rocket(world, pos, dir, character)
{
}

#define ROCKET_HIT_DISTANCE 400.0f

void Rocket::on_destroy()
{
    World::ObjectList objects;
    vec3 pos = get_center() - get_velocity() * (1 / 60.0f);
    world->test_aabb(pos, ROCKET_HIT_DISTANCE, World::COL_BULLET,
        World::COL_BULLET_MASK, objects);
    World::ObjectList::const_iterator it;
    for (it = objects.begin(); it != objects.end(); it++) {
        World::WorldObject * obj = *it;
        vec3 pos2 = obj->get_center();
        World::WorldObject * hit = world->test_ray(pos, pos2);
        if (hit != NULL && hit != obj)
            continue;
        float damage = glm::distance(pos, pos2);
        damage = std::max(0.0f, 1.0f - (damage / ROCKET_HIT_DISTANCE));
        damage *= 10.0f;
        obj->apply_damage(damage, this);
    }
}

// CrateData

CrateData::CrateData(MapObject * obj)
: obj(obj), spawned(false)
{
}

// ServerHost

ServerHost * current_host = NULL;

void close_handler()
{
    current_host->close();
}

ServerHost::ServerHost(int port)
: NetworkHost(port), closed(false), master_reconnect(0.0f), crate_timer(0.0f),
  master_client(NULL)
{
    st_init_time();

    file_mount(".", NULL, 1);
    file_set_write_dir(".");

    script = new ScriptManager(this);
    world = new ServerWorld(this);
    current_host = this;

    // set CTRL+C handler
    set_signal_handler(close_handler);

    connect_master();

    script->call_void("on_start");

    max_crates = script->get_int("max_crates");
    crate_spawn_time = script->get_float("crate_spawn_time");
}

ServerHost::~ServerHost()
{
    file_deinit();
}

void ServerHost::connect_master()
{
    if (!script->get_bool("master"))
        return;
    connect(MASTER_HOST, MASTER_PORT);
}

void ServerHost::set_map(const std::string & map)
{
    named_objects.clear();
    world->set_map(map);

    crates.clear();
    crate_count = 0;

    // reset peers
    PeerList::iterator it2;
    for (it2 = peers.begin(); it2 != peers.end(); it2++) {
        ServerPeer * peer = (ServerPeer*)*it2;
        peer->initialize();
    }

    MapObjects::const_iterator it;
    MapObjects & objects = world->map.objects;
    for (it = objects.begin(); it != objects.end(); it++) {
        MapObject * obj = *it;
        const vec3 & pos = obj->pos;
        World::WorldObject * new_obj;
        if (obj->type == "intel")
            new_obj = new Intel(this, pos, obj->team);
        else if (obj->type == "spawn")
            new_obj = new SpawnPoint(this, pos, obj->team);
        else if (obj->type == "train")
            new_obj = new World::Train(this->world, pos);
        else if (obj->type == "model")
            new_obj = new World::ModelEntity(this->world, obj->get_voxel(),
                pos, obj->scale, obj->health);
        else if (obj->type == "node")
            new_obj = new World::Node(this->world, pos);
        else if (obj->type == "crate") {
            crates.push_back(CrateData(obj));
            continue;
        } else
            continue;
        if (!obj->id.empty())
            named_objects[obj->id] = new_obj;
    }

    float timer = get_map()->timer;
    map_time_enabled = timer != 0.0f;
    if (map_time_enabled)
        set_map_time(timer);
}

Map * ServerHost::get_map()
{
    return &world->map;
}

void ServerHost::update_master_data()
{
    if (master_client == NULL)
        return;
    SetServer packet;
    packet.count = 0;
    PeerList::const_iterator it;
    for (it = peers.begin(); it != peers.end(); it++) {
        ServerPeer * peer = (ServerPeer*)*it;
        if (peer->character != NULL)
            packet.count++;
    }
    master_client->send_packet(packet);
}

NetworkPeer * ServerHost::create_peer(ENetPeer * peer, bool is_client)
{
    if (is_client)
        return new MasterPeer(this, peer);
    else {
        if (peer->eventData > ST_VERSION)
            enet_peer_disconnect(peer, HIGH_VERSION);
        else if (peer->eventData < ST_VERSION)
            enet_peer_disconnect(peer, LOW_VERSION);
        else
            return new ServerPeer(this, peer);
    }
    return NULL;
}

void ServerHost::broadcast_team(Packet & packet, int team)
{
    DataStream stream;
    write_packet(packet, stream);
    ENetPacket * new_packet = enet_packet_create(stream.data, stream.size,
        packet.flags);

    PeerList::const_iterator it;
    for (it = peers.begin(); it != peers.end(); it++) {
        ServerPeer * peer = (ServerPeer*)*it;
        if (!peer->broadcast || peer->character == NULL ||
            peer->character->team != team)
            continue;
        enet_peer_send(peer->peer, packet.channel, new_packet);
    }
}

void ServerHost::broadcast_packet(Packet & packet)
{
    DataStream stream;
    write_packet(packet, stream);
    ENetPacket * new_packet = enet_packet_create(stream.data, stream.size,
        packet.flags);

    PeerList::const_iterator it;
    for (it = peers.begin(); it != peers.end(); it++) {
        ServerPeer * peer = (ServerPeer*)*it;
        if (!peer->broadcast)
            continue;
        enet_peer_send(peer->peer, packet.channel, new_packet);
    }
}

void ServerHost::send_chat(const std::string & message)
{
    ServerChatPacket packet;
    packet.chat_type = SERVER_MESSAGE;
    packet.value = message;
    broadcast_packet(packet);
}

float ServerHost::get_map_time()
{
    if (!map_time_enabled)
        return 0.0f;
    return float(map_time_end - st_get_time());
}

void ServerHost::set_map_time(float value)
{
    map_time_enabled = true;
    map_time_end = st_get_time() + value;
    TimerPacket packet;
    packet.set(value);
    broadcast_packet(packet);
}

void ServerHost::play_sound(const std::string & name, bool music)
{
    PlaySample packet;
    packet.name = name;
    packet.music = music;
    broadcast_packet(packet);
}

float ServerHost::get_average_distance(const vec3 & pos, int team)
{
    float dist = 0.0f;
    int n = 0;

    World::ObjectList::const_iterator it;
    World::ObjectList & objects = world->objects;
    for (it = objects.begin(); it != objects.end(); it++) {
        World::WorldObject * obj = *it;
        if (obj->type != World::CHARACTER)
            continue;
        World::Character * character = (World::Character*)obj;
        if (character->team != team || character->dead)
            continue;
        dist += glm::distance(pos, obj->get_position());
        n++;
    }
    if (n == 0)
        return 0.0f;
    return dist / n;
}

void ServerHost::update_world(float dt)
{
    script->call_void("update", dt);

    if (master_reconnect > 0.0f) {
        master_reconnect -= dt;
        if (master_reconnect <= 0.0f)
            connect_master();
    }

    world->update(dt);

    PeerList::const_iterator it;
    for (it = peers.begin(); it != peers.end(); it++) {
        ServerPeer * peer = (ServerPeer*)*it;
        peer->update(dt);
    }

    if (crate_count < max_crates && crate_count < int(crates.size())) {
        crate_timer += dt;
        if (crate_timer >= crate_spawn_time) {
            crate_timer = 0.0f;
            int team = randint(TEAM_1, TEAM_2+1);
            CrateData * closest_crate = NULL;
            float dist;
            Crates::iterator it;
            for (it = crates.begin(); it != crates.end(); it++) {
                if ((*it).spawned)
                    continue;
                CrateData * crate = &(*it);
                float new_dist = get_average_distance(
                    crate->obj->pos, team);
                if (new_dist == 0.0f)
                    continue;
                if (closest_crate == NULL || new_dist < dist) {
                    dist = new_dist;
                    closest_crate = crate;
                }
            }
            if (closest_crate == NULL)
                return;
            MapObject * obj = closest_crate->obj;
            new Crate(this, obj->pos, closest_crate);
        }
    }
}

void ServerHost::send_world_packet()
{
    WorldPacket update_packet;

    PlayerData player_data;
    PeerList::const_iterator it;
    for (it = peers.begin(); it != peers.end(); it++) {
        ServerPeer * peer = (ServerPeer*)*it;
        Character * character = peer->character;
        if (character == NULL)
            continue;
        vec3 pos = character->get_position();
        vec3 vel = character->get_velocity();
        player_data.set(peer->id, pos, vel, character->is_walking,
            character->walk_angle, character->look_angle, character->has_jumped,
            peer->peer->roundTripTime);
        update_packet.players.push_back(player_data);
        character->has_jumped = false;
    }

    World::ObjectList::const_iterator it2;
    EntityData entity_data;
    for (it2 = world->objects.begin(); it2 != world->objects.end(); it2++) {
        World::WorldObject * object = *it2;
        if (object->type != World::ENTITY)
            continue;
        World::DynamicEntity * entity = (World::DynamicEntity*)object;
        if (!entity->dynamic)
            continue;
        entity_data.set(entity->id, entity->get_position(),
            entity->velocity, entity->get_rotation());
        update_packet.entities.push_back(entity_data);
    }

    broadcast_packet(update_packet);
}

void ServerHost::run()
{
    double current_time, next_frame;
    while (!closed) {
        flush(); // send all queued packets
        current_time = st_get_time();
        next_frame = current_time + SERVER_UPDATE_INTERVAL;
        while (st_get_time() <= next_frame)
            update(1);
        double dt = st_get_time() - current_time;
        update_world(float(dt));
        send_timer += dt;
        while (send_timer >= SERVER_SEND_INTERVAL) {
            send_timer -= SERVER_SEND_INTERVAL;
            send_world_packet();
        }
    }
}

void ServerHost::close()
{
    closed = true;
}

// ServerPeer

ServerPeer::ServerPeer(NetworkHost * host, ENetPeer * peer)
: NetworkPeer(host, peer), character(NULL), broadcast(false),
  id((unsigned char)peer->incomingPeerID), respawn_time(-1.0f)
{
}

bool ServerPeer::on_connect()
{
/*    PackInfo packet;
    StringList items;
    host->script->get_string_list("packs", items);
    StringList::const_iterator it;
    for (it = items.begin(); it != items.end(); it++) {
        std::string path = "data/packs" + (*it) + ".7z";
    }*/
    std::cout << "Client " << int(id) << " connected." << std::endl;

    return true;
}

/*void ServerPeer::start_pack_transfer()
{
    
}*/

void ServerPeer::on_disconnect(unsigned int reason)
{
    ServerHost * host = (ServerHost*)this->host;
    broadcast = false;
    if (character == NULL)
        return;
    std::cout << character->name << " disconnected." << std::endl;
    character->remove_entity();
    DestroyPacket packet;
    packet.id = id;
    host->broadcast_packet(packet);
    character->script->call_void("on_destroy");
    character->destroy();
    character = NULL;
    host->update_master_data();
}

void ServerPeer::update(float dt)
{
    if (character == NULL)
        return;
    if (respawn_time >= 0.0f) {
        respawn_time -= dt;
        if (respawn_time < 0.0f)
            respawn();
    }
}

void ServerPeer::respawn(float time)
{
    if (character->team == TEAM_SPEC)
        return;
    respawn_time = time;
}

void ServerPeer::get_spawn_point(vec3 & pos)
{
    World::ObjectList::const_iterator it;
    World::ObjectList & objects = ((ServerHost*)host)->world->objects;
    std::vector<SpawnPoint*> spawns;
    for (it = objects.begin(); it != objects.end(); it++) {
        World::WorldObject * obj = *it;
        if (obj->type != World::ENTITY)
            continue;
        SpawnPoint * spawn = (SpawnPoint*)obj;
        if (spawn->entity_type != World::SPAWN)
            continue;
        if (spawn->team != TEAM_NONE && spawn->team != character->team)
            continue;
        spawns.push_back(spawn);
    }

    if (spawns.empty()) {
        std::cerr << "No spawns defined in map" << std::endl;
        pos = vec3(0.0f);
        return;
    }

    pos = spawns[randint(spawns.size())]->get_position();
}

void ServerPeer::respawn()
{
    SpawnPacket packet;
    if (character->team != TEAM_SPEC) {
        character->set_dead(false);
        vec3 pos;
        get_spawn_point(pos);
        character->reset();
        character->set_position(pos, false);
        character->play_sound("heal", false);
        packet.x = pos.x;
        packet.y = pos.y;
        packet.z = pos.z;
    }
    packet.dead = character->dead;
    packet.team = character->team;
    packet.id = id;
    packet.name = character->name;
    ((ServerHost*)host)->broadcast_packet(packet);
    if (character->team != TEAM_SPEC)
        character->set_hp(START_HP, false);
}

std::string ServerPeer::get_name(const std::string & requested)
{
    PeerList::const_iterator it;
    std::string name = requested;
    int i = 0;
    bool found;
    PeerList & peers = host->peers;
    while (true) {
        found = false;
        for (it = peers.begin(); it != peers.end(); it++) {
            ServerPeer * peer = (ServerPeer*)*it;
            if (peer->character != NULL && peer->character->name == name) {
                found = true;
                break;
            }
        }
        if (!found)
            return name;
        i++;
        name = requested + number_to_string(i);
    }
}

void ServerPeer::send_chat(const std::string & message)
{
    ServerChatPacket packet;
    packet.chat_type = SERVER_MESSAGE;
    packet.value = message;
    send_packet(packet);
}

void ServerPeer::initialize()
{
    ServerHost * host = (ServerHost*)this->host;
    ServerInfo new_info;
    new_info.map = host->get_map()->short_name;
    send_packet(new_info);
    if (character == NULL) {
        character = new Character(host->world, this, name, vec3(0.0f),
            TEAM_SPEC);
        respawn();
    } else {
        character->set_dead(true);
        character->set_team(TEAM_SPEC);
    }
}

void ServerPeer::on_packet(Packet & data)
{
    ServerHost * host = (ServerHost*)this->host;
    switch (data.type) {
        case CONNECTION_INFO: {
            ConnectionInfo & info = (ConnectionInfo&)data;
            name = get_name(info.name);
            std::cout << "New player: " << name << std::endl;
            broadcast = true;
            initialize();
            host->update_master_data();
            PeerList & peers = host->peers;
            PeerList::const_iterator it;
            SpawnPacket packet;
            ScorePacket score_packet;
            for (it = peers.begin(); it != peers.end(); it++) {
                ServerPeer * peer = (ServerPeer*)*it;
                if (peer == this)
                    continue;
                Character * character = peer->character;
                if (character == NULL)
                    continue;
                packet.id = peer->id;
                packet.dead = character->dead;
                packet.team = character->team;
                vec3 pos = character->get_position();
                packet.x = pos.x;
                packet.y = pos.y;
                packet.z = pos.z;
                packet.name = character->name;
                send_packet(packet);
                score_packet.id = peer->id;
                score_packet.value = character->score;
                send_packet(score_packet);
            }
            CreateEntity create_entity;
            ChangeEntity change_entity;
            BuildBlocks blocks_packet;
            World::ObjectList::const_iterator it2;
            World::ObjectList & objects = host->world->objects;
            for (it2 = objects.begin(); it2 != objects.end(); it2++) {
                World::WorldObject * object = *it2;
                switch (object->type) {
                    case World::ENTITY: {
                        Entity * entity = (Entity*)object;
                        create_entity.id = change_entity.id = entity->id;
                        create_entity.entity_type = entity->entity_type;
                        set_entity_packet(change_entity, entity);
                        send_packet(create_entity);
                        send_packet(change_entity);
                        break;
                    }
                    case World::BLOCK: {
                        BlockObject * obj = (BlockObject*)object;
                        RGBColor & col = obj->color;
                        vec3 pos = obj->get_position();
                        blocks_packet.add(pos.x, pos.y, pos.z,
                            col.r, col.g, col.b);
                        break;
                    }
                    default:
                        continue;
                }
            }
            send_packet(blocks_packet);

            TimerPacket timer_packet;
            timer_packet.set(host->get_map_time());
            send_packet(timer_packet);

            if (host->get_map()->singleplayer) {
                character->set_team(TEAM_SINGLE);
                respawn();
            }

            break;
        }
        case CLIENT_SET_TEAM: {
            ClientSetTeam & packet = (ClientSetTeam&)data;
            if (character->team == packet.team)
                break;
            if (!character->script->call_handler("on_team", packet.team))
                break;
            character->set_team(packet.team);
            respawn(RESPAWN_TIME);
            break;
        }
        case MOVEMENT_PACKET: {
            if (character->dead)
                break;
            MovementPacket & packet = (MovementPacket&)data;
            character->set_walk(packet.is_walking, packet.walk_angle);
            character->set_look_angle(packet.look_angle);

            // perform check for jump (lag and hacks)

            if (packet.jump) {
                // extra check to see if player is actually near ground
                if (character->test_ground(50.0f)) {
                    character->do_jump();
                    character->has_jumped = true;
                }
            }

            // perform check (XXX make better?)
            vec3 new_pos = vec3(packet.x, packet.y, packet.z);
            vec3 pos = character->get_position();
            if (glm::distance(pos, new_pos) > INTERPOLATE_DISTANCE)
                // don't allow positions that are far from what we expect
                break;
            character->set_position(new_pos);
            break;
        }
        case CLIENT_SHOOT_PACKET: {
            if (!character->use_tool())
                break;
            ClientShootPacket & packet = (ClientShootPacket&)data;
            vec3 gun_pos;
            character->get_gun_pos(gun_pos);
            vec3 dir = vec3(packet.dir_x, packet.dir_y, packet.dir_z);
            switch (character->tool->id) {
                case HANDGUN_TOOL:
                    new Bullet(host->world, gun_pos, dir, character);
                    break;
                case RPG_TOOL:
                    new Rocket(host->world, gun_pos, dir, character);
                    break;
                case LAZER_TOOL:
                    character->hitscan(gun_pos, dir);
                    break;
                case PICKAXE_TOOL:
                    character->use_pickaxe(gun_pos, dir);
                    break;
            }
            ServerShootPacket new_packet;
            new_packet.id = id;
            new_packet.x = gun_pos.x;
            new_packet.y = gun_pos.y;
            new_packet.z = gun_pos.z;
            new_packet.dir_x = dir.x;
            new_packet.dir_y = dir.y;
            new_packet.dir_z = dir.z;
            broadcast = false;
            host->broadcast_packet(new_packet);
            broadcast = true;
            break;
        }
        case CLIENT_CHAT_PACKET: {
            ClientChatPacket & packet = (ClientChatPacket&)data;
            if (packet.chat_type == GLOBAL_CHAT &&
                !character->script->call_handler("on_chat", packet.value,
                    packet.sound))
                break;
            else if (!character->script->call_handler("on_team_chat",
                     packet.value, packet.sound))
                break;
            ServerChatPacket new_packet;
            new_packet.id = id;
            new_packet.chat_type = packet.chat_type;
            new_packet.value = packet.value;
            new_packet.sound = packet.sound;
            if (packet.chat_type == GLOBAL_CHAT)
                host->broadcast_packet(new_packet);
            else
                host->broadcast_team(new_packet, character->team);
            break;
        }
        case BUILD_BLOCK: {
            ColorBlock & packet = ((BuildBlock&)data).block;
            vec3 pos(packet.x, packet.y, packet.z);
            if (!host->world->can_place_block(pos))
                break;
            if (!character->use_tool())
                break;
            BuildBlocks new_packet;
            new_packet.add(packet.x, packet.y, packet.z,
                           packet.r, packet.g, packet.b);
            RGBColor color(packet.r, packet.g, packet.b);
            new BlockObject(host, pos, color);
            host->broadcast_packet(new_packet);
            break;
        }
        case CLIENT_SET_TOOL: {
            ClientSetTool & packet = (ClientSetTool&)data;
            character->set_tool(ToolType(packet.tool));
            break;
        }
    }
}

// MasterPeer

MasterPeer::MasterPeer(NetworkHost * host, ENetPeer * peer)
: NetworkPeer(host, peer), connected(false)
{
    ((ServerHost*)host)->master_client = this;
}

bool MasterPeer::on_connect()
{
    ServerHost * host = (ServerHost*)this->host;
    std::cout << "Connected to master server, adding to server list"
        << std::endl;
    AddServer server;
    host->script->get_string("name", server.name);
    server.port = NETWORK_PORT;
    send_packet(server);
    connected = true;
    return true;
}

void MasterPeer::on_disconnect(unsigned int reason)
{
    if (connected)
        std::cout << "Disconnected from master, ";
    else
        std::cout << "Could not connect to master, ";
    std::cout << "reconnecting in 20 seconds..." << std::endl;
    ServerHost * host = (ServerHost*)this->host;
    host->master_reconnect = 20.0f;
    host->master_client = NULL;
}

void MasterPeer::on_packet(Packet & data)
{
    ServerHost * host = (ServerHost*)this->host;
/*    switch (data.type) {
        case CONNECTION_INFO: {
        }*/
}

// main function

int main(int argc, char *argv[])
{
    file_init(argv[0]);

    int port = NETWORK_PORT;
    char * port_str = getenv("PORT");
    if (port_str == NULL && argc > 1)
        port_str = argv[1];
    if (port_str != NULL)
        port = string_to_int(std::string(port_str));
    std::cout << "Running Subversive Takedown on " << port << std::endl;
    ServerHost host(port);
    host.run();
}
