#ifndef ST_MAP_H
#define ST_MAP_H

#include <string>
#include <vector>

#include "glm.h"
#include "voxel.h"

#define BLOCK_BUILD_SIZE 32.0f

class MapObject;
class Map;
class Image;

typedef std::vector<MapObject*> MapObjects;

class MapObject
{
public:
    std::string type;
    std::string model;
    float scale;
    vec3 pos;
    char team;
    int health;
    std::string id;
    std::string icon;
    VoxelFile * voxel;
    MapObjects * objects;
    Map & map;

    MapObject(Map & map, MapObjects * objects);
    void update();
    VoxelFile * get_voxel();
    MapObject * clone();
    size_t get_index();
    void set_list(MapObjects * objects);
    ~MapObject();
#ifdef ST_IS_CLIENT
    Image * get_icon();
#endif
};

class Map
{
public:
    std::string name, short_name;
    std::string music;
    std::string skybox;
    std::string description;
    std::string objective1, objective2;
    MapObjects objects, props;
    float timer;
    bool singleplayer;

    Map();
    Map(const std::string & filename);
    void initialize();
    ~Map();
    void reset(bool objects_only = false);
    void load(const std::string & filename);
    void save(const std::string & filename);
    MapObject * add_prop(const std::string & name, const vec3 & pos,
                         float scale);
    MapObject * add_object(const std::string & type, const vec3 & pos,
                           float scale);
};

inline std::string get_map_path(const std::string & name)
{
    return "data/maps/" + name + ".json";
}

inline Map * load_map(const std::string & name)
{
    return new Map(get_map_path(name));
}

#endif // ST_MAP_H
