#ifndef ST_AUDIO_H
#define ST_AUDIO_H

#include <AL/al.h>
#include <AL/alc.h>
#include <math.h>
#include <tinythread/tinythread.h>
#include <algorithm>
#include <set>
#include <string>
#include <vector>

#include "types.h"

#define BUFFER_COUNT 3

class SoundStream;
class SoundDecoder;

class AudioDevice
{
public:
    ALCdevice * device;
    ALCcontext * context;
    ALboolean direct_channels_ext, sub_buffer_data_ext;
    tthread::thread * streaming_thread;
    std::vector<SoundStream*> streams;
    tthread::recursive_mutex stream_mutex;
    volatile bool closing;

    AudioDevice();
    static void _stream_update(void * data);
    void stream_update();
    void add_stream(SoundStream*);
    void remove_stream(SoundStream*);
    void close();
};

class Listener
{
public:
    static void set_volume(float volume);
    static float get_volume();
};

class SoundBuffer
{
public:
    ALuint buffer;
    std::size_t samples_size;
    signed short * samples;
    std::size_t sample_count;
    unsigned int sample_rate;
    unsigned int channels;
    ALenum format;

    SoundBuffer(unsigned int sample_rate, unsigned int channels, ALenum format);
    SoundBuffer(SoundDecoder & file, size_t sample_count);
    bool read(SoundDecoder & file, size_t read_samples);
    bool read(SoundDecoder & file);
    ~SoundBuffer();
};

class Sound;
typedef std::set<Sound*> SoundList;

class Sample
{
public:
    SoundBuffer * buffer;
    float duration; // duration in seconds
    unsigned int sample_rate;
    unsigned int channels;
    SoundList sounds;

    Sample(const std::string & filename);
    ~Sample();
    void add_sound(Sound* sound);
    void remove_sound(Sound* sound);
};

class SoundBase
{
public:
    enum Status
    {
        Stopped,
        Paused,
        Playing
    };

    ALuint source;
    ALenum format;
    float volume;
    float pitch;
    bool closed;

    SoundBase();
    void set_pitch(float pitch);
    float get_pitch();
    void set_volume(float value);
    float get_volume();
    virtual Status get_status();
    void set_frequency(int value);
    int get_frequency();
    void set_position(float x, float y, float z);
    void set_relative(bool value);
    void set_min_distance(float value);
    void set_attenuation(float value);
    void set_direct_channels(bool value);
    virtual void play() = 0;
    virtual void stop() = 0;
    virtual int get_sample_rate() = 0;
    virtual void set_loop(bool) = 0;
    virtual void set_playing_offset(float) = 0;
    virtual float get_playing_offset() = 0;
    virtual ~SoundBase();
};

class SoundStream : public SoundBase
{
public:
    bool playing;
    unsigned int channels;
    unsigned int sample_rate;
    bool loop;

    SoundStream(const std::string & filename);
    ~SoundStream();
    void play();
    void pause();
    void stop();
    Status get_status();
    void set_playing_offset(float time);
    float get_playing_offset();
    void set_loop(bool loop);
    bool get_loop();
    int get_sample_rate();
    void update();

private:
    SoundBuffer * buffers[BUFFER_COUNT];
    SoundDecoder * file;
    uint64_t samples_processed;
    bool end_buffers[BUFFER_COUNT];
    bool stopping;

    void on_seek(float offset);
    bool fill_buffer(unsigned int buffer_num);
    bool fill_queue();
    void clear_queue();
};

class Sound : public SoundBase
{
public:
    Sample & sample;
    Sound(Sample & sample);
    ~Sound();
    void play();
    void pause();
    void stop();
    bool get_loop();
    void set_loop(bool loop);
    float get_playing_offset();
    void set_playing_offset(float offset);
    int get_sample_rate();
    void reset_buffer();
};

void open_audio();
void close_audio();

#endif // ST_AUDIO_H
