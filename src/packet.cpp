#include "packet.h"

#include <iostream>

Packet * read_packet(char * data, size_t size)
{
    if (size < 1)
        return NULL;
    DataStream stream(data, size);
    PacketType type = PacketType(stream.read_uint8());
    Packet * packet;
    switch (type) {
#ifdef ST_IS_MASTER
        case ADD_SERVER:
            packet = new AddServer();
            break;
        case SET_SERVER:
            packet = new SetServer();
            break;
#elif ST_IS_SERVER
        case CONNECTION_INFO:
            packet = new ConnectionInfo();
            break;
        case MOVEMENT_PACKET:
            packet = new MovementPacket();
            break;
        case CLIENT_SHOOT_PACKET:
            packet = new ClientShootPacket();
            break;
        case CLIENT_CHAT_PACKET:
            packet = new ClientChatPacket();
            break;
        case CLIENT_SET_TEAM:
            packet = new ClientSetTeam();
            break;
        case BUILD_BLOCK:
            packet = new BuildBlock();
            break;
        case CLIENT_SET_TOOL:
            packet = new ClientSetTool();
            break;
#elif ST_IS_CLIENT
        case SERVER_INFO:
            packet = new ServerInfo();
            break;
        case WORLD_PACKET:
            packet = new WorldPacket();
            break;
        case SPAWN_PACKET:
            packet = new SpawnPacket();
            break;
        case DESTROY_PACKET:
            packet = new DestroyPacket();
            break;
        case SERVER_SHOOT_PACKET:
            packet = new ServerShootPacket();
            break;
        case SERVER_CHAT_PACKET:
            packet = new ServerChatPacket();
            break;
        case HP_PACKET:
            packet = new HPPacket();
            break;
        case KILL_PACKET:
            packet = new KillPacket();
            break;
        case CREATE_ENTITY:
            packet = new CreateEntity();
            break;
        case CHANGE_ENTITY:
            packet = new ChangeEntity();
            break;
        case DESTROY_ENTITY:
            packet = new DestroyEntity();
            break;
        case PLAY_SAMPLE:
            packet = new PlaySample();
            break;
        case SCORE_PACKET:
            packet = new ScorePacket();
            break;
        case SERVER_SET_TEAM:
            packet = new ServerSetTeam();
            break;
        case BUILD_BLOCKS:
            packet = new BuildBlocks();
            break;
        case REMOVE_BLOCKS:
            packet = new RemoveBlocks();
            break;
        case SET_AMMO:
            packet = new SetAmmo();
            break;
        case SET_TIMER:
            packet = new TimerPacket();
            break;
        case SERVER_SET_TOOL:
            packet = new ServerSetTool();
            break;
        case HIT_PACKET:
            packet = new HitPacket();
            break;
#endif
        default:
            std::cout << "Packet: Invalid packet type: " << type << std::endl;
            return NULL;
    }
    packet->read(stream);
    return packet;
}


void write_packet(Packet & packet, DataStream & stream)
{
    stream.write_uint8(packet.type);
    packet.write(stream);
}
