#include "configfile.h"

#include <GL/glfw3.h>
#include <map>

#include "ini.h"
#include "stringcommon.h"

#define CLIENT_SECTION "client"
#define LOAD_INT(name, def) \
    name = cfg.get_int(CLIENT_SECTION, #name, def)
#define LOAD_BOOL(name, def) \
    name = cfg.get_bool(CLIENT_SECTION, #name, def)
#define LOAD_FLOAT(name, def) \
    name = cfg.get_float(CLIENT_SECTION, #name, def)
#define LOAD_STR(name, def) \
    name = cfg.get(CLIENT_SECTION, #name, def)
#define LOAD_KEY(name, def) \
    name = convert_key(cfg.get(CLIENT_SECTION, #name, ""), def)

typedef fast_map<std::string, int> Keymap;

static Keymap keymap;

void initialize_keymap()
{
    keymap["SPACE"] = GLFW_KEY_SPACE;
    keymap["APOSTROPHE"] = GLFW_KEY_APOSTROPHE;
    keymap["COMMA"] = GLFW_KEY_COMMA;
    keymap["MINUS"] = GLFW_KEY_MINUS;
    keymap["PERIOD"] = GLFW_KEY_PERIOD;
    keymap["SLASH"] = GLFW_KEY_SLASH;
    keymap["0"] = GLFW_KEY_0;
    keymap["1"] = GLFW_KEY_1;
    keymap["2"] = GLFW_KEY_2;
    keymap["3"] = GLFW_KEY_3;
    keymap["4"] = GLFW_KEY_4;
    keymap["5"] = GLFW_KEY_5;
    keymap["6"] = GLFW_KEY_6;
    keymap["7"] = GLFW_KEY_7;
    keymap["8"] = GLFW_KEY_8;
    keymap["9"] = GLFW_KEY_9;
    keymap["SEMICOLON"] = GLFW_KEY_SEMICOLON;
    keymap["EQUAL"] = GLFW_KEY_EQUAL;
    keymap["A"] = GLFW_KEY_A;
    keymap["B"] = GLFW_KEY_B;
    keymap["C"] = GLFW_KEY_C;
    keymap["D"] = GLFW_KEY_D;
    keymap["E"] = GLFW_KEY_E;
    keymap["F"] = GLFW_KEY_F;
    keymap["G"] = GLFW_KEY_G;
    keymap["H"] = GLFW_KEY_H;
    keymap["I"] = GLFW_KEY_I;
    keymap["J"] = GLFW_KEY_J;
    keymap["K"] = GLFW_KEY_K;
    keymap["L"] = GLFW_KEY_L;
    keymap["M"] = GLFW_KEY_M;
    keymap["N"] = GLFW_KEY_N;
    keymap["O"] = GLFW_KEY_O;
    keymap["P"] = GLFW_KEY_P;
    keymap["Q"] = GLFW_KEY_Q;
    keymap["R"] = GLFW_KEY_R;
    keymap["S"] = GLFW_KEY_S;
    keymap["T"] = GLFW_KEY_T;
    keymap["U"] = GLFW_KEY_U;
    keymap["V"] = GLFW_KEY_V;
    keymap["W"] = GLFW_KEY_W;
    keymap["X"] = GLFW_KEY_X;
    keymap["Y"] = GLFW_KEY_Y;
    keymap["Z"] = GLFW_KEY_Z;
    keymap["LEFT_BRACKET"] = GLFW_KEY_LEFT_BRACKET;
    keymap["BACKSLASH"] = GLFW_KEY_BACKSLASH;
    keymap["RIGHT_BRACKET"] = GLFW_KEY_RIGHT_BRACKET;
    keymap["GRAVE_ACCENT"] = GLFW_KEY_GRAVE_ACCENT;
    keymap["WORLD_1"] = GLFW_KEY_WORLD_1;
    keymap["WORLD_2"] = GLFW_KEY_WORLD_2;
    keymap["ESCAPE"] = GLFW_KEY_ESCAPE;
    keymap["ENTER"] = GLFW_KEY_ENTER;
    keymap["TAB"] = GLFW_KEY_TAB;
    keymap["BACKSPACE"] = GLFW_KEY_BACKSPACE;
    keymap["INSERT"] = GLFW_KEY_INSERT;
    keymap["DELETE"] = GLFW_KEY_DELETE;
    keymap["RIGHT"] = GLFW_KEY_RIGHT;
    keymap["LEFT"] = GLFW_KEY_LEFT;
    keymap["DOWN"] = GLFW_KEY_DOWN;
    keymap["UP"] = GLFW_KEY_UP;
    keymap["PAGE_UP"] = GLFW_KEY_PAGE_UP;
    keymap["PAGE_DOWN"] = GLFW_KEY_PAGE_DOWN;
    keymap["HOME"] = GLFW_KEY_HOME;
    keymap["END"] = GLFW_KEY_END;
    keymap["CAPS_LOCK"] = GLFW_KEY_CAPS_LOCK;
    keymap["SCROLL_LOCK"] = GLFW_KEY_SCROLL_LOCK;
    keymap["NUM_LOCK"] = GLFW_KEY_NUM_LOCK;
    keymap["PRINT_SCREEN"] = GLFW_KEY_PRINT_SCREEN;
    keymap["PAUSE"] = GLFW_KEY_PAUSE;
    keymap["F1"] = GLFW_KEY_F1;
    keymap["F2"] = GLFW_KEY_F2;
    keymap["F3"] = GLFW_KEY_F3;
    keymap["F4"] = GLFW_KEY_F4;
    keymap["F5"] = GLFW_KEY_F5;
    keymap["F6"] = GLFW_KEY_F6;
    keymap["F7"] = GLFW_KEY_F7;
    keymap["F8"] = GLFW_KEY_F8;
    keymap["F9"] = GLFW_KEY_F9;
    keymap["F10"] = GLFW_KEY_F10;
    keymap["F11"] = GLFW_KEY_F11;
    keymap["F12"] = GLFW_KEY_F12;
    keymap["F13"] = GLFW_KEY_F13;
    keymap["F14"] = GLFW_KEY_F14;
    keymap["F15"] = GLFW_KEY_F15;
    keymap["F16"] = GLFW_KEY_F16;
    keymap["F17"] = GLFW_KEY_F17;
    keymap["F18"] = GLFW_KEY_F18;
    keymap["F19"] = GLFW_KEY_F19;
    keymap["F20"] = GLFW_KEY_F20;
    keymap["F21"] = GLFW_KEY_F21;
    keymap["F22"] = GLFW_KEY_F22;
    keymap["F23"] = GLFW_KEY_F23;
    keymap["F24"] = GLFW_KEY_F24;
    keymap["F25"] = GLFW_KEY_F25;
    keymap["KP_0"] = GLFW_KEY_KP_0;
    keymap["KP_1"] = GLFW_KEY_KP_1;
    keymap["KP_2"] = GLFW_KEY_KP_2;
    keymap["KP_3"] = GLFW_KEY_KP_3;
    keymap["KP_4"] = GLFW_KEY_KP_4;
    keymap["KP_5"] = GLFW_KEY_KP_5;
    keymap["KP_6"] = GLFW_KEY_KP_6;
    keymap["KP_7"] = GLFW_KEY_KP_7;
    keymap["KP_8"] = GLFW_KEY_KP_8;
    keymap["KP_9"] = GLFW_KEY_KP_9;
    keymap["KP_DECIMAL"] = GLFW_KEY_KP_DECIMAL;
    keymap["KP_DIVIDE"] = GLFW_KEY_KP_DIVIDE;
    keymap["KP_MULTIPLY"] = GLFW_KEY_KP_MULTIPLY;
    keymap["KP_SUBTRACT"] = GLFW_KEY_KP_SUBTRACT;
    keymap["KP_ADD"] = GLFW_KEY_KP_ADD;
    keymap["KP_ENTER"] = GLFW_KEY_KP_ENTER;
    keymap["KP_EQUAL"] = GLFW_KEY_KP_EQUAL;
    keymap["LEFT_SHIFT"] = GLFW_KEY_LEFT_SHIFT;
    keymap["LEFT_CONTROL"] = GLFW_KEY_LEFT_CONTROL;
    keymap["LEFT_ALT"] = GLFW_KEY_LEFT_ALT;
    keymap["LEFT_SUPER"] = GLFW_KEY_LEFT_SUPER;
    keymap["RIGHT_SHIFT"] = GLFW_KEY_RIGHT_SHIFT;
    keymap["RIGHT_CONTROL"] = GLFW_KEY_RIGHT_CONTROL;
    keymap["RIGHT_ALT"] = GLFW_KEY_RIGHT_ALT;
    keymap["RIGHT_SUPER"] = GLFW_KEY_RIGHT_SUPER;
    keymap["MENU"] = GLFW_KEY_MENU;
    keymap["1"] = GLFW_MOUSE_BUTTON_1;
    keymap["2"] = GLFW_MOUSE_BUTTON_2;
    keymap["3"] = GLFW_MOUSE_BUTTON_3;
    keymap["4"] = GLFW_MOUSE_BUTTON_4;
    keymap["5"] = GLFW_MOUSE_BUTTON_5;
    keymap["6"] = GLFW_MOUSE_BUTTON_6;
    keymap["7"] = GLFW_MOUSE_BUTTON_7;
    keymap["8"] = GLFW_MOUSE_BUTTON_8;
    keymap["LEFT"] = GLFW_MOUSE_BUTTON_LEFT;
    keymap["RIGHT"] = GLFW_MOUSE_BUTTON_RIGHT;
    keymap["MIDDLE"] = GLFW_MOUSE_BUTTON_MIDDLE;
}

int convert_key(const std::string & orig, int def = 0)
{
    std::string name = to_upper(orig);
    Keymap::const_iterator it = keymap.find(name);
    if (it == keymap.end())
        return def;
    return it->second;
}

static std::string empty_string;

const std::string & ConfigFile::get_binding(int key)
{
    Bindings::const_iterator it = bindings.find(key);
    if (it == bindings.end())
        return empty_string;
    return it->second;
}

ConfigFile::ConfigFile(const std::string & filename)
{
    static bool keymap_initialized = false;
    if (!keymap_initialized) {
        initialize_keymap();
        keymap_initialized = true;
    }

    INIParser cfg(filename);

    LOAD_STR(name, "Blockhead");
    LOAD_KEY(up, GLFW_KEY_W);
    LOAD_KEY(down, GLFW_KEY_S);
    LOAD_KEY(left, GLFW_KEY_A);
    LOAD_KEY(right, GLFW_KEY_D);
    LOAD_KEY(chat, GLFW_KEY_T);
    LOAD_KEY(team_chat, GLFW_KEY_Y);
    LOAD_KEY(shoot, GLFW_MOUSE_BUTTON_LEFT);
    LOAD_KEY(orient, GLFW_MOUSE_BUTTON_RIGHT);
    LOAD_KEY(jump, GLFW_KEY_SPACE);
    LOAD_KEY(show_map, GLFW_KEY_M);
    LOAD_KEY(pick_color, GLFW_KEY_C);
    LOAD_KEY(change_team, GLFW_KEY_COMMA);
    LOAD_KEY(switch_camera, GLFW_KEY_E);
    LOAD_KEY(quick_chat, GLFW_KEY_V);
    LOAD_FLOAT(mouse_sensitivity, 1.0f);
    LOAD_FLOAT(locked_sensitivity, 0.25f);
    LOAD_FLOAT(rotation_sensitivity, 0.5f);
    LOAD_INT(graphics, HIGH_GFX);
    LOAD_INT(samples, 4);
    LOAD_BOOL(music, true);
    LOAD_BOOL(vsync, true);
    LOAD_BOOL(particles, true);
    LOAD_BOOL(smoke, true);

    INIMap::const_iterator it;
    for (it = cfg.items.begin(); it != cfg.items.end(); it++) {
        if (!starts_with(it->first, "bindings."))
            continue;
        int k = convert_key(it->first.substr(9), -1);
        if (k == -1)
            continue;
        bindings[k] = it->second;
    }
}
