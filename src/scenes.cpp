#include "scene.h"
#include "scenes/menu.h"
#include "scenes/game.h"
#include "scenes/credits.h"

Scene * main_menu_scene = NULL;
Scene * game_scene = NULL;
Scene * credits_scene = NULL;

void initialize_scenes(GameManager * manager)
{
    main_menu_scene = new MainMenu(manager);
    game_scene = new GameScene(manager);
    credits_scene = new CreditsScene(manager);
}
