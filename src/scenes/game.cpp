#include "game.h"

#include <iostream>

#include "animation.h"
#include "assets.h"
#include "constants.h"
#include "debug.h"
#include "draw.h"
#include "easing.h"
#include "fonts.h"
#include "hud.h"
#include "include_gl.h"
#include "manager.h"
#include "mathcommon.h"
#include "movement.h"
#include "random.h"
#include "scene.h"
#include "scenes.h"
#include "voxel.h"

using World::Entity;
using World::BlockObject;
using World::DynamicEntity;

#define ZNEAR 50.0f
#define ZFAR 3000.0f

// Character

Character::Character(GameScene * scene, unsigned char id,
    const std::string & name, const vec3 & pos, char team, bool local)
: World::Character(scene->world, name, pos, team),
  animation(characterwalk_animation), time(0.0f), scene(scene), local(local),
  id(id), interpolated_angle(0.0f), name_alpha(0.0f), has_jumped(false),
  ping(0), old_vel(0.0f)
{
    scene->characters[id] = this;
    set_position(pos, false);
    set_team(team);
}

float interpolate_pos(float cur, float neww, float old, float dt)
{
    float diff = neww - old;
    float ret = cur + diff * (dt / SERVER_SEND_INTERVAL);
    if (diff < 0)
        return std::max<float>(ret, neww);
    else if (diff > 0)
        return std::min<float>(ret, neww);
    else
        return neww;
}

void Character::play_sample(const std::string & name)
{
    if (local)
        scene->audio->play_sample(name);
    else
        scene->audio->play_sample(name, get_position());
}

void Character::set_team(char team)
{
    this->team = team;
    if (team == TEAM_2)
        animation.set_data(character2walk_animation);
    else
        animation.set_data(characterwalk_animation);
}

#define SPECTATOR_SPEED 500.0f

void Character::update_spectator(float dt)
{
    vec3 vel(0.0f);
    if (is_walking) {
        vel.x = SPECTATOR_SPEED * cos_deg(walk_angle);
        vel.y = SPECTATOR_SPEED * sin_deg(walk_angle);
    }
    set_velocity(vel, false);
}

void Character::shoot(const vec3 & pos, const vec3 & dir)
{
    switch (tool->id) {
        case HANDGUN_TOOL:
            new Bullet(scene, pos, dir, this);
            break;
        case RPG_TOOL:
            new Rocket(scene, pos, dir, this);
            break;
        case LAZER_TOOL:
            new LazerTracer(scene, pos, dir, this);
            hitscan(pos, dir);
            break;
        case PICKAXE_TOOL:
            play_sample("pickaxe");
            use_pickaxe(pos, dir);
            break;
    }
}

void Character::shoot(const vec3 & dir)
{
    vec3 gun_pos;
    get_gun_pos(gun_pos);
    shoot(gun_pos, dir);
}

void Character::update(float dt)
{
    if (dead) {
        if (team == TEAM_SPEC)
            update_spectator(dt);
        else
            World::Character::update(dt);
        return;
    }
    bool old_jump = jump;
    bool old_ground = on_ground;
    vec3 old_vel = get_velocity();
    World::Character::update(dt);
    if (jump != old_jump) {
        play_sample("jump");
        has_jumped = true;
    }
    if (on_ground && old_ground != on_ground &&
        old_vel.z <= JUMP_VELOCITY * -0.25f) {
        play_sample("land");
    }

    if (is_walking) {
        int old_frame = animation.frame;
        animation.update(dt);
        if (on_ground && animation.frame != old_frame) {
            if (animation.frame == 1)
                play_sample("walk2");
            else if (animation.frame == 3)
                play_sample("walk1");
        }
    }
    time += dt;

    if (local)
        interpolated_angle = interpolate_angle(interpolated_angle, look_angle,
            dt * LOCAL_ANGLE_INTERPOLATION_SPEED);
    else
        interpolated_angle = interpolate_angle(interpolated_angle, look_angle,
            dt * ANGLE_INTERPOLATION_SPEED);

    // client-side interpolation of position
    net_pos.x = interpolate_pos(net_pos.x, next_pos.x, last_pos.x, dt);
    net_pos.y = interpolate_pos(net_pos.y, next_pos.y, last_pos.y, dt);
    net_pos.z = interpolate_pos(net_pos.z, next_pos.z, last_pos.z, dt);

    vec3 pos = get_position(false);

    if (local) {
        // some client-side prediction - movement should feel smooth on this
        // side of the connection
        vec3 test_pos = net_pos;

        if (is_walking) {
            // add some of the character's movement to the test position.
            // cheap client-side prediction!
            float speed = CHARACTER_SPEED * dt * 10;
            test_pos += vec3(cos_deg(walk_angle) * speed,
                             sin_deg(walk_angle) * speed, 0.0f);
        }

        float dist = glm::distance(test_pos, pos);
        if (dist >= RUBBERBAND_DISTANCE)
            pos = net_pos;
        else if (dist >= INTERPOLATE_DISTANCE) {
            // exponential interpolation of position if the distance between
            // the current test position is higher then the displayed,
            // actual position
            pos += (test_pos - pos) * (POSITION_INTERPOLATION_SPEED * dt);
        }

        vec3 vel = get_velocity();
        dist = glm::distance(vel, interp_vel);
        if (dist >= RUBBERBAND_DISTANCE) {
            vel += (interp_vel - vel) * (VELOCITY_INTERPOLATION_SPEED * dt);
            World::Character::set_velocity(vel);
        }
    } else {
        pos = net_pos;
    }

    World::Character::set_position(pos);

    old_vel = get_velocity();

    // test los
    Character * local_char = scene->character;
    if (local_char == this || local_char == NULL || local_char->team == team) {
        hide = false;
    } else {
        vec3 off(0.0f, 0.0f, BULLET_Z);
        vec3 test_pos = local_char->get_position() + off;
        vec3 hit_pos, hit_normal;
        hide = scene->world->test_ray(test_pos, pos + off, hit_pos, hit_normal
            ) != NULL;
    }

    float add = dt * 5.0f;
    if (hide)
        add = -add;
    name_alpha = std::max<float>(0.0f, std::min<float>(1.0f, name_alpha + add));
}

float Character::get_angle()
{
    return interpolated_angle;
}

void Character::draw()
{
    if (hide || dead)
        return;

    float angle = get_angle();
    vec3 pos = get_position();
    glPushMatrix();
    glTranslatef(pos.x, pos.y, pos.z);
    glScalef(2.0, 2.0, 2.0);
    glRotatef(angle, 0.0, 0.0, 1.0);
    VoxelModel * model = animation.get();
    model->draw();
    model->get_point("gun")->translate();
    tool->model->draw();
    glPopMatrix();

    // draw shadow
    vec3 hit, normal;
    pos.z += CHARACTER_Z_SIZE;
    bool has_hit = world->test_ray(pos, pos - vec3(0.0f, 0.0f, 1000.0f),
                                   hit, normal) != NULL;
    if (has_hit) {
        glDepthMask(GL_FALSE);
        glPushMatrix();
        glTranslatef(hit.x, hit.y, hit.z + 10.0f);
        float height = pos.z - hit.z;
        float scale = 3.4f - height * 0.010f;
        glScalef(scale, scale, scale);
        float alpha = 0.25f - height * 0.0008f;
        glColor4f(1.0f, 1.0f, 1.0f, alpha);
        shadow_image->draw(0.0f, 0.0f);
        glPopMatrix();
        glDepthMask(GL_TRUE);
    }

/*    vec3 test_pos = net_pos;*/
/*    if (is_walking) {
        float speed = CHARACTER_SPEED * (1 / 60.0f) * 10;
        test_pos += vec3(cos_deg(walk_angle) * speed,
            sin_deg(walk_angle) * speed, 0.0f);
    }*/
/*    vec3 size(20.0f);
    vec3 test_min = test_pos - size;
    vec3 test_max = test_pos + size;
    draw_rect(test_min.x, test_min.y, test_max.z,
              test_max.x, test_max.y, test_max.z,
              255, 0, 0, 255);*/
}

void Character::set_walk(bool walking, float angle)
{
    bool old_moving = is_walking;
    World::Character::set_walk(walking, angle);
    if (!walking)
        animation.reset();
    else if (old_moving != walking)
        animation.set_frame(1);
}

void Character::set_position(const vec3 & pos, bool interpolate)
{
    next_pos = pos;
    if (!interpolate) {
        net_pos = pos;
        set_velocity(vec3(0.0f, 0.0f, 0.0f));
        World::Character::set_position(pos);
    }
    last_pos = net_pos;
}

void Character::set_velocity(const vec3 & vel, bool interpolate)
{
    interp_vel = vel;
    if (!interpolate || !local)
        World::Character::set_velocity(vel);
}

void Character::do_jump()
{
    World::Character::do_jump();
    play_sample("jump");
}

void Character::on_destroy()
{
    World::Character::on_destroy();
    scene->characters.erase(id);
}

void Character::kill()
{
    if (dead)
        return;
    play_sample("die");
    World::Character::kill();
    scene->melt_model(animation.get(), get_position(), get_angle(), 2.0f);
}

// Bullet

Bullet::Bullet(GameScene * scene, const vec3 & pos, const vec3 & dir,
               Character * character)
: World::Bullet(scene->world, pos, dir, character), scene(scene)
{
    character->play_sample("shoot1");
}

void Bullet::draw()
{
    glDisable(GL_LIGHTING); // needs to be glowy
    glPushMatrix();
    vec3 pos = get_position();
    glTranslatef(pos.x, pos.y, pos.z);
    float rot = scene->get_time() * 250.0f;
    glRotatef(rot, 1.0f, 1.0f, 1.0f);
    glScalef(1.2f, 1.2f, 1.2f);
    bullet_model->draw();
    glPopMatrix();
    glEnable(GL_LIGHTING);
}

void Bullet::on_hit(World::WorldObject * object)
{
    if (object->type == World::CHARACTER)
        ((Character*)object)->play_sample("hit");
    else {
        scene->audio->play_sample("blockhit", object->get_position());
        object->apply_damage(1, this);
    }
}

// Rocket

Rocket::Rocket(GameScene * scene, const vec3 & pos, const vec3 & dir,
               Character * character)
: World::Rocket(scene->world, pos, dir, character), scene(scene),
  smoke_time(0.0f)
{
    character->play_sample("shoot2");
    sound = scene->audio->play_sample("rocket", pos, 0.7f);
    sound->set_loop(true);
}

void Rocket::draw()
{
    glPushMatrix();
    vec3 pos = get_position();
    glTranslatef(pos.x, pos.y, pos.z);
    ::multiply_matrix(get_rotation());
    glScalef(2.0f, 2.0f, 2.0f);
    rocket_model->draw();
    glPopMatrix();
}

void Rocket::update(float dt)
{
    World::Rocket::update(dt);
    vec3 pos = get_position();
    sound->set_position(pos.x, pos.y, pos.z);
    if (!scene->manager->config->smoke)
        return;
    smoke_time += dt;
    vec3 v;
    while (smoke_time >= 0.01f) {
        vecrand(v);
        smoke_time -= 0.01f;
        RGBColor col = fire_color;
        col.multiply(randrange(0.2f, 1.4f));
        new SmokeParticle(scene, pos + v * 10.0f, col, 1.0f, 15.0f);
    }
}

void Rocket::on_destroy()
{
    sound->stop();
    vec3 pos = get_position();
    scene->audio->play_sample("crash", pos, 1.0f);
    pos -= get_velocity() * (1 / 60.0f);
    scene->melt_model(rocket_model, pos, 0.0f, 1.0f,
        400.0f, 1.0f);
}

// LazerTracer

#define LAZERTRACER_SPEED 3000.0f
#define LAZERTRACER_TTL 3.0f

LazerTracer::LazerTracer(GameScene * scene, const vec3 & pos, const vec3 & dir,
                         Character * character)
: World::WorldObject(scene->world, World::BULLET), scene(scene), 
  character(character), ttl(LAZERTRACER_TTL), start_pos(pos), size(1.0f)
{
    character->play_sample("shoot3");
    static World::BoxShape shape(5.0f);
    initialize(&shape, pos, 1.0f, 1.0f, 0.0f, World::COL_BULLET, 
               World::COL_BULLET_MASK);
    set_contact_response(false);
    set_velocity(dir * LAZERTRACER_SPEED);
    set_gravity(vec3(0.0f, 0.0f, 0.0f));
    enable_collision_callback();
/*    sound = scene->audio->play_sample("rocket", pos, 0.7f);
    sound->set_loop(true);*/
}

quat LazerTracer::get_rotation()
{
    return look_at(glm::normalize(get_velocity()));
}

void LazerTracer::draw()
{
    glDisable(GL_LIGHTING);
    glPushMatrix();
    vec3 pos = get_position();
    glTranslatef(start_pos.x, start_pos.y, start_pos.z);
    ::multiply_matrix(get_rotation());
    float len = glm::distance(start_pos, pos) / lazertracer_model->file->x_size;
    glScalef(len, 1.0f, 1.0f);
    float size = this->size * 10.0f;
    glScalef(1.0f, size, size);
    lazertracer_model->draw_immediate(0.5f);
    glPopMatrix();
    glEnable(GL_LIGHTING);
}

void LazerTracer::on_collision(WorldObject * obj, btManifoldPoint & pt,
                               bool is_a)
{
    if (obj == character)
        return;
    destroy();
}

void LazerTracer::update(float dt)
{
    ttl -= dt;
    if (ttl <= 0)
        destroy();
    size = interpolate(size, 0.0f, dt * 10.0f);
}

void LazerTracer::on_destroy()
{
    // sound->stop();
    vec3 pos = get_position();
    scene->audio->play_sample("blockhit", pos, 1.0f);
}

// CubeParticle

CubeParticle::CubeParticle(GameScene * scene, const vec3 & pos,
                           const RGBColor & col, float vel)
: World::WorldObject(scene->world, World::PARTICLE), color(col),
  ttl(3.0f), x_rot(0.0f), y_rot(0.0f), z_rot(0.0f)

{
    vec3 vec;
    vecrand(vec);
    static World::BoxShape shape(5.5f);
    initialize(&shape, pos, 1.0f, 0.5f, 10.0f);
    set_velocity(vec * vel);
    vecrand(vec);
    set_angular_velocity(vec * 20.0f);
}

void CubeParticle::update(float dt)
{
    ttl -= dt;
    if (ttl <= 0) {
        destroy();
        return;
    }
}

void CubeParticle::draw()
{
    float f = std::min<float>(1.0f, (ttl / 3.0f) * 14.0f);
    vec3 p2 = vec3(5.5f) * f * 0.5f;
    glPushMatrix();
    multiply_matrix();
    draw_cube(-p2.x, -p2.y, -p2.z, p2.x, p2.y, p2.z,
        color.r, color.g, color.b, 255);
    glPopMatrix();
}

// Intel

Intel::Intel(GameScene * scene, const vec3 & pos, char team)
: World::Intel(scene->world, pos, team), rotation(0.0f)
{

}

void Intel::update(float dt)
{
    rotation += dt * 50.0f;
}

void Intel::draw()
{
    if (character != NULL)
        return;
    glPushMatrix();
    vec3 pos = get_position();
    glTranslatef(pos.x, pos.y, pos.z);
    glRotatef(rotation, 0.0f, 0.0f, 1.0f);
    glScalef(6.0f, 6.0f, 6.0f);
    glTranslatef(0.5f, 0.5f, 0.0f);
    if (team == TEAM_1)
        intel1_model->draw();
    else if (team == TEAM_2)
        intel2_model->draw();
    else
        intel_model->draw();
    glPopMatrix();
}

void Intel::draw_overhead(const vec3 & pos)
{
    glPushMatrix();
    glTranslatef(pos.x, pos.y, pos.z);
    glScalef(2.0f, 2.0f, 2.0f);
    glRotatef(rotation, 0.0f, 0.0f, 1.0f);
    glTranslatef(0.5f, 0.5f, 0.0f);
    if (team == TEAM_1)
        intel1_model->draw();
    else if (team == TEAM_2)
        intel2_model->draw();
    else
        intel_model->draw();
    glPopMatrix();
}

// Crate

Crate::Crate(GameScene * scene, const vec3 & pos)
: World::Crate(scene->world, pos), rotation(0.0f)
{

}

void Crate::update(float dt)
{
    rotation += dt * 50.0f;
}

void Crate::draw()
{
    glPushMatrix();
    vec3 pos = get_position();
    glTranslatef(pos.x, pos.y, pos.z);
    glRotatef(rotation, 0.0f, 0.0f, 1.0f);
    glScalef(1.75f, 1.75f, 1.75f);
    crate_model->draw();
    glPopMatrix();
}

// WhirlCube

#define WHIRL_SIZE 5.0f
#define WHIRL_COUNT 20
#define WHIRL_RADIUS 45.0f

WhirlCube::WhirlCube(const vec3 & dir)
: dir(dir), z(0), time(0.0f)
{

}

void WhirlCube::update(float dt)
{
    time += dt;
    dir = glm::rotateZ(dir, 70.0f * dt);
}

void WhirlCube::draw(char team)
{
    vec3 pos = dir;
    pos.z += 10.0f;
    vec3 half_size(WHIRL_SIZE * 0.5f, WHIRL_SIZE * 0.5f, WHIRL_SIZE * 0.5f);
    half_size *= (sin(time * 5.0f) + 1.0f) * 0.25f + 1.0f;
    vec3 min = pos - half_size;
    vec3 max = pos + half_size;

    const RGBColor & col = get_team_color(team);
    draw_cube(min.x, min.y, min.z, max.x, max.y, max.z,
              col.r, col.g, col.b, 255);
}

// SpawnPoint

SpawnPoint::SpawnPoint(GameScene * scene, const vec3 & pos, char team)
: World::SpawnPoint(scene->world, pos, team)
{
    for (int i = 0; i < WHIRL_COUNT; i++) {
        float f = float(i) / float(WHIRL_COUNT);
        float v = 2 * PI_F * f;
        vec3 start(cos(v) * WHIRL_RADIUS, sin(v) * WHIRL_RADIUS, 2.0f);
        cubes.push_back(new WhirlCube(start));
    }
}

void SpawnPoint::update(float dt)
{
    std::vector<WhirlCube*>::const_iterator it;
    for (it = cubes.begin(); it != cubes.end(); it++) {
        (*it)->update(dt);
    }
}

void SpawnPoint::draw()
{
    glPushMatrix();
    vec3 pos = get_position();
    glTranslatef(pos.x, pos.y, pos.z);

    std::vector<WhirlCube*>::const_iterator it;
    for (it = cubes.begin(); it != cubes.end(); it++) {
        (*it)->draw(team);
    }

    glScalef(7.0f, 7.0f, 7.0f);
    if (team == TEAM_1)
        base1_model->draw();
    else if (team == TEAM_2)
        base2_model->draw();
    else
        base3_model->draw();
    glPopMatrix();
}

// SmokeParticle

#define SMOKE_TTL 6.0f

SmokeParticle::SmokeParticle(GameScene * scene, const vec3 & pos,
    const RGBColor & color, float min_size, float max_size)
: World::WorldObject(scene->world, World::PARTICLE), ttl(SMOKE_TTL),
  color(color)
{
    initialize(NULL, pos, 0.5f, 0.5f, 0.0f, World::COL_NONE,
        World::COL_NONE_MASK);
    set_gravity(vec3(0.0f, 0.0f, 30.0f));
    set_contact_response(false);
    vec3 vel;
    vecrand(vel);
    set_velocity(vel * 10.0f);
    vecrand(vel);
    set_angular_velocity(vel * 2.0f);
    size = randrange(min_size, max_size);
}

void SmokeParticle::update(float dt)
{
    ttl -= dt;
    if (ttl <= 0.0f)
        destroy();
}

void SmokeParticle::draw()
{
    glPushMatrix();
    multiply_matrix();
    float scale = std::min(size, (ttl / SMOKE_TTL) * 50.0f);
    draw_cube(0, 0, 0, scale, color.r, color.g, color.b, 127);
    glPopMatrix();
}

// Train

Train::Train(GameScene * scene, const vec3 & pos)
: World::Train(scene->world, pos), scene(scene), smoke_time(0.0f)
{
    sound = scene->audio->play_sample("train", pos, 0.5f);
    sound->set_loop(true);
}

void Train::draw()
{
    glPushMatrix();
    multiply_matrix();
    glScalef(TRAIN_SCALE, TRAIN_SCALE, TRAIN_SCALE);
    vec3 shake;
    vecrand(shake);
    shake *= 0.1f;
    glTranslatef(shake.x, shake.y, shake.z);
    get_voxel()->get_model()->draw();
    glPopMatrix();
}

void Train::update(float dt)
{
    vec3 vel = get_velocity();
    float pitch = std::min(1.0f, std::max(0.5f, (glm::length(vel) / 25.0f)));
    sound->set_pitch(pitch);
    sound->set_volume(pitch * 0.5f);
    World::Train::update(dt);
    vec3 pos = get_position();
    sound->set_position(pos.x, pos.y, pos.z);
    if (!scene->manager->config->smoke)
        return;
    smoke_time += dt;
    if (smoke_time >= 0.5f) {
        smoke_time -= 0.5f;
        new SmokeParticle(scene, get_transform(vec3(-110.0f, 0.0f, 10.0f)));
    }
}

void Train::on_destroy()
{
    sound->stop();
    World::Train::on_destroy();
}

// ModelEntity

ModelEntity::ModelEntity(GameScene * scene)
: World::ModelEntity(scene->world), scene(scene)
{

}

void ModelEntity::draw()
{
    if (voxel == NULL)
        return;
    glPushMatrix();
    multiply_matrix();
    glScalef(scale, scale, scale);
    voxel->get_model()->draw();
    glPopMatrix();
}

void ModelEntity::on_destroy()
{
    World::ModelEntity::on_destroy();
    vec3 pos = get_position();
    scene->audio->play_sample("crash", pos);
    scene->melt_model(voxel->get_model(), pos, 0.0f, scale, 400.0f, 1.0f);
}

// GameScene

GameScene::GameScene(GameManager * manager)
: Scene(manager), crosshair_x(0.0f), crosshair_y(0.0f),
  view_x(0.0f), view_y(0.0f), status("Connecting..."), peer(NULL),
  movement_send_timer(0.0f), crosshair_scale(1.0f),
  orienting(false), rotate_z(0.0f), up(false), down(false),
  left(false), right(false), rotate_z_interp(0.0f), hit_time(0.0f),
  debug_time(0.0f), world(NULL), map(NULL), shake_x(0.0f), shake_y(0.0f),
  heal_time(0.0f), rotate_x(X_ROTATION), rotate_x_interp(0.0f),
  zoom(CAMERA_DISTANCE), zoom_interp(0.0f), add_center_z(0.0f),
  skybox_image(NULL)
{
    manager->get_mouse_pos(old_x, old_y);
    hud_projection_matrix = glm::ortho(0.0f, float(WINDOW_WIDTH), 0.0f,
        float(WINDOW_HEIGHT), -5000.0f, 5000.0f);
    projection_matrix = glm::perspective(PERSPECTIVE_FOV,
        float(WINDOW_WIDTH) / float(WINDOW_HEIGHT), ZNEAR, ZFAR);
    viewport = vec4(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
}

void GameScene::on_start()
{
    locked_camera = false;
    character = NULL;
    connected = false;
    peer = NULL;
    map = NULL;
    world = new World::World();
    hud = new HUD(this);
    client = new ClientHost(this);
    client->connect(manager->host_ip, manager->host_port);
    manager->set_captured_mouse();
}

void GameScene::on_end()
{
    audio->stop_samples();
    delete hud;
    delete world;
    delete client;
    peer = NULL;
    characters.clear();
}

bool GameScene::on_quit()
{
    if (hud->focus)
        hud->close_chat_input();
    return false;
}

void GameScene::disconnect()
{
    if (peer != NULL)
        peer->disconnect(true);
    set_scene(main_menu_scene);
}

void GameScene::on_server_info(ServerInfo & info)
{
    audio->stop_samples();
    entities.clear();
    hide_objects.clear();

    status = "";
    connected = true;
    reload_voxels();
    world->set_map(info.map);
    map = &world->map;

    skybox_image = load_image(map->skybox, 0, 0, false);

    MapObjects::const_iterator it;
    for (it = map->objects.begin(); it != map->objects.end(); it++) {
        MapObject * obj = *it;
        if (obj->type != "node")
            continue;
        World::Node * node = new World::Node(world, obj->pos);
        node->icon = obj->get_icon();
    }

    audio->play_music(map->music, 0.3f);
}

bool GameScene::is_tool(ToolType id)
{
    return character != NULL && character->is_tool(id);
}

void GameScene::update_build(float dt)
{
    if (ray_obj == NULL || ray_obj->type == World::CHARACTER)
        return;

    vec3 normal = glm::sign(glm::round(ray_normal));

    if (ray_obj->type == World::BLOCK) {
        vec3 hit = ray_obj->get_position();
        build_mid = hit + normal * BLOCK_BUILD_SIZE;
        build_min = build_mid - vec3(BLOCK_BUILD_SIZE / 2);
        build_max = build_mid + vec3(BLOCK_BUILD_SIZE / 2);
    } else {
        // align to grid (depending on block face)
        vec3 hit = ray_hit;
        vec3 hit2 = glm::floor(hit / BLOCK_BUILD_SIZE) * BLOCK_BUILD_SIZE;
        if (normal.x != 0.0f) {
            hit.y = hit2.y;
            hit.z = hit2.z;
        } else if (normal.y != 0.0f) {
            hit.x = hit2.x;
            hit.z = hit2.z;
        } else if (normal.z != 0.0f) {
            hit.x = hit2.x;
            hit.y = hit2.y;
        }
        build_min = glm::min(hit + normal * BLOCK_BUILD_SIZE, hit);
        build_max = build_min + vec3(BLOCK_BUILD_SIZE);
        build_mid = (build_min + build_max) * 0.5f;
    }

    float dist = glm::distance(build_mid, character->get_position());
    if (dist >= MAX_BUILD_DISTANCE)
        return;

    if (!world->can_place_block(build_mid))
        return;

    can_build = true;
}

void GameScene::update_locked_camera(float dt)
{
    rotate_x = std::max(-145.0f, std::min(-15.0f, rotate_x));
    crosshair_x = WINDOW_WIDTH / 2;
    crosshair_y = WINDOW_HEIGHT / 2;
    zoom_interp = interpolate(zoom_interp, 150.0f, dt * 10.0f);
    view_x = interpolate(view_x, 0.0f, dt * 5.0f);
    view_y = interpolate(view_y, 0.0f, dt * 5.0f);
    rotate_x_interp = interpolate_angle(rotate_x_interp, rotate_x, dt * 15.0f);
    rotate_z_interp = interpolate_angle(rotate_z_interp, rotate_z, dt * 15.0f);
    add_center_z = interpolate(add_center_z, 80.0f, dt * 5.0f);
}

void GameScene::update_camera(float dt)
{
    rotate_x = std::max(-80.0f, std::min(-15.0f, rotate_x));
    static const float mid_x = WINDOW_WIDTH / 2;
    static const float mid_y = WINDOW_HEIGHT / 2;
    float dist = distance(mid_x, mid_y, crosshair_x, crosshair_y);
    if (dist > 0.0f) {
        float max_dist = std::min<float>(dist, CROSSHAIR_RANGE);
        float dist_x, dist_y;
        dist_x = ((crosshair_x - mid_x) / dist) * max_dist;
        dist_y = ((crosshair_y - mid_y) / dist) * max_dist;
        view_x = interpolate(view_x, -dist_x * CROSSHAIR_MULT, dt * 5.0f);
        view_y = interpolate(view_y, -dist_y * CROSSHAIR_MULT, dt * 5.0f);
    }

    zoom_interp = interpolate(zoom_interp, zoom, dt * 10.0f);
    rotate_x_interp = interpolate_angle(rotate_x_interp, rotate_x, dt * 15.0f);
    rotate_z_interp = interpolate_angle(rotate_z_interp, rotate_z, dt * 15.0f);
    add_center_z = interpolate(add_center_z, 0.0f, dt * 5.0f);
}

void GameScene::update(float dt)
{
    client->update();

    if (world == NULL)
        return;

    hud->update(dt);
    if (peer != NULL && character != NULL) {
        movement_send_timer -= dt;
        if (movement_send_timer <= 0 || character->has_jumped) {
            movement_send_timer += MOVEMENT_UPDATE_INTERVAL;
            send_movement();
            character->has_jumped = false;
        }
    }
    world->update(dt);
    hit_time = std::max(0.0f, hit_time - dt);
    heal_time = std::max(0.0f, heal_time - dt);

    vec3 char_pos(0.0f);
    if (character != NULL)
        char_pos = character->get_position();

    center = char_pos;

    if (locked_camera)
        update_locked_camera(dt);
    else
        update_camera(dt);

    center.z += add_center_z;
    audio->set_listener(char_pos, rotate_z_interp);

    float z_angle = -rotate_z_interp - 180.0f;
    center += glm::rotateZ(vec3(view_x, view_y, 0.0f), z_angle);
    vec3 rotation(0.0f, 0.0f, 1.0f);
    rotation = glm::rotateX(rotation, rotate_x_interp);
    rotation = glm::rotateZ(rotation, z_angle);
    eye = center + rotation * zoom_interp;

    // hide objects that go between the eye and character position
    World::ObjectList::const_iterator it;
    for (it = hide_objects.begin(); it != hide_objects.end(); it++)
        ((World::ModelObject*)*it)->set_hide(false);
    hide_objects.clear();
    if (character != NULL && character->team != TEAM_SPEC && !hud->show_map)
        world->test_ray(char_pos + vec3(0.0f, 0.0f, CHARACTER_Z_SIZE * 0.5f),
                        eye, hide_objects);
    for (it = hide_objects.begin(); it != hide_objects.end(); it++)
        ((World::ModelObject*)*it)->set_hide(true);

    crosshair_scale = interpolate(crosshair_scale, 1.0f, dt * 5.0f);

    // shake when hit
    float hit_perc = hit_time / HIT_TIME;
    if (hit_time > 0) {
        shake_x = randrange(-SHAKE_RANGE, SHAKE_RANGE) * hit_perc;
        shake_y = randrange(-SHAKE_RANGE, SHAKE_RANGE) * hit_perc;
    } else
        shake_x = shake_y = 0.0f;

    // add shake if falling really fast
    if (character != NULL && !character->dead) {
        float z_vel = character->get_velocity().z;
        if (z_vel <= -800.0f) {
            float speed = (-z_vel - 800.0f) * 0.0007f;
            shake_x += randrange(-SHAKE_RANGE, SHAKE_RANGE) * speed;
            shake_y += randrange(-SHAKE_RANGE, SHAKE_RANGE) * speed;
        }
    }

    view_matrix = glm::translate(mat4(), vec3(shake_x, shake_y, 0.0f)) *
        glm::lookAt(eye, center, vec3(0.0f, 0.0f, 1.0f));

    mvp = projection_matrix * view_matrix;
    inverse_mvp = glm::inverse(mvp);

    get_window_ray(vec2(crosshair_x, crosshair_y), inverse_mvp, viewport,
        ray_pos, ray_dir);
    ray_obj = world->test_ray(ray_pos, ray_pos + ray_dir * 3000.0f, ray_hit,
                              ray_normal, true);
    if (ray_obj == character)
        ray_obj = NULL;

    if (locked_camera)
        character->set_look_angle(-rotate_z_interp + 90.0f);

    if (ray_obj != NULL) {
        vec3 gun_pos;
        character->get_gun_pos(gun_pos);
        vec3 dir = glm::normalize(ray_hit - gun_pos);
        character->set_look_direction(dir);
        if (!locked_camera) {
            // set character angle
            vec3 pos = character->get_position();
            pos.z += BULLET_Z;
            float angle = circle_tangent_angle(pos.x, pos.y,
                ray_hit.x, ray_hit.y, -10.0f) - 90.0f;
            // test for nan
            if (angle == angle) {
                angle = mod(angle, 360.0f);
                character->set_look_angle(angle);
            }
        }
    } else {
        if (locked_camera)
            character->set_look_direction(
                glm::normalize(ray_dir + vec3(0.0f, 0.0f, 0.05f)));
    }

    can_build = false;
    if (is_tool(BLOCK_TOOL))
        update_build(dt);

    client->flush(); // send all queued packets

#if 0
    debug_time += dt;
    while (debug_time >= 0.5f) {
        debug_time -= 0.5f;
        audio->play_sample("soundtest", vec3(0.0f, 0.0f, 0.0f));
    }
#endif
}

#define SKY_TEX_X (1.0f / 4.0f)
#define SKY_TEX_Y (1.0f / 3.0f)

#define SS 250.0f

inline void draw_skybox(GLuint tex, int xx, int yy,
                        vec3 a, vec3 b, vec3 c, vec3 d)
{
    float x1 = float(xx) * SKY_TEX_X;
    float x2 = x1 + SKY_TEX_X;
    float y1 = float(yy) * SKY_TEX_Y;
    float y2 = y1 + SKY_TEX_Y;
    glTexCoord2f(x1, y1);
    glVertex3f(a.x, a.y, a.z);
    glTexCoord2f(x1, y2);
    glVertex3f(b.x, b.y, b.z);
    glTexCoord2f(x2, y2);
    glVertex3f(c.x, c.y, c.z);
    glTexCoord2f(x2, y1);
    glVertex3f(d.x, d.y, d.z);
}

void GameScene::draw_world()
{
    glMatrixMode(GL_PROJECTION);
    load_matrix(projection_matrix);
    glMatrixMode(GL_MODELVIEW);

    // draw world + crosshair
    load_matrix(view_matrix);
    setup_lighting();

    glEnable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);

    if (skybox_image != NULL) {
        glPushMatrix();
        glTranslatef(eye.x, eye.y, eye.z);

        // draw skybox
        skybox_image->upload_texture();
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, skybox_image->tex);
        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        glBegin(GL_QUADS);

        float a = -SS - 1.0f;
        float b = -a;
        float a2 = -SS;
        float b2 = SS;

        // front
        draw_skybox(skybox_image->tex, 1, 1,
            vec3(a, b2, b),
            vec3(a, b2, a),
            vec3(b, b2, a),
            vec3(b, b2, b)
        );

        // back
        draw_skybox(skybox_image->tex, 3, 1,
            vec3(b, a2, b),
            vec3(b, a2, a),
            vec3(a, a2, a),
            vec3(a, a2, b)
        );

        // top
        draw_skybox(skybox_image->tex, 1, 0,
            vec3(a, a, b2),
            vec3(a, b, b2),
            vec3(b, b, b2),
            vec3(b, a, b2)
        );

        // bottom
        draw_skybox(skybox_image->tex, 1, 2,
            vec3(a, b, a2),
            vec3(a, a, a2),
            vec3(b, a, a2),
            vec3(b, b, a2)
        );

        // left
        draw_skybox(skybox_image->tex, 0, 1,
            vec3(a2, a, b),
            vec3(a2, a, a),
            vec3(a2, b, a),
            vec3(a2, b, b)
        );

        // right
        draw_skybox(skybox_image->tex, 2, 1,
            vec3(b2, b, b),
            vec3(b2, b, a),
            vec3(b2, a, a),
            vec3(b2, a, b)
        );

        glEnd();
        glDisable(GL_TEXTURE_2D);
        glPopMatrix();
    }

    glClear(GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    glEnable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);

    GLfloat fog_col[] = {0.99f, 0.84f, 0.49f, 1.0f};
    glFogfv(GL_FOG_COLOR, fog_col);
    glFogi(GL_FOG_MODE, GL_LINEAR);
    glFogf(GL_FOG_DENSITY, 0.009f);
    glHint(GL_FOG_HINT, GL_NICEST);
    glFogf(GL_FOG_START, 2500);
    glFogf(GL_FOG_END, ZFAR);
    glEnable(GL_FOG);
    world->draw(mvp);

    glDisable(GL_FOG);

    // build mode
    if (can_build) {
        vec3 & min = build_min;
        vec3 & max = build_max;
        RGBColor & col = hud->get_palette_color();
        draw_cube(min.x, min.y, min.z, max.x, max.y, max.z,
                  col.r, col.g, col.b, 100);
    }

    manager->post_process(ZNEAR, ZFAR);

    glDepthMask(GL_FALSE);
    World::ObjectList::const_iterator it1;
    for (it1 = hide_objects.begin(); it1 != hide_objects.end(); it1++) {
        ((World::ModelObject*)*it1)->draw(0.25f);
    }
    glDepthMask(GL_TRUE);

    // for overhead 3d icons and blocks
    glClear(GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    // draw overhead 3d icons
    World::ObjectList::const_iterator it2;
    for (it2 = world->objects.begin(); it2 != world->objects.end(); it2++) {
        World::WorldObject * object = *it2;
        switch (object->type) {
            case World::CHARACTER:
                break;
            default:
                continue;
        }
        Character * character = (Character*)object;
        if (character->hold_entity == NULL)
            continue;
        character->hold_entity->draw_overhead(
            character->get_position() + vec3(0.0f, 0.0f, 100.0f));
    }
}

void GameScene::draw()
{
    glClearColor(0.49f, 0.62f, 0.75f, 1.0f);
    glClear(GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    bool world_drawn = false;
    if (world == NULL) {
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
    } else if (!hud->show_map) {
        draw_world();
        world_drawn = true;
    }

    glMatrixMode(GL_PROJECTION);
    load_matrix(hud_projection_matrix);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);

    if (world_drawn) {
        // draw player names
        World::ObjectList::const_iterator it;
        int r = int(sin_wave(time, 150, 255, 0.2f));
        for (it = world->objects.begin(); it != world->objects.end(); it++) {
            World::WorldObject * object = *it;
            Image * icon = NULL;
            switch (object->type) {
                case World::CHARACTER: {
                    Character * character = (Character*)object;
                    if (character->team == TEAM_SPEC)
                        break;
                    vec3 pos = object->get_position();
                    vec2 draw_pos;
                    if (!project(pos + vec3(0.0f, 0.0f, 60.0f), mvp, viewport,
                                 draw_pos))
                        break;
                    RGBColor color = get_team_color(character->team);
                    color.mix(white_color, 0.6f);
                    color.set_current(character->name_alpha);
                    fixedsys_font->draw_center(character->name, draw_pos.x,
                                               draw_pos.y + 16.0f);
                    break;
                }
                case World::ENTITY: {
                    Entity * entity = (Entity*)object;
                    if (entity->entity_type == World::INTEL 
                        && entity->character ==  NULL)
                        icon = intel_image;
                    int hp = entity->get_health();
                    if (hp == 0)
                        break;
                    vec3 pos = entity->get_position();
                    vec2 draw_pos;
                    if (!project(pos, mvp, viewport, draw_pos))
                        break;
                    vec2 size(40.0f, 10.0f);
                    vec2 min = draw_pos - size;
                    vec2 max = draw_pos + size;
                    draw_rect(min.x, min.y, max.x, max.y, r, 0, 0, 120);
                    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
                    fixedsys_font->draw_center(number_to_string(hp),
                        draw_pos.x, draw_pos.y + 1.0f);
                    break;
                }
                case World::NODE: {
                    World::Node * node = (World::Node*)object;
                    icon = node->icon;
                    break;
                }
                default:
                    break;
            }

            // draw item icon
            if (icon != NULL) {
                vec3 pos = object->get_position();
                pos.z += 20.0f;
                vec2 draw_pos;
                if (!project(pos, mvp, viewport, draw_pos))
                    continue;
                draw_pos.y += 50.0f;
                float dist = glm::distance(center, pos) - 400.0f;
                float alpha = glm::min(1.0f, glm::max(0.0f, dist * 0.001f));
                glColor4f(1.0f, 1.0f, 1.0f, alpha);
                float scale = sin_wave(time, 2.0f, 3.0f, 0.4f);
                iconbox_image->draw(draw_pos.x, draw_pos.y - 6.0f, 0.0f, 
                    3.0f, 3.0f);
                icon->draw(draw_pos.x, draw_pos.y, 0.0f, scale, scale);
            }
        }
    }

    if (world != NULL) {
        glTranslatef(shake_x, shake_y, 0.0f);

        // HUD
        if (character != NULL) {
            float scale;
            Image * crosshair;
            if (is_tool(BLOCK_TOOL)) {
                crosshair = build_crosshair_image;
                scale = 2.0f;
            } else {
                crosshair = crosshair_image;
                scale = 2.0f * crosshair_scale;
            }

            glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            crosshair->draw(crosshair_x, crosshair_y, time * 100.0f,
                scale, scale);
        }

        hud->draw();
    }

    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

    if (!connected)
        fixedsys_font->draw_right(status, WINDOW_WIDTH - 20, 20);
    else if (has_team_select()) {
        fixedsys_font->draw_center("Press 1 to join Greenites",
                                   WINDOW_WIDTH - 150, 40);
        fixedsys_font->draw_center("Press 2 to join Bluekins",
                                   WINDOW_WIDTH - 150, 20);
    }

    if (hit_time > 0) {
        draw_rect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, 255, 0, 0,
            int(127 * (hit_time / HIT_TIME)));
    } else if (heal_time > 0) {
        draw_rect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, 70, 70, 200,
            int(127 * (heal_time / HEAL_TIME)));
    }
}

void GameScene::show_hit()
{
    hit_time = HIT_TIME;
}

void GameScene::show_heal()
{
    heal_time = HEAL_TIME;
}

void GameScene::melt_model(VoxelModel * model, const vec3 & pos, float z_angle,
                           float scale, float vel, float block_size)
{
    if (!manager->config->particles)
        return;
    float x, y, z;
    int x2, y2, z2;
    vec3 p;
    VoxelFile * file = model->file;
    for (x = 0; x < file->x_size; x += block_size)
    for (y = 0; y < file->y_size; y += block_size)
    for (z = 0; z < file->z_size; z += block_size) {
        x2 = int(x);
        y2 = int(y);
        z2 = int(z);
        if (!file->is_solid_fast(x2, y2, z2) || !file->is_surface(x2, y2, z2))
            continue;
        const RGBColor & color = file->get_color(x2, y2, z2);
        p.x = x + file->x_offset;
        p.y = y + file->y_offset;
        p.z = z + file->z_offset;
        p = glm::rotateZ(p * scale, z_angle) + pos;
        CubeParticle * particle = new CubeParticle(this, p, color, vel);
    }
}

void GameScene::set_team(char team)
{
    if (character->team == team)
        return;
    ClientSetTeam packet;
    packet.team = team;
    peer->send_packet(packet);
}

bool GameScene::has_team_select()
{
    return character == NULL || character->team == TEAM_SPEC;
}

void GameScene::on_key(int button, bool value)
{
    hud->on_key(button, value);
    if (hud->focus)
        return;
    if (character == NULL)
        return;
    ConfigFile * conf = manager->config;
    if (button == conf->up)
        up = value;
    else if (button == conf->down)
        down = value;
    else if (button == conf->left)
        left = value;
    else if (button == conf->right)
        right = value;
    else if (button == conf->jump)
        character->set_jump(value);
    else if (button == GLFW_KEY_1 && has_team_select())
        set_team(TEAM_1);
    else if (button == GLFW_KEY_2 && has_team_select())
        set_team(TEAM_2);
    else if (button == conf->change_team && value) {
        set_team(TEAM_SPEC);
    } else if (character != NULL && button >= GLFW_KEY_1 && button <= GLFW_KEY_5
               && value) {
        int i = button - GLFW_KEY_1;
        character->set_tool(ToolType(i));
        send_tool();
    } else if (button == conf->show_map) {
        hud->show_map = value;
        return;
    } else if (button == conf->pick_color && value) {
        if (ray_obj == NULL || ray_obj->type != World::BLOCK)
            return;
        hud->set_color(((BlockObject*)ray_obj)->color);
        return;
    } else if (button == conf->switch_camera && value) {
        set_locked_camera(!locked_camera);
    } else {
        if (!value)
            return;
        const std::string & command = conf->get_binding(button);
        if (command.empty())
            return;
        send_command(command);
        return;
    }
    update_walk_angle(true);
}

void GameScene::send_tool()
{
    ClientSetTool packet;
    packet.tool = character->tool->id;
    peer->send_packet(packet);
}

void GameScene::update_walk_angle(bool send)
{
    bool walking, old_walking;
    float angle, old_angle;
    old_walking = character->is_walking;
    old_angle = character->walk_angle;
    get_control_angle(up, down, left, right, walking, angle);
    angle = mod(angle - rotate_z, 360.0f);
    character->set_walk(walking, angle);
    if (send && (walking != old_walking || old_angle != angle))
        send_movement();
}

void GameScene::on_mouse_key(int button, bool value)
{
    ConfigFile * conf = manager->config;
    if (button == conf->orient)
        orienting = value;
    if (character == NULL || character->dead)
        return;
    if (button == conf->shoot) {
        if (!value)
            return;
        Tool * tool = character->tool;
        switch (tool->id) {
            case BLOCK_TOOL: {
                if (!can_build)
                    break;
                if (!character->use_tool())
                    break;
                BuildBlock packet;
                packet.block.x = build_mid.x;
                packet.block.y = build_mid.y;
                packet.block.z = build_mid.z;
                RGBColor & col = hud->get_palette_color();
                packet.block.r = col.r;
                packet.block.g = col.g;
                packet.block.b = col.b;
                peer->send_packet(packet);
                break;
            }
            case RPG_TOOL:
            case HANDGUN_TOOL:
            case LAZER_TOOL: {
                if (!character->use_tool())
                    return;
                float angle = character->look_angle;
                vec3 error;
                vecrand(error);
                error *= CROSSHAIR_ERROR * 0.2f;
                error = error * ((crosshair_scale - 1.0f) * 3.0f);
                vec3 dir = glm::normalize(character->look_dir + error);
                character->shoot(dir);
                ClientShootPacket packet;
                packet.dir_x = dir.x;
                packet.dir_y = dir.y;
                packet.dir_z = dir.z;
                peer->send_packet(packet);
                crosshair_scale *= 1.5f;
                break;
            }
            case PICKAXE_TOOL:
                if (!character->use_tool())
                    return;
                character->shoot(character->look_dir);
                ClientShootPacket packet;
                packet.dir_x = character->look_dir.x;
                packet.dir_y = character->look_dir.y;
                packet.dir_z = character->look_dir.z;
                peer->send_packet(packet);
                break;
        }
    }
}

void GameScene::on_mouse_move(int x, int y)
{
    if (character == NULL)
        return;
    float dx = float(x - old_x);
    float dy = float(y - old_y);
    old_x = x;
    old_y = y;
    if (orienting || locked_camera) {
        if (locked_camera) {
            dx *= manager->config->locked_sensitivity;
            dy *= manager->config->locked_sensitivity;
        } else {
            dx *= manager->config->rotation_sensitivity;
            dy *= manager->config->rotation_sensitivity;
        }
        rotate_z += dx;
        rotate_x -= dy;
        if (locked_camera) {
            rotate_z_interp += dx;
            rotate_x_interp -= dy;
        }
        update_walk_angle(false);
    } else {
        dx *= manager->config->mouse_sensitivity;
        dy *= manager->config->mouse_sensitivity;
        crosshair_x = std::min<float>(WINDOW_WIDTH, std::max<float>(0.0f,
            crosshair_x + dx));
        crosshair_y = std::min<float>(WINDOW_HEIGHT, std::max<float>(0.0f,
            crosshair_y + dy));
    }
}

void GameScene::set_locked_camera(bool value)
{
    if (locked_camera == value)
        return;
    locked_camera = value;
    if (locked_camera)
        rotate_x = -80.0f;
}

void GameScene::on_mouse_scroll(double dx, double dy)
{
    set_locked_camera(false);
    zoom = std::min(1000.0f, std::max(250.0f, zoom - float(dy) * 100.0f));
}

void GameScene::on_text(const std::string & text)
{
    if (!hud->focus)
        return;
    hud->on_text(text);
}

void GameScene::create_character(SpawnPacket & data)
{
    Character * character = get_character(data.id);
    bool local = data.id == peer->id;
    if (character == NULL) {
        vec3 pos(0.0f);
        if (data.team != TEAM_SPEC)
            pos = vec3(data.x, data.y, data.z);
        character = new Character(this, data.id, data.name, pos, data.team,
            local);
    } else if (character->team != TEAM_SPEC && !data.dead) {
        character->set_position(vec3(data.x, data.y, data.z), false);
    }

    character->reset();
    character->set_dead(data.dead);

    if (local)
        this->character = character;
}

void GameScene::destroy_character(unsigned char id)
{
    Character * character = get_character(id);
    character->destroy();
}

Character * GameScene::get_character(unsigned char id)
{
    if (id == NONE_CHARACTER)
        return NULL;
    CharacterMap::const_iterator it = characters.find(id);
    if (it == characters.end())
        return NULL;
    return it->second;
}

World::Entity * GameScene::get_entity(unsigned char id)
{
    EntityMap::const_iterator it = entities.find(id);
    if (it == entities.end())
        return NULL;
    return it->second;
}

// GameScene networking

void GameScene::send_movement()
{
    if (character->dead)
        return;
    MovementPacket movement_packet;
    vec3 pos = character->get_position();
    movement_packet.set(pos.x, pos.y, pos.z, character->is_walking,
        character->walk_angle, character->look_angle, character->has_jumped);
    peer->send_packet(movement_packet);
}

void GameScene::send_chat(const std::string & text, int type)
{
    ClientChatPacket packet;
    packet.value = text;
    packet.chat_type = type;
    packet.sound = "";
    peer->send_packet(packet);
}

void GameScene::send_chat(const std::string & text, const std::string & sound, 
                          int type)
{
    ClientChatPacket packet;
    packet.value = text;
    packet.chat_type = type;
    packet.sound = sound;
    peer->send_packet(packet);
}

void GameScene::send_command(const std::string & name)
{
    send_chat("/" + name);
}

// ClientHost

ClientHost::ClientHost(GameScene * scene)
: NetworkHost(), scene(scene)
{

}

NetworkPeer * ClientHost::create_peer(ENetPeer * peer, bool is_client)
{
    return new ClientPeer(scene, this, peer);
}

// ClientPeer

ClientPeer::ClientPeer(GameScene * scene, NetworkHost * host, ENetPeer * peer)
: NetworkPeer(host, peer), scene(scene)
{
}

bool ClientPeer::on_connect()
{
    id = (unsigned char)(peer->outgoingPeerID);
    std::cout << "Connected, id: " << int(id) << std::endl;
    scene->peer = this;
    ConnectionInfo info;
    info.name = scene->manager->config->name;
    send_packet(info);
    return true;
}

void ClientPeer::on_disconnect(unsigned int reason)
{
    scene->set_scene(main_menu_scene);
}

void ClientPeer::on_packet(Packet & data)
{
    switch (data.type) {
        case SERVER_INFO: {
            scene->on_server_info((ServerInfo&)data);
            break;
        }
        case WORLD_PACKET: {
            WorldPacket & packet = (WorldPacket&)data;
            PlayerDataList & players = packet.players;
            PlayerDataList::const_iterator it;
            for (it = players.begin(); it != players.end(); it++) {
                const PlayerData & player = *it;
                Character * character = scene->get_character(player.id);
                if (character == NULL)
                    continue;
                if (!character->local) {
                    character->set_walk(player.is_walking, player.walk_angle);
                    character->set_look_angle(player.look_angle);
                    if (player.jump)
                        character->do_jump();
                }
                if (character->team != TEAM_SPEC && !character->dead) {
                    character->set_position(player.pos, true);
                    character->set_velocity(player.vel, true);
                }
                character->ping = player.ping;
            }

            EntityDataList & entities = packet.entities;
            EntityDataList::const_iterator it2;
            for (it2 = entities.begin(); it2 != entities.end(); it2++) {
                const EntityData & entity_data = *it2;
                DynamicEntity * entity = (DynamicEntity*)scene->get_entity(
                    entity_data.id);
                entity->set_position(entity_data.pos);
                entity->set_constant_velocity(entity_data.vel);
                entity->set_rotation(entity_data.rot);
            }
            break;
        }
        case SERVER_SET_TEAM: {
            ServerSetTeam & packet = (ServerSetTeam&)data;
            scene->get_character(packet.id)->set_team(packet.team);
            break;
        }
        case SPAWN_PACKET: {
            SpawnPacket & packet = (SpawnPacket&)data;
            scene->create_character(packet);
            break;
        }
        case DESTROY_PACKET: {
            DestroyPacket & packet = (DestroyPacket&)data;
            scene->destroy_character(packet.id);
            break;
        }
        case SERVER_SHOOT_PACKET: {
            ServerShootPacket & packet = (ServerShootPacket&)data;
            Character * character = scene->get_character(packet.id);
            vec3 pos(packet.x, packet.y, packet.z);
            vec3 dir(packet.dir_x, packet.dir_y, packet.dir_z);
            character->shoot(pos, dir);
            break;
        }
        case SERVER_CHAT_PACKET: {
            ServerChatPacket & packet = (ServerChatPacket&)data;
            if (packet.chat_type == SERVER_MESSAGE)
                scene->hud->add_chat(packet.value);
            else {
                Character * character = scene->get_character(packet.id);
                scene->hud->add_chat(character, packet.value, packet.chat_type);
            }
            if (!packet.sound.empty())
                scene->audio->play_sample(packet.sound, 0.25f);
            break;
        }
        case HP_PACKET: {
            HPPacket & packet = (HPPacket&)data;
            scene->hud->set_hp(packet.value);
            if (packet.show_hit)
                scene->show_hit();
            else
                scene->show_heal();
            break;
        }
        case KILL_PACKET: {
            KillPacket & packet = (KillPacket&)data;
            Character * character = scene->get_character(packet.id);
            Character * by = scene->get_character(packet.by);
            scene->hud->add_kill(character, by);
            character->kill();
            if (character->local) {
                scene->hud->set_hp(0);
                scene->orienting = false;
            }
            break;
        }
        case CREATE_ENTITY: {
            CreateEntity & packet = (CreateEntity&)data;
            World::Entity * entity = NULL;
            switch (packet.entity_type) {
                case World::INTEL:
                    entity = new Intel(scene, vec3(0.0f), TEAM_NONE);
                    break;
                case World::SPAWN:
                    entity = new SpawnPoint(scene, vec3(0.0f), TEAM_NONE);
                    break;
                case World::TRAIN:
                    entity = new Train(scene, vec3(0.0f));
                    break;
                case World::MODEL:
                    entity = new ModelEntity(scene);
                    break;
                case World::CRATE:
                    entity = new Crate(scene, vec3(0.0f));
                    break;
                default:
                    std::cout << "Unknown entity type: " << packet.entity_type
                        << std::endl;
                    break;
            }
            entity->id = packet.id;
            scene->entities[packet.id] = entity;
            break;
        }
        case CHANGE_ENTITY: {
            ChangeEntity & packet = (ChangeEntity&)data;
            World::Entity * entity = scene->get_entity(packet.id);
            if (entity->entity_type == World::MODEL) {
                VoxelFile * voxel = load_voxel(packet.model);
                ((ModelEntity*)entity)->set_entity(voxel, vec3(0.0f),
                    packet.scale);
            }
            Character * character;
            if (packet.is_set())
                character = scene->get_character(packet.character);
            else
                character = NULL;
            entity->set_health(packet.health);
            entity->set_character(character);
            entity->set_position(packet.pos, false);
            entity->set_team(packet.team);
            break;
        }
        case DESTROY_ENTITY: {
            DestroyEntity & packet = (DestroyEntity&)data;
            World::Entity * entity = scene->get_entity(packet.id);
            entity->destroy();
            break;
        }
        case PLAY_SAMPLE: {
            PlaySample & packet = (PlaySample&)data;
            if (packet.music)
                scene->audio->play_event_sample(packet.name);
            else
                scene->audio->play_sample(packet.name);
            break;
        }
        case SCORE_PACKET: {
            ScorePacket & packet = (ScorePacket&)data;
            scene->get_character(packet.id)->score = packet.value;
            break;
        }
        case BUILD_BLOCKS: {
            BuildBlocks & packet = (BuildBlocks&)data;
            std::vector<ColorBlock>::const_iterator it;
            bool sound_played = false;
            for (it = packet.blocks.begin(); it != packet.blocks.end(); it++) {
                const ColorBlock & block = *it;
                vec3 pos(block.x, block.y, block.z);
                if (!sound_played) {
                    scene->audio->play_sample("build", pos);
                    sound_played = true;
                }
                new BlockObject(scene->world, pos,
                    RGBColor(block.r, block.g, block.b));
            }
            break;
        }
        case REMOVE_BLOCKS: {
            RemoveBlocks & packet = (RemoveBlocks&)data;
            std::vector<unsigned int>::const_iterator it;
            World::ObjectList blocks;
            for (it = packet.blocks.begin(); it != packet.blocks.end(); it++)
                blocks.push_back(scene->world->get_block(*it));
            World::ObjectList::const_iterator it2;
            for (it2 = blocks.begin(); it2 != blocks.end(); it2++) {
                BlockObject * object = (BlockObject*)*it2;

                vec3 pos = object->get_position();

                if (scene->manager->config->particles) {
                    RGBColor & col = object->color;
                    for (int i = 0; i < 3; i++)
                        new CubeParticle(scene, pos, col);
                }

                scene->audio->play_sample("blockdestroy", pos);
                object->destroy();
            }
            break;
        }
        case SET_AMMO: {
            SetAmmo & packet = (SetAmmo&)data;
            scene->character->get_tool(ToolType(packet.tool))->set(packet.ammo);
            break;
        }
        case SET_TIMER: {
            TimerPacket & packet = (TimerPacket&)data;
            scene->hud->set_map_time(packet.timer);
            break;
        }
        case SERVER_SET_TOOL: {
            ServerSetTool & packet = (ServerSetTool&)data;
            Character * character = scene->get_character(packet.id);
            character->set_tool(ToolType(packet.tool));
            break;
        }
        case HIT_PACKET: {
            scene->audio->play_sample("hit2");
            break;
        }
        default:
            std::cout << "Client: Invalid packet type: " << data.type
                << std::endl;
            break;
    }
}
