#include "credits.h"
#include "include_gl.h"
#include "constants.h"
#include "fonts.h"
#include "assets.h"
#include "scenes.h"

static const char * lines[] = {
    ".                                         by Mathias Kaerlev",
    "",
    "* PROGRAMMING",
    "",
    "Mathias Kaerlev",
    "",
    "* MUSIC",
    "",
    "Mathias Kaerlev",
    "Jan Lotz",
    "",
    "* SOUND EFFECTS",
    "",
    "Mathias Kaerlev",
    "",
    "* VOXEL MODELING",
    "",
    "Mathias Kaerlev",
    "Hammaad Khan",
    "Tegu",
    "",
    "* MAP DESIGN",
    "",
    "Mathias Kaerlev",
    "",
    "* SPECIAL THANKS",
    "",
    "Christian Nellemose Petersen",
    "homps",
    "neersighted",
    "#st @ QuakeNet",
    "#buildandshoot @ QuakeNet",
    "",
    "* TESTERS",
    "",
    "Tegu",
    "Tank",
    "Jan Lotz",
    "Jet-stream",
    "Hammaad Khan",
    "PABH",
    "AEM",
    "Ulexos",
    "Venator",
    "neersighted",
    "blha303",
    "MooseElkingtons",
    "Ben Russell",
    "",
    "* SOFTWARE LIBRARIES",
    "",
    "OpenAL-Soft",
    "GLFW",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "Thank you for playing!"
};

#define LINECOUNT (sizeof(lines) / sizeof(*lines))
#define LINE_HEIGHT 35

CreditsScene::CreditsScene(GameManager * manager)
: Scene(manager)
{
}

void CreditsScene::on_start()
{
    manager->set_normal_mouse();
    audio->play_music("Credits");
    current_y = -175;
}

#define MAX_Y (LINECOUNT * LINE_HEIGHT + WINDOW_HEIGHT)

void CreditsScene::update(float dt)
{
    current_y += dt * 70;
    if (current_y >= MAX_Y) {
        float vol = 1.0f - (current_y - MAX_Y) * 0.01f;
        audio->music->set_volume(vol);
        if (vol <= 0.0f)
            manager->set_scene(main_menu_scene);
    }
}

inline float get_alpha(float y)
{
    float alpha = 1.0f - fabs(y - WINDOW_HEIGHT / 2) / (WINDOW_HEIGHT / 2);
    return std::min(1.0f, alpha * 5.0f);
}

void CreditsScene::draw()
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, WINDOW_WIDTH, 0, WINDOW_HEIGHT, -1, 1);
    glMatrixMode(GL_MODELVIEW);
    glDisable(GL_DEPTH_TEST);
    glLoadIdentity();
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    float current_y = float(int(this->current_y));

    glColor4f(1.0f, 1.0f, 1.0f, get_alpha(current_y + 100));
    splash_image->draw(WINDOW_WIDTH / 2, current_y + 100);

    for (int i = 0; i < LINECOUNT; i++) {
        float alpha = get_alpha(current_y);
        if (alpha <= 0.0f) {
            current_y -= LINE_HEIGHT;
            continue;
        }
        glPushMatrix();
        glTranslatef(WINDOW_WIDTH / 2, current_y, 0.0f);
        float scale = 2.0f;
        const char * line = lines[i];
        if (line[0] == '*') {
            line = &line[2];
            glColor4f(0.6f, 0.6f, 0.6f, alpha);
        } else if (line[0] == '.') {
            line = &line[2];
            glColor4f(1.0f, 1.0f, 1.0f, alpha);
            scale = 1.0f;
        } else {
            glColor4f(1.0f, 1.0f, 1.0f, alpha);
        }
        glScalef(scale, scale, scale);
        fixedsys_font->draw_center(line, 0.0f, 0.0f);
        glPopMatrix();
        current_y -= LINE_HEIGHT;
    }
}

void CreditsScene::on_mouse_key(int button, bool value)
{
    
}