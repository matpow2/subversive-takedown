#ifndef ST_SCENES_MENU_H
#define ST_SCENES_MENU_H

#include "gui.h"
#include "scene.h"

class MainMenu : public Scene
{
public:
    Button play_button, credits_button, local_button;
    ControlList controls;
    Scene * next_scene;
    float fade_value;

    MainMenu(GameManager * manager);
    void on_start();
    void update(float dt);
    void draw();
    void on_mouse_move(int x, int y);
    void on_mouse_key(int button, bool value);
    void draw_character(char team, float x);
    static void on_play(ControlBase * control, void * data);
    static void on_credits(ControlBase * control, void * data);
    static void on_local(ControlBase * control, void * data);
};

#endif // ST_SCENES_MENU_H
