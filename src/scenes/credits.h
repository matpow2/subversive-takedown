#ifndef ST_SCENES_CREDITS_H
#define ST_SCENES_CREDITS_H

#include "gui.h"
#include "scene.h"

class CreditsScene : public Scene
{
public:
    float current_y;

    CreditsScene(GameManager * manager);
    void on_start();
    void update(float dt);
    void draw();
    void on_mouse_key(int button, bool value);
};

#endif // ST_SCENES_CREDITS_H
