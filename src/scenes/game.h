#ifndef ST_SCENES_GAME_H
#define ST_SCENES_GAME_H

#include "animation.h"
#include "glm.h"
#include "image.h"
#include "network.h"
#include "scene.h"
#include "world.h"

class GameScene;

// Character

class Character : public World::Character
{
public:
    VoxelAnimation animation;
    float time;
    GameScene * scene;
    bool local;
    unsigned char id;
    float interpolated_angle;
    vec3 last_pos, next_pos, net_pos;
    vec3 interp_vel;
    bool hide;
    float name_alpha;
    bool has_jumped;
    unsigned short ping;
    vec3 old_vel;

    Character(GameScene * scene, unsigned char id, const std::string & name,
        const vec3 & pos, char team, bool local);
    void set_team(char team);
    void on_destroy();
    void update(float dt);
    void draw();
    void set_walk(bool walking, float angle);
    void play_sample(const std::string & name);
    float get_angle();
    void set_position(const vec3 & pos, bool interpolate = true);
    void set_velocity(const vec3 & vel, bool interpolate = true);
    void kill();
    void do_jump();
    void update_spectator(float dt);
    void shoot(const vec3 & dir);
    void shoot(const vec3 & pos, const vec3 & dir);
};

// Bullet

class Bullet : public World::Bullet
{
public:
    GameScene * scene;

    Bullet(GameScene * scene, const vec3 & pos, const vec3 & dir,
           Character * character);
    void draw();
    void on_hit(World::WorldObject * object);
};

// Rocket

class Rocket : public World::Rocket
{
public:
    GameScene * scene;
    float smoke_time;
    SoundBase * sound;

    Rocket(GameScene * scene, const vec3 & pos, const vec3 & dir,
           Character * character);
    void draw();
    void on_destroy();
    void update(float dt);
};

// CubeParticle

class CubeParticle : public World::WorldObject
{
public:
    RGBColor color;
    float x_rot, y_rot, z_rot;
    float ttl;

    CubeParticle(GameScene * scene, const vec3 & pos, const RGBColor & col,
                 float vel = 210.0f);
    void update(float dt);
    void draw();
};

// Intel

class Intel : public World::Intel
{
public:
    float rotation;

    Intel(GameScene * scene, const vec3 & pos, char team);
    void update(float dt);
    void draw();
    void draw_overhead(const vec3 & pos);
};

// Crate

class Crate : public World::Crate
{
public:
    float rotation;

    Crate(GameScene * scene, const vec3 & pos);
    void update(float dt);
    void draw();
};

class WhirlCube
{
public:
    vec3 dir;
    float z;
    float time;

    WhirlCube(const vec3 & dir);
    void update(float dt);
    void draw(char team);
};

class SpawnPoint : public World::SpawnPoint
{
public:
    std::vector<WhirlCube*> cubes;

    SpawnPoint(GameScene * scene, const vec3 & pos, char team);
    void update(float dt);
    void draw();
};

class LazerTracer : public World::WorldObject
{
public:
    vec3 start_pos;
    float ttl, size;
    Character * character;
    GameScene * scene;

    LazerTracer(GameScene * scene, const vec3 & pos, const vec3 & dir,
                Character * character);
    void update(float dt);
    quat get_rotation();
    void draw();
    void on_destroy();
    void on_collision(WorldObject * obj, btManifoldPoint & pt, bool is_a);
};

class SmokeParticle : public World::WorldObject
{
public:
    float size;
    float ttl;
    RGBColor color;

    SmokeParticle(GameScene * scene, const vec3 & pos,
                  const RGBColor & color = smoke_color,
                  float min_size = 5.0f, float max_size = 20.0f);
    void update(float dt);
    void draw();
};

class Train : public World::Train
{
public:
    GameScene * scene;
    SoundBase * sound;
    float smoke_time;

    Train(GameScene * scene, const vec3 & pos);
    void update(float dt);
    void draw();
    void on_destroy();
};

class ModelEntity : public World::ModelEntity
{
public:
    GameScene * scene;

    ModelEntity(GameScene * scene);
    void draw();
    void on_destroy();
};

// GameScene

class ClientHost;
class ClientPeer;
class HUD;

typedef fast_map<unsigned char, Character*> CharacterMap;
typedef fast_map<unsigned char, World::Entity*> EntityMap;

class GameScene : public Scene
{
public:
    bool connected;
    float crosshair_x, crosshair_y;
    float crosshair_scale;
    int old_x, old_y;
    World::World * world;
    HUD * hud;
    Character * character;
    ClientHost * client;
    ClientPeer * peer;
    float view_x, view_y;
    std::string status;
    float movement_send_timer;
    CharacterMap characters;
    EntityMap entities;
    bool orienting;
    float rotate_z, rotate_z_interp;
    float rotate_x, rotate_x_interp;
    float zoom, zoom_interp;
    mat4 hud_projection_matrix, projection_matrix, view_matrix, mvp, inverse_mvp;
    vec3 eye, center;
    vec4 viewport;
    float character_x, character_y;
    bool up, down, left, right;
    float heal_time;
    float hit_time;
    float shake_x, shake_y;
    float debug_time;
    Map * map;
    vec3 ray_pos, ray_dir;
    World::WorldObject * ray_obj;
    vec3 ray_hit, ray_normal;
    bool can_build;
    vec3 build_min, build_max, build_mid;
    World::ObjectList hide_objects;
    bool debug_ao;
    bool locked_camera;
    float add_center_z;
    Image * skybox_image;

    GameScene(GameManager * manager);
    void on_start();
    void on_end();
    void on_server_info(ServerInfo & info);
    void update(float dt);
    void set_locked_camera(bool value);
    void update_locked_camera(float dt);
    void update_camera(float dt);
    void update_build(float dt);
    void draw();
    void draw_world();
    void show_hit();
    void show_heal();
    void on_mouse_key(int button, bool value);
    void on_mouse_move(int x, int y);
    void on_mouse_scroll(double dx, double dy);
    void on_key(int button, bool value);
    void on_text(const std::string & text);
    bool on_quit();
    void disconnect();
    void send_chat(const std::string & text, int type = GLOBAL_CHAT);
    void send_chat(const std::string & text, const std::string & sound, 
                   int type = GLOBAL_CHAT);
    void send_command(const std::string & name);
    void set_team(char team);
    Character * get_character(unsigned char id);
    World::Entity * get_entity(unsigned char id);
    void create_character(SpawnPacket & packet);
    void destroy_character(unsigned char id);
    void send_movement();
    void send_tool();
    void update_walk_angle(bool send_movement);
    void melt_model(VoxelModel * file, const vec3 & pos, float z_angle,
                    float scale, float vel = 210.0f, float block_size = 2.0f);
    bool is_tool(ToolType id);
    bool has_team_select();
};

// ClientHost

class ClientHost : public NetworkHost
{
public:
    GameScene * scene;

    ClientHost(GameScene * scene);
    NetworkPeer * create_peer(ENetPeer * peer, bool is_client);
};

// ClientPeer

class ClientPeer : public NetworkPeer
{
public:
    unsigned char id;
    GameScene * scene;

    ClientPeer(GameScene * scene, NetworkHost * host, ENetPeer * peer);
    bool on_connect();
    void on_disconnect(unsigned int reason);
    void on_packet(Packet & packet);
};

#endif // ST_SCENES_GAME_H
