#include "menu.h"

#include <math.h>
#include <iostream>

#include "assets.h"
#include "constants.h"
#include "draw.h"
#include "fonts.h"
#include "include_gl.h"
#include "manager.h"
#include "scene.h"
#include "scenes.h"

#define MENU_BUTTON_WIDTH 320
#define MENU_BUTTON_HEIGHT 50
#define MENU_PAD 15
#define MENU_BUTTON_X (WINDOW_WIDTH / 2 - MENU_BUTTON_WIDTH / 2)
#define MENU_BUTTON_Y1 295
#define MENU_BUTTON_Y2 (MENU_BUTTON_Y1 - MENU_BUTTON_HEIGHT - MENU_PAD)
#define MENU_BUTTON_Y3 (MENU_BUTTON_Y2 - MENU_BUTTON_HEIGHT - MENU_PAD)

#define ZNEAR -400
#define ZFAR 150

MainMenu::MainMenu(GameManager * manager)
: Scene(manager),
  play_button("Connect to official server", MENU_BUTTON_X, MENU_BUTTON_Y1,
              MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT),
  local_button("Connect to local server", MENU_BUTTON_X, MENU_BUTTON_Y2,
              MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT),
  credits_button("Credits", MENU_BUTTON_X, MENU_BUTTON_Y3,
                 MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT)
{
    controls.add(&play_button);
    controls.add(&credits_button);
    controls.add(&local_button);
    play_button.set_callback(&on_play, this);
    local_button.set_callback(&on_local, this);
    credits_button.set_callback(&on_credits, this);
}

void MainMenu::on_start()
{
    next_scene = NULL;
    fade_value = 0.0f;
    audio->play_music("Subversive Takedown");
    manager->set_hidden_mouse();
}

void MainMenu::update(float dt)
{
    controls.update(dt);
    if (next_scene != NULL && fade_value < 1.0f)
        fade_value += 5.0f * dt;
    if (fade_value >= 1.0f)
        set_scene(next_scene);
}

void MainMenu::draw_character(char team, float x)
{
    float angle;
    VoxelAnimationData * anim;
    float r = 45;
    if (team == TEAM_1) {
        angle = -r;
        anim = characterwalk_animation;
    } else {
        angle = -180.0f + r;
        anim = character2walk_animation;
    }

    glPushMatrix();
    glTranslatef(x, WINDOW_HEIGHT / 2 - 200.0f, 0.0);
    glRotatef(-50.0f, 1.0, 0.0, 0.0);
    glScalef(12.0f, 12.0f, 12.0f);
    glRotatef(angle, 0.0, 0.0, 1.0);
    anim->items[0]->draw();
    anim->items[0]->get_point("gun")->translate();
    glScalef(0.975f, 0.975f, 0.975f);
    if (team == TEAM_1)
        rpg_model->draw();
    else
        gun_model->draw();
    glPopMatrix();
}

void MainMenu::draw()
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, WINDOW_WIDTH, 0, WINDOW_HEIGHT, ZNEAR, ZFAR);
    glMatrixMode(GL_MODELVIEW);
    glDisable(GL_DEPTH_TEST);
    glLoadIdentity();
    glClear(GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    // draw scrolling tile
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, tile_image->tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    float time = get_time();
    glColor4f(sin_wave(time, 0.7f, 1.0f, 1.2f),
              sin_wave(time, 0.7f, 1.0f, 2.3f),
              sin_wave(time, 0.7f, 1.0f, 3.1f), 1.0f);
    glBegin(GL_QUADS);
    float x_offset = get_time() * 0.2f;
    float y_offset = 0.0f;
    float size = 1.5f;
    float x1, y1, x2, y2, x3, y3, x4, y4;
    float r = 0.0f;
    x1 = cos_deg(r) * size + x_offset;
    y1 = sin_deg(r) * size + y_offset;
    x2 = cos_deg(r + 90) * size + x_offset;
    y2 = sin_deg(r + 90) * size + y_offset;
    x3 = cos_deg(r + 180) * size + x_offset;
    y3 = sin_deg(r + 180) * size + y_offset;
    x4 = cos_deg(r + 270) * size + x_offset;
    y4 = sin_deg(r + 270) * size + y_offset;
    glTexCoord2f(x1, y1); glVertex2i(0, 0);
    glTexCoord2f(x2, y2); glVertex2i(WINDOW_WIDTH, 0);
    glTexCoord2f(x3, y3); glVertex2i(WINDOW_WIDTH, WINDOW_WIDTH);
    glTexCoord2f(x4, y4); glVertex2i(0, WINDOW_WIDTH);
    glEnd();
    glDisable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);

    setup_lighting();

    // draw characters
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glPushMatrix();
    float x_div = WINDOW_WIDTH / 10.0f;
    draw_character(TEAM_1, x_div);
    draw_character(TEAM_2, x_div * 9.0f);
    glPopMatrix();

    // draw table
    glPushMatrix();
    glTranslatef(WINDOW_WIDTH * 0.5f, WINDOW_HEIGHT * 0.5f - 90, 0.0f);
    glRotatef(-90.0f, 1.0, 0.0, 0.0);
    glScalef(30.0f, 30.0f, 30.0f);
    table_model->draw();
    glPopMatrix();

    glDisable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);

    // use other values because we use an orthogonal projection
    manager->post_process(2.0f, 15.0f);

    // draw grey overlay
    draw_rect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, 255, 255, 255, 40);

    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    float scale = 1.0f + sin(time * 3) * 0.03f;
    splash_image->draw(WINDOW_WIDTH / 2, 500, 0.0,
                       scale, scale);

    controls.draw();

    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    fixedsys_font->draw_right("For other servers, see mp2.dk/st",
        WINDOW_WIDTH - 20, 20);/**/

    if (next_scene != NULL)
        draw_rect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT,
                  0, 0, 0, int(std::min(fade_value, 1.0f) * 255));

    int x, y;
    manager->get_mouse_pos(x, y);
    glColor4f(1.0f, 1.0f, 1.0f, 0.8f);
    cursor_image->draw(x, y - cursor_image->height * 2, 0.0f,
        2.0f, 2.0f);
}

void MainMenu::on_play(ControlBase * control, void * data)
{
    MainMenu * self = (MainMenu*)data;
    self->next_scene = game_scene;
    self->play_select();
}

void MainMenu::on_local(ControlBase * control, void * data)
{
    MainMenu * self = (MainMenu*)data;
    self->manager->host_ip = "127.0.0.1";
    self->manager->host_port = 7777;
    self->next_scene = game_scene;
    self->play_select();
}

void MainMenu::on_credits(ControlBase * control, void * data)
{
    MainMenu * self = (MainMenu*)data;
    self->next_scene = credits_scene;
    self->play_select();
}

void MainMenu::on_mouse_move(int x, int y)
{
    if (next_scene != NULL)
        return;
    controls.on_mouse_move(x, y);
}

void MainMenu::on_mouse_key(int button, bool value)
{
    if (next_scene != NULL)
        return;
    controls.on_mouse_key(button, value);
}
