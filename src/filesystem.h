#ifndef ST_FILESYSTEM_H
#define ST_FILESYSTEM_H

#include <iostream>

#include <physfs.h>

typedef PHYSFS_File FSFile;

#define file_write PHYSFS_write
#define file_read PHYSFS_read
#define file_tell PHYSFS_tell
#define file_seek PHYSFS_seek
#define file_init PHYSFS_init
#define file_deinit PHYSFS_deinit
#define file_length PHYSFS_fileLength
#define file_close PHYSFS_close

inline void print_filesystem_error()
{
    std::cout << "Filesystem error: " << PHYSFS_getLastError() << std::endl;
}

inline void print_filesystem_error(const char * msg = "")
{
    std::cout << "Filesystem error (" << msg << "): " << PHYSFS_getLastError()
        << std::endl;
}

inline int file_set_write_dir(const char * dir)
{
    int ret = PHYSFS_setWriteDir(dir);
    if (ret == 0)
        print_filesystem_error(dir);
    return ret;
}

inline int file_mount(const char * dir, const char * mountpoint, int append)
{
    int ret = PHYSFS_mount(dir, mountpoint, append);
    if (ret == 0)
        print_filesystem_error(dir);
    return ret;
}

inline FSFile * file_open_read(const char * name)
{
    FSFile * fp = PHYSFS_openRead(name);
    if (fp == NULL)
        print_filesystem_error(name);
    return fp;
}

inline FSFile * file_open_write(const char * name)
{
    FSFile * fp = PHYSFS_openWrite(name);
    if (fp == NULL)
        print_filesystem_error(name);
    return fp;
}

#endif // ST_FILESYSTEM_H
