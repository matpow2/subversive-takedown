#ifndef ST_SERVER_H
#define ST_SERVER_H

#include "idpool.h"
#include "network.h"
#include "script.h"
#include "world.h"

class ServerPeer;
class ServerHost;

using World::Entity;

class ServerWorld : public World::World
{
public:
    ServerHost * host;

    ServerWorld(ServerHost * host);
    void on_entity_create(Entity * entity);
    void on_entity_change(Entity * entity);
    void on_entity_destroy(Entity * entity);
};

class Character : public World::Character
{
public:
    float hp;
    ServerPeer * peer;
    ScriptCharacter * script;
    bool has_jumped;
    float heal_delay;
    vec3 old_velocity;

    Character(World::World * world, ServerPeer * peer, const std::string & name,
              const vec3 & pos, char team);
    void set_hp(float value, bool show_hit = true, Character * by = NULL);
    void set_score(int value);
    void update(float dt);
    void kill();
    void kill(Character * by);
    bool use_tool();
    void update_ammo(Tool * tool);
    void add_ammo(ToolType id, int value);
    void reset();
    void play_sound(const std::string & name, bool music = false);
    void heal();
    void set_team(char team);
    void set_tool(ToolType i);
    void apply_damage(float damage, World::WorldObject * obj);
};

class Bullet : public World::Bullet
{
public:
    Bullet(World::World * world, const vec3 & pos, const vec3 & dir,
           Character * character);
    void on_hit(World::WorldObject * object);
};

class Rocket : public World::Rocket
{
public:
    Rocket(World::World * world, const vec3 & pos, const vec3 & dir,
           Character * character);
    void on_destroy();
};

class Intel : public World::Intel
{
public:
    vec3 start_pos;
    ServerHost * host;
    bool has_reset;
    float reset_time;

    Intel(ServerHost * host, const vec3 & pos, char team);
    void on_set(World::Character * character);
    void on_collision(World::WorldObject * obj, btManifoldPoint & pt,
                      bool is_a);
    void update(float dt);
    void reset();
};

class CrateData
{
public:
    MapObject * obj;
    bool spawned;

    CrateData(MapObject * obj);
};

class Crate : public World::Crate
{
public:
    ServerHost * host;
    CrateData * data;

    Crate(ServerHost * host, const vec3 & pos, CrateData * data = NULL);
    void on_collision(World::WorldObject * obj, btManifoldPoint & pt,
                      bool is_a);
    void on_destroy();
};

class SpawnPoint : public World::SpawnPoint
{
public:
    ServerHost * host;

    SpawnPoint(ServerHost * host, const vec3 & pos, char team);
    void on_collision(World::WorldObject * obj, btManifoldPoint & pt,
                      bool is_a);
};

class BlockObject : public World::BlockObject
{
public:
    BlockObject(ServerHost * host, const vec3 & pos, const RGBColor & color);
    void apply_damage(float value, World::WorldObject * src);
};

class MasterPeer;

typedef fast_map<std::string, World::WorldObject*> NamedObjects;

typedef std::vector<CrateData> Crates;

class ServerHost : public NetworkHost
{
public:
    World::World * world;
    IDPool entity_ids;
    ScriptManager * script;
    bool closed;
    float master_reconnect;
    MasterPeer * master_client;
    NamedObjects named_objects;
    double map_time_end;
    double send_timer;
    Crates crates;
    int max_crates;
    int crate_count;
    float crate_spawn_time;
    float crate_timer;
    bool map_time_enabled;

    ServerHost(int port);
    ~ServerHost();
    void set_map(const std::string & map);
    Map * get_map();
    void connect_master();
    void update_master_data();
    void update_world(float dt);
    NetworkPeer * create_peer(ENetPeer * peer, bool is_client);
    void broadcast_packet(Packet & packet);
    void broadcast_team(Packet & packet, int team);
    float get_map_time();
    void set_map_time(float value);
    void send_chat(const std::string & message);
    void play_sound(const std::string & name, bool music = false);
    void send_world_packet();
    void run();
    void close();

    // util functions
    float get_average_distance(const vec3 & pos, int team);
};

class ServerPeer : public NetworkPeer
{
public:
    unsigned char id;
    bool broadcast;
    Character * character;
    float respawn_time;
    std::string name;

    ServerPeer(NetworkHost * host, ENetPeer * peer);
    bool on_connect();
    void on_disconnect(unsigned int reason);
    void initialize();
    void on_packet(Packet & packet);
    void update(float dt);
    void respawn();
    void respawn(float time);
    void get_spawn_point(vec3 & pos);
    std::string get_name(const std::string & requested);
    void send_chat(const std::string & message);
};

class MasterPeer : public NetworkPeer
{
public:
    bool connected;

    MasterPeer(NetworkHost * host, ENetPeer * peer);
    bool on_connect();
    void on_disconnect(unsigned int reason);
    void on_packet(Packet & packet);
};

#endif
