#include "master.h"

#include <fstream>
#include <sstream>

#include "signalhandler.h"
#include "stringcommon.h"
#include "timer.h"

// #define LOCAL_MASTER

#ifdef LOCAL_MASTER
#define LIST_PATH "list.html"
#else
#define LIST_PATH "/s/mp2.dk/st/servers/list.html"
#endif

static MasterHost * current_host = NULL;

// MasterHost

void close_handler()
{
    current_host->close();
}

MasterHost::MasterHost(int port)
: NetworkHost(port), closed(false)
{
    st_init_time();
    current_host = this;
    set_signal_handler(close_handler);
}

void escape(const std::string & in, std::string & out) {
    out.clear();
    out.reserve(in.size());
    for(size_t pos = 0; pos != in.size(); ++pos) {
        switch(in[pos]) {
            case '&':
                out.append("&amp;");
                break;
            case '\"':
                out.append("&quot;");
                break;
            case '\'':
                out.append("&apos;");
                break;
            case '<':
                out.append("&lt;");
                break;
            case '>':
                out.append("&gt;");
                break;
            default:
                out.append(&in[pos], 1);
                break;
        }
    }
}

void MasterHost::update_servers(float dt)
{
    PeerList::const_iterator it;
    std::stringstream stream;

    stream <<
        "<table>" << std::endl <<
        "<tbody>" << std::endl <<
        "<tr>" << std::endl <<
        "<td>Slots</td>" << std::endl <<
        "<td>Name</td>" << std::endl <<
        "</tr>" << std::endl;

    for (it = peers.begin(); it != peers.end(); it++) {
        MasterPeer * peer = (MasterPeer*)*it;
        peer->update(dt);
        if (!peer->verified)
            return;
        stream <<
            "<tr>" << std::endl <<
            "<td>" << peer->count << "/" << peer->max_players << "</td>" <<
            std::endl << "<td><a href=\"" << peer->identifier << "\">" <<
            peer->escaped_name << "</a></td>" << std::endl;
    }

    stream <<
        "</tbody>" << std::endl <<
        "</table>" << std::endl;

    std::ofstream list(LIST_PATH);
    list << stream.str();
    list.close();
}

void MasterHost::run()
{
    double current_time, next_frame;
    while (!closed) {
        flush(); // send all queued packets
        current_time = st_get_time();
        next_frame = st_get_time() + MASTER_UPDATE_INTERVAL;
        while (st_get_time() <= next_frame)
            update(1);
        float dt = float(st_get_time() - current_time);
        update_servers(dt);
    }
}

NetworkPeer * MasterHost::create_peer(ENetPeer * peer, bool is_client)
{
    return new MasterPeer(this, peer);
}

void MasterHost::close()
{
    closed = true;
}

// MasterPeer

MasterPeer::MasterPeer(NetworkHost * host, ENetPeer * peer)
: NetworkPeer(host, peer), id((unsigned char)peer->incomingPeerID),
  verified(false), count(0), max_players(MAX_CONNECTIONS)
{
    char host_ip[MAXHOSTNAME];
    enet_address_get_host_ip(&peer->address, host_ip, MAXHOSTNAME);
    this->host_ip = std::string(host_ip);
}

bool MasterPeer::on_connect()
{
    std::cout << "Server " << int(id) << " connected." << std::endl;
    return true;
}

void MasterPeer::on_disconnect(unsigned int reason)
{
    MasterHost * host = (MasterHost*)this->host;
}

void MasterPeer::update(float dt)
{

}

void MasterPeer::on_packet(Packet & data)
{
    MasterHost * host = (MasterHost*)this->host;

    switch (data.type) {
        case ADD_SERVER: {
            AddServer & packet = (AddServer&)data;
            // XXX verify
            verified = true;
            name = packet.name;
            escape(name, escaped_name);
            port = packet.port;
            identifier = "st://" + host_ip + ":" + number_to_string(port);
            std::cout << "Added server \"" << name << "\" with identifier " <<
                identifier << std::endl;
            break;
        }
        case SET_SERVER: {
            SetServer & packet = (SetServer&)data;
            count = packet.count;
            break;
        }
    }
}

// main function

int main(int argc, char *argv[])
{
    MasterHost host(MASTER_PORT);
    std::cout << "Running Subversive Takedown master server on "
        << MASTER_PORT << std::endl;
    host.run();
}
