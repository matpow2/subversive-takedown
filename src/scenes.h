#ifndef ST_SCENES_H
#define ST_SCENES_H

extern Scene * main_menu_scene;
extern Scene * game_scene;
extern Scene * credits_scene;

void initialize_scenes(GameManager * manager);

#endif // ST_SCENES_H
