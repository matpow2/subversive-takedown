#ifndef ST_TOOL_H
#define ST_TOOL_H

#include <string>

#include "glm.h"

#ifdef ST_IS_CLIENT
class VoxelModel;
#endif

enum ToolType
{
    PICKAXE_TOOL = 0,
    BLOCK_TOOL = 1,
    HANDGUN_TOOL = 2,
    LAZER_TOOL = 3,
    RPG_TOOL = 4,
    TOOL_MAX = 5
};

class Tool
{
public:
    ToolType id;
    int max_ammo, start_ammo;
    int ammo;
    float use_delay;
    float delay;
#ifdef ST_IS_CLIENT
    VoxelModel * model;
#endif

    Tool(ToolType id, int max_ammo, int start_ammo, float use_delay);
    Tool(ToolType id, float use_delay);
    void reset();
    void set(int ammo);
    virtual std::string get_name();
    bool use();
};

class HandgunTool : public Tool
{
public:
    HandgunTool();
    std::string get_name();
};

class RPGTool : public Tool
{
public:
    RPGTool();
    std::string get_name();
};

class BlockTool : public Tool
{
public:
    BlockTool();
    std::string get_name();
};

class PickaxeTool : public Tool
{
public:
    PickaxeTool();
    std::string get_name();
};

class LazerTool : public Tool
{
public:
    LazerTool();
    std::string get_name();
};

Tool * create_tool(ToolType id);

#endif // ST_TOOL_H
