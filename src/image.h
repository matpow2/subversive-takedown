#ifndef ST_IMAGE_H
#define ST_IMAGE_H

#include <string>

#include "include_gl.h"
#include "path.h"

class Image
{
public:
    float hotspot_x, hotspot_y;
    GLuint tex;
    unsigned char * image;
    int channels;
    int width, height;
    bool filter;

    Image(const std::string & filename, float hot_x = 0.0f, float hot_y = 0.0f,
          bool filter = true);
    ~Image();
    void load();
    void upload_texture();
    void draw(double x, double y, double angle = 0.0,
              double scale_x = 1.0, double scale_y = 1.0, bool flip_x = false,
              bool flip_y = false, GLuint back = 0);
};

Image * load_image(const std::string & name,
                   float hot_x = 0.0f, float hot_y = 0.0f,
                   bool filter = true);

Image * load_image_center(const std::string & name, bool filter = true);

#endif // ST_IMAGE_H
