#ifndef ST_SCRIPT_H
#define ST_SCRIPT_H

#include "types.h"

class ServerHost;
class Character;

typedef struct _object PyObject;

class ScriptCharacter
{
public:
    Character * character;
    PyObject * object;

    ScriptCharacter(Character * character);
    void call_void(const char * name);
    void call_void(const char * name, PyObject * object);
    bool call_handler(const char * name);
    bool call_handler(const char * name, PyObject * object);
    bool call_handler(const char * name, int value);
    bool call_handler(const char * name, const std::string & value);
    bool call_handler(const char * name, const std::string & value,
                      const std::string & value2);
};

class ScriptManager
{
public:
    ServerHost * host;

    // modules
    PyObject * main_mod;
    PyObject * stlib_mod;
    PyObject * server_mod;
    PyObject * base_mod;
    PyObject * constants_mod;

    // objects
    PyObject * server_object;

    ScriptManager(ServerHost * host);
    void call_void(const char * name);
    void call_void(const char * name, float value);
    void call_string_list(const char * name, StringList & items);
    PyObject * get_attr(const char * name);
    void get_string(const char * name, std::string & str);
    int get_int(const char * name);
    bool get_bool(const char * name);
    float get_float(const char * name);
};

#endif // ST_SCRIPT_H
