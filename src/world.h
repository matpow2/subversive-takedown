#ifndef ST_WORLD_H
#define ST_WORLD_H

#include <LinearMath/btAlignedObjectArray.h>
#include <LinearMath/btQuaternion.h>
#include <LinearMath/btVector3.h>
#include <vector>

#include "constants.h"
#include "glm.h"
#include "tool.h"
#include "types.h"
#include "map.h"

// inline glm stuff

inline quat look_at(const vec3 & v)
{
    float z_angle = glm::degrees(atan2(v.y, v.x));
    float y_angle = glm::degrees(asin(-v.z));
    quat q = glm::angleAxis(z_angle, 0.0f, 0.0f, 1.0f);
    q = glm::rotate(q, y_angle, vec3(0.0f, 1.0f, 0.0f));
    return q;
}

inline btVector3 convert_vec(const vec3 & value)
{
    return btVector3(value.x, value.y, value.z);
}

inline vec3 convert_vec(const btVector3 & value)
{
    return vec3(value.x(), value.y(), value.z());
}

inline btQuaternion convert_quat(const quat & q)
{
    return btQuaternion(q.x, q.y, q.z, q.w);
}

inline quat convert_quat(const btQuaternion & q)
{
    return quat(q.w(), q.x(), q.y(), q.z());
}

// Bullet class prototypes
class btBroadphaseInterface;
struct btDbvtBroadphase;
class btCollisionShape;
class btOverlappingPairCache;
class btCollisionDispatcher;
class btConstraintSolver;
struct btCollisionAlgorithmCreateFunc;
class btDefaultCollisionConfiguration;
class btDynamicsWorld;
class btBoxShape;
class btCollisionObject;
class btRigidBody;
class btUniformScalingShape;
class btMotionState;
class btBvhTriangleMeshShape;
class btManifoldPoint;
class btCompoundShape;

class Tool;

namespace World {

class CollisionShape
{
public:
    btCollisionShape * shape;

    virtual btCollisionShape * get_shape(vec3 & off);
    ~CollisionShape();
};

class BoxShape : public CollisionShape
{
public:
    vec3 offset;

    BoxShape(const vec3 & min, const vec3 & max);
    BoxShape(float size);
    btCollisionShape * get_shape(vec3 & off);
};

class CapsuleShape : public CollisionShape
{
public:
    vec3 offset;

    CapsuleShape(float radius, float height);
    btCollisionShape * get_shape(vec3 & off);
};

typedef fast_map<float, btCollisionShape*> CachedShapes;

class ModelShape : public CollisionShape
{
public:
    btBoxShape * box_shape;

    ModelShape(VoxelFile * voxel, float scale);
    ~ModelShape();
};

class World;

enum ObjectType
{
    PROP,
    BLOCK,
    BULLET,
    ROCKET,
    CHARACTER,
    PARTICLE,
    ENTITY,
    NODE,
    ABSTRACT
};

enum EntityType
{
    INTEL = 0,
    SPAWN = 1,
    TRAIN = 2,
    MODEL = 3,
    CRATE = 4
};

#define BIT(x) (1<<(x))

enum CollisionType
{
    COL_NONE = 0,
    COL_NONE_MASK = 0,
    COL_PROP = BIT(0),
    COL_BULLET = BIT(1),
    COL_CHARACTER = BIT(2),
    COL_RAY = BIT(3),
    COL_OTHER = BIT(4),
    COL_ENTITY = BIT(5),
    COL_BULLET_MASK = COL_PROP | COL_CHARACTER,
    COL_CHARACTER_MASK = COL_PROP | COL_BULLET | COL_RAY | COL_ENTITY,
    COL_ENTITY_MASK = COL_CHARACTER,
    COL_PROP_MASK = COL_BULLET | COL_CHARACTER | COL_OTHER | COL_RAY | COL_PROP
};

class WorldObject
{
public:
    World * world;
    ObjectType type;
    bool destroyed;
    btRigidBody * body;
    btCollisionShape * shape;
    btMotionState * state;
    vec3 offset;
    bool collision_callback;

    WorldObject(World * world, ObjectType type);
    virtual void update(float dt) {};
    virtual void on_collision(WorldObject * obj, btManifoldPoint & pt,
                              bool is_a);
    void initialize(CollisionShape * shape, const vec3 & pos, float mass = 0.0f,
                    float friction = 0.5f, float restitution = 0.0f,
                    short int group = COL_OTHER, short int mask = COL_PROP);
    void enable_collision_callback();
    virtual vec3 get_position(bool interpolate = true);
    vec3 get_center(bool interpolate = true);
    void get_aabb(vec3 & min, vec3 & max);
    void set_position(const vec3 & pos, bool interpolate = true);
    vec3 get_velocity();
    vec3 get_transform(const vec3 & v);
    void set_velocity(const vec3 & value);
    void apply_central_impulse(const vec3 & value);
    void set_euler_rotation(float yaw, float pitch, float roll);
    void set_rotation(const quat & r);
    quat get_rotation(bool interpolate = true);
    void set_angular_velocity(const vec3 & vec);
    void set_gravity(const vec3 & vec);
    void set_enabled(bool value);
    void set_contact_response(bool value);
    void destroy();
    virtual void apply_damage(float value, WorldObject * src = NULL);
    virtual void on_destroy();
    virtual void on_reset();
    virtual ~WorldObject();

#ifdef ST_IS_CLIENT
    void multiply_matrix();
    virtual void draw();
#endif
};

class Entity;

class Character : public WorldObject
{
public:
    char team;
    bool is_walking, jump;
    float look_angle, walk_angle;
    bool dead;
    Entity * hold_entity;
    std::string name;
    int score;
    vec3 look_dir;
    bool on_ground;
    Tool * tools[TOOL_MAX];
    Tool * tool;
    float use_delay;

    Character(World * world, const std::string & name, const vec3 & pos,
              char team);
    void update(float dt);
    void remove_entity();
    void set_jump(bool value);
    void do_jump();
    void set_walk(bool walking, float angle);
    void set_look_angle(float value);
    void set_look_direction(const vec3 & pos);
    void get_gun_pos(vec3 & pos);
    bool test_ground(float x, float y, float dist = 5.0f);
    bool test_ground(float dist = 5.0f);
    bool is_moving();
    void set_dead(bool value);
    Tool * get_tool(ToolType id);
    Tool * set_tool(ToolType id);
    void reset();
    bool is_tool(ToolType id);
    bool use_tool();
    void use_pickaxe(const vec3 & pos, const vec3 & dir);
    void hitscan(const vec3 & pos, const vec3 & dir);
    virtual void kill();
    void on_reset();
    void on_collision(WorldObject * obj, btManifoldPoint & pt, bool is_a);
};

class Bullet : public WorldObject
{
public:
    float ttl;
    Character * character;
    vec3 dir;

    Bullet(World * world, const vec3 & pos, const vec3 & dir,
           Character * character);
    void update(float dt);
    void on_collision(WorldObject * obj, btManifoldPoint & pt, bool is_a);
    virtual void on_hit(WorldObject * object);
};

class Rocket : public WorldObject
{
public:
    float ttl;
    Character * character;
    vec3 dir;

    Rocket(World * world, const vec3 & pos, const vec3 & dir,
           Character * character);
    void update(float dt);
    void on_collision(WorldObject * obj, btManifoldPoint & pt, bool is_a);
    quat get_rotation();
};

// entity system

class Entity : public WorldObject
{
public:
    unsigned char id;
    EntityType entity_type;
    Character * character;
    char team;
    bool dynamic;

    Entity(World * world, EntityType type, char team = TEAM_NONE);
    void on_destroy();
    void set_character(Character * character);
    void set_team(char team);
    vec3 get_position(bool interpolate = true);
    void update_state();
    virtual void on_set(Character * character) {};
    virtual void draw_overhead(const vec3 & pos) {};
    virtual bool set_node(Entity * other, bool position = false) {return false;};
    virtual void set_health(int health);
    virtual int get_health();
};

class Intel : public Entity
{
public:
    Intel(World * world, const vec3 & pos, char team = TEAM_NONE);
};

class Crate : public Entity
{
public:
    Crate(World * world, const vec3 & pos);
};

class SpawnPoint : public Entity
{
public:
    SpawnPoint(World * world, const vec3 & pos, char team);
};

class ModelEntity : public Entity
{
public:
    VoxelFile * voxel;
    float scale;
    int health;

    ModelEntity(World * world, VoxelFile * voxel, const vec3 & pos,
                float scale, int health);
    ModelEntity(World * world);
    void on_collision(WorldObject * obj, btManifoldPoint & pt, bool is_a);
    void set_entity(VoxelFile * voxel, const vec3 & pos, float scale);
    void set_health(int health);
    void apply_damage(float value, WorldObject * src);
    int get_health();
};

class DynamicEntity : public Entity
{
public:
    vec3 velocity;

    DynamicEntity(World * world, EntityType type, char team = TEAM_NONE);
    void update(float dt);
    void set_constant_velocity(const vec3 & v);
};

#define TRAIN_SCALE 10.0f

class Train : public DynamicEntity
{
public:
    Train(World * world, const vec3 & pos);
    VoxelFile * get_voxel();
    void update(float dt);
    bool set_node(Entity * entity, bool position);
};

class ModelObject : public WorldObject
{
public:
#ifdef ST_IS_CLIENT
    VoxelModel * model;
    float scale, depth_offset;
    bool hide;
#endif

    ModelObject(World * world, VoxelFile * voxel, const vec3 & pos,
                float scale);
#ifdef ST_IS_CLIENT
    void draw();
    void draw(float alpha);
    void set_hide(bool value);
#endif
};

class BlockObject : public WorldObject
{
public:
    RGBColor color;
    int damage;

    BlockObject(World * world, const vec3 & pos, const RGBColor & color);
    void apply_damage(float amount, WorldObject * src);
    bool is_destroyed();
    size_t get_index();
#ifdef ST_IS_CLIENT
    void draw();
#endif
};

class Node : public WorldObject
{
public:
#ifdef ST_IS_CLIENT
    Image * icon;
#endif

    Node(World * world, const vec3 & pos);
};

// main world class

typedef std::vector<WorldObject*> ObjectList;

class World
{
public:
    int tick_count;
    ObjectList objects;
    Map map;

    btDynamicsWorld * physics;
    btDbvtBroadphase * broadphase;
    btCollisionDispatcher * dispatcher;
    btConstraintSolver * solver;
    btDefaultCollisionConfiguration * config;

    World();
    void add_object(MapObject * obj);
    WorldObject * get_block(unsigned int i);
    void set_map(const std::string & name, bool add_objects = true);
    void update(float dt);
    void on_tick(float dt);
    virtual void on_entity_create(Entity * entity) {};
    virtual void on_entity_change(Entity * entity) {};
    virtual void on_entity_destroy(Entity * entity) {};
    void test_aabb(const vec3 & pos, float size, unsigned short group,
                   unsigned short mask, ObjectList & objects);
    WorldObject * test_ray(const vec3 & start, const vec3 & end,
                           vec3 & hit, vec3 & normal, bool characters = false);
    WorldObject * test_ray(const vec3 & start, const vec3 & end,
                           bool characters = false);
    WorldObject * test_ray_all(const vec3 & start, const vec3 & end);
    WorldObject * test_ray_all(const vec3 & start, const vec3 & end,
                               vec3 & hit, vec3 & normal);
    void test_ray(const vec3 & start, const vec3 & end, ObjectList & objects);
    bool can_place_block(const vec3 & pos);

#ifdef ST_IS_CLIENT
    void draw(const mat4 & mvp);
    void draw();
#endif
};

} // namespace World

#endif // ST_WORLD_H
