#ifndef ST_SHADER_H
#define ST_SHADER_H

#include <string>

#include "include_gl.h"

class Shader
{
public:
    GLuint program;

    Shader(const std::string & vert, const std::string & frag);
    GLuint attach_source(const std::string & path, GLenum type);
    void bind();
    static void unbind();
    void uniform(const char * value, GLfloat f);
    void uniform(const char * value, GLfloat f1, float f2);
    void uniform(const char * value, float f1, float f2, float f3);
    void uniform(const char * value, GLint i);
    GLint get_uniform(const char * value);
};

extern Shader * ssao_shader;
Shader * load_shader();
void load_shaders();

#endif // ST_SHADER_H
