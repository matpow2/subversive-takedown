#ifndef ST_CONSTANTS_H
#define ST_CONSTANTS_H

// application constants
#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600
#define WINDOW_NAME "Subversive Takedown"
#define ST_VERSION 41

#ifndef NDEBUG
#define ST_DEBUG
#endif

// game constants
#define CHARACTER_SPEED 100.0f
#define CROSSHAIR_RANGE 300.0f
#define CROSSHAIR_MULT 0.6f
#define CROSSHAIR_ERROR 0.2f
#define BULLET_SPEED 1000.0f
#define ROCKET_SPEED 1000.0f
#define BULLET_Z 40.0f
#define GRAVITY -600.0f
#define JUMP_VELOCITY 270.0f
#define MAX_VELOCITY -2000.0f
// #define X_ROTATION -50.0f // for orthogonal
#define X_ROTATION -35.0f // for perspective
#define PERSPECTIVE_FOV 50.0f
#define CAMERA_DISTANCE 500.0f // distance from player to camera
#define HIT_DAMAGE 10.0f
#define START_HP 100.0f
#define RESPAWN_TIME 3.0f
#define MAX_BUILD_DISTANCE 180.0f
#define MAX_PICKAXE_DISTANCE 125.0f

#define TEAM_NONE -1
#define TEAM_SPEC 0
#define TEAM_1 1
#define TEAM_2 2
#define TEAM_SINGLE 3

// network constants
#ifdef CONNECT_LOCAL
#define NETWORK_IP "127.0.0.1"
#else
#define NETWORK_IP "mp2.dk"
#endif

#define MAXHOSTNAME 257

#define NETWORK_PORT 7777
#define MAX_CONNECTIONS 32
#define CHANNEL_COUNT 4
#define MOVEMENT_UPDATE_INTERVAL (1.0f / 5.0f)
#define SERVER_UPDATE_INTERVAL (1.0f / 60.0f)
#define SERVER_UPDATE_INTERVAL_INT (int(SERVER_UPDATE_INTERVAL * 1000.0f))
#define SERVER_SEND_INTERVAL (1 / 30.0f)
#define SERVER_SEND_INTERVAL_INT (int(SERVER_SEND_INTERVAL * 1000.0f))
#define ANGLE_INTERPOLATION_SPEED 10.0f
#define LOCAL_ANGLE_INTERPOLATION_SPEED 20.0f
#define POSITION_INTERPOLATION_SPEED 2.0f
#define VELOCITY_INTERPOLATION_SPEED 0.05f
#define RUBBERBAND_DISTANCE 50.0f
#define INTERPOLATE_DISTANCE 20.0f
/*#ifdef ST_IS_CLIENT
#define SIMULATE_LATENCY 0.1 // in s
#endif*/
#define GLOBAL_CHAT 0
#define TEAM_CHAT 1
#define SERVER_MESSAGE 2

// master settings
#define MASTER_HOST NETWORK_IP
#define MASTER_PORT 7778
#define MASTER_UPDATE_INTERVAL 1.0f
#define MASTER_UPDATE_INTERVAL_INT (int(SERVER_UPDATE_INTERVAL * 1000.0f))

// object sizes
#define CHARACTER_XY_SIZE 25.0f
#define CHARACTER_Z_SIZE 60.0f
#define BULLET_SIZE 4.0f

// rendering constants
#define HP_BAR_WIDTH 200.0f
#define SHAKE_RANGE 4.0f
#define HIT_TIME 1.0f
#define HEAL_TIME 1.0f

#endif // ST_CONSTANTS_H
