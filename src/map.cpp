#include "map.h"

#include <iostream>
#include <rapidjson/prettywriter.h>
#include <rapidjson/filestream.h>
#include <rapidjson/document.h>
#include <stdio.h>

#include "path.h"
#include "world.h"

#ifdef ST_IS_CLIENT
#include "assets.h"
#include "constants.h"
#include "include_gl.h"
#include "random.h"
#endif

using namespace rapidjson;

// MapObject

#define OBJECT_SIZE 32

MapObject::MapObject(Map & map, MapObjects * objects)
: objects(objects), scale(0.0f), team(-1), voxel(NULL),
  health(0), map(map)
{
    objects->push_back(this);
}

void MapObject::update()
{
    if (model.empty())
        return;
    voxel = load_voxel(model);
}

VoxelFile * MapObject::get_voxel()
{
    if (model.empty())
        return NULL;
    if (voxel == NULL)
        update();
    return voxel;
}

MapObject * MapObject::clone()
{
    MapObject * obj = new MapObject(this->map, objects);
    obj->type = type;
    obj->scale = scale;
    obj->pos = pos;
    obj->team = team;
    obj->model = model;
    return obj;
}

size_t MapObject::get_index()
{
    return std::find(objects->begin(), objects->end(), this) - objects->begin();
}

void MapObject::set_list(MapObjects * new_objects)
{
    objects->erase(std::remove(objects->begin(), objects->end(), this),
                  objects->end());
    new_objects->push_back(this);
    objects = new_objects;
}

MapObject::~MapObject()
{
    objects->erase(std::remove(objects->begin(), objects->end(), this),
                   objects->end());
}

#ifdef ST_IS_CLIENT

Image * MapObject::get_icon()
{
    if (icon.empty())
        return NULL;
    return load_image_center(icon);
}

#endif // ST_IS_CLIENT

// Map

Map::Map()
{
}

Map::Map(const std::string & filename)
{
    load(filename);
}

void Map::initialize()
{
}

Map::~Map()
{
}

void read_vec3(const Value & ref, vec3 & p)
{
    p.x = ref[0u].get_float();
    p.y = ref[1u].get_float();
    p.z = ref[2u].get_float();
}

void write_vec3(PrettyWriter<FileStream> & writer, const vec3 & p)
{
    writer.start_array();
    writer.put_float(p.x);
    writer.put_float(p.y);
    writer.put_float(p.z);
    writer.end_array();
}

inline void load_objects(Map & map, MapObjects * objects, const Value & data)
{
    Value::ConstValueIterator it;
    for (it = data.begin(); it != data.end(); it++) {
        const Value & obj = *it;
        MapObject * new_object = new MapObject(map, objects);
        new_object->type = obj["type"].get_str();
        read_vec3(obj["pos"], new_object->pos);
        new_object->scale = obj["scale"].get_float(0);
        new_object->model = obj["model"].get_str();
        new_object->team = obj["team"].get_int(-1);
        new_object->health = obj["health"].get_int(0);
        new_object->id = obj["id"].get_str();
        new_object->icon = obj["icon"].get_str();
        new_object->update();
    }
}

inline void clear_objects(MapObjects & objects)
{
    while (!objects.empty()) {
        delete objects[0];
    }
}

void Map::reset(bool objects_only)
{
    clear_objects(objects);
    clear_objects(props);

    if (objects_only)
        return;

    name = "Untitled map";
    short_name = "newmap";
    music = "Subversive Takedown";
    skybox = "skybox";
    timer = 600.0f;
}

void Map::load(const std::string & filename)
{
    reset(true);

    short_name = get_path_basename(filename);

    Document document;
    FSFile * fp = file_open_read(filename.c_str());
    if (!fp) {
        std::cout << "Could not load map: " << filename << std::endl;
        return;
    }
    FileStream stream(fp);
    document.parse_stream<0>(stream);
    file_close(fp);
    if (document.has_parse_error()) {
        std::cout << "Error parsing map: " << filename << std::endl;
        return;
    }
    name = document["name"].get_str();
    music = document["music"].get_str();
    skybox = document["skybox"].get_str();
    description = document["description"].get_str();
    objective1 = document["objective1"].get_str();
    objective2 = document["objective2"].get_str();
    if (skybox.empty())
        skybox = "skybox";
    timer = document["timer"].get_float(600);
    singleplayer = document["singleplayer"].get_bool(false);
    // max_crates = document["max_crates"].get_int(0);

    load_objects(*this, &objects, document["objects"]);
    load_objects(*this, &props, document["props"]);
}

inline void save_objects(const std::string & name, MapObjects & objects,
                         PrettyWriter<FileStream> & writer)
{
    writer.str(name); writer.start_array();

    MapObjects::const_iterator it;
    for (it = objects.begin(); it != objects.end(); it++) {
        MapObject * object = *it;
        writer.start_object();
        writer.str("pos"); write_vec3(writer, object->pos);
        if (!object->type.empty())
            writer.str("type").str(object->type);
        if (!object->model.empty())
            writer.str("model").str(object->model);
        if (object->team != -1)
            writer.str("team").put_int(object->team);
        if (object->health != 0)
            writer.str("health").put_int(object->health);
        if (object->scale != 0.0f)
            writer.str("scale").put_float(object->scale);
        if (!object->id.empty())
            writer.str("id").str(object->id);
        if (!object->icon.empty())
            writer.str("icon").str(object->icon);
        writer.end_object();
    }
    writer.end_array();
}

void Map::save(const std::string & filename)
{
    Document document;
    FSFile * fp = file_open_write(filename.c_str());
    if (!fp) {
        std::cout << "Could not save map: " << filename << std::endl;
        return;
    }
    FileStream stream(fp);
    PrettyWriter<FileStream> writer(stream);
    writer.start_object();
    writer.str("name").str(name);
    writer.str("music").str(music);
    writer.str("skybox").str(skybox);
    writer.str("objective1").str(objective1);
    writer.str("objective2").str(objective2);
    writer.str("description").str(description);
    writer.str("timer").put_float(timer);
    writer.str("singleplayer").put_bool(singleplayer);
    // writer.str("max_crates").put_int(max_crates);

    save_objects("objects", objects, writer);
    save_objects("props", props, writer);
    writer.end_object();

    file_close(fp);
}

MapObject * Map::add_prop(const std::string & name, const vec3 & pos,
                          float scale)
{
    MapObject * new_prop = new MapObject(*this, &props);
    new_prop->pos = pos;
    new_prop->scale = 8.0f;
    new_prop->model = name;
    new_prop->update();
    return new_prop;
}

MapObject * Map::add_object(const std::string & type, const vec3 & pos,
                            float scale)
{
    MapObject * new_object = new MapObject(*this, &objects);
    new_object->pos = pos;
    new_object->type = type;
    new_object->update();
    return new_object;
}