#include "world.h"

#include <btBulletDynamicsCommon.h>
#include <iostream>

#include "constants.h"
#include "voxel.h"
#include "random.h"
#include "mathcommon.h"

#ifdef ST_IS_CLIENT
#include "bulletdebugdraw.h"
#include "draw.h"
static GLDebugDrawer debug_draw;
#endif

namespace World {

// CollisionShape

btCollisionShape * CollisionShape::get_shape(vec3 & off)
{
    off.x = off.y = off.z = 0.0f;
    return shape;
}

CollisionShape::~CollisionShape()
{
    delete shape;
}

// BoxShape

BoxShape::BoxShape(const vec3 & min, const vec3 & max)
{
    offset = (max + min) * 0.5f;
    vec3 size = (max - min) * 0.5f;
    shape = new btBoxShape(btVector3(size.x, size.y, size.z));
}

BoxShape::BoxShape(float size)
{
    size *= 0.5f;
    offset = vec3(0.0f);
    shape = new btBoxShape(btVector3(size, size, size));
}

btCollisionShape * BoxShape::get_shape(vec3 & offset)
{
    offset = this->offset;
    return shape;
}

// CapsuleShape

CapsuleShape::CapsuleShape(float radius, float height)
{
    offset = vec3(0.0f, 0.0f, height * 0.5f + radius);
    shape = new btCapsuleShapeZ(radius, height);
}

btCollisionShape * CapsuleShape::get_shape(vec3 & offset)
{
    offset = this->offset;
    return shape;
}

// ModelShape

ModelShape::ModelShape(VoxelFile * voxel, float scale)
{
    btCompoundShape * shape = new btCompoundShape(true);
    box_shape = new btBoxShape(btVector3(scale, scale, scale) * 0.5f);
    btTransform transform;
    for (int x = 0; x < voxel->x_size; x++)
    for (int y = 0; y < voxel->y_size; y++)
    for (int z = 0; z < voxel->z_size; z++) {
        if (!voxel->is_solid(x, y, z))
            continue;
        // ignore if not an exposed block
        if (voxel->is_solid(x + 1, y, z) &&
            voxel->is_solid(x - 1, y, z) &&
            voxel->is_solid(x, y + 1, z) &&
            voxel->is_solid(x, y - 1, z) &&
            voxel->is_solid(x, y, z + 1) &&
            voxel->is_solid(x, y, z - 1))
            continue;

        transform.setIdentity();
        transform.setOrigin(btVector3(x + voxel->x_offset + 0.5f,
                                      y + voxel->y_offset + 0.5f,
                                      z + voxel->z_offset + 0.5f) * scale);
        shape->addChildShape(transform, box_shape);
    }
    this->shape = shape;
}

ModelShape::~ModelShape()
{
    delete box_shape;
}

// WorldObject

WorldObject::WorldObject(World * world, ObjectType type)
: world(world), destroyed(false), type(type), collision_callback(false)
{
    world->objects.push_back(this);
}

static btEmptyShape empty_shape;

void WorldObject::initialize(CollisionShape * col_shape, const vec3 & in_pos,
                             float mass, float friction, float restitution,
                             short int group, short int mask)
{
    if (col_shape == NULL) {
        shape = &empty_shape;
        offset = vec3(0.0f);
    } else
        shape = col_shape->get_shape(offset);
    btTransform transform;
    transform.setIdentity();
    vec3 pos = in_pos + offset;
    transform.setOrigin(btVector3(pos.x, pos.y, pos.z));

    // rigidbody is dynamic if and only if mass is non zero, otherwise static
    bool dynamic = (mass != 0.f);

    btVector3 local_inertia(0,0,0);
    if (dynamic)
        shape->calculateLocalInertia(mass, local_inertia);

    // using motionstate is recommended, it provides interpolation capabilities,
    // and only synchronizes 'active' objects
    state = new btDefaultMotionState(transform);
    btRigidBody::btRigidBodyConstructionInfo info(mass, state, shape,
        local_inertia);
    info.m_friction = friction;
    info.m_restitution = restitution;
    body = new btRigidBody(info);
    body->setUserPointer(this);
    world->physics->addRigidBody(body, group, mask);
    if (type == ENTITY)
        world->on_entity_create((Entity*)this);
}

void WorldObject::enable_collision_callback()
{
    collision_callback = true;
}

void WorldObject::on_collision(WorldObject * obj, btManifoldPoint & pt,
                               bool is_a)
{

}

void WorldObject::set_enabled(bool value)
{
    if (value)
        body->forceActivationState(ACTIVE_TAG);
    else
        body->forceActivationState(DISABLE_SIMULATION);
}

void WorldObject::set_contact_response(bool value)
{
    if (value)
        body->setCollisionFlags(body->getCollisionFlags() &
            ~btCollisionObject::CF_NO_CONTACT_RESPONSE);
    else
        body->setCollisionFlags(body->getCollisionFlags() |
            btCollisionObject::CF_NO_CONTACT_RESPONSE);
}

vec3 WorldObject::get_center(bool interpolate)
{
    btTransform transform;
    if (interpolate)
        state->getWorldTransform(transform);
    else
        transform = body->getWorldTransform();
    return convert_vec(transform.getOrigin());
}

void WorldObject::get_aabb(vec3 & min, vec3 & max)
{
    btVector3 bmin, bmax;
    body->getAabb(bmin, bmax);
    min = convert_vec(bmin);
    max = convert_vec(bmax);
}

vec3 WorldObject::get_position(bool interpolate)
{
    btTransform transform;
    if (interpolate)
        state->getWorldTransform(transform);
    else
        transform = body->getWorldTransform();
    return convert_vec(transform.getOrigin()) - offset;
}

vec3 WorldObject::get_transform(const vec3 & value)
{
    btTransform transform;
    state->getWorldTransform(transform);
    return convert_vec(transform * convert_vec(value));
}

void WorldObject::set_position(const vec3 & value, bool interpolate)
{
    btTransform transform = body->getWorldTransform();
    transform.setOrigin(convert_vec(value + offset));
    body->setWorldTransform(transform);
    if (!interpolate)
        state->setWorldTransform(transform);
}

void WorldObject::set_velocity(const vec3 & value)
{
    body->setLinearVelocity(convert_vec(value));
}

vec3 WorldObject::get_velocity()
{
    return convert_vec(body->getLinearVelocity());
}

void WorldObject::apply_central_impulse(const vec3 & value)
{
    body->applyCentralImpulse(convert_vec(value));
}

void WorldObject::set_euler_rotation(float x, float y, float z)
{
    btQuaternion q;
    q.setEuler(x, y, z);
    btTransform transform = body->getWorldTransform();
    transform.setRotation(q);
    body->setWorldTransform(transform);
}

quat WorldObject::get_rotation(bool interpolate)
{
    btTransform transform = body->getWorldTransform();
    return convert_quat(transform.getRotation());
}

void WorldObject::set_rotation(const quat & q)
{
    btTransform transform = body->getWorldTransform();
    transform.setRotation(convert_quat(q));
    body->setWorldTransform(transform);
}

void WorldObject::set_angular_velocity(const vec3 & value)
{
    body->setAngularVelocity(convert_vec(value));
}

void WorldObject::set_gravity(const vec3 & v)
{
    body->setGravity(convert_vec(v));
}

void WorldObject::apply_damage(float value, WorldObject * src)
{

}

void WorldObject::destroy()
{
    if (destroyed)
        return;
    destroyed = true;
    collision_callback = false;
    on_destroy();
    world->physics->removeRigidBody(body);
}

void WorldObject::on_destroy()
{

}

void WorldObject::on_reset()
{

}

#ifdef ST_IS_CLIENT
void WorldObject::draw()
{

}
#endif

WorldObject::~WorldObject()
{
    if (!destroyed)
        world->physics->removeRigidBody(body);
    delete state;
    delete body;
}


#ifdef ST_IS_CLIENT
void WorldObject::multiply_matrix()
{
    btTransform transform;
    state->getWorldTransform(transform);
    btScalar m[16];
    transform.getOpenGLMatrix(m);
    glMultMatrixf((GLfloat*)m);
}
#endif

// Character

Character::Character(World * world, const std::string & name, const vec3 & pos,
                     char team)
: WorldObject(world, CHARACTER), look_angle(0.0f), walk_angle(0.0f),
  is_walking(false), dead(false), hold_entity(NULL), score(0), name(name),
  jump(false), team(team), on_ground(false), tool(NULL), use_delay(0.0f)
{
    const float radius = CHARACTER_XY_SIZE / 2;
    static CapsuleShape shape(radius, CHARACTER_Z_SIZE - radius);
    initialize(&shape, pos, 1.0f, 0.0f, 0.0f, COL_CHARACTER,
        COL_CHARACTER_MASK);
    body->setSleepingThresholds(0.0, 0.0);
    body->setAngularFactor(0.0);

    // initialize tools
    for (int i = 0; i < TOOL_MAX; i++) {
        tools[i] = NULL;
    }

    set_tool(HANDGUN_TOOL);
    set_dead(true);

#ifdef ST_IS_SERVER
    enable_collision_callback();
#endif
}

void Character::on_collision(WorldObject * obj, btManifoldPoint & pt, bool is_a)
{
    if (dead)
        return;

    if (pt.getAppliedImpulse() >= 850.0f)
        kill();
}

Tool * Character::get_tool(ToolType id)
{
    if (tools[id] == NULL)
        tools[id] = create_tool(id);
    return tools[id];
}

Tool * Character::set_tool(ToolType id)
{
    tool = get_tool(id);
    return tool;
}

void Character::reset()
{
    for (int i = 0; i < TOOL_MAX; i++) {
        Tool * tool = tools[i];
        if (tool == NULL)
            continue;
        tool->set(0);
    }
    set_velocity(vec3(0.0f, 0.0f, 0.0f));
}

bool Character::is_tool(ToolType id)
{
    if (dead)
        return false;
    return tool->id == id;
}

bool Character::use_tool()
{
    if (dead)
        return false;
#ifdef ST_IS_CLIENT
    if (use_delay > 0.0f)
        return false;
#endif
    if (!tool->use())
        return false;
    use_delay = tool->use_delay;
    return true;
}

void Character::use_pickaxe(const vec3 & pos, const vec3 & dir)
{
    WorldObject * hit = world->test_ray(pos, pos + dir * MAX_PICKAXE_DISTANCE,
        false);
    if (hit == NULL)
        return;
    hit->apply_damage(4.0f);
}

void Character::hitscan(const vec3 & pos, const vec3 & dir)
{
    vec3 hit, normal;
    WorldObject * obj = world->test_ray(pos, pos + dir * 5000.0f,
       hit, normal, true);
    if (obj == NULL)
        return;
    obj->apply_damage(5, this);
}

void Character::update(float dt)
{
    if (dead) {
        set_velocity(vec3(0.0f, 0.0f, 0.0f));
        return;
    }

    if (use_delay > 0.0f)
        use_delay = std::max(0.0f, use_delay - dt);

    if (is_walking) {
        float x_move = CHARACTER_SPEED * cos_deg(walk_angle);
        float y_move = CHARACTER_SPEED * sin_deg(walk_angle);
        set_velocity(vec3(x_move, y_move, get_velocity().z));
    } else {
        set_velocity(vec3(0.0f, 0.0f, get_velocity().z));
    }

    on_ground = test_ground();

    if (jump && on_ground)
        do_jump();
}

bool Character::test_ground(float x, float y, float dist)
{
    vec3 pos = get_position(false) + vec3(x, y, CHARACTER_Z_SIZE);
    vec3 hit, normal;
    return world->test_ray(pos, pos - vec3(0.0f, 0.0f, CHARACTER_Z_SIZE + dist),
        hit, normal) != NULL;
}

bool Character::test_ground(float dist)
{
    const float size = CHARACTER_XY_SIZE / 2.0f - 2.0f;
    return test_ground(0, 0, dist) ||
           test_ground(-size, -size, dist) ||
           test_ground(size, -size, dist) ||
           test_ground(size, size, dist) ||
           test_ground(-size, size, dist);
}

void Character::remove_entity()
{
    if (hold_entity == NULL)
        return;
    hold_entity->set_character(NULL);
}

void Character::set_jump(bool value)
{
    jump = value;
}

void Character::do_jump()
{
    vec3 vel = get_velocity();
    set_velocity(vec3(vel.x, vel.y, JUMP_VELOCITY));
    jump = false;
}

void Character::set_walk(bool walking, float angle)
{
    is_walking = walking;
    walk_angle = angle;
}

void Character::set_look_angle(float angle)
{
    look_angle = angle;
}

void Character::set_look_direction(const vec3 & dir)
{
    look_dir = glm::normalize(dir);
}

void Character::get_gun_pos(vec3 & out_pos)
{
    const static int gun_x = 20;
    const static int gun_y = -10;
    float angle = this->look_angle;
    float co = cos_deg(angle);
    float si = sin_deg(angle);
    vec3 pos = get_position();
    out_pos.x = pos.x + co * gun_x - si * gun_y;
    out_pos.y = pos.y + si * gun_x + co * gun_y;
    out_pos.z = pos.z + BULLET_Z;
}

void Character::set_dead(bool value)
{
    if (dead == value)
        return;
    dead = value;
    if (dead)
        set_gravity(vec3(0.0f));
    else
        set_gravity(vec3(0.0f, 0.0f, GRAVITY));
    set_velocity(vec3(0.0f));
    set_contact_response(!value);
}

void Character::kill()
{
    set_dead(true);
}

void Character::on_reset()
{
    set_dead(true);
    if (hold_entity != NULL)
        hold_entity = NULL;
}

// Bullet

Bullet::Bullet(World * world, const vec3 & pos, const vec3 & dir,
               Character * character)
: WorldObject(world, BULLET), dir(dir), ttl(3.0), character(character)
{
    static BoxShape shape(BULLET_SIZE);
    initialize(&shape, pos + dir * 2.0f, 1.0f, 1.0f, 0.0f, COL_BULLET,
               COL_BULLET_MASK);
    set_contact_response(false);
    set_velocity(dir * BULLET_SPEED);
    body->setGravity(btVector3(0.0f, 0.0f, 0.0f));
    enable_collision_callback();
}

void Bullet::on_collision(WorldObject * obj, btManifoldPoint & pt, bool is_a)
{
    if (obj == character)
        return;
    on_hit(obj);
    destroy();
}

void Bullet::update(float dt)
{
    ttl -= dt;
    if (ttl <= 0)
        destroy();
}

void Bullet::on_hit(WorldObject * object)
{

}

// Rocket

Rocket::Rocket(World * world, const vec3 & pos, const vec3 & dir,
               Character * character)
: WorldObject(world, ROCKET), ttl(7.0), character(character)
{
    static BoxShape shape(BULLET_SIZE);
    initialize(&shape, pos + dir * 4.0f, 1.0f, 1.0f, 0.0f, COL_BULLET,
               COL_BULLET_MASK);
    set_contact_response(false);
    set_velocity(dir * ROCKET_SPEED + vec3(0.0f, 0.0f, 100.0f));
    body->setGravity(btVector3(0.0f, 0.0f, -100.0f));
    enable_collision_callback();
}

quat Rocket::get_rotation()
{
    return look_at(glm::normalize(get_velocity()));
}

void Rocket::on_collision(WorldObject * obj, btManifoldPoint & pt, bool is_a)
{
    if (obj == character)
        return;
    destroy();
}

void Rocket::update(float dt)
{
    ttl -= dt;
    if (ttl <= 0)
        destroy();
}

// Entity

Entity::Entity(World * world, EntityType type, char team)
: WorldObject(world, ENTITY), entity_type(type), character(NULL),
  team(team), dynamic(false)
{
}

void Entity::on_destroy()
{
    if (character != NULL)
        character->hold_entity = NULL;
    character = NULL;
    world->on_entity_destroy(this);
}

void Entity::set_character(Character * character)
{
    if (character == this->character)
        return;
    if (this->character != NULL)
        this->character->hold_entity = NULL;
    on_set(character);
    this->character = character;
    if (character != NULL)
        character->hold_entity = this;
    update_state();
}

void Entity::set_team(char team)
{
    this->team = team;
    update_state();
}

void Entity::update_state()
{
    world->on_entity_change(this);
}

int Entity::get_health()
{
    return 0;
}

void Entity::set_health(int health)
{
}

vec3 Entity::get_position(bool interpolate)
{
    if (character != NULL)
        return character->get_position(interpolate);
    return WorldObject::get_position(interpolate);
}

// Intel

static BoxShape common_entity_shape(vec3(-20.0f, -20.0f, 0.0f),
                                    vec3(20.0f, 20.0f, 20.0f));

Intel::Intel(World * world, const vec3 & pos, char team)
: Entity(world, INTEL, team)
{
    initialize(&common_entity_shape, pos, 0.0f,
        0.5f, 0.0f, COL_ENTITY, COL_ENTITY_MASK);
    set_contact_response(false);
}

// Crate

Crate::Crate(World * world, const vec3 & pos)
: Entity(world, CRATE)
{
    initialize(&common_entity_shape, pos, 0.0f,
        0.5f, 0.0f, COL_ENTITY, COL_ENTITY_MASK);
    set_contact_response(false);
}

// DynamicEntity

DynamicEntity::DynamicEntity(World * world, EntityType type, char team)
: Entity(world, type, team)
{
    dynamic = true;
}

void DynamicEntity::set_constant_velocity(const vec3 & v)
{
    velocity = v;
}

void DynamicEntity::update(float dt)
{
    set_velocity(velocity);
}

// Train

VoxelFile * Train::get_voxel()
{
    static VoxelFile * train_voxel = load_voxel("train");
    return train_voxel;
}

Train::Train(World * world, const vec3 & pos)
: DynamicEntity(world, TRAIN)
{
    initialize(get_voxel()->get_shape(TRAIN_SCALE), pos, 5000.0f, 0.5f, 0.0f,
        COL_PROP, COL_PROP_MASK);
    body->setSleepingThresholds(0.0, 0.0);
    body->setAngularFactor(0.0);
    set_constant_velocity(vec3(0.0f, 0.0f, 0.0f));
}

bool Train::set_node(Entity * node, bool position)
{
    if (node == NULL) {
        set_constant_velocity(vec3(0.0f));
        return true;
    }

    if (position) {
        set_position(node->get_position());
        set_constant_velocity(vec3(0.0f));
        return true;
    }

    vec3 diff = node->get_position() - get_position();
    if (glm::length(diff) < 5.0f) {
        set_constant_velocity(vec3(0.0f));
        return false;
    }
    set_constant_velocity(glm::normalize(diff) * 30.0f);
    return true;
}

void Train::update(float dt)
{
    DynamicEntity::update(dt);
    if (velocity.x == 0.0f && velocity.y == 0.0f)
        return;
    float yaw = glm::radians(glm::roll(get_rotation()));
    yaw += (atan2(velocity.y, velocity.x) - yaw) * dt;
    set_euler_rotation(0.0f, 0.0f, yaw);
}

// SpawnPoint

SpawnPoint::SpawnPoint(World * world, const vec3 & pos, char team)
: Entity(world, SPAWN, team)
{
    initialize(&common_entity_shape, pos, 0.0f,
        0.5f, 0.0f, COL_ENTITY, COL_ENTITY_MASK);
    set_contact_response(false);
}


// ModelObject

ModelObject::ModelObject(World * world, VoxelFile * voxel, const vec3 & pos,
                         float scale)
: WorldObject(world, PROP)
{
    initialize(voxel->get_shape(scale), pos, 0.0f, 0.5f, 0.0f,
               COL_PROP, COL_PROP_MASK);
#ifdef ST_IS_CLIENT
    model = voxel->get_model();
    this->scale = scale;
    depth_offset = randrange(0.5f, 1.0f);
    hide = false;
#endif
}

#ifdef ST_IS_CLIENT

void ModelObject::set_hide(bool value)
{
    hide = value;
}

void ModelObject::draw(float alpha)
{
    if (hide && alpha == 1.0f)
        return;
    glEnable(GL_POLYGON_OFFSET_FILL);
    glPolygonOffset(depth_offset, depth_offset);
    glPushMatrix();
    vec3 pos = get_position();
    glTranslatef(pos.x, pos.y, pos.z);
    glScalef(scale, scale, scale);
    if (alpha == 1.0f)
        model->draw();
    else
        model->draw_immediate(alpha);
    glPopMatrix();
    glDisable(GL_POLYGON_OFFSET_FILL);
}

void ModelObject::draw()
{
    draw(1.0f);
}

#endif

// BlockObject

BlockObject::BlockObject(World * world, const vec3 & pos, const RGBColor & col)
: WorldObject(world, BLOCK), damage(0), color(col)
{
    static BoxShape shape(BLOCK_BUILD_SIZE);
    initialize(&shape, pos, 0.0f, 0.5f, 0.0f, COL_PROP, COL_PROP_MASK);
    body->setSleepingThresholds(0.0, 0.0);
    body->setAngularFactor(0.0);
    body->setLinearFactor(btVector3(0, 0, 1));
}

void BlockObject::apply_damage(float amount, WorldObject * src)
{
    damage += int(amount);
}

bool BlockObject::is_destroyed()
{
    return damage >= 6;
}

size_t BlockObject::get_index()
{
    ObjectList::const_iterator it;
    size_t i = 0;
    for (it = world->objects.begin(); it != world->objects.end(); it++) {
        WorldObject * obj = *it;
        if (obj == this)
            break;
        if (obj->type == BLOCK)
            i++;
    }
    return i;
}

#ifdef ST_IS_CLIENT
void BlockObject::draw()
{
    RGBColor col = color;
    col.multiply(1.0f - damage * 0.075f);
    glPushMatrix();
    multiply_matrix();
    draw_cube(0.0f, 0.0f, 0.0f, BLOCK_BUILD_SIZE, col.r, col.g, col.b, 255);
    glPopMatrix();
}
#endif

// Node

Node::Node(World * world, const vec3 & pos)
: WorldObject(world, NODE)
{
    initialize(&common_entity_shape, pos, 0.0f, 0.5f, 0.0f,
        COL_ENTITY, COL_ENTITY_MASK);
    set_contact_response(false);
}

// ModelEntity

ModelEntity::ModelEntity(World * world, VoxelFile * voxel, const vec3 & pos,
                         float scale, int health)
: Entity(world, MODEL), voxel(NULL), health(health)
{
    set_entity(voxel, pos, scale);
}

ModelEntity::ModelEntity(World * world)
: Entity(world, MODEL), voxel(NULL), scale(1.0f), health(0)
{

}

void ModelEntity::set_entity(VoxelFile * voxel, const vec3 & pos, float scale)
{
    if (this->voxel != NULL)
        return;
    this->voxel = voxel;
    this->scale = scale;
    initialize(voxel->get_shape(scale), pos, 0.0f, 0.5f, 0.0f,
               COL_PROP, COL_PROP_MASK);
#ifdef ST_IS_SERVER
    enable_collision_callback();
#endif
}

void ModelEntity::apply_damage(float value, WorldObject * src)
{
#ifdef ST_IS_CLIENT
    return;
#else
    health -= int(value);
    if (health <= 0) {
        destroy();
        return;
    }
    world->on_entity_change(this);
#endif
}

void ModelEntity::on_collision(WorldObject * obj, btManifoldPoint & pt,
                               bool is_a)
{
    if (pt.getAppliedImpulse() < 400.0f)
        return;
    float damage = (pt.getAppliedImpulse() - 400.0f) * 0.5f;
    apply_damage(damage, obj);
}

void ModelEntity::set_health(int health)
{
    this->health = health;
}

int ModelEntity::get_health()
{
    return health;
}

// World

void _on_tick(btDynamicsWorld * physics, btScalar timeStep)
{
    World * world = (World*)physics->getWorldUserInfo();
    world->on_tick(float(timeStep));
}

World::World()
: tick_count(0)
{
    // collision configuration contains default setup for memory, collision
    // setup
    config = new btDefaultCollisionConfiguration();

    // use the default collision dispatcher. For parallel processing you can
    // use a diffent dispatcher (see Extras/BulletMultiThreaded)
    dispatcher = new btCollisionDispatcher(config);
    broadphase = new btDbvtBroadphase();

    // the default constraint solver. For parallel processing you can use a
    // different solver (see Extras/BulletMultiThreaded)
    solver = new btSequentialImpulseConstraintSolver;

    physics = new btDiscreteDynamicsWorld(dispatcher, broadphase, solver,
                                          config);
#ifdef ST_IS_CLIENT
    physics->setDebugDrawer(&debug_draw);
#endif
    physics->setGravity(btVector3(0, 0, GRAVITY));
    physics->setInternalTickCallback(_on_tick, this);
}

void World::update(float dt)
{
    physics->stepSimulation(dt, 4);
}

struct ClosestRayCallback : public btCollisionWorld::ClosestRayResultCallback
{
    bool ignore_hidden;

    ClosestRayCallback(const btVector3 & start, const btVector3 & end,
                       bool ignore_hidden)
    : btCollisionWorld::ClosestRayResultCallback(start, end),
      ignore_hidden(ignore_hidden)
    {
    }

#ifdef ST_IS_CLIENT
    bool needsCollision(btBroadphaseProxy * proxy0) const
    {
        if (ignore_hidden) {
            btCollisionObject * v = (btCollisionObject*)proxy0->m_clientObject;
            WorldObject * obj = (WorldObject*)v->getUserPointer();
            if (obj != NULL && obj->type == PROP) {
                ModelObject * model = (ModelObject*)obj;
                if (model->hide)
                    return false;
            }
        }
        return btCollisionWorld::RayResultCallback::needsCollision(proxy0);
    }
#endif
};

struct ClosestAllRayCallback : public btCollisionWorld::ClosestRayResultCallback
{
    ClosestAllRayCallback(const btVector3 & start, const btVector3 & end)
    : btCollisionWorld::ClosestRayResultCallback(start, end)
    {
    }

    bool needsCollision(btBroadphaseProxy * proxy0) const
    {
        return true;
    }
};

WorldObject * World::test_ray(const vec3 & start, const vec3 & end,
                              vec3 & pos, vec3 & normal, bool characters)
{
    btVector3 bt_start = convert_vec(start);
    btVector3 bt_end = convert_vec(end);

    ClosestRayCallback callback(bt_start, bt_end, characters);
    callback.m_collisionFilterGroup = COL_RAY;
    callback.m_collisionFilterMask = COL_PROP;
    if (characters)
        callback.m_collisionFilterMask |= COL_CHARACTER;

    physics->rayTest(bt_start, bt_end, callback);

    if(!callback.hasHit())
        return NULL;

    pos = convert_vec(callback.m_hitPointWorld);
    normal = convert_vec(callback.m_hitNormalWorld);

    return (WorldObject*)callback.m_collisionObject->getUserPointer();
}

WorldObject * World::test_ray(const vec3 & start, const vec3 & end,
                              bool characters)
{
    vec3 hit, normal;
    return test_ray(start, end, hit, normal, characters);
}

WorldObject * World::test_ray_all(const vec3 & start, const vec3 & end,
                                  vec3 & pos, vec3 & normal)
{
    btVector3 bt_start = convert_vec(start);
    btVector3 bt_end = convert_vec(end);

    ClosestAllRayCallback callback(bt_start, bt_end);
    physics->rayTest(bt_start, bt_end, callback);

    if(!callback.hasHit())
        return NULL;

    pos = convert_vec(callback.m_hitPointWorld);
    normal = convert_vec(callback.m_hitNormalWorld);

    return (WorldObject*)callback.m_collisionObject->getUserPointer();
}

WorldObject * World::test_ray_all(const vec3 & start, const vec3 & end)
{
    vec3 hit, normal;
    return test_ray_all(start, end, hit, normal);
}

void World::test_ray(const vec3 & start, const vec3 & end,
                     ObjectList & objects)
{
    btVector3 bt_start = convert_vec(start);
    btVector3 bt_end = convert_vec(end);

    btCollisionWorld::AllHitsRayResultCallback callback(bt_start, bt_end);
    callback.m_collisionFilterGroup = COL_RAY;
    callback.m_collisionFilterMask = COL_PROP;

    physics->rayTest(bt_start, bt_end, callback);

    WorldObject * obj;
    ObjectList::const_iterator it;
    for (int i = 0; i < callback.m_collisionObjects.size(); i++) {
        obj = (WorldObject*)callback.m_collisionObjects[i]->getUserPointer();
        if (obj->type != PROP)
            continue;
        for (it = objects.begin(); it != objects.end(); it++) {
            if (*it == obj)
                goto end_loop;
        }
        objects.push_back(obj);
end_loop:
        continue;
    }
}

/*void World::test_sweep(WorldObject * obj, const vec3 & end,
                       ObjectList & objects)
{
    btVector3 bt_start = convert_vec(obj->get_position());
    btVector3 bt_end = convert_vec(end);

    btCollisionWorld::AllHitsRayResultCallback callback(bt_start, bt_end);
    callback.m_collisionFilterGroup = COL_RAY;
    callback.m_collisionFilterMask = COL_PROP;

    physics->rayTest(bt_start, bt_end, callback);

    WorldObject * obj;
    ObjectList::const_iterator it;
    for (int i = 0; i < callback.m_collisionObjects.size(); i++) {
        obj = (WorldObject*)callback.m_collisionObjects[i]->getUserPointer();
        if (obj->type != PROP)
            continue;
        for (it = objects.begin(); it != objects.end(); it++) {
            if (*it == obj)
                goto end_loop;
        }
        objects.push_back(obj);
end_loop:
        continue;
    }
}*/

void World::on_tick(float dt)
{
    tick_count++;
    if (tick_count == -1)
        tick_count++;

    // contact manifolds
    int manifolds = dispatcher->getNumManifolds();
    for (int i = 0; i < manifolds; i++) {
        btPersistentManifold * manifold = dispatcher->getManifoldByIndexInternal(i);
        btCollisionObject * obj_a = (btCollisionObject*)manifold->getBody0();
        btCollisionObject * obj_b = (btCollisionObject*)manifold->getBody1();
        WorldObject * obj_a2 = (WorldObject*)obj_a->getUserPointer();
        WorldObject * obj_b2 = (WorldObject*)obj_b->getUserPointer();

        if (obj_a2 == NULL || obj_b2 == NULL)
            continue;
        if (!obj_a2->collision_callback && !obj_b2->collision_callback)
            continue;
        if (obj_a2->destroyed || obj_b2->destroyed)
            continue;

        int contacts = manifold->getNumContacts();
        for (int j = 0; j < contacts; j++) {
            btManifoldPoint & pt = manifold->getContactPoint(j);

            if (obj_a2->collision_callback) {
                obj_a2->on_collision(obj_b2, pt, true);
            }

            if (obj_b2->collision_callback) {
                obj_b2->on_collision(obj_a2, pt, false);
            }

            if (obj_a2->destroyed || obj_b2->destroyed)
                break;
        }
    }

    // iterate objects
    ObjectList iter_objects = objects;
    ObjectList::iterator it;
    for (it = iter_objects.begin(); it != iter_objects.end(); it++) {
        WorldObject * object = *it;
        if (object->destroyed) {
            objects.erase(std::remove(objects.begin(), objects.end(), object),
                objects.end());
            delete object;
            continue;
        }
        object->update(dt);
    }
}

void World::add_object(MapObject * obj)
{
    ModelObject * new_obj;
    if (obj->voxel != NULL)
        new_obj = new ModelObject(this, obj->voxel, obj->pos, obj->scale);
    else
        return;
}

struct ContactSensorCallback : public btCollisionWorld::ContactResultCallback
{
    bool has_collided;

    ContactSensorCallback()
    : btCollisionWorld::ContactResultCallback(), has_collided(false)
    {
    }

    btScalar addSingleResult(btManifoldPoint& cp,
        const btCollisionObjectWrapper* colObj0, int partId0, int index0,
        const btCollisionObjectWrapper* colObj1, int partId1, int index1)
    {
        has_collided = true;
        return 0;
    }
};

btCollisionObject * get_block_object()
{
    float size = BLOCK_BUILD_SIZE * 0.5f - 0.1f;
    btCollisionShape * shape = new btBoxShape(btVector3(size, size, size));
    btCollisionObject * obj = new btCollisionObject();
    obj->setCollisionShape(shape);
    return obj;
}

bool World::can_place_block(const vec3 & pos)
{
    static btCollisionObject * box_obj = get_block_object();
    btTransform transform;
    transform.setIdentity();
    transform.setOrigin(convert_vec(pos));
    box_obj->setWorldTransform(transform);
    ContactSensorCallback callback;
    callback.m_collisionFilterGroup = COL_PROP;
    callback.m_collisionFilterMask = COL_PROP_MASK;
    physics->contactTest(box_obj, callback);
    return !callback.has_collided;
}

WorldObject * World::get_block(unsigned int index)
{
    ObjectList::const_iterator it;
    unsigned int i = 0;
    for (it = objects.begin(); it != objects.end(); it++) {
        if ((*it)->type != BLOCK)
            continue;
        if (i == index)
            return *it;
        i++;
    }
    return NULL;
}

inline WorldObject * resolve_proxy(const btCollisionObjectWrapper * v)
{
    return (WorldObject*)v->m_collisionObject->getUserPointer();
}

struct AABBSensorCallback : public btCollisionWorld::ContactResultCallback
{
    ObjectList & objects;
    btCollisionObject * obj;

    AABBSensorCallback(btCollisionObject * obj, ObjectList & objects)
    : btCollisionWorld::ContactResultCallback(), objects(objects), obj(obj)
    {
    }

    btScalar addSingleResult(btManifoldPoint& cp,
        const btCollisionObjectWrapper * colObj0, int partId0, int index0,
        const btCollisionObjectWrapper * colObj1, int partId1, int index1)
    {
        WorldObject * col;
        if (colObj0->m_collisionObject == obj)
            col = resolve_proxy(colObj1);
        else
            col = resolve_proxy(colObj0);
        ObjectList::const_iterator it;
        for (it = objects.begin(); it != objects.end(); it++) {
            if (*it == col)
                return 0;
        }
        objects.push_back(col);
        return 0;
    }
};

void World::test_aabb(const vec3 & pos, float size, unsigned short group,
                      unsigned short mask, ObjectList & objects)
{
    size *= 0.5f;
    btCollisionShape * shape = new btBoxShape(btVector3(size, size, size));
    btCollisionObject * obj = new btCollisionObject();
    obj->setCollisionShape(shape);
    btTransform transform;
    transform.setIdentity();
    transform.setOrigin(convert_vec(pos));
    obj->setWorldTransform(transform);
    AABBSensorCallback callback(obj, objects);
    callback.m_collisionFilterGroup = group;
    callback.m_collisionFilterMask = mask;
    physics->contactTest(obj, callback);
    delete obj;
    delete shape;
}

void World::set_map(const std::string & name, bool add_objects)
{
    // iterate objects
    ObjectList iter_objects = objects;
    objects.clear();
    ObjectList::iterator it1;
    for (it1 = iter_objects.begin(); it1 != iter_objects.end(); it1++) {
        WorldObject * obj = *it1;
        if (obj->type == CHARACTER) {
            obj->on_reset();
            objects.push_back(obj);
            continue;
        }
        delete obj;
    }

    map.load(get_map_path(name));

    if (!add_objects)
        return;

    MapObjects::const_iterator it2;
    for (it2 = map.props.begin(); it2 != map.props.end(); it2++) {
        MapObject * obj = *it2;
        add_object(*it2);
    }
}

#ifdef ST_IS_CLIENT

struct FrustumRenderer : btDbvt::ICollide
{
    ObjectList & particles;

    FrustumRenderer(ObjectList & particles)
    : particles(particles)
    {
    }

    void Process(const btDbvtNode* leaf)
    {
        btBroadphaseProxy * proxy=(btBroadphaseProxy*)leaf->data;
        btRigidBody * body = (btRigidBody*)proxy->m_clientObject;
        WorldObject * obj = (WorldObject*)body->getUserPointer();
        if (obj->destroyed)
            return;
        if (obj->type == PARTICLE) {
            particles.push_back(obj);
            return;
        }
        obj->draw();
    }
};

void World::draw(const mat4 & mvp)
{
/*    physics->debugDrawWorld();
    return;*/

    // frustum culling
    btVector3 planes_n[6];
    btScalar planes_o[6];

    planes_n[0] = btVector3(mvp[0][3] - mvp[0][0], 
                            mvp[1][3] - mvp[1][0], 
                            mvp[2][3] - mvp[2][0]); // Right
    planes_n[1] = btVector3(mvp[0][3] + mvp[0][0], 
                            mvp[1][3] + mvp[1][0], 
                            mvp[2][3] + mvp[2][0]); // Left
    planes_n[2] = btVector3(mvp[0][3] - mvp[0][1],
                            mvp[1][3] - mvp[1][1],
                            mvp[2][3] - mvp[2][1]); // Top
    planes_n[3] = btVector3(mvp[0][3] + mvp[0][1],
                            mvp[1][3] + mvp[1][1],
                            mvp[2][3] + mvp[2][1]); // Bottom
    planes_n[4] = btVector3(mvp[0][3] - mvp[0][2],
                            mvp[1][3] - mvp[1][2],
                            mvp[2][3] - mvp[2][2]); // Far
    planes_n[5] = btVector3(mvp[0][3] + mvp[0][2],
                            mvp[1][3] + mvp[1][2],
                            mvp[2][3] + mvp[2][2]); // Near

    planes_o[0] = mvp[3][3] - mvp[3][0]; // Right
    planes_o[1] = mvp[3][3] + mvp[3][0]; // Left
    planes_o[2] = mvp[3][3] - mvp[3][1]; // Top
    planes_o[3] = mvp[3][3] + mvp[3][1]; // Bottom
    planes_o[4] = mvp[3][3] - mvp[3][2]; // Far
    planes_o[5] = mvp[3][3] + mvp[3][2]; // Near

    ObjectList particles;

    FrustumRenderer policy(particles);

    btDbvt::collideKDOP(broadphase->m_sets[1].m_root, planes_n, planes_o, 5,
        policy);
    btDbvt::collideKDOP(broadphase->m_sets[0].m_root, planes_n, planes_o, 5,
        policy);

    ObjectList::const_iterator it;
    for (it = particles.begin(); it != particles.end(); it++) {
        WorldObject * object = *it;
        object->draw();
    }
}

void World::draw()
{
    ObjectList::const_iterator it;
    for (it = objects.begin(); it != objects.end(); it++) {
        WorldObject * object = *it;
        if (object->destroyed)
            continue;
        object->draw();
    }
}

#endif

} // namespace World
