#ifndef ST_SCENE_H
#define ST_SCENE_H

#include <string>

#include "audiomanager.h"
#include "manager.h"

class Scene
{
public:
    GameManager * manager;
    AudioManager * audio;
    double time;

    Scene(GameManager * manager);
    void set_scene(Scene * new_scene);
    void play_select();
    float get_time();
    virtual void on_start() {};
    virtual void on_end() {};
    virtual void update(float dt) = 0;
    virtual void draw() = 0;
    virtual void on_mouse_key(int button, bool action) {}
    virtual void on_mouse_move(int x, int y) {};
    virtual void on_mouse_scroll(double dx, double dy) {};
    virtual void on_key(int button, bool value) {};
    virtual void on_text(const std::string & text) {};
    virtual void on_resize(int w, int h) {};
    virtual bool on_quit()
    {
        return true;
    }
};

#endif // ST_SCENE_H
