#include "voxel.h"

#include <iostream>

#include "world.h"
#include "datastream.h"

#ifdef ST_IS_CLIENT

// VoxelModel

VoxelModel::VoxelModel(VoxelFile * file)
: file(file), value(0)
{
}

void VoxelModel::draw_immediate(float alpha, bool offset)
{
    int x_offset, y_offset, z_offset;
    if (offset) {
        x_offset = file->x_offset;
        y_offset = file->y_offset;
        z_offset = file->z_offset;
    } else
        x_offset = y_offset = z_offset = 0;

    glBegin(GL_QUADS);
    unsigned char alpha_c = (unsigned char)(alpha * 255.0f);
    int x, y, z, i;
    i = 0;
    for (x = 0; x < file->x_size; x++)
    for (y = 0; y < file->y_size; y++)
    for (z = 0; z < file->z_size; z++) {
        unsigned char color = file->get(x, y, z);
        if (color == 255)
            continue;
        RGBColor & color2 = file->palette[color];

        glColor4ub(color2.r, color2.g, color2.b, alpha_c);

        float gl_x, gl_y, gl_z;
        gl_x = x + x_offset + 0.5f;
        gl_y = y + y_offset + 0.5f;
        gl_z = z + z_offset + 0.5f;

        // Left face
        if (!file->is_solid(x, y + 1, z)) {
            glNormal3f(0.0f, 1.0f, 0.0f);
            glVertex3f(gl_x - 0.5f, gl_y + 0.5f, gl_z - 0.5f);
            glVertex3f(gl_x - 0.5f, gl_y + 0.5f, gl_z + 0.5f);
            glVertex3f(gl_x + 0.5f, gl_y + 0.5f, gl_z + 0.5f);
            glVertex3f(gl_x + 0.5f, gl_y + 0.5f, gl_z - 0.5f);
        }

        // Right face
        if (!file->is_solid(x, y - 1, z)) {
            glNormal3f(0.0f, -1.0f, 0.0f);
            glVertex3f(gl_x - 0.5f, gl_y - 0.5f, gl_z - 0.5f); // Top right
            glVertex3f(gl_x + 0.5f, gl_y - 0.5f, gl_z - 0.5f); // Top left
            glVertex3f(gl_x + 0.5f, gl_y - 0.5f, gl_z + 0.5f); // Bottom left
            glVertex3f(gl_x - 0.5f, gl_y - 0.5f, gl_z + 0.5f); // Bottom right
        }

        // Top face
        if (!file->is_solid(x, y, z + 1)) {
            glNormal3f(0.0f, 0.0f, -1.0f);
            glVertex3f(gl_x - 0.5f, gl_y - 0.5f, gl_z + 0.5f); // Bottom left
            glVertex3f(gl_x + 0.5f, gl_y - 0.5f, gl_z + 0.5f); // Bottom right
            glVertex3f(gl_x + 0.5f, gl_y + 0.5f, gl_z + 0.5f); // Top right
            glVertex3f(gl_x - 0.5f, gl_y + 0.5f, gl_z + 0.5f); // Top left
        }

        // Bottom face
        if (!file->is_solid(x, y, z - 1)) {
            glNormal3f(0.0f, 0.0f, 1.0f);
            glVertex3f(gl_x - 0.5f, gl_y - 0.5f, gl_z - 0.5f); // Bottom right
            glVertex3f(gl_x - 0.5f, gl_y + 0.5f, gl_z - 0.5f); // Top right
            glVertex3f(gl_x + 0.5f, gl_y + 0.5f, gl_z - 0.5f); // Top left
            glVertex3f(gl_x + 0.5f, gl_y - 0.5f, gl_z - 0.5f); // Bottom left
        }

        // Right face
        if (!file->is_solid(x + 1, y, z)) {
            glNormal3f(1.0f, 0.0f, 0.0f);
            glVertex3f(gl_x + 0.5f, gl_y - 0.5f, gl_z - 0.5f); // Bottom right
            glVertex3f(gl_x + 0.5f, gl_y + 0.5f, gl_z - 0.5f); // Top right
            glVertex3f(gl_x + 0.5f, gl_y + 0.5f, gl_z + 0.5f); // Top left
            glVertex3f(gl_x + 0.5f, gl_y - 0.5f, gl_z + 0.5f); // Bottom left
        }

        // Left Face
        if (!file->is_solid(x - 1, y, z)) {
            glNormal3f(-1.0f, 0.0f, 0.0f);
            glVertex3f(gl_x - 0.5f, gl_y - 0.5f, gl_z - 0.5f); // Bottom left
            glVertex3f(gl_x - 0.5f, gl_y - 0.5f, gl_z + 0.5f); // Bottom right
            glVertex3f(gl_x - 0.5f, gl_y + 0.5f, gl_z + 0.5f); // Top right
            glVertex3f(gl_x - 0.5f, gl_y + 0.5f, gl_z - 0.5f); // Top left
        }
    }
    glEnd();
}

void VoxelModel::update(bool force)
{
    if (!force && value != 0)
        return;
    glNewList(value, GL_COMPILE);
    draw_immediate();
    glEndList();
}

void VoxelModel::draw()
{
    if (value == 0) {
        value = glGenLists(1);
        update();
    }
    glDisable(GL_TEXTURE_2D);
    glCallList(value);
}

VoxelModel::~VoxelModel()
{
    glDeleteLists(value, 1);
}

ReferencePoint * VoxelModel::get_point(const std::string & name)
{
    return file->get_point(name);
}

#endif // ST_IS_CLIENT

// ReferencePoint

ReferencePoint::ReferencePoint(const std::string & name,int x, int y, int z)
: name(name), x(x), y(y), z(z)
{

}

#ifdef ST_IS_CLIENT

void ReferencePoint::translate()
{
    glTranslatef(float(x) + 0.01f, float(y) + 0.01f, float(z) + 0.01f);
}

#endif // ST_IS_CLIENT

// VoxelFile

VoxelFile::VoxelFile()
: data(NULL)
{
#ifdef ST_IS_CLIENT
    model = NULL;
#endif
}

VoxelFile::VoxelFile(FSFile * fp)
: data(NULL)
{
#ifdef ST_IS_CLIENT
    model = NULL;
#endif
    load_fp(fp);
}

VoxelFile::VoxelFile(const std::string & filename)
: data(NULL)
{
#ifdef ST_IS_CLIENT
    model = NULL;
#endif
    load(filename);
}

VoxelFile::VoxelFile(int x_size, int y_size, int z_size)
: x_offset(0), y_offset(0), z_offset(0), data(NULL)
{
#ifdef ST_IS_CLIENT
    model = NULL;
#endif
    reset(x_size, y_size, z_size);
}

#ifdef ST_IS_CLIENT
void VoxelFile::update_model()
{
    if (model == NULL)
        return;
    model->update();
}
#endif

void VoxelFile::reset(int x_size, int y_size, int z_size)
{
    this->x_size = x_size;
    this->y_size = y_size;
    this->z_size = z_size;
    if (data != NULL)
        delete[] data;
    data = new unsigned char[x_size * y_size * z_size];
    memset(data, 255, x_size * y_size * z_size);
    points.clear();
    update_box();
}

void VoxelFile::add_point(const std::string & name,
                          int x, int y, int z)
{
    points.push_back(ReferencePoint(name, x, y, z));
}

void VoxelFile::remove_point(size_t i)
{
    points.erase(points.begin() + i);
}

ReferencePoint * VoxelFile::get_point(const std::string & name)
{
    // XXX use fast_map
    ReferencePoints::iterator it;
    for (it = points.begin(); it != points.end(); it++) {
        if ((*it).name == name)
            return &(*it);
    }
    return NULL;
}

ReferencePoint * VoxelFile::get_point(int i)
{
    if (i < 0 || i >= int(points.size()))
        return NULL;
    return &points[i];
}

void VoxelFile::resize(int x1, int y1, int z1, int new_x, int new_y, int new_z)
{
    if (x1 == 0 && y1 == 0 && z1 == 0 &&
        x_size == new_x && y_size == new_y && z_size == new_z)
        return;

    unsigned char * new_data = new unsigned char[new_x * new_y * new_z];
    int nx, ny, nz;
    for (int x = x1; x < x1 + new_x; x++)
    for (int y = y1; y < y1 + new_y; y++)
    for (int z = z1; z < z1 + new_z; z++) {
        unsigned char c;
        if (x < 0 || x >= this->x_size ||
            y < 0 || y >= this->y_size ||
            z < 0 ||z >= this->z_size)
            c = VOXEL_AIR;
        else
            c = get(x, y, z);
        nx = x - x1;
        ny = y - y1;
        nz = z - z1;
        new_data[nz + ny * new_z + nx * new_z * new_y] = c;
    }
    delete[] data;
    data = new_data;
    this->x_size = new_x;
    this->y_size = new_y;
    this->z_size = new_z;
    this->x_offset += x1;
    this->y_offset += y1;
    this->z_offset += z1;
    update_box();
}

void VoxelFile::scale(float sx, float sy, float sz)
{
    if (sx == 1.0f && sy == 1.0f && sz == 1.0f)
        return;

    int new_x = int(x_size * sx);
    int new_y = int(y_size * sy);
    int new_z = int(z_size * sz);

    unsigned char * new_data = new unsigned char[new_x * new_y * new_z];

    for (int x = 0; x < new_x; x++)
    for (int y = 0; y < new_y; y++)
    for (int z = 0; z < new_z; z++) {
        int x2 = int(x / sx);
        int y2 = int(y / sy);
        int z2 = int(z / sz);
        new_data[z + y * new_z + x * new_z * new_y] = get(x2, y2, z2);
    }
    delete[] data;
    data = new_data;
    x_size = new_x;
    y_size = new_y;
    z_size = new_z;
    x_offset = int(x_offset * sx);
    y_offset = int(y_offset * sy);
    z_offset = int(z_offset * sz);
    update_box();
}

void VoxelFile::set_offset(int new_x, int new_y, int new_z)
{
    x_offset = new_x;
    y_offset = new_y;
    z_offset = new_z;
    update_box();
}

void VoxelFile::update_box()
{
    vec3 size = vec3(x_size, y_size, z_size);
    vec3 offset = vec3(x_offset, y_offset, z_offset);
    min = offset;
    max = offset + size;
}

void VoxelFile::optimize()
{
    int x1, y1, z1, x2, y2, z2, x, y, z;

    // min coordinates

    for (x1 = 0; x1 < x_size; x1++)
    for (y = 0; y < y_size; y++)
    for (z = 0; z < z_size; z++)
        if (is_solid_fast(x1, y, z))
            goto x1_end;
x1_end:

    for (y1 = 0; y1 < y_size; y1++)
    for (x = 0; x < x_size; x++)
    for (z = 0; z < z_size; z++)
        if (is_solid_fast(x, y1, z))
            goto y1_end;
y1_end:

    for (z1 = 0; z1 < z_size; z1++)
    for (x = 0; x < x_size; x++)
    for (y = 0; y < y_size; y++)
        if (is_solid_fast(x, y, z1))
            goto z1_end;
z1_end:

    // max coordinates

    for (x2 = x_size - 1; x2 >= x1; x2--)
    for (y = y1; y < y_size; y++)
    for (z = z1; z < z_size; z++)
        if (is_solid_fast(x2, y, z))
            goto x2_end;
x2_end:

    for (y2 = y_size - 1; y2 >= y1; y2--)
    for (x = x1; x < x_size; x++)
    for (z = z1; z < z_size; z++)
        if (is_solid_fast(x, y2, z))
            goto y2_end;
y2_end:

    for (z2 = z_size - 1; z2 >= z1; z2--)
    for (x = x1; x < x_size; x++)
    for (y = y1; y < y_size; y++)
        if (is_solid_fast(x, y, z2))
            goto z2_end;
z2_end:

    resize(x1, y1, z1, x2 - x1 + 1, y2 - y1 + 1, z2 - z1 + 1);
}

void VoxelFile::rotate()
{
    int new_x = y_size;
    int new_y = x_size;
    int new_z = z_size;

    unsigned char * new_data = new unsigned char[new_x * new_y * new_z];
    for (int x = 0; x < x_size; x++)
    for (int y = 0; y < y_size; y++)
    for (int z = 0; z < z_size; z++) {
        int nx = y_size - y - 1;
        int ny = x;
        int nz = z;
        new_data[nz + ny * new_z + nx * new_z * new_y] = get(x, y, z);
    }
    delete[] data;
    data = new_data;
    int x_off = x_offset;
    int y_off = y_offset;
    x_offset = -y_off - y_size;
    y_offset = x_off;
    x_size = new_x;
    y_size = new_y;
    z_size = new_z;
    update_box();
}

bool VoxelFile::load(const std::string & filename)
{
    FSFile * fp = file_open_read(filename.c_str());
    if (!fp) {
        return false;
    }
    load_fp(fp);
    file_close(fp);
    return true;
}

void VoxelFile::load_fp(FSFile * fp)
{
    FileStream stream(fp);
    stream.read((char*)&x_size, 4);
    stream.read((char*)&y_size, 4);
    stream.read((char*)&z_size, 4);
    stream.read((char*)&x_offset, 4);
    stream.read((char*)&y_offset, 4);
    stream.read((char*)&z_offset, 4);
    if (data != NULL)
        delete[] data;
    data = new unsigned char[x_size * y_size * z_size];
    stream.read((char*)data, x_size * y_size * z_size);
    stream.read((char*)palette, 256 * 3);

    // reference points
    points.clear();
    int point_count;
    if (stream.at_end())
        point_count = 0;
    else
        point_count = stream.read_uint8();
    std::string name;
    int x, y, z;
    for (int i = 0; i < point_count; i++) {
        stream.read_string(name);
        x = stream.read_int32();
        y = stream.read_int32();
        z = stream.read_int32();
        points.push_back(ReferencePoint(name, x, y, z));
    }
    update_box();
}

void VoxelFile::save(const std::string & filename)
{
    FSFile * fp = file_open_write(filename.c_str());
    save_fp(fp);
    file_close(fp);
}

void VoxelFile::save_fp(FSFile * fp)
{
    FileStream stream(fp);
    stream.write((char*)&x_size, 4);
    stream.write((char*)&y_size, 4);
    stream.write((char*)&z_size, 4);
    stream.write((char*)&x_offset, 4);
    stream.write((char*)&y_offset, 4);
    stream.write((char*)&z_offset, 4);
    stream.write((char*)data, x_size * y_size * z_size);
    stream.write((char*)palette, 256 * 3);
    stream.write_uint8(points.size());
    ReferencePoints::const_iterator it;
    for (it = points.begin(); it != points.end(); it++) {
        const ReferencePoint & point = *it;
        stream.write_string(point.name);
        stream.write_int32(point.x);
        stream.write_int32(point.y);
        stream.write_int32(point.z);
    }
}

void VoxelFile::reset_shapes()
{
    CachedShapes::iterator it;
    for (it = shapes.begin(); it != shapes.end(); it++) {
        delete it->second;
    }
    shapes.clear();
}

World::ModelShape * VoxelFile::get_shape(float scale)
{
    CachedShapes::const_iterator it = shapes.find(scale);
    if (it != shapes.end())
        return it->second;
    World::ModelShape * shape = new World::ModelShape(this, scale);
    shapes[scale] = shape;
    return shape;
}

#ifdef ST_IS_CLIENT

VoxelModel * VoxelFile::get_model()
{
    if (model != NULL)
        return model;

    model = new VoxelModel(this);
    return model;
}

#endif // ST_IS_CLIENT

static fast_map<std::string, VoxelFile*> voxel_cache;

inline std::string get_voxel_filename(const std::string & name)
{
    return "data/gfx/" + name + ".vox";
}

void reload_voxels()
{
    fast_map<std::string, VoxelFile*>::const_iterator it;
    for (it = voxel_cache.begin(); it != voxel_cache.end(); it++) {
        VoxelFile * voxel = it->second;
        voxel->reset_shapes();
        voxel->load(get_voxel_filename(it->first));
#ifdef ST_IS_CLIENT
        voxel->get_model()->update();
#endif
    }
}

void reload_voxel(const std::string & name)
{
    fast_map<std::string, VoxelFile*>::iterator it = voxel_cache.find(name);
    if (it == voxel_cache.end())
        return;
    VoxelFile * voxel = it->second;
    voxel->reset_shapes();
    voxel->load(get_voxel_filename(it->first));
#ifdef ST_IS_CLIENT
    voxel->get_model()->update();
#endif
}

VoxelFile * load_voxel(const std::string & name)
{
    fast_map<std::string, VoxelFile*>::iterator it = voxel_cache.find(name);
    if (it != voxel_cache.end())
        return it->second;
    std::string filename = get_voxel_filename(name);
    FSFile * fp = file_open_read(filename.c_str());
    if (!fp) {
        return NULL;
    }
    VoxelFile * file = new VoxelFile(fp);
    file->name = name;
    file_close(fp);
    voxel_cache[name] = file;
    return file;
}

#ifdef ST_IS_CLIENT

VoxelModel * load_model(const std::string & name)
{
    VoxelFile * file = load_voxel(name);
    if (file == NULL)
        return NULL;
    return file->get_model();
}

#endif // ST_IS_CLIENT
