#ifndef ST_MANAGER_H
#define ST_MANAGER_H

#include <string>

#include "configfile.h"
#include "include_gl.h"

class Scene;
class AudioManager;

class GameManager
{
public:
    std::string host_ip;
    unsigned short host_port;

    bool fullscreen;
    GLuint color_texture, depth_texture;
    GLuint screen_fbo;
    int window_width, window_height;
    int display_width, display_height;
    int off_x, off_y, x_size, y_size;
    int mouse_x, mouse_y;
    GLFWwindow * window;
    Scene * scene;
    Scene * next_scene;
    AudioManager * audio;
    ConfigFile * config;
    double time;
    bool has_quit;
    bool try_quit;

    GameManager();
    void update();
    void run();
    void draw();
    void quit();
    void set_scene(Scene * scene);
    void transform_mouse_coordinates(int & x, int & y);
    void on_key(int key, bool value);
    void on_mouse_key(int key, bool value);
    void on_mouse_scroll(double dx, double dy);
    void on_mouse_move(int x, int y);
    void on_text(const std::string & text);
    void on_resize(int w, int h);
    void set_normal_mouse();
    void set_hidden_mouse();
    void set_captured_mouse();
    void set_display_size(int w, int h);
    void resize(int w, int h);
    void get_mouse_pos(int & x, int & y);
    void post_process(float near, float far);
};

#endif // ST_MANAGER_H
