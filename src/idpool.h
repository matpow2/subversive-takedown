#ifndef ST_IDPOOL_H
#define ST_IDPOOL_H

#include "types.h"

class IDPool
{
public:
    int start;
    fast_set<int> pool;

    IDPool()
    : start(0)
    {
    }

    int take()
    {
        if (!pool.empty()) {
            fast_set<int>::iterator it = pool.begin();
            int i = *it;
            pool.erase(it);
            return i;
        }
        return start++;
    }

    void put_back(int i)
    {
        pool.insert(i);
    }
};

#endif // ST_IDPOOL_H
