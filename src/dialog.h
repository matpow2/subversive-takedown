#ifndef ST_DIALOG_H
#define ST_DIALOG_H

#ifdef _WIN32
#include <commdlg.h>
#elif !defined(__APPLE__)
#include <stdio.h>
#include <string>
#include <iostream>
#endif // _WIN32/__APPLE__

#ifdef _WIN32

static std::string null_s("\0", 1);

template <typename func>
inline bool get_file_dialog(const char * title, const std::string & filter_name,
                            const std::string & ext, std::string & out, func f)
{
    OPENFILENAME ofn;
    char filename[MAX_PATH];
    filename[0] = '\0';
    ZeroMemory(&ofn, sizeof(ofn));
    ofn.lStructSize = sizeof(ofn);
    ofn.hwndOwner = NULL;
    ofn.lpstrFile = filename;
    ofn.nMaxFile = MAX_PATH;
    std::string filter = filter_name + " (*." + ext + ")" + null_s +
        "*." + ext + null_s + "All (*.*)" + null_s + "*.*" + null_s;
    ofn.lpstrFilter = filter.c_str();
    ofn.lpstrTitle = title;
    ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY |
                OFN_NOCHANGEDIR;
    if (f(&ofn) != 0) {
        out = filename;
        return true;
    }
    return false;
}

#elif !defined(__APPLE__)

// linux
inline std::string exec(const char * cmd)
{
    FILE* pipe = popen(cmd, "r");
    if (!pipe) return "ERROR";
    char buffer[128];
    std::string result = "";
    while(!feof(pipe)) {
        if(fgets(buffer, 128, pipe) != NULL)
            result += buffer;
    }
    pclose(pipe);
    return result;
}

#endif

inline bool get_open_filename(const char * title,
    const std::string & filter_name, const std::string & ext,
    std::string & out)
{
#ifdef _WIN32
    return get_file_dialog(title, filter_name, ext, out, GetOpenFileName);
#elif __APPLE__
    return false;
#else
    out = exec("zenity --file-selection");
    return !out.empty();
#endif
}

inline bool get_save_filename(const char * title,
    const std::string & filter_name, const std::string & ext,
    std::string & out)
{
#ifdef _WIN32
    return get_file_dialog(title, filter_name, ext, out, GetSaveFileName);
#elif __APPLE__
    return false;
#else
    out = exec("zenity --save --file-selection");
    return !out.empty();
#endif
}

#endif // ST_DIALOG_H
