#ifndef ST_EASING_H
#define ST_EASING_H

#include "mathcommon.h"

inline float interpolate(float old, float neww, float amount)
{
    return old + (neww - old) * amount;
}

inline float interpolate_angle(float old, float neww, float amount)
{
    old = mod(old, 360.0f);
    neww = mod(neww, 360.0f);
    float diff = neww - old;
    if (diff > 180.0f)
        diff = diff - 360.0f;
    else if (diff < -180.0f)
        diff = diff + 360.0f;
    return old + diff * amount;
}

inline float ease_out_expo(float t)
{
    return t == 1 ? 1 : -pow(2, -10 * t) + 1;
}

#endif // ST_EASING_H
