#ifndef ST_NETWORK_H
#define ST_NETWORK_H

#include <enet/enet.h>
#include <string>
#include <vector>

#include "constants.h"
#include "packet.h"

enum DisconnectReason 
{
    LOW_VERSION = 1,
    HIGH_VERSION = 2,
    KICKED = 3
};

class NetworkPeer;

typedef std::vector<NetworkPeer*> PeerList;

#ifdef SIMULATE_LATENCY

struct StoredPacket
{
    NetworkPeer * peer;
    Packet * packet;
    double end_time;

    StoredPacket(NetworkPeer * peer, Packet * packet, double current)
    : peer(peer), packet(packet), end_time(current + SIMULATE_LATENCY)
    {}
};

typedef std::vector<StoredPacket> StoredPackets;

#endif // SIMULATE_LATENCY

class NetworkHost
{
public:
    ENetHost * host;
    std::vector<NetworkPeer*> peers;
#ifdef SIMULATE_LATENCY
    StoredPackets stored_packets;
#endif

    NetworkHost(int port = -1);
    void connect(const std::string & ip, int port);
    void update(unsigned int timeout = 0);
    void flush();
    virtual NetworkPeer * create_peer(ENetPeer * peer, bool is_client) = 0;
    ~NetworkHost();
};

class NetworkPeer
{
public:
    NetworkHost * host;
    ENetPeer * peer;

    NetworkPeer(NetworkHost * protocol, ENetPeer * peer);
    ~NetworkPeer();
    void send_packet(Packet & packet);
    void disconnect(bool now = false);

    virtual bool on_connect()
    {
        return true;
    };

    virtual void on_packet(Packet & packet) {};
    virtual void on_disconnect(unsigned int reason) {};
    virtual void on_packet() {};
};

#endif // ST_NETWORK_H
