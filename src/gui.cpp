#include "gui.h"

#include <iostream>
#include <string>

#include "draw.h"
#include "font.h"
#include "fonts.h"
#include "mathcommon.h"
#include "stringcommon.h"

inline bool intersects(int a_x1, int a_y1, int a_x2, int a_y2,
                       int b_x1, int b_y1, int b_x2, int b_y2)
{
    if (a_x2 <= b_x1 || a_y2 <= b_y1 || a_x1 >= b_x2 || a_y1 >= b_y2)
        return false;
    return true;
}

inline bool intersects(int a_x1, int a_y1, int a_x2, int a_y2,
                       int b_x, int b_y)
{
    if (a_x2 <= b_x || a_y2 <= b_y || a_x1 >= b_x || a_y1 >= b_y)
        return false;
    return true;
}

// ControlBase

ControlBase::ControlBase()
: callback(NULL), data(NULL), enabled(true), parent(NULL)
{
}

void ControlBase::set_callback(ControlCallback callback, void * data)
{
    this->callback = callback;
    this->data = data;
}

void ControlBase::set_enabled(bool value)
{
    enabled = value;
}

void ControlBase::fire_callback()
{
    if (callback == NULL)
        return;
    callback(this, data);
}

ControlBase * ControlBase::get_focus()
{
    if (parent == NULL)
        return NULL;
    return parent->focus;
}

bool ControlBase::has_focus()
{
    return get_focus() == this;
}

void ControlBase::set_focus(bool value)
{
    if (parent == NULL)
        return;
    if (value)
        parent->focus = this;
    else
        if (parent->focus == this)
            parent->focus = NULL;
}

// ControlList

ControlList::ControlList()
: focus(NULL), x(0), y(0)
{

}

void ControlList::update(float dt)
{
    ControlVector::const_iterator it;
    for (it = controls.begin(); it != controls.end(); it++)
        (*it)->update(dt);
}

void ControlList::draw()
{
    bool translate = x != 0 || y != 0;
    if (translate) {
        glPushMatrix();
        glTranslatef(float(x), float(y), 0.0f);
    }
    ControlVector::const_iterator it;
    for (it = controls.begin(); it != controls.end(); it++)
        (*it)->draw();
    if (translate)
        glPopMatrix();
}

void ControlList::add(ControlBase * control)
{
    control->parent = this;
    controls.push_back(control);
}

void ControlList::on_mouse_move(int x, int y)
{
    x -= this->x;
    y -= this->y;
    if (focus != NULL) {
        focus->on_mouse_move(x, y);
        return;
    }
    ControlVector::const_iterator it;
    for (it = controls.begin(); it != controls.end(); it++)
        (*it)->on_mouse_move(x, y);
}

void ControlList::on_mouse_key(int key, bool value)
{
    if (focus != NULL) {
        focus->on_mouse_key(key, value);
        return;
    }
    ControlVector::const_iterator it;
    for (it = controls.begin(); it != controls.end(); it++)
        (*it)->on_mouse_key(key, value);
}

void ControlList::on_text(const std::string & text)
{
    if (focus != NULL) {
        focus->on_text(text);
        return;
    }
    ControlVector::const_iterator it;
    for (it = controls.begin(); it != controls.end(); it++)
        (*it)->on_text(text);
}

void ControlList::on_key(int key, bool button)
{
    if (focus != NULL) {
        focus->on_key(key, button);
        return;
    }
    ControlVector::const_iterator it;
    for (it = controls.begin(); it != controls.end(); it++)
        (*it)->on_key(key, button);
}

bool ControlList::has_focus()
{
    return focus != NULL;
}

// Button

#define BUTTON_R 154
#define BUTTON_G 60
#define BUTTON_B 60

Button::Button(const std::string & text, int x, int y, int width, int height)
: ControlBase(), x(x), y(y), width(width), height(height), text(text),
  over(false), time(0.0f)
{
}

void Button::on_mouse_move(int m_x, int m_y)
{
    over = intersects(x, y, x + width, y + height, m_x, m_y);
    if (!over)
        time = 0.0f;
}

void Button::on_mouse_key(int key, bool value)
{
    if (over && key == GLFW_MOUSE_BUTTON_LEFT && value)
        fire_callback();
}

void Button::update(float dt)
{
    if (get_focus() != NULL)
        time = 0.0f;
    else if (over)
        time += dt;
}

void Button::draw()
{
    unsigned char r, g, b;
    r = BUTTON_R;
    g = BUTTON_G;
    b = BUTTON_B;
    if (over) {
        float mul = cos_wave(time, 0.75f, 1.0f, 0.15f);
        mul_color(r, g, b, mul);
    }
    draw_rounded_rect(x, y, x + width, y + height, r, g, b, 225);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    fixedsys_font->draw_center(text,
        float(x + width / 2), float(y + height / 2));
}

// ImageButton

ImageButton::ImageButton(Image * image, int x, int y, int width, int height,
                         float scale)
: ControlBase(), x(x), y(y), width(width), height(height), image(image),
  over(false), time(0.0f), scale(scale)
{

}

void ImageButton::on_mouse_move(int m_x, int m_y)
{
    over = intersects(x, y, x + width, y + height, m_x, m_y);
    if (!over)
        time = 0.0f;
}

void ImageButton::on_mouse_key(int key, bool value)
{
    if (over && key == GLFW_MOUSE_BUTTON_LEFT && value)
        fire_callback();
}

void ImageButton::update(float dt)
{
    if (over)
        time += dt;
}

void ImageButton::draw()
{
    unsigned char r, g, b;
    r = BUTTON_R;
    g = BUTTON_G;
    b = BUTTON_B;
    float mul = 1.0f;
    if (over) {
        mul = cos_wave(time, 0.75f, 1.0f, 0.15f);
        mul_color(r, g, b, mul);
    }
    draw_rect(x, y, x + width, y + height, r, g, b, 225);
    glColor4f(mul, mul, mul, 1.0f);
    image->draw(x + width / 2, y + height / 2, 0.0f, scale, scale);
}

// EditBox

EditBox::EditBox(const std::string & text, int x, int y, int width, int height)
: ControlBase(), x(x), y(y), width(width), height(height), text(text),
  over(false), time(0.0f)
{
}

void EditBox::set_focus(bool value)
{
    bool old_focus = has_focus();
    ControlBase::set_focus(value);
    if (!value && old_focus)
        fire_callback();
}

void EditBox::on_mouse_move(int m_x, int m_y)
{
    over = intersects(x, y, x + width, y + height, m_x, m_y);
    if (!over && !has_focus())
        time = 0.0f;
}

void EditBox::on_mouse_key(int key, bool value)
{
    if (!enabled)
        return;
    if (key != GLFW_MOUSE_BUTTON_LEFT || !value)
        return;
    set_focus(over);
}

void EditBox::set(const std::string & text)
{
    value = text;
}

void EditBox::set(float val)
{
    value = number_to_string(val);
}

void EditBox::set(int val)
{
    value = number_to_string(val);
}

int EditBox::get_int(int def)
{
    return string_to_int(value, def);
}

float EditBox::get_float(float def)
{
    return string_to_float(value, def);
}

void EditBox::on_text(const std::string & text)
{
    if (!has_focus() || !enabled)
        return;
    value += text;
}

void EditBox::on_key(int key, bool v)
{
    if (!v || !has_focus() || !enabled)
        return;
    if (key == GLFW_KEY_BACKSPACE) {
        value = value.substr(0, value.size() - 1);
    } else if (key == GLFW_KEY_ENTER)
        set_focus(false);
}

void EditBox::update(float dt)
{
    if (over || has_focus())
        time += dt;
}

void EditBox::draw()
{
    unsigned char r, g, b;
    r = 230;
    g = 230;
    b = 230;
    bool draw_caret = false;
    if (has_focus() && enabled) {
        mul_color(r, g, b, 1.2f);
        draw_caret = true;
    } else if (over && enabled) {
        float mul = cos_wave(time, 0.75f, 1.0f, 0.15f);
        mul_color(r, g, b, mul);
    }
    draw_rect(x, y, x + width, y + height, r, g, b, 225);
    float text_width;
    if (text.empty())
        text_width = 0.0f;
    else {
        float text_x = float(x + 5);
        float text_y = float(y + 5);
        glColor4f(0.4f, 0.4f, 0.4f, 1.0f);
        fixedsys_font->draw(text, text_x, text_y);
        BoundingBox box = fixedsys_font->get_box(text);
        text_width = box.get_width();
    }
    if (!enabled)
        return;
    float center_x = x + text_width + (width - text_width) * 0.5f;
    glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
    fixedsys_font->draw_center(value, center_x, y + height * 0.5f + 1);
    if (draw_caret && mod(time, 1.0f) < 0.5f) {
        BoundingBox box = fixedsys_font->get_box(value);
        float caret_x = center_x + box.get_width() * 0.5f + 3;
        draw_rect(caret_x, float(y + 3), caret_x + 2, float(y + height - 3),
                  0, 0, 0, 255);
    }
}

// RangeControl

#define RANGE_BAR_WIDTH 12

RangeControl::RangeControl(const std::string & text, int x, int y,
                           int width, int height, bool is_float)
: EditBox(text, x, y, width - RANGE_BAR_WIDTH, height), is_float(is_float),
  step(1.0f)
{

}

void RangeControl::on_text(const std::string & text)
{
    unsigned char c = text[0];
    if (!isdigit(c) && c != '-' && !(is_float && c == '.'))
        return;
    EditBox::on_text(text);
}

void RangeControl::draw()
{
    EditBox::draw();
    int x1 = x + width;
    int x2 = x1 + RANGE_BAR_WIDTH;
    int y2 = int(y + height * 0.5f);
    int y3 = int(y2 + height * 0.5f);
    draw_rect(x1, y, x2, y2, 227, 183, 184, 255);
    draw_rect(x1, y2, x2, y3, 185, 184, 226, 255);
}

void RangeControl::on_mouse_move(int m_x, int m_y)
{
    EditBox::on_mouse_move(m_x, m_y);
    int x1 = x + width;
    int x2 = x1 + RANGE_BAR_WIDTH;
    int y2 = int(y + height * 0.5f);
    int y3 = int(y2 + height * 0.5f);
    over_down = intersects(x1, y, x2, y2, m_x, m_y);
    over_up = intersects(x1, y2, x2, y3, m_x, m_y);
}

void RangeControl::set_step(float value)
{
    step = value;
}

void RangeControl::on_mouse_key(int key, bool value)
{
    if (!enabled)
        return;
    EditBox::on_mouse_key(key, value);
    if (!value)
        return;
    if (key != GLFW_MOUSE_BUTTON_LEFT)
        return;
    step_down = step_up = false;
    if (over_up) {
        step_up = true;
        set(get_float() + step);
    } else if (over_down) {
        step_down = true;
        set(get_float() - step);
    } else
        return;
    fire_callback();
}

// ListItem

ListItem::ListItem(const std::string & text, void * data)
: text(text), data(data)
{
}

// ListControl

ListControl::ListControl(const std::string & text, int x, int y,
                         int width, int height)
: EditBox(text, x, y, width - RANGE_BAR_WIDTH, height), index(-1),
  index_changed(false)
{
}

void ListControl::add_item(const std::string & text, void * data)
{
    items.push_back(ListItem(text, data));
    if (items.size() == 1)
        set_index(0);
}

void ListControl::on_text(const std::string & text)
{
    EditBox::on_text(text);
    if (!has_focus())
        return;
    get_item()->text = this->value;
}

void ListControl::remove_item(ListItem * item)
{
    items.erase(items.begin() + (item - &items.front()));
    set_index(index);
}

ListItem * ListControl::get_item()
{
    if (items.size() == 0)
        return NULL;
    return &items[index];
}

void ListControl::set_index(int value)
{
    index = std::max(0, std::min(value, int(items.size()) - 1));
    if (items.size() == 0)
        set("");
    else
        set(get_item()->text);
}

void ListControl::draw()
{
    EditBox::draw();
    int x1 = x + width;
    int x2 = x1 + RANGE_BAR_WIDTH;
    int y2 = int(y + height * 0.5f);
    int y3 = int(y2 + height * 0.5f);
    draw_rect(x1, y, x2, y2, 227, 183, 184, 255);
    draw_rect(x1, y2, x2, y3, 185, 184, 226, 255);
}

void ListControl::on_mouse_move(int m_x, int m_y)
{
    EditBox::on_mouse_move(m_x, m_y);
    int x1 = x + width;
    int x2 = x1 + RANGE_BAR_WIDTH;
    int y2 = int(y + height * 0.5f);
    int y3 = int(y2 + height * 0.5f);
    over_down = intersects(x1, y, x2, y2, m_x, m_y);
    over_up = intersects(x1, y2, x2, y3, m_x, m_y);
}

void ListControl::on_mouse_key(int key, bool value)
{
    if (!enabled || items.size() == 0)
        return;
    EditBox::on_mouse_key(key, value);
    if (!value)
        return;
    if (key != GLFW_MOUSE_BUTTON_LEFT)
        return;
    if (over_up)
        set_index(index + 1);
    else if (over_down)
        set_index(index - 1);
    else
        return;
    index_changed = true;
    fire_callback();
    index_changed = false;
}

// ContextMenu

#define CONTEXT_MENU_PAD 5

ContextMenu::ContextMenu(int width, int height)
: width(width), height(height), x(0), y(0), over(false)
{
    set_enabled(false);
}

void ContextMenu::add(const std::string & text, ControlCallback callback,
                      void * data)
{
    names.push_back(text);
    callbacks.push_back(callback);
    callback_data.push_back(data);
}

void ContextMenu::initialize()
{
    int count = names.size();
    int x = CONTEXT_MENU_PAD;
    int y = CONTEXT_MENU_PAD;
    int width = this->width - CONTEXT_MENU_PAD * 2;
    int height = (this->height - CONTEXT_MENU_PAD * (1 + count)) / count;

    for (int i = count-1; i >= 0; i--) {
        Button * new_control = new Button(names[i], x, y, width, height);
        new_control->set_callback(callbacks[i], callback_data[i]);
        controls.add(new_control);
        y += height + CONTEXT_MENU_PAD;
    }
}

void ContextMenu::set_pos(int x, int y)
{
    this->x = x - width / 2;
    this->y = y - 2;
    over = intersects(this->x, this->y, this->x + width, this->y + height,
                      m_x, m_y);
}

void ContextMenu::update(float dt)
{
    if (!enabled)
        return;
    controls.update(dt);
}

void ContextMenu::draw()
{
    if (!enabled)
        return;
    glPushMatrix();
    glTranslatef(float(x), float(y), 0.0f);
    draw_rect(0, 0, width, height, 127, 127, 127, 220);
    controls.draw();
    glPopMatrix();
}

void ContextMenu::on_mouse_move(int x, int y)
{
    m_x = x;
    m_y = y;
    x -= this->x;
    y -= this->y;
    over = intersects(0, 0, width, height, x, y);
    controls.on_mouse_move(x, y);
}

void ContextMenu::on_mouse_key(int key, bool value)
{
    if (!enabled)
        return;
    if (!over) {
        m_x = m_y = 0;
        fire_callback();
        return;
    }
    controls.on_mouse_key(key, value);
}

// InputDialog

#define DIALOG_HEADER_SIZE 32
#define DIALOG_PAD 8

InputDialog::InputDialog()
: width(500), height(200), ok_button("OK", 0, 0, 0, 0),
  cancel_button("Cancel", 0, 0, 0, 0), input("", 0, 0, 0, 0)
{
    int pad = DIALOG_PAD;
    int button_width = (width - pad * 3) / 2;
    int button_height = 50;
    ok_button.x = pad;
    ok_button.y = pad;
    ok_button.width = button_width;
    ok_button.height = button_height;
    cancel_button.x = ok_button.x + ok_button.width + pad;
    cancel_button.y = pad;
    cancel_button.width = button_width;
    cancel_button.height = button_height;
    input.x = pad;
    input.y = pad + button_height + pad;
    input.width = width - pad * 2;
    input.height = height - input.y - DIALOG_HEADER_SIZE - pad * 2;

    ok_button.set_callback(&on_ok, this);
    cancel_button.set_callback(&on_cancel, this);

    controls.add(&ok_button);
    controls.add(&cancel_button);
    controls.add(&input);
}

void InputDialog::on_ok(ControlBase * control, void * data)
{
    InputDialog * self = (InputDialog*)data;
    self->end(true);
}

void InputDialog::on_cancel(ControlBase * control, void * data)
{
    InputDialog * self = (InputDialog*)data;
    self->end(false);
}

void InputDialog::end(bool result)
{
    set_focus(false);
    if (!result)
        return;
    fire_callback();
}

const std::string & InputDialog::get()
{
    return input.value;
}

void InputDialog::start(const std::string & title, int xx, int yy)
{
    set_focus(true);
    this->title = title;
    input.set("");
    x = xx - width / 2;
    y = yy - height / 2;
    controls.x = x;
    controls.y = y;
    input.set_focus(true);
}

void InputDialog::draw()
{
    if (!has_focus())
        return;
    draw_rounded_rect(x, y, x + width, y + height, 120, 30, 30,
                      255);
    controls.draw();
    int pad = DIALOG_PAD;
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    fixedsys_font->draw_left_center(title,
        float(x + pad), float(y + height - pad - DIALOG_HEADER_SIZE / 2));
}

void InputDialog::update(float dt)
{
    if (!has_focus())
        return;
    controls.update(dt);
}

void InputDialog::on_key(int button, bool value)
{
    if (!has_focus())
        return;
    if (button == GLFW_KEY_ENTER && value && !input.has_focus()) {
        end(true);
        return;
    }
    controls.on_key(button, value);
}

void InputDialog::on_text(const std::string & text)
{
    if (!has_focus())
        return;
    controls.on_text(text);
}

void InputDialog::on_mouse_move(int x, int y)
{
    if (!has_focus())
        return;
    controls.on_mouse_move(x, y);
}

void InputDialog::on_mouse_key(int key, bool value)
{
    if (!has_focus())
        return;
    controls.on_mouse_key(key, value);
}
