#include "image.h"
#include "types.h"
#include "stb_image.c"

Image::Image(const std::string & filename, float hot_x, float hot_y,
             bool filter)
: hotspot_x(hot_x), hotspot_y(hot_y), tex(0), image(NULL), filter(filter)
{
    image = stbi_load(filename.c_str(), &width, &height, &channels, 4);

    if(image == NULL) {
        printf("Could not load %s\n", filename.c_str());
        return;
    }

    upload_texture();
}

Image::~Image()
{
    if (image != NULL)
        free(image);
    if (tex != 0)
        glDeleteTextures(1, &tex);
    image = NULL;
    tex = 0;
}

int up_scale_image(const unsigned char* const orig, int width, int height,
                   int channels, unsigned char* resampled, int resampled_width,
                   int resampled_height)
{
    float dx, dy;
    int x, y, c;

    if (resampled_width < 2 || resampled_height < 2)
        return 0;

    dx = (width - 1.0f) / (resampled_width - 1.0f);
    dy = (height - 1.0f) / (resampled_height - 1.0f);

    for (y = 0; y < resampled_height; ++y) {
        // find the base y index and fractional offset from that
        float sampley = y * dy;
        int inty = (int)sampley;

        if(inty > height - 2)
            inty = height - 2;

        sampley -= inty;
        for (x = 0; x < resampled_width; ++x) {
            float samplex = x * dx;
            int intx = (int)samplex;
            int base_index;
            // find the base x index and fractional offset from that
            if( intx > width - 2 ) { intx = width - 2; }
            samplex -= intx;
            /*  base index into the original image  */
            base_index = (inty * width + intx) * channels;
            for (c = 0; c < channels; ++c) {
                /*  do the sampling */
                float value = 0.5f;
                value += orig[base_index]
                            *(1.0f-samplex)*(1.0f-sampley);
                value += orig[base_index+channels]
                            *(samplex)*(1.0f-sampley);
                value += orig[base_index+width*channels]
                            *(1.0f-samplex)*(sampley);
                value += orig[base_index+width*channels+channels]
                            *(samplex)*(sampley);
                /*  move to the next channel    */
                ++base_index;
                /*  save the new value  */
                resampled[y*resampled_width*channels+x*channels+c] =
                    (unsigned char)(value);
            }
        }
    }

    // done
    return 1;
}

void Image::upload_texture()
{
    if (tex != 0)
        return;
    else if (image == NULL)
        return;

    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);

    unsigned char * img = image;
    int w, h;

    if (GLEW_ARB_texture_non_power_of_two) {
        w = width;
        h = height;
    } else {
        w = 1;
        h = 1;

        while(w < width)
            w *= 2;
        while(h < height)
            h *= 2;

        if (w != width || h != height) {
            img = (unsigned char*)malloc(channels * w * h);
            up_scale_image(image, width, height, channels, img, w, h);
        }
    }

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA,
        GL_UNSIGNED_BYTE, img);

    free(img);
    if (img != image)
        free(image);
    image = NULL;

    if (filter) {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    } else {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    }
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

void Image::draw(double x, double y, double angle,
                 double scale_x, double scale_y, bool flip_x, bool flip_y,
                 GLuint background)
{
    if (tex == 0) {
        upload_texture();

        if (tex == 0)
            return;
    }

    glPushMatrix();
    glTranslated(x, y, 0.0);
    glRotated(-angle, 0.0, 0.0, 1.0);
    glScaled(scale_x, scale_y, 1.0);
    x -= hotspot_x;
    y -= hotspot_y;
    if (background != 0)
        glActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, tex);
    if (background != 0) {
        glActiveTexture(GL_TEXTURE1);
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, background);
    }

    float tex_coords[8];
    if (flip_x) {
         tex_coords[0] = 1.0; tex_coords[1] = 1.0;
         tex_coords[2] = 0.0; tex_coords[3] = 1.0;
         tex_coords[4] = 0.0; tex_coords[5] = 0.0;
         tex_coords[6] = 1.0; tex_coords[7] = 0.0;
    } else {
         tex_coords[0] = 0.0; tex_coords[1] = 1.0;
         tex_coords[2] = 1.0; tex_coords[3] = 1.0;
         tex_coords[4] = 1.0; tex_coords[5] = 0.0;
         tex_coords[6] = 0.0; tex_coords[7] = 0.0;
    }

    glBegin(GL_QUADS);
    glTexCoord2f(tex_coords[0], tex_coords[1]);
    if (background != 0)
        glMultiTexCoord2f(GL_TEXTURE1, tex_coords[0], tex_coords[1]);
    glVertex2f(-hotspot_x, -hotspot_y);
    glTexCoord2f(tex_coords[2], tex_coords[3]);
    if (background != 0)
        glMultiTexCoord2f(GL_TEXTURE1, tex_coords[2], tex_coords[3]);
    glVertex2f(-hotspot_x + width, -hotspot_y);
    glTexCoord2f(tex_coords[4], tex_coords[5]);
    if (background != 0)
        glMultiTexCoord2f(GL_TEXTURE1, tex_coords[4], tex_coords[5]);
    glVertex2f(-hotspot_x + width, -hotspot_y + height);
    glTexCoord2f(tex_coords[6], tex_coords[7]);
    if (background != 0)
        glMultiTexCoord2f(GL_TEXTURE1, tex_coords[6], tex_coords[7]);
    glVertex2f(-hotspot_x, -hotspot_y + height);
    glEnd();
    if (background != 0) {
        glDisable(GL_TEXTURE_2D);
        glActiveTexture(GL_TEXTURE0);
    }
    glDisable(GL_TEXTURE_2D);
    glPopMatrix();
}

typedef fast_map<std::string, Image*> ImageCache;
static ImageCache image_cache;

Image * load_image(const std::string & name, float hot_x, float hot_y,
                   bool filter)
{
    ImageCache::const_iterator it = image_cache.find(name);
    if (it != image_cache.end())
        return it->second;
    std::string path = std::string("data/gfx/") + name + std::string(".png");
    if (!file_exists(path))
        return NULL;
    Image * img = new Image(path, hot_x, hot_y, filter);
    image_cache[name] = img;
    return img;
}

Image * load_image_center(const std::string & name, bool filter)
{
    Image * img = load_image(name, 0, 0, filter);
    if (img == NULL)
        return NULL;
    img->hotspot_x = img->width / 2.0f;
    img->hotspot_y = img->height / 2.0f;
    return img;
}