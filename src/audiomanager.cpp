#include "audiomanager.h"

#include <iostream>

#include "configfile.h"
#include "path.h"

AudioManager::AudioManager(ConfigFile * config)
: music(NULL), event_sample(NULL), music_volume(0.0f), config(config)
{
    open_audio();
}

void AudioManager::play_music(const std::string & name, float volume)
{
    if (!config->music)
        return;
    if (music_name == name)
        return;
    if (music != NULL)
        delete music;
    music_name = name;
    music = new SoundStream(
        std::string("data/music/") + name + std::string(".ogg"));
    music->set_loop(true);
    music_volume = volume;
    music->set_volume(music_volume);
    music->set_direct_channels(true);
    music->play();
}

void AudioManager::stop_music()
{
    if (music != NULL)
        delete music;
    music = NULL;
    music_name = "";
}

SoundBase * AudioManager::get_sample(const std::string & name)
{
    SoundBase * sound = NULL;
    Sample * sample;
    SampleMap::const_iterator it = cached_samples.find(name);
    if (it == cached_samples.end()) {
        std::string path = std::string("data/sounds/") + name + std::string(".ogg");
        if (get_file_size(path.c_str()) >= 1 * 1024 * 1024) { // 1 mb
            sound = new SoundStream(path);
        } else {
            sample = new Sample(path);
            cached_samples[name] = sample;
        }
    } else {
        sample = it->second;
    }
    if (sound == NULL)
        sound = new Sound(*sample);
    sounds.push_back(sound);
    return sound;
}

SoundBase * AudioManager::play_sample(const std::string & name, float volume)
{
    SoundBase * sound = get_sample(name);
    sound->set_volume(volume);
    sound->set_relative(true);
    sound->set_direct_channels(true);
    sound->play();
    return sound;
}

SoundBase * AudioManager::play_sample(const std::string & name, const vec3 & pos,
                                  float volume)
{
    SoundBase * sound = get_sample(name);
    sound->set_volume(volume);
    sound->set_relative(false);
    sound->set_direct_channels(false);
    sound->set_position(pos.x, pos.y, pos.z);
    sound->set_attenuation(0.5f);
    sound->set_min_distance(100.0f);
    sound->play();
    return sound;
}

void AudioManager::stop_samples()
{
    SoundVector::iterator it;
    for (it = sounds.begin(); it != sounds.end(); it++) {
        (*it)->stop();
    }
}

SoundBase * AudioManager::play_event_sample(const std::string & name)
{
    if (event_sample != NULL)
        event_sample->stop();
    event_sample = play_sample(name);
    if (music != NULL)
        music->set_volume(0.0f);
    return event_sample;
}

void AudioManager::set_listener(const vec3 & position, float rotation)
{
    alListener3f(AL_POSITION, position.x, position.y, position.z);
    rotation = -rotation + 90.0f;
    ALfloat orientation[] = {cos_deg(rotation), sin_deg(rotation), 0.0f,
                             0.0f, 0.0f, 1.0f};
    alListenerfv(AL_ORIENTATION, orientation);
}

void AudioManager::update(float dt)
{
    SoundVector::iterator it = sounds.begin();
    while (it != sounds.end()) {
        SoundBase * sound = *it;
        if (sound->get_status() == Sound::Stopped) {
            if (sound == event_sample) {
                event_sample = NULL;
                if (music != NULL)
                    music->set_volume(music_volume);
            }
            delete sound;
            it = sounds.erase(it);
        } else
            ++it;
    }
}

AudioManager::~AudioManager()
{
    close_audio();
}
