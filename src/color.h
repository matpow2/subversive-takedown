#ifndef ST_COLOR_H
#define ST_COLOR_H

#include <algorithm>

#pragma pack(push)
#pragma pack(1)

class RGBColor
{
public:
    unsigned char r, g, b;

    RGBColor();
    RGBColor(int r, int g, int b);
    RGBColor(unsigned char r, unsigned char g, unsigned char b);
    RGBColor(unsigned int v);
    RGBColor(float r, float g, float b);
    void multiply(float mul);
    void mix(const RGBColor & other, float mul);
#ifdef ST_IS_CLIENT
    void set_current(unsigned char alpha = 255) const;
    void set_current(float alpha) const;
#endif
};

#pragma pack(pop)

const RGBColor team1_color(72, 175, 69);
const RGBColor team2_color(72, 69, 175);
const RGBColor spec_color(127, 127, 127);
const RGBColor server_color(240, 100, 100);
const RGBColor hud_color(20, 20, 20);
const RGBColor white_color(255, 255, 255);
const RGBColor smoke_color(30, 30, 30);
const RGBColor fire_color(255, 119, 0);

const RGBColor & get_team_color(int team);

// r, g, b values are from 0 to 1
// h = [0, 360], s = [0, 1], v = [0, 1]
// if s == 0, then h = -1 (undefined)

void rgb_to_hsv(float r, float g, float b,
                float & h, float & s, float & v);

void hsv_to_rgb(float h, float s, float v,
                float & r, float & g, float & b);

#endif // ST_COLOR_H
