#ifndef ST_TYPES_H
#define ST_TYPES_H

#include <vector>
#include <string>

#if defined(_MSC_VER)

// Define _W64 macros to mark types changing their size, like intptr_t.
#ifndef _W64
#if !defined(__midl) && (defined(_X86_) || defined(_M_IX86)) && _MSC_VER >= 1300
#define _W64 __w64
#else
#define _W64
#endif
#endif

#ifdef _WIN64
   typedef signed __int64 intptr_t;
   typedef unsigned __int64 uintptr_t;
#else
   typedef _W64 signed int intptr_t;
   typedef _W64 unsigned int uintptr_t;
#endif // _WIN64

typedef signed __int64 int64_t;
typedef unsigned __int64 uint64_t;

#include <unordered_map>
#include <unordered_set>
#define fast_map std::tr1::unordered_map
#define fast_set std::tr1::unordered_set

#else

#include <stdint.h>
#include <tr1/unordered_map>
#include <tr1/unordered_set>
#define fast_map std::tr1::unordered_map
#define fast_set std::tr1::unordered_set

#endif // _MSC_VER

typedef std::vector<std::string> StringList;

#endif // ST_TYPES_H
