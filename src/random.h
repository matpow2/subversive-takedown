#ifndef ST_RANDOM_H
#define ST_RANDOM_H

#define _USE_MATH_DEFINES
#include <math.h>
#include <stdlib.h>

#include "glm.h"

inline float randf()
{
    return float(rand()) / float(RAND_MAX);
}

inline float randrange(float a, float b)
{
    return a + (b - a) * randf();
}

inline float randrange(float a)
{
    return a * randf();
}

inline int randint(int a)
{
    return int(randrange(float(a)));
}

inline int randint(int a, int b)
{
    return int(randrange(float(a), float(b)));
}

inline void vecrand(vec3 & a)
{
    // uniform spherical randomization
    float f;
    a.z = -(float)rand()/(float)(RAND_MAX>>1) + 1.0f;
    float rad = rand()*(float(M_PI)*2.0f/float(RAND_MAX));
    a.x = cos(rad);
    a.y = sin(rad);
    f = sqrtf(1.0f - a.z*a.z);
    a.x *= f;
    a.y *= f;
}

#endif // ST_RANDOM_H
