#ifndef ST_TIMER_H
#define ST_TIMER_H

void st_init_time();
double st_get_time();

#endif // ST_TIMER_H
