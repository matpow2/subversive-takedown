#ifndef ST_GUI_H
#define ST_GUI_H

#include <vector>

#include "image.h"

class ControlBase;
class ControlList;

typedef void (*ControlCallback)(ControlBase*, void*);

class ControlBase
{
public:
    ControlCallback callback;
    void * data;
    bool enabled;
    ControlList * parent;

    ControlBase();
    void set_callback(ControlCallback callback, void * data);
    void fire_callback();
    void set_enabled(bool value);
    ControlBase * get_focus();
    bool has_focus();
    void set_focus(bool value);
    virtual void on_mouse_move(int x, int y) {};
    virtual void on_mouse_key(int key, bool value) {};
    virtual void on_text(const std::string & text) {};
    virtual void on_key(int key, bool value) {};
    virtual void update(float dt) {};
    virtual void draw() = 0;
};

typedef std::vector<ControlBase*> ControlVector;

class ControlList
{
public:
    ControlVector controls;
    ControlBase * focus;
    int x, y;

    ControlList();
    void update(float dt);
    void draw();
    void on_mouse_move(int x, int y);
    void on_mouse_key(int key, bool value);
    void on_text(const std::string & text);
    void on_key(int key, bool value);
    void add(ControlBase * control);
    bool has_focus();
};

class Button : public ControlBase
{
public:
    int x, y, width, height;
    bool over;
    std::string text;
    float time;

    Button(const std::string & text, int x, int y, int width, int height);
    void on_mouse_move(int x, int y);
    void on_mouse_key(int key, bool value);
    void update(float dt);
    void draw();
};

class ImageButton : public ControlBase
{
public:
    int x, y, width, height;
    bool over;
    Image * image;
    float time;
    float scale;

    ImageButton(Image * img, int x, int y, int width, int height, float scale);
    void on_mouse_move(int x, int y);
    void on_mouse_key(int key, bool value);
    void update(float dt);
    void draw();
};

class EditBox : public ControlBase
{
public:
    int x, y, width, height;
    bool over;
    std::string text, value;
    float time;

    EditBox(const std::string & text, int x, int y, int width, int height);
    void on_mouse_move(int x, int y);
    void on_mouse_key(int key, bool value);
    void on_text(const std::string & text);
    void set(const std::string & text);
    void set(int value);
    void set(float value);
    void set_focus(bool value);
    int get_int(int def = 0);
    float get_float(float def = 0.0f);
    void on_key(int key, bool value);
    void update(float dt);
    void draw();
};

class RangeControl : public EditBox
{
public:
    bool over_up, over_down;
    bool is_float;
    float step;
    bool step_down, step_up;

    RangeControl(const std::string & text, int x, int y, int width, int height,
                 bool is_float = false);
    void on_text(const std::string & text);
    void draw();
    void on_mouse_move(int x, int y);
    void on_mouse_key(int key, bool value);
    void set_step(float v);
};

class ListItem
{
public:
    std::string text;
    void * data;

    ListItem(const std::string & text, void * data = NULL);
};

typedef std::vector<ListItem> ListItems;

class ListControl : public EditBox
{
public:
    int index;
    bool over_up, over_down;
    ListItems items;
    bool index_changed;

    ListControl(const std::string & text, int x, int y, int width, int height);
    void on_text(const std::string & text);
    void add_item(const std::string & text, void * data = NULL);
    ListItem * get_item();
    void remove_item(ListItem * item);
    void set_index(int index);
    void draw();
    void on_mouse_move(int x, int y);
    void on_mouse_key(int key, bool value);
};

class ContextMenu : public ControlBase
{
public:
    bool over;
    ControlList controls;
    std::vector<std::string> names;
    std::vector<ControlCallback> callbacks;
    std::vector<void*> callback_data;
    int x, y, width, height;
    int m_x, m_y;

    ContextMenu(int width, int height);
    void add(const std::string & text, ControlCallback callback = NULL,
             void * data = NULL);
    void initialize();
    void set_pos(int x, int y);
    void update(float dt);
    void draw();
    void on_mouse_move(int x, int y);
    void on_mouse_key(int key, bool value);
};

class InputDialog : public ControlBase
{
public:
    std::string title;
    int x, y, width, height;
    ControlCallback callback;
    Button ok_button, cancel_button;
    EditBox input;
    ControlList controls;

    InputDialog();
    void start(const std::string & title, int x, int y);
    void on_text(const std::string & text);
    void on_key(int button, bool value);
    void on_mouse_move(int x, int y);
    void on_mouse_key(int key, bool value);
    void draw();
    void update(float dt);
    void end(bool result);
    const std::string & get();
    static void on_ok(ControlBase * control, void * data);
    static void on_cancel(ControlBase * control, void * data);
};

#endif // ST_GUI_H
