#include "animation.h"
#include "image.h"
#include "voxel.h"

#define ST_DECLARE_ASSETS
#include "assets.h"
#undef ST_DECLARE_ASSETS

void initialize_assets()
{
    #define ST_LOAD_ASSETS
    #include "assets.h"
    #undef ST_LOAD_ASSETS
}
