#include "debug.h"

#include <rapidjson/prettywriter.h>
#include <rapidjson/filestream.h>
#include <rapidjson/document.h>

#include "filesystem.h"

using namespace rapidjson;

Debugger::Debugger()
{

}

inline void get_document(Document & document)
{
    FSFile * fp = file_open_read("data/debug.json");
    if (!fp)
        return;
    FileStream stream(fp);
    document.parse_stream<0>(stream);
    file_close(fp);
    if (document.has_parse_error()) {
        return;
    }
}

float Debugger::get_float(const char * name)
{
    Document document;
    get_document(document);
    return document[name].get_float();
}

RGBColor Debugger::get_color(const char * name)
{
    Document document;
    get_document(document);
    Value & ref = document[name];
    RGBColor color;
    color.r = ref[0u].get_int();
    color.g = ref[1u].get_int();
    color.b = ref[2u].get_int();
    return color;
}

vec3 Debugger::get_vec3(const char * name)
{
    Document document;
    get_document(document);
    Value & ref = document[name];
    vec3 v;
    v.x = ref[0u].get_float();
    v.y = ref[1u].get_float();
    v.z = ref[2u].get_float();
    return v;
}

Debugger debug;
