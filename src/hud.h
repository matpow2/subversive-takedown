#ifndef ST_HUD_H
#define ST_HUD_H

#include <string>
#include <vector>

#include "voxel.h"

class GameScene;
class HUD;

class ChatLine
{
public:
    std::string name;
    std::string text;
    HUD * hud;
    float time;
    RGBColor color;

    ChatLine(HUD * hud, const std::string & name, const std::string & text,
             const RGBColor & color);
    void draw();
};

class KillLine
{
public:
    std::string name;
    std::string other;
    VoxelModel * tool;
    float time;

    KillLine(const std::string & name, VoxelModel * tool,
             const std::string & other);
    void draw();
};

class Character;

class HUD
{
public:
    GameScene * scene;
    bool focus, ignore_next;
    std::string current_text;
    std::vector<ChatLine> lines;
    std::vector<KillLine> killfeed;
    float extra_alpha;
    float hp, hp_interp;
    bool show_list;
    int palette_x, palette_y;
    RGBColor saved_color;
    bool show_map;
    double map_time_end;
    int chat_type;
    bool quit;
    bool quick_chat;

    HUD(GameScene * scene);
    void set_hp(int value);
    void on_key(int button, bool value);
    void on_text(const std::string & text);
    void set_map_time(float value);
    float get_map_time();
    void add_kill(Character * character, Character * by);
    void add_chat(Character * character, const std::string & value,
                  int chat_type);
    void add_chat(const std::string & value);
    void set_palette_index(int x, int y);
    void set_color(RGBColor & color);
    RGBColor & get_palette_color(int x = -1, int y = -1);
    void close_chat_input();
    void update(float dt);
    void draw();
    void draw_lists();
    void draw_map();
};

#endif
