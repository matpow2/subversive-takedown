#include "script.h"

#include "server.h"

#define Py_PYMATH_H
#include <Python.h>

#ifdef _MSC_VER
#undef HAVE_UNISTD_H
#undef HAVE_STDINT_H
#endif
#include <structmember.h>

// The Python API takes *char(s), not const *char(s), which can cause warnings
#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wdeprecated-writable-strings"
#endif

inline PyObject * string_to_object(const std::string & value)
{
    return PyString_FromStringAndSize(&value[0], value.size());
}

inline std::string object_to_string(PyObject * object)
{
    char * buf;
    Py_ssize_t len;
    PyString_AsStringAndSize(object, &buf, &len);
    return std::string(buf, len);
}

inline void object_to_string(PyObject * object, std::string & str)
{
    char * buf;
    Py_ssize_t len;
    PyString_AsStringAndSize(object, &buf, &len);
    str.assign(buf, len);
}

inline PyObject * get_py(PyObject * object)
{
    if (object == NULL)
        return Py_None;
    return object;
}

bool print_python_errors()
{
    if (!PyErr_Occurred())
        return false;
    PyErr_Print();
    return true;
}

bool print_python_errors(PyObject * ret)
{
    if (ret != NULL)
        return false;
    PyErr_Print();
    return true;
}

// Character type

typedef struct {
    PyObject_HEAD
    Character * character;
} CharacterObject;

static void Character_dealloc(CharacterObject* self)
{
    self->ob_type->tp_free((PyObject*)self);
}

static PyObject * Character_new(PyTypeObject *type, PyObject *args,
                                PyObject *kwds)
{
    CharacterObject * self;
    self = (CharacterObject*)type->tp_alloc(type, 0);
    return (PyObject*)self;
}

static int Character_init(CharacterObject *self, PyObject *args, PyObject *kwds)
{
    PyObject * character;
    if (!PyArg_ParseTuple(args, "O", &character))
        return -1;
    self->character = (Character*)PyCapsule_GetPointer(character, NULL);
    return 0;
}

static PyObject * Character_set_team(CharacterObject * self,
                                     PyObject * value)
{
    self->character->set_team(char(PyInt_AsLong(value)));
    Py_RETURN_NONE;
}

static PyObject * Character_get_team(CharacterObject * self,
                                      PyObject * value)
{
    return PyInt_FromLong(self->character->team);
}

static PyObject * Character_send_chat(CharacterObject * self,
                                      PyObject * message)
{
    self->character->peer->send_chat(PyString_AsString(message));
    Py_RETURN_NONE;
}

static PyObject * Character_set_score(CharacterObject * self,
                                      PyObject * value)
{
    self->character->set_score(PyInt_AsLong(value));
    Py_RETURN_NONE;
}

static PyObject * Character_get_score(CharacterObject * self,
                                      PyObject * value)
{
    return PyInt_FromLong(self->character->score);
}

static PyObject * Character_get_name(CharacterObject * self, PyObject * nop)
{
    return string_to_object(self->character->name);
}

static PyObject * Character_respawn(CharacterObject * self, PyObject * nop)
{
    self->character->peer->respawn();
    Py_RETURN_NONE;
}

static PyObject * Character_kill(CharacterObject * self, PyObject * nop)
{
    self->character->kill();
    Py_RETURN_NONE;
}

static PyMethodDef Character_methods[] = {
    {"get_team", (PyCFunction)Character_get_team, METH_NOARGS, NULL},
    {"set_team", (PyCFunction)Character_set_team, METH_O, NULL},
    {"get_name", (PyCFunction)Character_get_name, METH_NOARGS, NULL},
    {"send_chat", (PyCFunction)Character_send_chat, METH_O, NULL},
    {"get_score", (PyCFunction)Character_get_score, METH_NOARGS, NULL},
    {"set_score", (PyCFunction)Character_set_score, METH_O, NULL},
    {"respawn", (PyCFunction)Character_respawn, METH_NOARGS, NULL},
    {"kill", (PyCFunction)Character_kill, METH_NOARGS, NULL},
    {NULL}
};

static PyMemberDef Character_members[] = {
    {NULL}
};

static PyTypeObject CharacterType = {
    PyObject_HEAD_INIT(NULL)
    0,                         /*ob_size*/
    "stlib.server.Character",  /*tp_name*/
    sizeof(CharacterObject),   /*tp_basicsize*/
    0,                         /*tp_itemsize*/
    0,                         /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    0,                         /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    0,                         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags*/
    "Character object",           /* tp_doc */
    0,                         /* tp_traverse */
    0,                         /* tp_clear */
    0,                         /* tp_richcompare */
    0,                         /* tp_weaklistoffset */
    0,                         /* tp_iter */
    0,                         /* tp_iternext */
    Character_methods,         /* tp_methods */
    Character_members,         /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    (initproc)Character_init,  /* tp_init */
    0,                         /* tp_alloc */
    Character_new,             /* tp_new */
};

// Entity type

typedef struct {
    PyObject_HEAD
    World::Entity * entity;
} EntityObject;

static void Entity_dealloc(CharacterObject* self)
{
    self->ob_type->tp_free((PyObject*)self);
}

static PyObject * Entity_new(PyTypeObject *type, PyObject *args,
                             PyObject *kwds)
{
    EntityObject * self;
    self = (EntityObject*)type->tp_alloc(type, 0);
    return (PyObject*)self;
}

static int Entity_init(EntityObject *self, PyObject *args, PyObject *kwds)
{
    PyObject * entity;
    if (!PyArg_ParseTuple(args, "O", &entity))
        return -1;
    self->entity = (World::Entity*)PyCapsule_GetPointer(entity, NULL);
    return 0;
}

static PyObject * Entity_set_node(EntityObject * self, PyObject * value)
{
    PyObject * other_py;
    int position = 0;
    PyArg_ParseTuple(value, "O|i", &other_py, &position);
    World::Entity * other;
    if (other_py == Py_None)
        other = NULL;
    else
        other = ((EntityObject*)other_py)->entity;
    return PyBool_FromLong(self->entity->set_node(other, position != 0));
}

static PyObject * Entity_get_team_distance(EntityObject * self,
                                           PyObject * value)
{
    int team = PyInt_AsLong(value);
    World::ObjectList::const_iterator it;
    World::ObjectList & objects = self->entity->world->objects;
    bool has_dist = false;
    float dist;
    for (it = objects.begin(); it != objects.end(); it++) {
        World::WorldObject * obj = *it;
        if (obj->type != World::CHARACTER)
            continue;
        World::Character * character = (World::Character*)obj;
        if (character->team != team || character->dead)
            continue;
        float new_dist = glm::distance(self->entity->get_position(),
            obj->get_position());
        if (!has_dist || new_dist < dist) {
            has_dist = true;
            dist = new_dist;
        }
    }
    if (!has_dist)
        Py_RETURN_NONE;
    return PyFloat_FromDouble(double(dist));
}

static PyObject * Entity_get_entity_distance(EntityObject * self,
                                             PyObject * value)
{
    World::Entity * other = ((EntityObject*)value)->entity;
    float dist = glm::distance(self->entity->get_position(),
                               other->get_position());
    return PyFloat_FromDouble(double(dist));
}

static PyObject * Entity_set_team(EntityObject * self, PyObject * value)
{
    self->entity->set_team(char(PyInt_AsLong(value)));
    Py_RETURN_NONE;
}

static PyObject * Entity_destroy(EntityObject * self, PyObject * nop)
{
    self->entity->destroy();
    Py_RETURN_NONE;
}

static PyMethodDef Entity_methods[] = {
    {"set_node", (PyCFunction)Entity_set_node, METH_VARARGS, NULL},
    {"get_team_distance", (PyCFunction)Entity_get_team_distance, METH_O, NULL},
    {"get_entity_distance", (PyCFunction)Entity_get_entity_distance, METH_O,
        NULL},
    {"set_team", (PyCFunction)Entity_set_team, METH_O, NULL},
    {"destroy", (PyCFunction)Entity_destroy, METH_NOARGS, NULL},
    {NULL}
};

static PyMemberDef Entity_members[] = {
    {NULL}
};

static PyTypeObject EntityType = {
    PyObject_HEAD_INIT(NULL)
    0,                         /*ob_size*/
    "stlib.server.Entity",  /*tp_name*/
    sizeof(EntityObject),   /*tp_basicsize*/
    0,                         /*tp_itemsize*/
    0,                         /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    0,                         /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    0,                         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags*/
    "Entity object",           /* tp_doc */
    0,                         /* tp_traverse */
    0,                         /* tp_clear */
    0,                         /* tp_richcompare */
    0,                         /* tp_weaklistoffset */
    0,                         /* tp_iter */
    0,                         /* tp_iternext */
    Entity_methods,         /* tp_methods */
    Entity_members,         /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    (initproc)Entity_init,  /* tp_init */
    0,                         /* tp_alloc */
    Entity_new,             /* tp_new */
};

// Server type

typedef struct {
    PyObject_HEAD
    ServerHost * host;
} ServerObject;

static void Server_dealloc(ServerObject* self)
{
    self->ob_type->tp_free((PyObject*)self);
}

static PyObject * Server_new(PyTypeObject *type, PyObject *args,
                             PyObject *kwds)
{
    ServerObject *self;
    self = (ServerObject*)type->tp_alloc(type, 0);
    return (PyObject*)self;
}

static int Server_init(ServerObject *self, PyObject *args, PyObject *kwds)
{
    PyObject * host;
    if (!PyArg_ParseTuple(args, "O", &host))
        return -1;
    self->host = (ServerHost*)PyCapsule_GetPointer(host, NULL);
    return 0;
}

static PyObject * Server_set_map(ServerObject * self,
                                 PyObject * map)
{
    self->host->set_map(PyString_AsString(map));
    Py_RETURN_NONE;
}

static PyObject * Server_get_map_name(ServerObject * self, PyObject * nop)
{
    return string_to_object(self->host->get_map()->name);
}

static PyObject * Server_get_objective(ServerObject * self, PyObject * value)
{
    int team = PyLong_AsLong(value);
    if (team == TEAM_1)
        return string_to_object(self->host->get_map()->objective1);
    else
        return string_to_object(self->host->get_map()->objective2);
}

static PyObject * Server_get_description(ServerObject * self, PyObject * nop)
{
    return string_to_object(self->host->get_map()->description);
}

static PyObject * Server_get_entity(ServerObject * self, PyObject * value)
{
    std::string name = object_to_string(value);
    NamedObjects::const_iterator it;
    it = self->host->named_objects.find(name);
    if (it == self->host->named_objects.end()) {
        Py_RETURN_NONE;
    }
    PyObject * capsule = PyCapsule_New(it->second, NULL, NULL);
    return capsule;
}

static PyObject * Server_send_chat(ServerObject * self, PyObject * value)
{
    self->host->send_chat(object_to_string(value));
    Py_RETURN_NONE;
}

static PyObject * Server_play_sound(ServerObject * self, PyObject * value)
{
    PyObject *name;
    int as_music;
    PyArg_ParseTuple(value, "Si", &name, &as_music);
    self->host->play_sound(object_to_string(name), as_music != 0);
    Py_RETURN_NONE;
}

static PyObject * Server_get_map_time(ServerObject * self, PyObject * nop)
{
    return PyFloat_FromDouble(double(self->host->get_map_time()));
}

static PyObject * Server_set_map_time(ServerObject * self, PyObject * value)
{
    self->host->set_map_time(float(PyFloat_AsDouble(value)));
    Py_RETURN_NONE;
}

static PyMethodDef Server_methods[] = {
    {"set_map", (PyCFunction)Server_set_map, METH_O, NULL},
    {"get_map_name", (PyCFunction)Server_get_map_name, METH_NOARGS, NULL},
    {"get_objective", (PyCFunction)Server_get_objective, METH_O, NULL},
    {"get_description", (PyCFunction)Server_get_objective, METH_NOARGS, NULL},
    {"send_chat", (PyCFunction)Server_send_chat, METH_O, NULL},
    {"get_entity", (PyCFunction)Server_get_entity, METH_O, NULL},
    {"play_sound", (PyCFunction)Server_play_sound, METH_VARARGS, NULL},
    {"get_map_time", (PyCFunction)Server_get_map_time, METH_NOARGS, NULL},
    {"set_map_time", (PyCFunction)Server_set_map_time, METH_O, NULL},
    {NULL}
};

static PyMemberDef Server_members[] = {
    {NULL}
};

static PyTypeObject ServerType = {
    PyObject_HEAD_INIT(NULL)
    0,                         /*ob_size*/
    "stlib.server.Server",  /*tp_name*/
    sizeof(ServerObject),      /*tp_basicsize*/
    0,                         /*tp_itemsize*/
    0,                         /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    0,                         /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    0,                         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags*/
    "Server object",           /* tp_doc */
    0,                         /* tp_traverse */
    0,                         /* tp_clear */
    0,                         /* tp_richcompare */
    0,                         /* tp_weaklistoffset */
    0,                         /* tp_iter */
    0,                         /* tp_iternext */
    Server_methods,            /* tp_methods */
    Server_members,            /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    (initproc)Server_init,     /* tp_init */
    0,                         /* tp_alloc */
    Server_new,                /* tp_new */
};

// ScriptCharacter

ScriptCharacter::ScriptCharacter(Character * character)
: character(character)
{
    ServerHost * host = (ServerHost*)character->peer->host;
    PyObject * server_object = host->script->server_object;
    PyObject * character_capsule = PyCapsule_New(character, NULL, NULL);
    object = PyObject_CallMethod(server_object, "create_character", "O",
        character_capsule);
    print_python_errors(object);
}

void ScriptCharacter::call_void(const char * name)
{
    PyObject * ret = PyObject_CallMethod(object, (char*)name, "");
    print_python_errors(ret);
    Py_XDECREF(ret);
}

void ScriptCharacter::call_void(const char * name, PyObject * value)
{
    PyObject * ret = PyObject_CallMethod(object, (char*)name, "O",
        get_py(value));
    print_python_errors(ret);
    Py_XDECREF(ret);
}

bool ScriptCharacter::call_handler(const char * name)
{
    PyObject * ret = PyObject_CallMethod(object, (char*)name, "");
    bool accept = ret != Py_False && ret != NULL;
    print_python_errors(ret);
    Py_XDECREF(ret);
    return accept;
}

bool ScriptCharacter::call_handler(const char * name, int value)
{
    PyObject * ret = PyObject_CallMethod(object, (char*)name, "i", value);
    bool accept = ret != Py_False && ret != NULL;
    print_python_errors(ret);
    Py_XDECREF(ret);
    return accept;
}

bool ScriptCharacter::call_handler(const char * name, PyObject * value)
{
    PyObject * ret = PyObject_CallMethod(object, (char*)name, "O",
        get_py(value));
    bool accept = ret != Py_False && ret != NULL;
    print_python_errors(ret);
    Py_XDECREF(ret);
    return accept;
}

bool ScriptCharacter::call_handler(const char * name, const std::string & v)
{
    PyObject * ret = PyObject_CallMethod(object, (char*)name, "O",
        string_to_object(v));
    bool accept = ret != Py_False && ret != NULL;
    print_python_errors(ret);
    Py_XDECREF(ret);
    return accept;
}

bool ScriptCharacter::call_handler(const char * name, const std::string & v,
                                   const std::string & v2)
{
    PyObject * ret = PyObject_CallMethod(object, (char*)name, "OO",
        string_to_object(v), string_to_object(v2));
    bool accept = ret != Py_False && ret != NULL;
    print_python_errors(ret);
    Py_XDECREF(ret);
    return accept;
}

// ScriptManager

ScriptManager::ScriptManager(ServerHost * host)
: host(host)
{
    // Py_NoSiteFlag = 1;
    // Py_SetPythonHome("./server/lib/");
    Py_Initialize();
    PyList_Append(PySys_GetObject("path"), PyString_FromString("./server/"));
    PyList_Append(PySys_GetObject("path"), PyString_FromString("./server/lib"));
    main_mod = PyImport_AddModule("__main__");
    stlib_mod = Py_InitModule("stlib", NULL);
    server_mod = PyModule_New("server");
    PyModule_AddObject(stlib_mod, "server", server_mod);
    constants_mod = PyModule_New("constants");
    PyModule_AddObject(stlib_mod, "constants", constants_mod);

    // add constants
    PyModule_AddIntMacro(constants_mod, TEAM_SPEC);
    PyModule_AddIntMacro(constants_mod, TEAM_1);
    PyModule_AddIntMacro(constants_mod, TEAM_2);
    PyModule_AddIntMacro(constants_mod, TEAM_SINGLE);

    // create character type
    if (PyType_Ready(&CharacterType) < 0)
        return;
    Py_INCREF(&CharacterType);
    PyModule_AddObject(server_mod, "Character", (PyObject*)&CharacterType);

    // create server type
    if (PyType_Ready(&ServerType) < 0)
        return;
    Py_INCREF(&ServerType);
    PyModule_AddObject(server_mod, "Server", (PyObject*)&ServerType);

    // create entity type
    if (PyType_Ready(&EntityType) < 0)
        return;
    Py_INCREF(&EntityType);
    PyModule_AddObject(server_mod, "Entity", (PyObject*)&EntityType);

    base_mod = PyImport_ImportModule("base");
    print_python_errors();

    PyObject * server_capsule = PyCapsule_New(host, NULL, NULL);
    print_python_errors();

    server_object = PyObject_CallMethod(base_mod, "create_server", "O",
        server_capsule);
    print_python_errors();
}

void ScriptManager::call_void(const char * name)
{
    PyObject * ret = PyObject_CallMethod(server_object, (char*)name, "");
    print_python_errors(ret);
    Py_XDECREF(ret);
}

void ScriptManager::call_void(const char * name, float value)
{
    PyObject * ret = PyObject_CallMethod(server_object, (char*)name, "f",
        value);
    print_python_errors(ret);
    Py_XDECREF(ret);
}

void ScriptManager::call_string_list(const char * name, StringList & items)
{
    PyObject * ret = PyObject_CallMethod(server_object, (char*)name, "");
    print_python_errors(ret);
    PyObject * seq = PySequence_Fast(ret, "expected a sequence");
    size_t len = PySequence_Size(ret);
    for (unsigned int i = 0; i < len; i++) {
        PyObject * item = PySequence_Fast_GET_ITEM(seq, i);
        items.push_back(std::string(PyString_AsString(item)));
    }
    Py_DECREF(seq);
    Py_XDECREF(ret);
}

PyObject * ScriptManager::get_attr(const char * name)
{
    return PyObject_GetAttrString(server_object, name);
}

bool ScriptManager::get_bool(const char * name)
{
    PyObject * v = get_attr(name);
    int r = PyObject_IsTrue(v);
    Py_XDECREF(v);
    return r != 0;
}

int ScriptManager::get_int(const char * name)
{
    PyObject * v = get_attr(name);
    int r = PyLong_AsLong(v);
    Py_XDECREF(v);
    return r;
}

float ScriptManager::get_float(const char * name)
{
    PyObject * v = get_attr(name);
    float r = float(PyFloat_AsDouble(v));
    Py_XDECREF(v);
    return r;
}

void ScriptManager::get_string(const char * name, std::string & str)
{
    PyObject * v = get_attr(name);
    object_to_string(v, str);
    Py_XDECREF(v);
}

#ifdef __GNUC__
#pragma GCC diagnostic warning "-Wdeprecated-writable-strings"
#endif
