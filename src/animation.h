#ifndef ST_ANIMATION_H
#define ST_ANIMATION_H

#include <vector>
#include <iostream>

#include "image.h"
#include "stringcommon.h"
#include "voxel.h"

template <class T>
class AnimationData
{
public:
    float delay;
    std::vector<T*> items;

    AnimationData(float delay)
    : delay(delay)
    {
    }

    void add_item(T * item)
    {
        items.push_back(item);
    }
};

typedef AnimationData<VoxelModel> VoxelAnimationData;
typedef AnimationData<Image> ImageAnimationData;

template <class T>
class Animation
{
public:
    AnimationData<T> * data;
    float time;
    unsigned int frame;

    Animation(AnimationData<T> * data)
    {
        set_data(data);
    }

    void set_data(AnimationData<T> * data)
    {
        this->data = data;
        reset();
    }

    void update(float dt)
    {
        time += dt;
        if (time >= data->delay) {
            time -= data->delay;
            frame++;
            if (frame >= data->items.size())
                frame = 0;
        }
    }

    void reset()
    {
        time = 0.0f;
        frame = 0;
    }

    void set_frame(int frame)
    {
        frame = frame;
    }

    T * get()
    {
        return data->items[frame];
    }
};

typedef Animation<VoxelModel> VoxelAnimation;
typedef Animation<Image> ImageAnimation;

inline ImageAnimationData * load_image_animation(const std::string & name,
    float delay, bool center = false, bool filter = true)
{
    ImageAnimationData * data = new ImageAnimationData(delay);
    int i;
    for (i = 0; ; i++) {
        Image * img = load_image(name + number_to_string_pad(i, 4),
            0, 0, filter);
        if (img == NULL)
            break;
        if (center) {
            img->hotspot_x = img->width / 2.0f;
            img->hotspot_y = img->height / 2.0f;
        }
        data->add_item(img);
    }
    std::cout << "Loaded animation " << name << ", frames: " << i
        << std::endl;
    return data;
}

inline VoxelAnimationData * load_voxel_animation(const std::string & name,
    float delay)
{
    VoxelAnimationData * data = new VoxelAnimationData(delay);
    int i;
    for (i = 0; ; i++) {
        VoxelModel * model = load_model(name + number_to_string_pad(i, 4));
        if (model == NULL)
            break;
        data->add_item(model);
    }
    std::cout << "Loaded animation " << name << ", frames: " << i
        << std::endl;
    return data;
}

#endif // ST_ANIMATION_H
