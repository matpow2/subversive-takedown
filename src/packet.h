#ifndef ST_PACKET_H
#define ST_PACKET_H

#include <algorithm>
#include <enet/enet.h>
#include "types.h"

#include "constants.h"
#include "datastream.h"
#include "glm.h"

enum PacketType {
    // client to server
    CONNECTION_INFO = 0,
    MOVEMENT_PACKET = 1,
    CLIENT_SHOOT_PACKET = 2,
    CLIENT_CHAT_PACKET = 3,
    CLIENT_SET_TEAM = 4,
    BUILD_BLOCK = 5,
    CLIENT_SET_TOOL = 6,

    // server to client
    SERVER_INFO = 0,
    WORLD_PACKET = 1,
    SPAWN_PACKET = 2,
    DESTROY_PACKET = 3,
    SERVER_SHOOT_PACKET = 4,
    SERVER_CHAT_PACKET = 5,
    HP_PACKET = 6,
    KILL_PACKET = 7,
    CREATE_ENTITY = 8,
    CHANGE_ENTITY = 9,
    DESTROY_ENTITY = 10,
    PLAY_SAMPLE = 11,
    SCORE_PACKET = 12,
    SERVER_SET_TEAM = 13,
    BUILD_BLOCKS = 14,
    REMOVE_BLOCKS = 15,
    SET_AMMO = 16,
    SET_TIMER = 17,
    SERVER_SET_TOOL = 18,
    HIT_PACKET = 19,
    PACK_INFO = 20,

    // server to master
    ADD_SERVER = 0,
    SET_SERVER = 1
};

enum {
    NONE_CHARACTER = 0xFF
};

#define RELIABLE_PACKET ENET_PACKET_FLAG_RELIABLE
#define UNRELIABLE_PACKET
#define UNSEQUENCED_PACKET ENET_PACKET_FLAG_UNSEQUENCED

class Packet
{
public:
    PacketType type;
    int flags;
    unsigned char channel;

    Packet(PacketType type, int flags, int channel)
    : type(type), flags(flags), channel(channel)
    {

    }

    virtual ~Packet()
    {
        
    }

    virtual void read(DataStream & stream) = 0;
    virtual void write(DataStream & stream) = 0;
};

class ServerInfo : public Packet
{
public:
    std::string map;

    ServerInfo()
    : Packet(SERVER_INFO, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        stream.read_string(map);
    }

    void write(DataStream & stream)
    {
        stream.write_string(map);
    }
};

class ConnectionInfo : public Packet
{
public:
    std::string name;

    ConnectionInfo()
    : Packet(CONNECTION_INFO, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        stream.read_string(name);
    }

    void write(DataStream & stream)
    {
        stream.write_string(name);
    }
};

class MovementPacket : public Packet
{
public:
    float x, y, z;
    float look_angle, walk_angle;
    bool is_walking;
    bool jump;

    MovementPacket()
    : Packet(MOVEMENT_PACKET, UNSEQUENCED_PACKET, 1)
    {
    }

    void set(float x, float y, float z, bool walking,
             float walk_angle, float look_angle, bool jump)
    {
        this->x = x;
        this->y = y;
        this->z = z;
        this->is_walking = walking;
        this->walk_angle = walk_angle;
        this->look_angle = look_angle;
        this->jump = jump;
    }

    void read(DataStream & stream)
    {
        x = stream.read_float();
        y = stream.read_float();
        z = stream.read_float();
        is_walking = stream.read_uint8() != 0;
        if (is_walking)
            walk_angle = stream.read_float();
        look_angle = stream.read_float();
        jump = stream.read_uint8() != 0;
    }

    void write(DataStream & stream)
    {
        stream.write_float(x);
        stream.write_float(y);
        stream.write_float(z);
        stream.write_uint8(is_walking);
        if (is_walking)
            stream.write_float(walk_angle);
        stream.write_float(look_angle);
        stream.write_uint8(jump);
    }
};

class PlayerData
{
public:
    unsigned char id;
    vec3 pos, vel;
    float look_angle, walk_angle;
    bool is_walking;
    bool jump;
    unsigned short ping;

    void set(unsigned char id, const vec3 & pos, const vec3 & vel, bool walking,
             float walk_angle, float look_angle, bool jump, unsigned short ping)
    {
        this->id = id;
        this->pos = pos;
        this->vel = vel;
        this->is_walking = walking;
        this->walk_angle = walk_angle;
        this->look_angle = look_angle;
        this->jump = jump;
        this->ping = ping;
    }

    void read(DataStream & stream, bool has_id)
    {
        id = stream.read_uint8();
        stream.read_vec3(pos);
        stream.read_vec3(vel);
        is_walking = stream.read_uint8() != 0;
        if (is_walking)
            walk_angle = stream.read_float();
        look_angle = stream.read_float();
        jump = stream.read_uint8() != 0;
        ping = stream.read_uint16();
    }

    void write(DataStream & stream, bool has_id)
    {
        stream.write_uint8(id);
        stream.write_vec3(pos);
        stream.write_vec3(vel);
        stream.write_uint8(is_walking);
        if (is_walking)
            stream.write_float(walk_angle);
        stream.write_float(look_angle);
        stream.write_uint8(jump);
        stream.write_uint16(ping);
    }
};

class EntityData
{
public:
    unsigned char id;
    vec3 pos;
    vec3 vel;
    quat rot;

    void set(unsigned char id, const vec3 & pos, const vec3 & vel,
             const quat & rot)
    {
        this->id = id;
        this->pos = pos;
        this->vel = vel;
        this->rot = rot;
    }

    void read(DataStream & stream)
    {
        id = stream.read_uint8();
        stream.read_vec3(pos);
        stream.read_vec3(vel);
        stream.read_quat(rot);
    }

    void write(DataStream & stream)
    {
        stream.write_uint8(id);
        stream.write_vec3(pos);
        stream.write_vec3(vel);
        stream.write_quat(rot);
    }
};

typedef std::vector<PlayerData> PlayerDataList;
typedef std::vector<EntityData> EntityDataList;

class WorldPacket : public Packet
{
public:
    PlayerDataList players;
    EntityDataList entities;

    WorldPacket()
    : Packet(WORLD_PACKET, UNSEQUENCED_PACKET, 2)
    {
    }

    void read(DataStream & stream)
    {
        int player_count = stream.read_uint8();
        int entity_count = stream.read_uint8();
        int i;

        PlayerData player;
        for (i = 0; i < player_count; i++) {
            player.read(stream, true);
            players.push_back(player);
        }

        EntityData entity;
        for (i = 0; i < entity_count; i++) {
            entity.read(stream);
            entities.push_back(entity);
        }
    }

    void write(DataStream & stream)
    {
        stream.write_uint8(players.size());
        stream.write_uint8(entities.size());

        PlayerDataList::iterator it;
        for (it = players.begin(); it != players.end(); it++) {
            PlayerData & player = *it;
            (*it).write(stream, true);
        }

        EntityDataList::iterator it2;
        for (it2 = entities.begin(); it2 != entities.end(); it2++) {
            (*it2).write(stream);
        }
    }
};

class ServerSetTeam : public Packet
{
public:
    unsigned char id;
    char team;

    ServerSetTeam()
    : Packet(SERVER_SET_TEAM, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        id = stream.read_uint8();
        team = stream.read_uint8();
    }

    void write(DataStream & stream)
    {
        stream.write_uint8(id);
        stream.write_uint8(team);
    }
};

class ClientSetTeam : public Packet
{
public:
    char team;

    ClientSetTeam()
    : Packet(CLIENT_SET_TEAM, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        team = stream.read_uint8();
    }

    void write(DataStream & stream)
    {
        stream.write_uint8(team);
    }
};

class SpawnPacket : public Packet
{
public:
    unsigned char id;
    int team;
    bool dead;
    float x, y, z;
    std::string name;

    SpawnPacket()
    : Packet(SPAWN_PACKET, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        id = stream.read_uint8();
        stream.read_string(name);
        team = stream.read_uint8();
        dead = stream.read_uint8() != 0;
        if (dead)
            return;
        x = stream.read_float();
        y = stream.read_float();
        z = stream.read_float();
    }

    void write(DataStream & stream)
    {
        stream.write_uint8(id);
        stream.write_string(name);
        stream.write_uint8(team);
        stream.write_uint8(dead);
        if (dead)
            return;
        stream.write_float(x);
        stream.write_float(y);
        stream.write_float(z);
    }
};

class DestroyPacket : public Packet
{
public:
    unsigned char id;

    DestroyPacket()
    : Packet(DESTROY_PACKET, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        id = stream.read_uint8();
    }

    void write(DataStream & stream)
    {
        stream.write_uint8(id);
    }
};

class ClientShootPacket : public Packet
{
public:
    float dir_x, dir_y, dir_z;

    ClientShootPacket()
    : Packet(CLIENT_SHOOT_PACKET, RELIABLE_PACKET, 3)
    {
    }

    void read(DataStream & stream)
    {
        dir_x = stream.read_float();
        dir_y = stream.read_float();
        dir_z = stream.read_float();
    }

    void write(DataStream & stream)
    {
        stream.write_float(dir_x);
        stream.write_float(dir_y);
        stream.write_float(dir_z);
    }
};

class ServerShootPacket : public Packet
{
public:
    unsigned char id;
    float x, y, z, dir_x, dir_y, dir_z;

    ServerShootPacket()
    : Packet(SERVER_SHOOT_PACKET, RELIABLE_PACKET, 3)
    {
    }

    void read(DataStream & stream)
    {
        id = stream.read_uint8();
        x = stream.read_float();
        y = stream.read_float();
        z = stream.read_float();
        dir_x = stream.read_float();
        dir_y = stream.read_float();
        dir_z = stream.read_float();
    }

    void write(DataStream & stream)
    {
        stream.write_uint8(id);
        stream.write_float(x);
        stream.write_float(y);
        stream.write_float(z);
        stream.write_float(dir_x);
        stream.write_float(dir_y);
        stream.write_float(dir_z);
    }
};

class ClientChatPacket : public Packet
{
public:
    unsigned char chat_type;
    std::string value;
    std::string sound;

    ClientChatPacket()
    : Packet(CLIENT_CHAT_PACKET, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        chat_type = stream.read_uint8();
        stream.read_string(value);
        stream.read_string(sound);
    }

    void write(DataStream & stream)
    {
        stream.write_uint8(chat_type);
        stream.write_string(value);
        stream.write_string(sound);
    }
};

class ServerChatPacket : public Packet
{
public:
    unsigned char chat_type;
    unsigned char id;
    std::string value;
    std::string sound;

    ServerChatPacket()
    : Packet(SERVER_CHAT_PACKET, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        chat_type = stream.read_uint8();
        if (chat_type != SERVER_MESSAGE)
            id = stream.read_uint8();
        stream.read_string(value);
        stream.read_string(sound);
    }

    void write(DataStream & stream)
    {
        stream.write_uint8(chat_type);
        if (chat_type != SERVER_MESSAGE)
            stream.write_uint8(id);
        stream.write_string(value);
        stream.write_string(sound);
    }
};

class HPPacket : public Packet
{
public:
    unsigned char value;
    bool show_hit;

    HPPacket()
    : Packet(HP_PACKET, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        value = stream.read_uint8();
        show_hit = stream.read_uint8() != 0;
    }

    void write(DataStream & stream)
    {
        stream.write_uint8(value);
        stream.write_uint8(int(show_hit));
    }
};

class HitPacket : public Packet
{
public:
    unsigned char id;

    HitPacket()
    : Packet(HIT_PACKET, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        id = stream.read_uint8();
    }

    void write(DataStream & stream)
    {
        stream.write_uint8(id);
    }
};

class KillPacket : public Packet
{
public:
    unsigned char id;
    unsigned char by;

    KillPacket()
    : Packet(KILL_PACKET, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        id = stream.read_uint8();
        by = stream.read_uint8();
    }

    void write(DataStream & stream)
    {
        stream.write_uint8(id);
        stream.write_uint8(by);
    }
};

class CreateEntity : public Packet
{
public:
    unsigned char id;
    unsigned char entity_type;

    CreateEntity()
    : Packet(CREATE_ENTITY, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        id = stream.read_uint8();
        entity_type = stream.read_uint8();
    }

    void write(DataStream & stream)
    {
        stream.write_uint8(id);
        stream.write_uint8(entity_type);
    }
};

class ChangeEntity : public Packet
{
public:
    unsigned char id;
    unsigned char character;
    vec3 pos;
    char team;
    std::string model;
    float scale;
    int health;

    ChangeEntity()
    : Packet(CHANGE_ENTITY, RELIABLE_PACKET, 0)
    {
    }

    void unset()
    {
        character = NONE_CHARACTER;
    }

    bool is_set()
    {
        return character != NONE_CHARACTER;
    }

    void set(unsigned char character)
    {
        this->character = character;
    }

    void read(DataStream & stream)
    {
        id = stream.read_uint8();
        team = stream.read_uint8();
        stream.read_vec3(pos);
        character = stream.read_uint8();
        health = stream.read_uint16();
        stream.read_string(model);
        if (!model.empty()) {
            scale = stream.read_float();
        }
    }

    void write(DataStream & stream)
    {
        stream.write_uint8(id);
        stream.write_uint8(team);
        stream.write_vec3(pos);
        stream.write_uint8(character);
        stream.write_uint16(health);
        stream.write_string(model);
        if (!model.empty()) {
            stream.write_float(scale);
        }
    }
};

class DestroyEntity : public Packet
{
public:
    unsigned char id;

    DestroyEntity()
    : Packet(DESTROY_ENTITY, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        id = stream.read_uint8();
    }

    void write(DataStream & stream)
    {
        stream.write_uint8(id);
    }
};

class PlaySample : public Packet
{
public:
    std::string name;
    bool music;

    PlaySample()
    : Packet(PLAY_SAMPLE, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        music = stream.read_uint8() != 0;
        stream.read(name);
    }

    void write(DataStream & stream)
    {
        stream.write_uint8(music);
        stream.write(name);
    }
};

class ScorePacket : public Packet
{
public:
    unsigned char id;
    int value;

    ScorePacket()
    : Packet(SCORE_PACKET, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        id = stream.read_uint8();
        value = stream.read_int32();
    }

    void write(DataStream & stream)
    {
        stream.write_uint8(id);
        stream.write_int32(value);
    }
};

class Block
{
public:
    float x, y, z;
};

struct ColorBlock
{
    float x, y, z;
    unsigned char r, g, b;
};

class BuildBlock : public Packet
{
public:
    ColorBlock block;

    BuildBlock()
    : Packet(BUILD_BLOCK, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        block.x = stream.read_float();
        block.y = stream.read_float();
        block.z = stream.read_float();
        block.r = stream.read_uint8();
        block.g = stream.read_uint8();
        block.b = stream.read_uint8();
    }

    void set(float x, float y, float z,
             unsigned char r, unsigned char g, unsigned char b)
    {
        block.x = x;
        block.y = y;
        block.z = z;
        block.r = r;
        block.g = g;
        block.b = b;
    }

    void write(DataStream & stream)
    {
        stream.write_float(block.x);
        stream.write_float(block.y);
        stream.write_float(block.z);
        stream.write_uint8(block.r);
        stream.write_uint8(block.g);
        stream.write_uint8(block.b);
    }
};

class BuildBlocks : public Packet
{
public:
    std::vector<ColorBlock> blocks;

    BuildBlocks()
    : Packet(BUILD_BLOCKS, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        ColorBlock block;
        while (!stream.at_end()) {
            block.x = stream.read_float();
            block.y = stream.read_float();
            block.z = stream.read_float();
            block.r = stream.read_uint8();
            block.g = stream.read_uint8();
            block.b = stream.read_uint8();
            blocks.push_back(block);
        }
    }

    void add(float x, float y, float z,
             unsigned char r, unsigned char g, unsigned char b)
    {
        ColorBlock block;
        block.x = x;
        block.y = y;
        block.z = z;
        block.r = r;
        block.g = g;
        block.b = b;
        blocks.push_back(block);
    }

    void write(DataStream & stream)
    {
        std::vector<ColorBlock>::const_iterator it;
        for (it = blocks.begin(); it != blocks.end(); it++) {
            const ColorBlock & block = *it;
            stream.write_float(block.x);
            stream.write_float(block.y);
            stream.write_float(block.z);
            stream.write_uint8(block.r);
            stream.write_uint8(block.g);
            stream.write_uint8(block.b);
        }
    }
};

class RemoveBlocks : public Packet
{
public:
    std::vector<unsigned int> blocks;

    RemoveBlocks()
    : Packet(REMOVE_BLOCKS, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        while (!stream.at_end()) {
            blocks.push_back(stream.read_uint32());
        }
    }

    void add(unsigned int i)
    {
        blocks.push_back(i);
    }

    void write(DataStream & stream)
    {
        std::vector<unsigned int>::const_iterator it;
        for (it = blocks.begin(); it != blocks.end(); it++) {
            stream.write_uint32(*it);
        }
    }
};

class SetAmmo : public Packet
{
public:
    unsigned char tool, ammo, clip;

    SetAmmo()
    : Packet(SET_AMMO, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        tool = stream.read_uint8();
        ammo = stream.read_uint8();
    }

    void write(DataStream & stream)
    {
        stream.write_uint8(tool);
        stream.write_uint8(ammo);
    }
};

class TimerPacket : public Packet
{
public:
    float timer;

    TimerPacket()
    : Packet(SET_TIMER, RELIABLE_PACKET, 0)
    {
    }

    void set(float v)
    {
        timer = v;
    }

    void read(DataStream & stream)
    {
        timer = stream.read_float();
    }

    void write(DataStream & stream)
    {
        stream.write_float(timer);
    }
};

class ClientSetTool : public Packet
{
public:
    unsigned char tool;

    ClientSetTool()
    : Packet(CLIENT_SET_TOOL, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        tool = stream.read_uint8();
    }

    void write(DataStream & stream)
    {
        stream.write_uint8(tool);
    }
};

class ServerSetTool : public Packet
{
public:
    unsigned char id;
    unsigned char tool;

    ServerSetTool()
    : Packet(SERVER_SET_TOOL, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        id = stream.read_uint8();
        tool = stream.read_uint8();
    }

    void write(DataStream & stream)
    {
        stream.write_uint8(id);
        stream.write_uint8(tool);
    }
};

class PackInfo : public Packet
{
public:
    StringList items;

    PackInfo()
    : Packet(PACK_INFO, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        std::string value;
        while (!stream.at_end()) {
            stream.read_string(value);
            items.push_back(value);
        }
    }

    void write(DataStream & stream)
    {
        StringList::const_iterator it;
        for (it = items.begin(); it != items.end(); it++) {
            stream.write_string(*it);
        }
    }
};

class AddServer : public Packet
{
public:
    std::string name;
    unsigned short port;
    unsigned char max;

    AddServer()
    : Packet(ADD_SERVER, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        stream.read_string(name);
        port = stream.read_uint16();
        max = stream.read_uint8();
    }

    void write(DataStream & stream)
    {
        stream.write_string(name);
        stream.write_uint16(port);
        stream.write_uint8(max);
    }
};

class SetServer : public Packet
{
public:
    unsigned char count;

    SetServer()
    : Packet(SET_SERVER, RELIABLE_PACKET, 0)
    {
    }

    void read(DataStream & stream)
    {
        count = stream.read_uint8();
    }

    void write(DataStream & stream)
    {
        stream.write_uint8(count);
    }
};

Packet * read_packet(char * data, size_t size);
void write_packet(Packet & packet, DataStream & stream);

#endif // ST_PACKET_H
