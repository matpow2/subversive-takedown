#include "hud.h"

#include <algorithm>
#include <vector>

#include "constants.h"
#include "fonts.h"

#include "assets.h"
#include "debug.h"
#include "draw.h"
#include "easing.h"
#include "include_gl.h"
#include "mathcommon.h"
#include "scenes/game.h"
#include "stringcommon.h"

#define CHAT_BUFFER_SIZE 15
#define KILL_FEED_SIZE 5

#define SCOREBOARD_WIDTH (WINDOW_WIDTH - 50)
#define SCOREBOARD_HEIGHT (WINDOW_HEIGHT - 50)
#define SCOREBOARD_X1 (WINDOW_WIDTH / 2 - SCOREBOARD_WIDTH / 2)
#define SCOREBOARD_X2 (SCOREBOARD_X1 + SCOREBOARD_WIDTH)
#define SCOREBOARD_Y1 (WINDOW_HEIGHT / 2 - SCOREBOARD_HEIGHT / 2)
#define SCOREBOARD_Y2 (SCOREBOARD_Y1 + SCOREBOARD_HEIGHT)
#define TEAM_BOARD_WIDTH (SCOREBOARD_WIDTH / 2)
#define SCOREBOARD_X_PAD 15
#define SCOREBOARD_Y_PAD 5
#define TEAM1_X1 SCOREBOARD_X1
#define TEAM1_Y1 SCOREBOARD_Y1
#define TEAM1_WIDTH TEAM_BOARD_WIDTH
#define TEAM1_HEIGHT SCOREBOARD_HEIGHT
#define TEAM2_X1 (SCOREBOARD_X1 + TEAM_BOARD_WIDTH)
#define TEAM2_Y1 SCOREBOARD_Y1
#define TEAM2_WIDTH TEAM_BOARD_WIDTH
#define TEAM2_HEIGHT SCOREBOARD_HEIGHT

#define PALETTE_ITEM_SIZE 8
#define PALETTE_PAD 1
#define PALETTE_SIZE (PALETTE_ITEM_SIZE * 16 + PALETTE_PAD * 15)
#define PALETTE_X1 (WINDOW_WIDTH - 20 - PALETTE_SIZE)
#define PALETTE_Y1 80
#define PALETTE_OUTLINE_SIZE 1
#define PALETTE_WIDTH ((PALETTE_ITEM_SIZE + PALETTE_PAD) * 16)
#define PALETTE_X_MID (PALETTE_X1 + (PALETTE_WIDTH / 2))

#define BOTTOM_Y 40
#define TOOLAMMO_X1 PALETTE_X1
#define TOOLAMMO_X2 (PALETTE_X1 + PALETTE_WIDTH)
#define TOOLAMMO_Y1 (BOTTOM_Y - 25)
#define TOOLAMMO_Y2 (TOOLAMMO_Y1 + 50)
#define TOOL_X (PALETTE_X_MID - PALETTE_WIDTH / 4)
#define TOOL_MIDX (PALETTE_X_MID)
#define AMMO_X (PALETTE_X_MID + PALETTE_WIDTH / 4 - 10)

#define HP_Y ((TOOLAMMO_Y2 + TOOLAMMO_Y1) / 2)

#define CHAT_Y (HP_Y + 50.0f)

#define TIMER_WIDTH 100.0f
#define TIMER_HEIGHT 20.0f
#define TIMER_X1 (WINDOW_WIDTH - TIMER_WIDTH - 20.0f)
#define TIMER_Y1 (WINDOW_HEIGHT - TIMER_HEIGHT - 20.0f)

#define FEED_Y (WINDOW_HEIGHT - 25)

#define QCHAT_WIDTH 200
#define QCHAT_HEIGHT 200
#define QCHAT_X1 (WINDOW_WIDTH / 2 - QCHAT_WIDTH / 2)
#define QCHAT_Y1 (WINDOW_HEIGHT / 2 - QCHAT_HEIGHT / 2)

// ChatLine

ChatLine::ChatLine(HUD * hud, const std::string & name,
                   const std::string & text, const RGBColor & col)
: name(name), text(text), time(0.0f), hud(hud), color(col)
{

}

void ChatLine::draw()
{
    float alpha = std::max<float>(0.0f, 2.0f - time * 0.25f);
    alpha += hud->extra_alpha;
    alpha = std::min<float>(1.0f, alpha);
    if (alpha <= 0.0f)
        return;
    unsigned char alpha_c = (unsigned char)(alpha * 255.0f);
    BoundingBox bb;
    if (name.empty())
        bb = fixedsys_font->get_box(text);
    else
        bb = fixedsys_font->get_box(name + " " + text);
    int width = int(bb.get_width());
    int height = int(fixedsys_font->LineHeight());
    int x_pad = 6;
    int y_pad = 4;
    int x1 = -x_pad;
    int y1 = -y_pad - height / 2 - 1;
    int x2 = x1 + width + x_pad * 2;
    int y2 = y1 + height + y_pad * 2;
    draw_rounded_rect(x1, y1, x2, y2, hud_color, int(100 * alpha));
    color.set_current(alpha_c);
    if (name.empty()) {
        fixedsys_font->draw_left_center(text, 0.0f, 0.0f);
    } else {
        fixedsys_font->draw_left_center(name, 0.0f, 0.0f);
        RGBColor chat_color(255, 255, 255);
        chat_color.mix(color, 0.2f);
        chat_color.set_current(alpha_c);
        fixedsys_font->draw_left_center(text,
            fixedsys_font->get_advance(name + " "), 0.0f);
    }
}

// KillLine

KillLine::KillLine(const std::string & name, VoxelModel * tool,
                   const std::string & other)
: name(name), tool(tool), other(other), time(0.0f)
{

}

void draw_hud_tool(VoxelModel * model, float x, float y, float scale,
                   float alpha)
{
    float size = (model->file->x_size +
                  model->file->y_size +
                  model->file->z_size) / 3.0f;
    size = std::max(size, 4.0f);
    scale /= size;
    glEnable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);
    glPushMatrix();
    glTranslatef(x, y, 0.0f);
    glScalef(scale, scale, scale);
    float rot;
    if (model == selfkill_model)
        rot = -90.0f;
    else
        rot = -45.0f;
    glRotatef(rot, 1.0, 0.0, 0.0);
    glRotatef(rot, 0.0, 0.0, 1.0);
    ReferencePoint * ref = model->get_point("hud");
    glTranslatef(float(-ref->x), float(-ref->y), float(-ref->z));
    model->draw_immediate(alpha);
    glPopMatrix();
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);
}

#define FEED_TOOL_WIDTH 50

void KillLine::draw()
{
    float alpha = std::max<float>(0.0f, 2.0f - time * 0.25f);
    alpha = std::min<float>(1.0f, alpha);
    if (alpha <= 0.0f)
        return;
    unsigned char alpha_c = (unsigned char)(alpha * 255.0f);
    BoundingBox bb1 = fixedsys_font->get_box(name);
    BoundingBox bb2 = fixedsys_font->get_box(other);
    float width1 = bb1.get_width();
    int width = int(width1 + FEED_TOOL_WIDTH + bb2.get_width());
    if (other.empty())
        width -= 20;
    int height = int(fixedsys_font->LineHeight());
    int x_pad = 6;
    int y_pad = 4;
    int x1 = -x_pad;
    int y1 = -y_pad - height / 2 - 1;
    int x2 = x1 + width + x_pad * 2;
    int y2 = y1 + height + y_pad * 2;
    draw_rounded_rect(x1, y1, x2, y2, hud_color, int(100 * alpha));
    RGBColor chat_color(255, 255, 255);
    chat_color.set_current(alpha_c);
    fixedsys_font->draw_left_center(name, 0.0f, 0.0f);
    draw_hud_tool(tool, width1 + FEED_TOOL_WIDTH * 0.5f - 2.0f, -1.0f, 15.0f,
                  alpha);
    chat_color.set_current(alpha_c);
    fixedsys_font->draw_left_center(other, width1 + FEED_TOOL_WIDTH, 0.0f);
}

// HUD

HUD::HUD(GameScene * scene)
: scene(scene), focus(false), ignore_next(false), extra_alpha(0.0f),
  hp(0.0f), hp_interp(0.0f), show_list(false), palette_x(8), palette_y(8),
  show_map(false), map_time_end(0.0), quit(false), quick_chat(false)
{

}

void HUD::set_hp(int value)
{
    hp = float(value);
}

void HUD::set_map_time(float value)
{
    map_time_end = scene->time + double(value);
}

float HUD::get_map_time()
{
    return float(map_time_end - scene->time);
}

void HUD::close_chat_input()
{
    current_text.clear();
    focus = false;
}

const static char *quick_chat_entries[] = {
    "Hello!", "hello",
    "It's only a game!", "it is only a game"
};

#define QCHAT_SIZE (sizeof(quick_chat_entries) / sizeof(*quick_chat_entries) / 2)

void HUD::on_key(int button, bool value)
{
    if (quit) {
        if (button == GLFW_KEY_Y) {
            scene->disconnect();
        } else if (button == GLFW_KEY_N) {
            quit = false;
            scene->manager->set_captured_mouse();
        }
        return;
    }
    if (quick_chat && button >= GLFW_KEY_1 && button <= GLFW_KEY_9) {
        quick_chat = false;
        int i = button - GLFW_KEY_1;
        if (i >= QCHAT_SIZE)
            return;
        scene->send_chat(quick_chat_entries[i * 2], 
                         quick_chat_entries[i * 2 + 1]);
    }
    if (focus) {
        if (button == GLFW_KEY_BACKSPACE && value)
            current_text = current_text.substr(0, current_text.size() - 1);
        else if (button == GLFW_KEY_ENTER && value) {
            if (current_text.size() > 0)
                scene->send_chat(current_text, chat_type);
            close_chat_input();
        }
        return;
    } else if (scene->is_tool(BLOCK_TOOL) && value) {
        if (button == GLFW_KEY_LEFT)
            set_palette_index(-1, 0);
        else if (button == GLFW_KEY_RIGHT)
            set_palette_index(1, 0);
        else if (button == GLFW_KEY_UP)
            set_palette_index(0, 1);
        else if (button == GLFW_KEY_DOWN)
            set_palette_index(0, -1);
    }
    ConfigFile * config = scene->manager->config;
    if (button == config->chat && value) {
        focus = ignore_next = true;
        chat_type = GLOBAL_CHAT;
    } else if (button == config->team_chat && value) {
        focus = ignore_next = true;
        chat_type = TEAM_CHAT;
    } else if (button == GLFW_KEY_TAB)
        show_list = value;
    else if (button == GLFW_KEY_ESCAPE && value) {
        quit = true;
        scene->manager->set_normal_mouse();
    } else if (button == config->quick_chat && value) {
        quick_chat = !quick_chat;
    }

}

void HUD::set_color(RGBColor & color)
{
    // try and find the color in the palette
    for (int x = 0; x < 16; x++)
    for (int y = 0; y < 16; y++) {
        RGBColor & other = get_palette_color(x, y);
        if (other.r != color.r || other.g != color.g || other.b != color.b)
            continue;
        palette_x = x;
        palette_y = y;
        return;
    }

    // just set the color and ignore the palette index
    saved_color = color;
    palette_x = palette_y = -1;
}

void HUD::set_palette_index(int x, int y)
{
    palette_x = (palette_x + x) & 15;
    palette_y = (palette_y - y) & 15;
}

RGBColor & HUD::get_palette_color(int x, int y)
{
    if (x == -1)
        x = palette_x;
    if (y == -1)
        y = palette_y;
    if (x == -1 || y == -1)
        return saved_color;
    return palette_voxel->palette[x + y * 16];
}

void HUD::on_text(const std::string & text)
{
    if (ignore_next) {
        ignore_next = false;
        return;
    }
    current_text += text;
}

void HUD::add_kill(Character * character, Character * by)
{
    std::string killed;
    std::string killer;
    VoxelModel * model = NULL;
    if (by == NULL) {
        killer = character->name;
        killed = "";
        model = selfkill_model;
    } else {
        killer = by->name;
        killed = character->name;
        model = by->tool->model;
    }

    killfeed.push_back(KillLine(killer, model, killed));
    if (killfeed.size() > KILL_FEED_SIZE)
        killfeed.erase(killfeed.begin());

}

void HUD::add_chat(Character * character, const std::string & value,
                   int chat_type)
{
    RGBColor color;
    std::string name;
    if (character == NULL)
        color = server_color;
    else {
        name = character->name;
        color = get_team_color(character->team);
        color.multiply(1.5f);
        if (chat_type == TEAM_CHAT)
            name = name + " (Team)";
    }
    lines.push_back(ChatLine(this, name, value, color));
    if (lines.size() > CHAT_BUFFER_SIZE)
        lines.erase(lines.begin());
}

void HUD::add_chat(const std::string & value)
{
    add_chat(NULL, value, SERVER_MESSAGE);
}

void HUD::update(float dt)
{
    float add = dt * 2.0f;
    if (focus)
        extra_alpha += add;
    else
        extra_alpha -= add;
    extra_alpha = std::max<float>(0.0f, std::min<float>(1.0f, extra_alpha));
    std::vector<ChatLine>::iterator it;
    for (it = lines.begin(); it != lines.end(); it++) {
        (*it).time += dt;
    }
    std::vector<KillLine>::iterator it2;
    for (it2 = killfeed.begin(); it2 != killfeed.end(); it2++) {
        (*it2).time += dt;
    }
    hp_interp = interpolate(hp_interp, hp, dt * 3.0f);
}

typedef std::vector<Character*> TeamList;

inline bool compare_score(Character * char1, Character * char2)
{
    return char1->score > char2->score;
}

inline void draw_list(int x, int y, int width, int height, int team,
                      const CharacterMap & characters)
{
    draw_rounded_rect(x, y, x + width, y + height, hud_color, 240);
    int yy = y + height;
    x += SCOREBOARD_X_PAD;
    std::vector<Character*> players;
    CharacterMap::const_iterator map_it;
    for (map_it = characters.begin(); map_it != characters.end(); map_it++) {
        Character * character = map_it->second;
        if (team != character->team &&
           !(team == TEAM_1 && character->team == TEAM_SPEC))
            continue;
        players.push_back(character);
    }
    std::sort(players.begin(), players.end(), compare_score);
    std::vector<Character*>::const_iterator it = players.begin();
    bool header = true;
    while (it != players.end()) {
        yy -= int(fixedsys_font->LineHeight()) + SCOREBOARD_Y_PAD;
        int xx = x;
        std::string name, score, ping;
        if (header) {
            name = "Name";
            score = "Score";
            ping = "Ping";
            header = false;
            glColor4f(0.8f, 0.8f, 0.8f, 1.0f);
        } else {
            Character * character = *it;
            name = character->name;
            score = number_to_string(character->score);
            ping = number_to_string(character->ping);
            it++;

            RGBColor col = get_team_color(character->team);
            col.mix(RGBColor(255, 255, 255), 0.4f);
            col.set_current(1.0f);
        }
        fixedsys_font->draw(name, float(xx), float(yy));
        xx += TEAM_BOARD_WIDTH / 3;
        fixedsys_font->draw(score, float(xx), float(yy));
        xx += TEAM_BOARD_WIDTH / 3;
        fixedsys_font->draw(ping, float(xx), float(yy));
    }
}

void HUD::draw_lists()
{
    const CharacterMap & characters = scene->characters;
    draw_list(TEAM1_X1, TEAM1_Y1, TEAM1_WIDTH, TEAM1_HEIGHT, TEAM_1, characters);
    draw_list(TEAM2_X1, TEAM2_Y1, TEAM2_WIDTH, TEAM2_HEIGHT, TEAM_2, characters);
}

vec3 round_to(const vec3 & vec, float value)
{
    return glm::round(vec / value) * value;
}

void HUD::draw_map()
{
    // grey overlay
    draw_rect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, 127, 127, 127, 200);
    vec3 orig_pos;
    if (scene->character != NULL)
        orig_pos = scene->character->get_position();
    else
        orig_pos = vec3(0.0f, 0.0f, 0.0f);

    float scale = 8.0f / BLOCK_BUILD_SIZE;
    vec3 pos = round_to(orig_pos, 1.0f / scale);

    // draw map
    glPushMatrix();
    glEnable(GL_DEPTH_TEST);
    glTranslatef(WINDOW_WIDTH / 2.0f, WINDOW_HEIGHT / 2.0f, 0.0f);
    glScalef(scale, scale, scale);
    glTranslatef(-pos.x, -pos.y, 0.0f);
    scene->world->draw();
    glDisable(GL_DEPTH_TEST);
    glPopMatrix();

    // draw icons
/*    glPushMatrix();
    glTranslatef(WINDOW_WIDTH / 2.0f, WINDOW_HEIGHT / 2.0f, 0.0f);
    pos = glm::round(orig_pos * scale);
    glTranslatef(-pos.x, -pos.y, 0.0f);
    CharacterMap::iterator it;
    for (it = scene->characters.begin(); it != scene->characters.end(); it++) {
        Character * character = it->second;
        if (character->dead || character->hide)
            continue;
        unsigned char r, g, b;
        if (character == scene->character) {
            r = 200;
            g = b = 40;
        } else if (character->team == TEAM_1) {
            r = b = 0;
            g = 255;
        } else {
            r = g = 0;
            b = 255;
        }
        vec3 char_pos = glm::round(character->get_position() * scale);
        draw_pointer(char_pos.x, char_pos.y, r, g, b);
    }
    glPopMatrix();*/
}

void HUD::draw()
{
    if (quit) {
        draw_rect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, 0, 0, 0, 127);
        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        fixedsys_font->draw_center("Disconnect? Y/N",
            WINDOW_WIDTH / 2.0f, WINDOW_HEIGHT / 2.0f);
        return;
    }

    if (show_list) {
        draw_lists();
        return;
    }

    if (show_map) {
        draw_map();
        return;
    }

    setup_lighting();

    Character * character = scene->character;
    if (character != NULL && character->team != TEAM_SPEC && !character->dead) {
        RGBColor col = get_team_color(character->team);
        const static float mid_x = WINDOW_WIDTH / 2.0f;
        float hp_perc = std::min<float>(1.0f, hp_interp / float(START_HP));
        float half_size = (hp_perc * HP_BAR_WIDTH) * 0.5f;
        draw_rect(mid_x - half_size, HP_Y - 16.0f,
                  mid_x + half_size, HP_Y + 16.0f,
                  210, 72, 72, 127);
        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        fixedsys_font->draw_center("HP " +
            number_to_string(int_round(hp_interp)), mid_x, HP_Y + 1);

        Tool * tool = character->tool;

        // draw tool/ammo background rect
        col.mix(white_color, character->use_delay / tool->use_delay);
        draw_rounded_rect(TOOLAMMO_X1, TOOLAMMO_Y1, TOOLAMMO_X2, TOOLAMMO_Y2,
                          col.r, col.g, col.b, 127);

        // draw tool model
        VoxelModel * model = tool->model;
        VoxelFile * voxel = model->file;

        if (tool->id == BLOCK_TOOL)
            // change color depending on current palette selection
            voxel->palette[0] = get_palette_color();

        float off_x, off_y;
        if (tool->max_ammo == -1) {
            off_x = TOOL_MIDX;
            off_y = BOTTOM_Y;
        } else {
            off_x = TOOL_X;
            off_y = BOTTOM_Y;
        }

        draw_hud_tool(tool->model, off_x, off_y, 33.0f, 1.0f);

        if (tool->max_ammo != -1) {
            // draw ammo text
            glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            std::string ammo_text = number_to_string(tool->ammo);
            fixedsys_font->draw_center(ammo_text, AMMO_X, BOTTOM_Y);
        }
    }

    std::vector<ChatLine>::reverse_iterator it;
    glPushMatrix();
    glTranslatef(25.0f, CHAT_Y, 0.0f);
    for (it = lines.rbegin(); it != lines.rend(); it++) {
        (*it).draw();
        glTranslatef(0.0f, 25.0f, 0.0f);
    }
    glPopMatrix();

    std::vector<KillLine>::reverse_iterator it2;
    glPushMatrix();
    glTranslatef(25.0f, FEED_Y, 0.0f);
    for (it2 = killfeed.rbegin(); it2 != killfeed.rend(); it2++) {
        (*it2).draw();
        glTranslatef(0.0f, -25.0f, 0.0f);
    }
    glPopMatrix();

    // draw palette
    if (scene->is_tool(BLOCK_TOOL)) {
        float pal_x = PALETTE_X1;
        float pal_y = PALETTE_Y1;
        for (int y = 15; y >= 0; y--) {
            pal_x = PALETTE_X1;
            for (int x = 0; x < 16; x++) {
                RGBColor & col = palette_voxel->palette[x + y * 16];
                draw_rect(pal_x, pal_y,
                          pal_x + PALETTE_ITEM_SIZE, pal_y + PALETTE_ITEM_SIZE,
                          col.r, col.g, col.b, 255);
                if (palette_x == x && palette_y == y) {
                    int value = int(sin_wave(scene->time, 0.0f, 255.0f, 0.1f));
                    draw_rect_outline(pal_x, pal_y, pal_x + PALETTE_ITEM_SIZE,
                        pal_y + PALETTE_ITEM_SIZE, PALETTE_OUTLINE_SIZE,
                        value, value, value, 255);
                }
                pal_x += PALETTE_ITEM_SIZE + PALETTE_PAD;
            }
            pal_y += PALETTE_ITEM_SIZE + PALETTE_PAD;
        }
    }

    // draw timer
    float map_time = get_map_time();
    if (map_time >= 0.0f) {
        draw_rounded_rect(TIMER_X1, TIMER_Y1,
            TIMER_X1 + TIMER_WIDTH, TIMER_Y1 + TIMER_HEIGHT,
            hud_color, 200);
        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        fixedsys_font->draw_center(seconds_to_time(map_time),
            TIMER_X1 + TIMER_WIDTH * 0.5f, TIMER_Y1 + TIMER_HEIGHT * 0.5f + 1);
    }

    if (quick_chat) {
        draw_rounded_rect(QCHAT_X1, QCHAT_Y1, 
            QCHAT_X1 + QCHAT_WIDTH, QCHAT_Y1 + QCHAT_HEIGHT,
            hud_color, 200);
        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        glPushMatrix();
        glTranslatef(QCHAT_X1 + 15, QCHAT_Y1 + QCHAT_HEIGHT - 15, 0.0f);
        for (int i = 0; i < QCHAT_SIZE; i++) {
            fixedsys_font->draw_left_center(number_to_string(i+1) + ". " +
                quick_chat_entries[i * 2], 0.0f, 0.0f);
            glTranslatef(0.0f, -15.0f, 0.0f);
        }
        glPopMatrix();
    }

    if (!focus)
        return;
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    fixedsys_font->draw_left_center("Chat: " + current_text, 25.0f,
        CHAT_Y - 25.0f);
}

