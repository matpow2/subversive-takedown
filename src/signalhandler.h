#ifndef ST_SIGNALHANDLER_H
#define ST_SIGNALHANDLER_H

#ifndef _WIN32
#include <signal.h>
#endif

typedef void (*SignalCallback)();

static SignalCallback signal_callback = NULL;
static bool signal_initialized = false;

#ifdef _WIN32

BOOL signal_handler(DWORD ctrl_type)
{
    if (signal_callback == NULL)
        return FALSE;

    switch(ctrl_type) {
        case CTRL_C_EVENT:
        case CTRL_CLOSE_EVENT:
            signal_callback();
            return TRUE;
        case CTRL_BREAK_EVENT:
            return FALSE;
        case CTRL_LOGOFF_EVENT:
            return FALSE;
        case CTRL_SHUTDOWN_EVENT:
            return FALSE;
        default:
            return FALSE;
    }
}

#else

void signal_handler(int s)
{
    if (signal_callback == NULL)
        return;
    signal_callback();
}

#endif

void set_signal_handler(SignalCallback callback)
{
    signal_callback = callback;

    if (signal_initialized)
        return;

    signal_initialized = true;

#ifdef _WIN32
    SetConsoleCtrlHandler((PHANDLER_ROUTINE)signal_handler, TRUE);
#else
    struct sigaction sigint_handler;
    sigint_handler.sa_handler = signal_handler;
    sigemptyset(&sigint_handler.sa_mask);
    sigint_handler.sa_flags = 0;
    sigaction(SIGINT, &sigint_handler, NULL);
#endif
}

#endif // ST_SIGNALHANDLER_H
