#include "tool.h"

#include <algorithm>

#include "debug.h"

#ifdef ST_IS_CLIENT
#include "assets.h"
#endif

// default tool values

#define PICKAXE_DELAY 0.3f

#define BLOCK_MAX 30
#define BLOCK_DEFAULT BLOCK_MAX
#define BLOCK_DELAY 0.2f

#define HANDGUN_MAX 120
#define HANDGUN_DEFAULT HANDGUN_MAX
#define HANDGUN_DELAY 0.1f

#define RPG_MAX 10
#define RPG_DEFAULT 0
#define RPG_DELAY 2.0f

#define LAZER_MAX 10
#define LAZER_DEFAULT LAZER_MAX
#define LAZER_DELAY 0.8f

#if defined(ST_IS_CLIENT) && !defined(ST_IS_EDITOR)
#define SET_MODEL(v1) set_model(this, v1)
#else
#define SET_MODEL(v1)
#endif

// Tool

Tool::Tool(ToolType id, int max_ammo, int start_ammo, float use_delay)
: id(id), max_ammo(max_ammo), start_ammo(start_ammo), use_delay(use_delay)
{
    set(0);
}

Tool::Tool(ToolType id, float use_delay)
: id(id), max_ammo(-1), start_ammo(-1), use_delay(use_delay)
{
}

std::string Tool::get_name()
{
    return "Undefined";
}

void Tool::reset()
{
    if (max_ammo == -1)
        return;
    ammo = std::min(max_ammo, ammo + start_ammo);
}

bool Tool::use()
{
    if (max_ammo == -1)
        return true;
    if (ammo <= 0)
        return false;
#ifdef ST_IS_SERVER
    ammo--;
#endif
    return true;
}

void Tool::set(int ammo)
{
    if (max_ammo == -1)
        return;
    this->ammo = std::min(max_ammo, ammo);
}

#ifdef ST_IS_CLIENT
class VoxelModel;

inline void set_model(Tool * tool, VoxelModel * model)
{
    tool->model = model;
}
#endif

// HandgunTool

HandgunTool::HandgunTool()
: Tool(HANDGUN_TOOL, HANDGUN_MAX, HANDGUN_DEFAULT, HANDGUN_DELAY)
{
    SET_MODEL(gun_model);
}

std::string HandgunTool::get_name()
{
    return "Handgun";
}

// BlockTool

BlockTool::BlockTool()
: Tool(BLOCK_TOOL, BLOCK_MAX, BLOCK_DEFAULT, BLOCK_DELAY)
{
    SET_MODEL(block_model);
}

std::string BlockTool::get_name()
{
    return "Blocks";
}

// RPGTool

RPGTool::RPGTool()
: Tool(RPG_TOOL, RPG_MAX, RPG_DEFAULT, RPG_DELAY)
{
    SET_MODEL(rpg_model);
}

std::string RPGTool::get_name()
{
    return "RPG";
}

// PickaxeTool

PickaxeTool::PickaxeTool()
: Tool(PICKAXE_TOOL, PICKAXE_DELAY)
{
    SET_MODEL(pickaxe_model);
}

std::string PickaxeTool::get_name()
{
    return "Pickaxe";
}

// LazerTool

LazerTool::LazerTool()
: Tool(LAZER_TOOL, LAZER_MAX, LAZER_DEFAULT, LAZER_DELAY)
{
    SET_MODEL(lazergun_model);
}

std::string LazerTool::get_name()
{
    return "Lazer";
}

// Tools

Tool * create_tool(ToolType id)
{
    switch (id) {
        case HANDGUN_TOOL:
            return new HandgunTool();
        case BLOCK_TOOL:
            return new BlockTool();
        case RPG_TOOL:
            return new RPGTool();
        case PICKAXE_TOOL:
            return new PickaxeTool();
        case LAZER_TOOL:
            return new LazerTool();
    }
    return NULL;
}
