#ifndef ST_CONFIGFILE_H
#define ST_CONFIGFILE_H

#include <string>

#include "types.h"

#define LOW_GFX 0
#define HIGH_GFX 1

typedef fast_map<int,std::string> Bindings;

class ConfigFile
{
public:
    int up, down, left, right;
    int shoot;
    int orient;
    int chat;
    int team_chat;
    int jump;
    int show_map;
    int pick_color;
    int change_team;
    int switch_camera;
    int quick_chat;
    std::string name;
    int graphics;
    bool music;
    bool vsync;
    bool particles;
    bool smoke;
    float rotation_sensitivity;
    float mouse_sensitivity;
    float locked_sensitivity;
    int samples;
    Bindings bindings;

    ConfigFile(const std::string & filename);
    const std::string & get_binding(int k);
    inline bool has_high_graphics()
    {
        return graphics == HIGH_GFX;
    }
    inline bool has_low_graphics()
    {
        return graphics == LOW_GFX;
    }
};

#endif // ST_CONFIGFILE_H
