#include "fonts.h"

#include "font.h"

FTTextureFont * fixedsys_font = NULL;

FTTextureFont * load_font(const std::string & name, int size)
{
    std::string path = std::string("data/fonts/") + name + std::string(".ttf");
    FTTextureFont * font = new FTTextureFont(path.c_str(), false);
    font->FaceSize(size, 72);
    return font;
}

void initialize_fonts()
{
    fixedsys_font = load_font("04B03", 16);
}
