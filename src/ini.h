#ifndef ST_INI_H
#define ST_INI_H

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <algorithm>
#include <istream>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include "types.h"

/* Nonzero to allow multi-line value parsing, in the style of Python's
   ConfigParser. If allowed, ini_parse() will call the handler with the same
   name for each subsequent line parsed. */
#ifndef INI_ALLOW_MULTILINE
#define INI_ALLOW_MULTILINE 1
#endif

/* Nonzero to allow a UTF-8 BOM sequence (0xEF 0xBB 0xBF) at the start of
   the file. See http://code.google.com/p/inih/issues/detail?id=21 */
#ifndef INI_ALLOW_BOM
#define INI_ALLOW_BOM 1
#endif

#if !INI_USE_STACK
#include <stdlib.h>
#endif

#define MAX_SECTION 50
#define MAX_NAME 50

inline int is_space(unsigned char c)
{
    return isspace(c);
}

/* Strip whitespace chars off end of given string, in place. Return s. */
static char* rstrip(char* s)
{
    char* p = s + strlen(s);
    while (p > s && is_space(*--p))
        *p = '\0';
    return s;
}

/* Return pointer to first non-whitespace char in given string. */
static char* lskip(const char* s)
{
    while (*s && is_space(*s))
        s++;
    return (char*)s;
}

/* Return pointer to first char c or ';' comment in given string, or pointer to
   null at end of string if neither found. ';' must be prefixed by a whitespace
   character to register as a comment. */
static char* find_char_or_comment(const char* s, char c)
{
    int was_whitespace = 0;
    while (*s && *s != c && !(was_whitespace && *s == ';')) {
        was_whitespace = is_space(*s);
        s++;
    }
    return (char*)s;
}

/* Version of strncpy that ensures dest (size bytes) is null-terminated. */
static char* strncpy0(char* dest, const char* src, size_t size)
{
    strncpy(dest, src, size);
    dest[size - 1] = '\0';
    return dest;
}

int ini_parse_stream(std::istream & input,
                     int (*handler)(void*, const char*, const char*,
                                    const char*),
                     void* user)
{
    /* Uses a fair bit of stack (use heap instead if you need to) */
    char* line;
    char section[MAX_SECTION] = "";
    char prev_name[MAX_NAME] = "";

    char* start;
    char* end;
    char* name;
    char* value;
    int lineno = 0;
    int error = 0;

    /* Scan through file line by line */
    while (input) {
        std::string newline;
        std::getline(input, newline);
        line = (char*)newline.c_str();
        lineno++;

        start = line;
#if INI_ALLOW_BOM
        if (lineno == 1 && (unsigned char)start[0] == 0xEF &&
                           (unsigned char)start[1] == 0xBB &&
                           (unsigned char)start[2] == 0xBF) {
            start += 3;
        }
#endif
        start = lskip(rstrip(start));

        if (*start == ';' || *start == '#') {
            /* Per Python ConfigParser, allow '#' comments at start of line */
        }
#if INI_ALLOW_MULTILINE
        else if (*prev_name && *start && start > line) {
            /* Non-black line with leading whitespace, treat as continuation
               of previous name's value (as per Python ConfigParser). */
            if (!handler(user, section, prev_name, start) && !error)
                error = lineno;
        }
#endif
        else if (*start == '[') {
            /* A "[section]" line */
            end = find_char_or_comment(start + 1, ']');
            if (*end == ']') {
                *end = '\0';
                strncpy0(section, start + 1, sizeof(section));
                *prev_name = '\0';
            }
            else if (!error) {
                /* No ']' found on section line */
                error = lineno;
            }
        }
        else if (*start && *start != ';') {
            /* Not a comment, must be a name[=:]value pair */
            end = find_char_or_comment(start, '=');
            if (*end != '=') {
                end = find_char_or_comment(start, ':');
            }
            if (*end == '=' || *end == ':') {
                *end = '\0';
                name = rstrip(start);
                value = lskip(end + 1);
                end = find_char_or_comment(value, '\0');
                if (*end == ';')
                    *end = '\0';
                rstrip(value);

                /* Valid name[=:]value pair found, call handler */
                strncpy0(prev_name, name, sizeof(prev_name));
                if (!handler(user, section, name, value) && !error)
                    error = lineno;
            }
            else if (!error) {
                /* No '=' or ':' found on name[=:]value line */
                error = lineno;
            }
        }
    }

    return error;
}

int ini_parse_file(const char* filename,
                   int (*handler)(void*, const char*, const char*, const char*),
                   void* user)
{
    std::ifstream fp(filename);
    if (!fp)
        return -1;
    int error = ini_parse_stream(fp, handler, user);
    fp.close();
    return error;
}

typedef fast_map<std::string, std::string> INIMap;

class INIParser
{
public:
    INIMap items;

    INIParser(const std::string & filename)
    {
        if (ini_parse_file(filename.c_str(), _set_item, (void*)this) != 0) {
            std::cerr << "Could not load file " << filename << std::endl;
        }
    }

    int set_item(const std::string & section, const std::string & item,
                 const std::string & value)
    {
        items[make_key(section, item)] = value;
        return 1;
    }

    static int _set_item(void * user, const char * section, const char * item,
                         const char * value)
    {
        return ((INIParser*)user)->set_item(section, item, value);
    }

    std::string make_key(const std::string & section, const std::string & item)
    {
        std::string key = section + "." + item;
        std::transform(key.begin(), key.end(), key.begin(), ::tolower);
        return key;
    }

    bool get(const std::string & section, const std::string & item,
             const std::string ** result)
    {
        std::string key = make_key(section, item);
        INIMap::const_iterator it = items.find(key);
        if (it == items.end())
            return false;
        *result = &it->second;
        return true;
    }

    const std::string & get(const std::string & section,
                            const std::string & item,
                            const std::string & def = "")
    {
        const std::string * result;
        if (!get(section, item, &result))
            result = &def;
        return *result;
    }

    int get_int(const std::string & section, const std::string & item,
                int def = 0)
    {
        const std::string * result;
        if (!get(section, item, &result))
            return def;
        const char* value = (*result).c_str();
        char * end;
        int n = strtol(value, &end, 0);
        return end > value ? n : def;
    }

    float get_float(const std::string & section, const std::string & item,
                    float def = 0.0f)
    {
        const std::string * result;
        if (!get(section, item, &result))
            return def;
        const char* value = (*result).c_str();
        return float(atof(value));
    }

    bool get_bool(const std::string & section, const std::string & item,
                  bool def = false)
    {
        const std::string * result_p;
        if (!get(section, item, &result_p))
            return def;
        const std::string & result = *result_p;
        if (result == "true" || result == "yes" || result == "1")
            return true;
        else if (result == "false" || result == "no" || result == "0")
            return false;
        return def;
    }
};

#endif // ST_INI_H
