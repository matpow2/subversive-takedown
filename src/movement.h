#ifndef ST_MOVEMENT_H
#define ST_MOVEMENT_H

inline void get_control_angle(bool up, bool down, bool left, bool right,
                              bool & walking, float & angle)
{
    if (up && down)
        up = down = false;
    if (left && right)
        left = right = false;
    if (up && !(down || left || right))
        angle = 90.0f;
    else if (down && !(up || left || right))
        angle = 270.0f;
    else if (left && !(up || down || right))
        angle = 180.0f;
    else if (right && !(up || down || left))
        angle = 0.0f;
    else if (up && left)
        angle = 135.0f;
    else if (up && right)
        angle = 45.0f;
    else if (down && left)
        angle = 225.0f;
    else if (down && right)
        angle = 315.0f;
    else {
        walking = false;
        angle = 0.0f;
        return;
    }
    walking = true;
}

#endif // ST_MOVEMENT_H
