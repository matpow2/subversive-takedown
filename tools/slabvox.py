# Copyright (c) Mathias Kaerlev 2013.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Converts between SLAB6 .vox files and ST .vox files

Note that this may not be kept up to date as the .vox file format changes.
"""

import argparse
from bytes import ByteReader, ByteWriter

def main():
    parser = argparse.ArgumentParser(
        description='Converts between SLAB6 .vox files and ST .vox files')
    parser.add_argument('input', help='input file (ST vox per default)')
    parser.add_argument('output', help='output file (SLAB vox per default)')
    parser.add_argument('--st', help="convert to ST vox instead of SLAB vox",
                        action="store_true")
    args = parser.parse_args()

    to_st = args.st

    # read input file
    infp = ByteReader(fp = open(args.input, 'rb'))
    x_size = infp.read_uint32()
    y_size = infp.read_uint32()
    z_size = infp.read_uint32()
    if to_st:
        x_offset = -int(x_size * 0.5)
        y_offset = -int(y_size * 0.5)
        z_offset = -int(z_size * 0.5)
    else:
        x_offset = infp.read_int32()
        y_offset = infp.read_int32()
        z_offset = infp.read_int32()
    blocks = {}

    for x in xrange(x_size):
        for y in xrange(y_size):
            for z in xrange(z_size):
                blocks[(x, y, z)] = infp.read_uint8()
    palette = []
    for _ in xrange(256 * 3):
        palette.append(infp.read_uint8())
    infp.close()

    # wrte output file
    outfp = ByteWriter(open(args.output, 'wb'))
    outfp.write_uint32(x_size)
    outfp.write_uint32(y_size)
    outfp.write_uint32(z_size)
    if to_st:
        outfp.write_int32(x_offset)
        outfp.write_int32(y_offset)
        outfp.write_int32(z_offset)

    for x in xrange(x_size):
        x = x_size - x - 1
        for y in xrange(y_size):
            for z in xrange(z_size):
                z = z_size - z - 1
                outfp.write_uint8(blocks[(x, y, z)])

    # SLAB6 wants the palette to be in the range 0-63, while we in ST have a
    # palette that can be from 0-255

    old_max = float(63 if to_st else 255)
    new_max = float(255 if to_st else 63)

    for c in palette:
        outfp.write_uint8(int((c / old_max) * new_max))

    outfp.close()
    print 'Done!'

if __name__ == '__main__':
    main()