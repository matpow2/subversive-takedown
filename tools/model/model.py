from bytes import ByteReader

def read_string(reader):
    return reader.read(reader.read_uint8())

MAGIC = 'QBCL\x20'
VERSION = '1.02'

class QubicleModel(object):
    def __init__(self, reader):
        self.x_size = reader.read_uint32()
        self.y_size = reader.read_uint32()
        self.z_size = reader.read_uint32()
        self.name = read_string(reader)
        self.x_offset = reader.read_int32()
        self.y_offset = reader.read_int32()
        self.z_offset = reader.read_int32()
        self.hidden = reader.read_uint8()
        rle_count = reader.read_uint32()
        self.blocks = {}
        i = 0
        while rle_count:
            value = reader.read_uint32()
            rle_count -= 1
            if value == 2: # repetition
                times = reader.read_uint32()
                color = reader.read_uint32()
                rle_count -= 2
            else:
                times = 1
                color = value
            if color & 0xFF000000 == 0:
                i += times
                continue
            r = int(color & 0x0000FF)
            g = int((color & 0x00FF00) >> 8)
            b = int((color & 0xFF0000) >> 16)
            for _ in xrange(times):
                x = i / (self.y_size * self.z_size)
                y = (i % (self.y_size * self.z_size)) / self.z_size
                z = i % self.z_size
                self.blocks[(int(x), int(y), int(z))] = (r, g, b)
                i += 1

class QubicleFile(object):
    def __init__(self, data):
        reader = ByteReader(data)
        if read_string(reader) != MAGIC:
            raise NotImplementedError('invalid magic')
        elif read_string(reader) != VERSION:
            raise NotImplementedError('unsupported version')
        self.models = []
        for _ in xrange(reader.read_uint32()):
            self.models.append(QubicleModel(reader))
        # if reader.data_left() != 108:
        #     raise NotImplementedError
        # reader.skip_bytes(108)