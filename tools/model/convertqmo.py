import glob
import os

import sys
sys.path.append('..')

from bytes import ByteWriter
from model import QubicleFile

def convert_file(filename):
    name = os.path.splitext(os.path.basename(filename))[0]
    f = QubicleFile(open(filename, 'rb').read())
    models = []
    for model in f.models:
        if not model.blocks:
            continue
        models.append((model.name, model))
    models.sort()
    print models
    has_postfix = len(models) > 1
    for index, (model_name, model) in enumerate(models):
        local_name = name
        if has_postfix:
            local_name += str(index).zfill(4)
        filename = '../../data/gfx/%s.vox' % local_name
        convert_model(model, filename)

VIS_TOP = 1
VIS_BOTTOM = 2
VIS_FRONT = 4
VIS_BACK = 4
VIS_LEFT = 2

def convert_model(model, filename):
    out = ByteWriter()
    out.write_uint32(model.x_size)
    out.write_uint32(model.z_size)
    out.write_uint32(model.y_size)
    out.write_int32(model.x_offset)
    out.write_int32(model.z_offset)
    out.write_int32(-model.y_offset)
    palette = []
    for x in xrange(model.x_size):
        for z in xrange(model.z_size):
            for y in xrange(model.y_size):
                block = model.blocks.get((x, y, z), None)
                if block is None:
                    palette_index = 255
                else:
                    if block not in palette:
                        palette.append(block)
                    palette_index = palette.index(block)
                out.write_uint8(palette_index)
    for i in xrange(256):
        try:
            color = palette[i]
        except IndexError:
            color = (i, i, i)
        r, g, b = color
        out.write_uint8(r)
        out.write_uint8(g)
        out.write_uint8(b)
    open(filename, 'wb').write(out.fp.getvalue())
    print 'Saved to %r' % filename

def main():
    for filename in glob.glob('../../assetsources/models/*.qmo'):
        convert_file(filename)

if __name__ == '__main__':
    main()