from script import ServerScript, CharacterScript
from stlib import constants

class RepublicCharacter(CharacterScript):
    def on_intel_capture(self):
        return False

class RepublicServer(ServerScript):
    character_script = RepublicCharacter

    def on_map_start(self):
        self.train_entity = self.get_entity('train')
        self.intel_entity = self.get_entity('intel')
        self.trans_entity = self.get_entity('trans')
        self.current_node = 1
        self.train_entity.set_node(self.get_next_node(), True)
        self.wall_breached = False
        i = 1
        while True:
            entity = self.get_entity('spawn%s' % i)
            if entity is None:
                break
            entity.set_team(constants.TEAM_SPEC)
            i += 1

        self.get_entity('spawn1').set_team(constants.TEAM_1)
        self.get_entity('spawn2').set_team(constants.TEAM_2)

    def get_next_node(self):
        return self.get_entity('train%s' % self.current_node)

    def on_time_out(self):
        self.server.end_round(self.server.team_2)

    def update(self, dt):
        if self.intel_entity is None:
            return
        dist = self.intel_entity.get_entity_distance(self.trans_entity)
        if dist <= 50.0:
            self.intel_entity.destroy()
            self.intel_entity = None
            self.server.end_round(self.server.team_1)
            return
        if self.current_node is None:
            return
        dist = self.train_entity.get_team_distance(constants.TEAM_1)
        if dist is None or dist > 200.0:
            self.train_entity.set_node(None)
            return
        node = self.get_next_node()
        if node is None:
            self.current_node = None
            self.get_entity('spawn2').set_team(constants.TEAM_SPEC)
            self.get_entity('spawn3').set_team(constants.TEAM_1)
            self.get_entity('spawn4').set_team(constants.TEAM_2)
            self.server.send_chat('The Greenites have reached the Intelligence'
                                  ' Building!')
            self.server.play_sound("objective", True)
            self.server.add_time(5 * 60) # 5 minutes
        elif not self.train_entity.set_node(node):
            self.current_node += 1
        if not self.wall_breached and not self.get_entity('wall'):
            self.server.send_chat('The Greenites have breached through the '
                                  'defense barrier!')
            self.server.add_time(5 * 60) # 5 minutes
            self.get_entity('spawn1').set_team(constants.TEAM_SPEC)
            self.get_entity('spawn2').set_team(constants.TEAM_1)
            self.get_entity('spawn3').set_team(constants.TEAM_2)
            self.server.play_sound("objective", True)
            self.wall_breached = True

def get_class():
    return RepublicServer